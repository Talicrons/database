info face="Arial" size=42 bold=0 italic=0 charset="" unicode=1 stretchH=100 smooth=1 aa=1 padding=0,0,0,0 spacing=1,1 outline=0
common lineHeight=42 base=34 scaleW=256 scaleH=128 pages=1 packed=0 alphaChnl=1 redChnl=0 greenChnl=0 blueChnl=0
page id=0 file="BitmapFont_White_0.png"
chars count=10
char id=48   x=0     y=43    width=47    height=42    xoffset=0     yoffset=0     xadvance=47    page=0  chnl=15
char id=49   x=143   y=43    width=32    height=42    xoffset=0     yoffset=0     xadvance=32    page=0  chnl=15
char id=50   x=0     y=0     width=51    height=42    xoffset=0     yoffset=0     xadvance=51    page=0  chnl=15
char id=51   x=52    y=0     width=51    height=42    xoffset=0     yoffset=0     xadvance=51    page=0  chnl=15
char id=52   x=104   y=0     width=51    height=42    xoffset=0     yoffset=0     xadvance=51    page=0  chnl=15
char id=53   x=206   y=0     width=48    height=42    xoffset=0     yoffset=0     xadvance=48    page=0  chnl=15
char id=54   x=48    y=43    width=47    height=42    xoffset=0     yoffset=0     xadvance=47    page=0  chnl=15
char id=55   x=176   y=43    width=47    height=41    xoffset=0     yoffset=0     xadvance=47    page=0  chnl=15
char id=56   x=156   y=0     width=49    height=42    xoffset=0     yoffset=0     xadvance=49    page=0  chnl=15
char id=57   x=96    y=43    width=46    height=42    xoffset=0     yoffset=0     xadvance=46    page=0  chnl=15
