﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataInitInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataInitInfo")]
  [Serializable]
  public class ConfigDataInitInfo : IExtensible
  {
    private int _ID;
    private List<int> _Heros_ID;
    private int _Gold;
    private int _Crystal;
    private List<Goods> _BagItem;
    private int _ArenaTicket;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataInitInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public List<int> Heros_ID
    {
      get
      {
        return this._Heros_ID;
      }
      set
      {
        this._Heros_ID = value;
      }
    }

    [ProtoMember(4)]
    public int Gold
    {
      get
      {
        return this._Gold;
      }
      set
      {
        this._Gold = value;
      }
    }

    [ProtoMember(5)]
    public int Crystal
    {
      get
      {
        return this._Crystal;
      }
      set
      {
        this._Crystal = value;
      }
    }

    [ProtoMember(7)]
    public List<Goods> BagItem
    {
      get
      {
        return this._BagItem;
      }
      set
      {
        this._BagItem = value;
      }
    }

    [ProtoMember(8)]
    public int ArenaTicket
    {
      get
      {
        return this._ArenaTicket;
      }
      set
      {
        this._ArenaTicket = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
