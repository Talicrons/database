﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.PeakArenaConsecutiveLossesMatchmakingInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "PeakArenaConsecutiveLossesMatchmakingInfo")]
  [Serializable]
  public class PeakArenaConsecutiveLossesMatchmakingInfo : IExtensible
  {
    private int _Count;
    private int _DanMin;
    private int _DanMax;
    private bool _IsBot;
    private int _BotLevelAdjustment;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int Count
    {
      get
      {
        return this._Count;
      }
      set
      {
        this._Count = value;
      }
    }

    [ProtoMember(2)]
    public int DanMin
    {
      get
      {
        return this._DanMin;
      }
      set
      {
        this._DanMin = value;
      }
    }

    [ProtoMember(3)]
    public int DanMax
    {
      get
      {
        return this._DanMax;
      }
      set
      {
        this._DanMax = value;
      }
    }

    [ProtoMember(4)]
    public bool IsBot
    {
      get
      {
        return this._IsBot;
      }
      set
      {
        this._IsBot = value;
      }
    }

    [ProtoMember(5)]
    public int BotLevelAdjustment
    {
      get
      {
        return this._BotLevelAdjustment;
      }
      set
      {
        this._BotLevelAdjustment = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
