﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataCardPoolInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataCardPoolInfo")]
  [Serializable]
  public class ConfigDataCardPoolInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private CardPoolType _PoolType;
    private CardSelectType _SelectType;
    private int _CrystalCost;
    private int _TenSelectDiscount;
    private int _TenSelectCount;
    private int _TicketId;
    private int _CardPoolSelectMaxCount;
    private int _SortID;
    private string _Icon;
    private string _ClockResPath;
    private string _AdsImage;
    private int _SelectProbabilityInfoID;
    private int _SelectContentInfoID;
    private string _ToggleClickImage;
    private string _ToggleUnClickImage;
    private MissionCardPoolType _MissionCardPoolType;
    private string _CardPoolDetailDesc;
    private IExtension extensionObject;
    public Dictionary<int, ConfigDataCardPoolExchangeItemInfo> ExchangeItems;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCardPoolInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public CardPoolType PoolType
    {
      get
      {
        return this._PoolType;
      }
      set
      {
        this._PoolType = value;
      }
    }

    [ProtoMember(5)]
    public CardSelectType SelectType
    {
      get
      {
        return this._SelectType;
      }
      set
      {
        this._SelectType = value;
      }
    }

    [ProtoMember(6)]
    public int CrystalCost
    {
      get
      {
        return this._CrystalCost;
      }
      set
      {
        this._CrystalCost = value;
      }
    }

    [ProtoMember(7)]
    public int TenSelectDiscount
    {
      get
      {
        return this._TenSelectDiscount;
      }
      set
      {
        this._TenSelectDiscount = value;
      }
    }

    [ProtoMember(8)]
    public int TenSelectCount
    {
      get
      {
        return this._TenSelectCount;
      }
      set
      {
        this._TenSelectCount = value;
      }
    }

    [ProtoMember(9)]
    public int TicketId
    {
      get
      {
        return this._TicketId;
      }
      set
      {
        this._TicketId = value;
      }
    }

    [ProtoMember(10)]
    public int CardPoolSelectMaxCount
    {
      get
      {
        return this._CardPoolSelectMaxCount;
      }
      set
      {
        this._CardPoolSelectMaxCount = value;
      }
    }

    [ProtoMember(11)]
    public int SortID
    {
      get
      {
        return this._SortID;
      }
      set
      {
        this._SortID = value;
      }
    }

    [ProtoMember(13)]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(14)]
    public string ClockResPath
    {
      get
      {
        return this._ClockResPath;
      }
      set
      {
        this._ClockResPath = value;
      }
    }

    [ProtoMember(15)]
    public string AdsImage
    {
      get
      {
        return this._AdsImage;
      }
      set
      {
        this._AdsImage = value;
      }
    }

    [ProtoMember(16)]
    public int SelectProbabilityInfoID
    {
      get
      {
        return this._SelectProbabilityInfoID;
      }
      set
      {
        this._SelectProbabilityInfoID = value;
      }
    }

    [ProtoMember(17)]
    public int SelectContentInfoID
    {
      get
      {
        return this._SelectContentInfoID;
      }
      set
      {
        this._SelectContentInfoID = value;
      }
    }

    [ProtoMember(18)]
    public string ToggleClickImage
    {
      get
      {
        return this._ToggleClickImage;
      }
      set
      {
        this._ToggleClickImage = value;
      }
    }

    [ProtoMember(19)]
    public string ToggleUnClickImage
    {
      get
      {
        return this._ToggleUnClickImage;
      }
      set
      {
        this._ToggleUnClickImage = value;
      }
    }

    [ProtoMember(20)]
    public MissionCardPoolType MissionCardPoolType
    {
      get
      {
        return this._MissionCardPoolType;
      }
      set
      {
        this._MissionCardPoolType = value;
      }
    }

    [ProtoMember(26)]
    public string CardPoolDetailDesc
    {
      get
      {
        return this._CardPoolDetailDesc;
      }
      set
      {
        this._CardPoolDetailDesc = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    public bool IsExchangeGameFunctionOpen()
    {
      return this.ExchangeItems.Count > 0;
    }
  }
}
