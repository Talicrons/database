﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroDungeonLevellUnlockConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroDungeonLevellUnlockConditionType")]
  public enum HeroDungeonLevellUnlockConditionType
  {
    [ProtoEnum(Name = "HeroDungeonLevellUnlockConditionType_None", Value = 0)] HeroDungeonLevellUnlockConditionType_None,
    [ProtoEnum(Name = "HeroDungeonLevellUnlockConditionType_HeroFavorabilityLevel", Value = 1)] HeroDungeonLevellUnlockConditionType_HeroFavorabilityLevel,
  }
}
