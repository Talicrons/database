﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataHeroSkinInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataHeroSkinInfo")]
  [Serializable]
  public class ConfigDataHeroSkinInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _SpecifiedHero;
    private int _CharImageSkinResource_ID;
    private List<JobConnection2SkinResource> _SpecifiedModelSkinResource;
    private string _Desc;
    private string _Icon;
    private List<GetPathData> _GetPathList;
    private string _GetPathDesc;
    private int _CharImage_ID;
    private int _HeroInformation_ID;
    private string _RoundHeadImage;
    private string _SmallHeadImage;
    private string _CardHeadImage;
    private string _PeakArenaSignTitle;
    private List<HeroSkinInfoSummonSkins> _SummonSkins;
    private List<HeroSkinInfoChangeChars> _ChangeChars;
    private IExtension extensionObject;
    public int FixedStoreItemId;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroSkinInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public int SpecifiedHero
    {
      get
      {
        return this._SpecifiedHero;
      }
      set
      {
        this._SpecifiedHero = value;
      }
    }

    [ProtoMember(5)]
    public int CharImageSkinResource_ID
    {
      get
      {
        return this._CharImageSkinResource_ID;
      }
      set
      {
        this._CharImageSkinResource_ID = value;
      }
    }

    [ProtoMember(6)]
    public List<JobConnection2SkinResource> SpecifiedModelSkinResource
    {
      get
      {
        return this._SpecifiedModelSkinResource;
      }
      set
      {
        this._SpecifiedModelSkinResource = value;
      }
    }

    [ProtoMember(7)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(8)]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(9)]
    public List<GetPathData> GetPathList
    {
      get
      {
        return this._GetPathList;
      }
      set
      {
        this._GetPathList = value;
      }
    }

    [ProtoMember(10)]
    public string GetPathDesc
    {
      get
      {
        return this._GetPathDesc;
      }
      set
      {
        this._GetPathDesc = value;
      }
    }

    [ProtoMember(11)]
    public int CharImage_ID
    {
      get
      {
        return this._CharImage_ID;
      }
      set
      {
        this._CharImage_ID = value;
      }
    }

    [ProtoMember(12)]
    public int HeroInformation_ID
    {
      get
      {
        return this._HeroInformation_ID;
      }
      set
      {
        this._HeroInformation_ID = value;
      }
    }

    [ProtoMember(13)]
    public string RoundHeadImage
    {
      get
      {
        return this._RoundHeadImage;
      }
      set
      {
        this._RoundHeadImage = value;
      }
    }

    [ProtoMember(14)]
    public string SmallHeadImage
    {
      get
      {
        return this._SmallHeadImage;
      }
      set
      {
        this._SmallHeadImage = value;
      }
    }

    [ProtoMember(15)]
    public string CardHeadImage
    {
      get
      {
        return this._CardHeadImage;
      }
      set
      {
        this._CardHeadImage = value;
      }
    }

    [ProtoMember(16)]
    public string PeakArenaSignTitle
    {
      get
      {
        return this._PeakArenaSignTitle;
      }
      set
      {
        this._PeakArenaSignTitle = value;
      }
    }

    [ProtoMember(17)]
    public List<HeroSkinInfoSummonSkins> SummonSkins
    {
      get
      {
        return this._SummonSkins;
      }
      set
      {
        this._SummonSkins = value;
      }
    }

    [ProtoMember(18)]
    public List<HeroSkinInfoChangeChars> ChangeChars
    {
      get
      {
        return this._ChangeChars;
      }
      set
      {
        this._ChangeChars = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
