﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataHeroConfessionInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataHeroConfessionInfo")]
  [Serializable]
  public class ConfigDataHeroConfessionInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _BackGroundImage;
    private string _CharImage;
    private int _PreWordID;
    private int _CorrectID;
    private int _WrongID;
    private int _UnknownID;
    private int _ConfessionLetterID;
    private int _LastWordID;
    private int _UnlockHeroHeartFetterMinLevel;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string BackGroundImage
    {
      get
      {
        return this._BackGroundImage;
      }
      set
      {
        this._BackGroundImage = value;
      }
    }

    [ProtoMember(5)]
    public string CharImage
    {
      get
      {
        return this._CharImage;
      }
      set
      {
        this._CharImage = value;
      }
    }

    [ProtoMember(6)]
    public int PreWordID
    {
      get
      {
        return this._PreWordID;
      }
      set
      {
        this._PreWordID = value;
      }
    }

    [ProtoMember(7)]
    public int CorrectID
    {
      get
      {
        return this._CorrectID;
      }
      set
      {
        this._CorrectID = value;
      }
    }

    [ProtoMember(8)]
    public int WrongID
    {
      get
      {
        return this._WrongID;
      }
      set
      {
        this._WrongID = value;
      }
    }

    [ProtoMember(9)]
    public int UnknownID
    {
      get
      {
        return this._UnknownID;
      }
      set
      {
        this._UnknownID = value;
      }
    }

    [ProtoMember(10)]
    public int ConfessionLetterID
    {
      get
      {
        return this._ConfessionLetterID;
      }
      set
      {
        this._ConfessionLetterID = value;
      }
    }

    [ProtoMember(11)]
    public int LastWordID
    {
      get
      {
        return this._LastWordID;
      }
      set
      {
        this._LastWordID = value;
      }
    }

    [ProtoMember(12)]
    public int UnlockHeroHeartFetterMinLevel
    {
      get
      {
        return this._UnlockHeroHeartFetterMinLevel;
      }
      set
      {
        this._UnlockHeroHeartFetterMinLevel = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
