﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CollectionActivityWaypointStateType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "CollectionActivityWaypointStateType")]
  public enum CollectionActivityWaypointStateType
  {
    [ProtoEnum(Name = "CollectionActivityWaypointStateType_None", Value = 0)] CollectionActivityWaypointStateType_None,
    [ProtoEnum(Name = "CollectionActivityWaypointStateType_Hidden", Value = 1)] CollectionActivityWaypointStateType_Hidden,
    [ProtoEnum(Name = "CollectionActivityWaypointStateType_Locked", Value = 2)] CollectionActivityWaypointStateType_Locked,
    [ProtoEnum(Name = "CollectionActivityWaypointStateType_Unlocked", Value = 3)] CollectionActivityWaypointStateType_Unlocked,
  }
}
