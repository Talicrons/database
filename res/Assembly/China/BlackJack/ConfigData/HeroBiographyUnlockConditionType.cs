﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroBiographyUnlockConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroBiographyUnlockConditionType")]
  public enum HeroBiographyUnlockConditionType
  {
    [ProtoEnum(Name = "HeroBiographyUnlockConditionType_None", Value = 0)] HeroBiographyUnlockConditionType_None,
    [ProtoEnum(Name = "HeroBiographyUnlockConditionType_HeroFavorabilityLevel", Value = 1)] HeroBiographyUnlockConditionType_HeroFavorabilityLevel,
  }
}
