﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.TreeMemoryAgeType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "TreeMemoryAgeType")]
  public enum TreeMemoryAgeType
  {
    [ProtoEnum(Name = "TreeMemoryAgeType_None", Value = 0)] TreeMemoryAgeType_None,
    [ProtoEnum(Name = "TreeMemoryAgeType_Silver", Value = 1)] TreeMemoryAgeType_Silver,
    [ProtoEnum(Name = "TreeMemoryAgeType_Golden", Value = 2)] TreeMemoryAgeType_Golden,
    [ProtoEnum(Name = "TreeMemoryAgeType_Colour", Value = 3)] TreeMemoryAgeType_Colour,
  }
}
