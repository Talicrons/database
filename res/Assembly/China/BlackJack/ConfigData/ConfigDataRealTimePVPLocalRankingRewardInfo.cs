﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataRealTimePVPLocalRankingRewardInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataRealTimePVPLocalRankingRewardInfo")]
  [Serializable]
  public class ConfigDataRealTimePVPLocalRankingRewardInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _HighRank;
    private int _LowRank;
    private int _RankingRewardMailTemplateId;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public int HighRank
    {
      get
      {
        return this._HighRank;
      }
      set
      {
        this._HighRank = value;
      }
    }

    [ProtoMember(5)]
    public int LowRank
    {
      get
      {
        return this._LowRank;
      }
      set
      {
        this._LowRank = value;
      }
    }

    [ProtoMember(6)]
    public int RankingRewardMailTemplateId
    {
      get
      {
        return this._RankingRewardMailTemplateId;
      }
      set
      {
        this._RankingRewardMailTemplateId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
