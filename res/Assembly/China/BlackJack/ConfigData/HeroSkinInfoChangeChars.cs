﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroSkinInfoChangeChars
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroSkinInfoChangeChars")]
  [Serializable]
  public class HeroSkinInfoChangeChars : IExtensible
  {
    private int _JobConnectionId;
    private int _NewJobConnectionId;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int JobConnectionId
    {
      get
      {
        return this._JobConnectionId;
      }
      set
      {
        this._JobConnectionId = value;
      }
    }

    [ProtoMember(2)]
    public int NewJobConnectionId
    {
      get
      {
        return this._NewJobConnectionId;
      }
      set
      {
        this._NewJobConnectionId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
