﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CompleteValueDropID
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "CompleteValueDropID")]
  [Serializable]
  public class CompleteValueDropID : IExtensible
  {
    private int _CompleteValue;
    private int _DropID;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int CompleteValue
    {
      get
      {
        return this._CompleteValue;
      }
      set
      {
        this._CompleteValue = value;
      }
    }

    [ProtoMember(2)]
    public int DropID
    {
      get
      {
        return this._DropID;
      }
      set
      {
        this._DropID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
