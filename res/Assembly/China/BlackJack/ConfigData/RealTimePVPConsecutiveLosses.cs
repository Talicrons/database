﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RealTimePVPConsecutiveLosses
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RealTimePVPConsecutiveLosses")]
  [Serializable]
  public class RealTimePVPConsecutiveLosses : IExtensible
  {
    private int _ConsecutiveLosses;
    private int _ScoreProtection;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int ConsecutiveLosses
    {
      get
      {
        return this._ConsecutiveLosses;
      }
      set
      {
        this._ConsecutiveLosses = value;
      }
    }

    [ProtoMember(2)]
    public int ScoreProtection
    {
      get
      {
        return this._ScoreProtection;
      }
      set
      {
        this._ScoreProtection = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
