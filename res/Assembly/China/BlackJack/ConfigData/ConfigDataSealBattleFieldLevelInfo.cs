﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataSealBattleFieldLevelInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataSealBattleFieldLevelInfo")]
  [Serializable]
  public class ConfigDataSealBattleFieldLevelInfo : IExtensible
  {
    private int _ID;
    private string _LevelName;
    private int _LevelAheadID;
    private int _MonsterLevel;
    private int _BattleID;
    private List<Goods> _FirstPassReward;
    private int _DisplayRewardCount;
    private List<int> _Achievement;
    private List<int> _OurPartAddPassiveSkills;
    private List<int> _EnemyPatrAddPassiveSkills;
    private IExtension extensionObject;
    public ConfigDataBattleInfo m_battleInfo;
    public ConfigDataSealBattleFieldLevelGroupInfo m_groupInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSealBattleFieldLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string LevelName
    {
      get
      {
        return this._LevelName;
      }
      set
      {
        this._LevelName = value;
      }
    }

    [ProtoMember(4)]
    public int LevelAheadID
    {
      get
      {
        return this._LevelAheadID;
      }
      set
      {
        this._LevelAheadID = value;
      }
    }

    [ProtoMember(5)]
    public int MonsterLevel
    {
      get
      {
        return this._MonsterLevel;
      }
      set
      {
        this._MonsterLevel = value;
      }
    }

    [ProtoMember(6)]
    public int BattleID
    {
      get
      {
        return this._BattleID;
      }
      set
      {
        this._BattleID = value;
      }
    }

    [ProtoMember(7)]
    public List<Goods> FirstPassReward
    {
      get
      {
        return this._FirstPassReward;
      }
      set
      {
        this._FirstPassReward = value;
      }
    }

    [ProtoMember(8)]
    public int DisplayRewardCount
    {
      get
      {
        return this._DisplayRewardCount;
      }
      set
      {
        this._DisplayRewardCount = value;
      }
    }

    [ProtoMember(9)]
    public List<int> Achievement
    {
      get
      {
        return this._Achievement;
      }
      set
      {
        this._Achievement = value;
      }
    }

    [ProtoMember(10)]
    public List<int> OurPartAddPassiveSkills
    {
      get
      {
        return this._OurPartAddPassiveSkills;
      }
      set
      {
        this._OurPartAddPassiveSkills = value;
      }
    }

    [ProtoMember(11)]
    public List<int> EnemyPatrAddPassiveSkills
    {
      get
      {
        return this._EnemyPatrAddPassiveSkills;
      }
      set
      {
        this._EnemyPatrAddPassiveSkills = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
