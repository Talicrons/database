﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RiftChapterInfoUnlockConditions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RiftChapterInfoUnlockConditions")]
  [Serializable]
  public class RiftChapterInfoUnlockConditions : IExtensible
  {
    private RiftChapterUnlockConditionType _ConditionType;
    private int _Param;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public RiftChapterUnlockConditionType ConditionType
    {
      get
      {
        return this._ConditionType;
      }
      set
      {
        this._ConditionType = value;
      }
    }

    [ProtoMember(2)]
    public int Param
    {
      get
      {
        return this._Param;
      }
      set
      {
        this._Param = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
