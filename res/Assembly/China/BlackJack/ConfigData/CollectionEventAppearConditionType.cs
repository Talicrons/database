﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CollectionEventAppearConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "CollectionEventAppearConditionType")]
  public enum CollectionEventAppearConditionType
  {
    [ProtoEnum(Name = "CollectionEventAppearConditionType_None", Value = 0)] CollectionEventAppearConditionType_None,
    [ProtoEnum(Name = "CollectionEventAppearConditionType_CompleteScenario", Value = 1)] CollectionEventAppearConditionType_CompleteScenario,
    [ProtoEnum(Name = "CollectionEventAppearConditionType_CompleteChallengeLevel", Value = 2)] CollectionEventAppearConditionType_CompleteChallengeLevel,
  }
}
