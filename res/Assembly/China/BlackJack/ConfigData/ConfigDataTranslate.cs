﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataTranslate
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataTranslate")]
  [Serializable]
  public class ConfigDataTranslate : IExtensible
  {
    private int _ID;
    private string _Chinese;
    private string _TargetLanguage;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Chinese
    {
      get
      {
        return this._Chinese;
      }
      set
      {
        this._Chinese = value;
      }
    }

    [ProtoMember(4)]
    public string TargetLanguage
    {
      get
      {
        return this._TargetLanguage;
      }
      set
      {
        this._TargetLanguage = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
