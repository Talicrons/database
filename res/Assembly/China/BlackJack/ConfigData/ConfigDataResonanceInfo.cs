﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataResonanceInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataResonanceInfo")]
  [Serializable]
  public class ConfigDataResonanceInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _SmallIcon;
    private string _InactiveIcon;
    private string _ActiveIcon;
    private string _Affix;
    private int _Effect1;
    private int _Effect2;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string SmallIcon
    {
      get
      {
        return this._SmallIcon;
      }
      set
      {
        this._SmallIcon = value;
      }
    }

    [ProtoMember(5)]
    public string InactiveIcon
    {
      get
      {
        return this._InactiveIcon;
      }
      set
      {
        this._InactiveIcon = value;
      }
    }

    [ProtoMember(6)]
    public string ActiveIcon
    {
      get
      {
        return this._ActiveIcon;
      }
      set
      {
        this._ActiveIcon = value;
      }
    }

    [ProtoMember(7)]
    public string Affix
    {
      get
      {
        return this._Affix;
      }
      set
      {
        this._Affix = value;
      }
    }

    [ProtoMember(8)]
    public int Effect1
    {
      get
      {
        return this._Effect1;
      }
      set
      {
        this._Effect1 = value;
      }
    }

    [ProtoMember(9)]
    public int Effect2
    {
      get
      {
        return this._Effect2;
      }
      set
      {
        this._Effect2 = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
