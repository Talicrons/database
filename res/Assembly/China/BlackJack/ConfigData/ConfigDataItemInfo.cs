﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataItemInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataItemInfo")]
  [Serializable]
  public class ConfigDataItemInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private int _Rank;
    private int _SellGold;
    private ItemDisplayType _DisplayType;
    private ItemFuncType _FuncType;
    private int _FuncTypeParam1;
    private int _FuncTypeParam2;
    private int _FuncTypeParam3;
    private List<int> _FuncTypeParam4;
    private string _Icon;
    private List<GetPathData> _GetPathList;
    private string _GetPathDesc;
    private List<Goods> _AlchemyReward;
    private int _RandomDropRewardID;
    private int _DisplayRewardCount;
    private int _UISortIndex;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataItemInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(5)]
    public int Rank
    {
      get
      {
        return this._Rank;
      }
      set
      {
        this._Rank = value;
      }
    }

    [ProtoMember(6)]
    public int SellGold
    {
      get
      {
        return this._SellGold;
      }
      set
      {
        this._SellGold = value;
      }
    }

    [ProtoMember(7)]
    public ItemDisplayType DisplayType
    {
      get
      {
        return this._DisplayType;
      }
      set
      {
        this._DisplayType = value;
      }
    }

    [ProtoMember(8)]
    public ItemFuncType FuncType
    {
      get
      {
        return this._FuncType;
      }
      set
      {
        this._FuncType = value;
      }
    }

    [ProtoMember(9)]
    public int FuncTypeParam1
    {
      get
      {
        return this._FuncTypeParam1;
      }
      set
      {
        this._FuncTypeParam1 = value;
      }
    }

    [ProtoMember(10)]
    public int FuncTypeParam2
    {
      get
      {
        return this._FuncTypeParam2;
      }
      set
      {
        this._FuncTypeParam2 = value;
      }
    }

    [ProtoMember(11)]
    public int FuncTypeParam3
    {
      get
      {
        return this._FuncTypeParam3;
      }
      set
      {
        this._FuncTypeParam3 = value;
      }
    }

    [ProtoMember(12)]
    public List<int> FuncTypeParam4
    {
      get
      {
        return this._FuncTypeParam4;
      }
      set
      {
        this._FuncTypeParam4 = value;
      }
    }

    [ProtoMember(13)]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(15)]
    public List<GetPathData> GetPathList
    {
      get
      {
        return this._GetPathList;
      }
      set
      {
        this._GetPathList = value;
      }
    }

    [ProtoMember(16)]
    public string GetPathDesc
    {
      get
      {
        return this._GetPathDesc;
      }
      set
      {
        this._GetPathDesc = value;
      }
    }

    [ProtoMember(17)]
    public List<Goods> AlchemyReward
    {
      get
      {
        return this._AlchemyReward;
      }
      set
      {
        this._AlchemyReward = value;
      }
    }

    [ProtoMember(18)]
    public int RandomDropRewardID
    {
      get
      {
        return this._RandomDropRewardID;
      }
      set
      {
        this._RandomDropRewardID = value;
      }
    }

    [ProtoMember(19)]
    public int DisplayRewardCount
    {
      get
      {
        return this._DisplayRewardCount;
      }
      set
      {
        this._DisplayRewardCount = value;
      }
    }

    [ProtoMember(20)]
    public int UISortIndex
    {
      get
      {
        return this._UISortIndex;
      }
      set
      {
        this._UISortIndex = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
