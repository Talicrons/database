﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RealTimePVPConsecutiveWinsMatchmakingInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RealTimePVPConsecutiveWinsMatchmakingInfo")]
  [Serializable]
  public class RealTimePVPConsecutiveWinsMatchmakingInfo : IExtensible
  {
    private int _Count;
    private int _DanMin;
    private int _DanMax;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int Count
    {
      get
      {
        return this._Count;
      }
      set
      {
        this._Count = value;
      }
    }

    [ProtoMember(2)]
    public int DanMin
    {
      get
      {
        return this._DanMin;
      }
      set
      {
        this._DanMin = value;
      }
    }

    [ProtoMember(3)]
    public int DanMax
    {
      get
      {
        return this._DanMax;
      }
      set
      {
        this._DanMax = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
