﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataPropertyModifyInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataPropertyModifyInfo")]
  [Serializable]
  public class ConfigDataPropertyModifyInfo : IExtensible
  {
    private int _ID;
    private PropertyModifyType _PropertyModifyType;
    private string _Name;
    private bool _IsAddType;
    private bool _IsDynamic;
    private PropertyType _PropertyType;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public PropertyModifyType PropertyModifyType
    {
      get
      {
        return this._PropertyModifyType;
      }
      set
      {
        this._PropertyModifyType = value;
      }
    }

    [ProtoMember(4)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5)]
    public bool IsAddType
    {
      get
      {
        return this._IsAddType;
      }
      set
      {
        this._IsAddType = value;
      }
    }

    [ProtoMember(6)]
    public bool IsDynamic
    {
      get
      {
        return this._IsDynamic;
      }
      set
      {
        this._IsDynamic = value;
      }
    }

    [ProtoMember(7)]
    public PropertyType PropertyType
    {
      get
      {
        return this._PropertyType;
      }
      set
      {
        this._PropertyType = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
