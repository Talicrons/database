﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataLinkageHeroInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataLinkageHeroInfo")]
  [Serializable]
  public class ConfigDataLinkageHeroInfo : IExtensible
  {
    private int _ID;
    private HeroBelongProduction _HeroBelongProduction;
    private string _Name;
    private string _HeroShowBG;
    private string _Copyright;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public HeroBelongProduction HeroBelongProduction
    {
      get
      {
        return this._HeroBelongProduction;
      }
      set
      {
        this._HeroBelongProduction = value;
      }
    }

    [ProtoMember(4)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5)]
    public string HeroShowBG
    {
      get
      {
        return this._HeroShowBG;
      }
      set
      {
        this._HeroShowBG = value;
      }
    }

    [ProtoMember(6)]
    public string Copyright
    {
      get
      {
        return this._Copyright;
      }
      set
      {
        this._Copyright = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
