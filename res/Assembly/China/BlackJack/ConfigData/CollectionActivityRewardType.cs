﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CollectionActivityRewardType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "CollectionActivityRewardType")]
  public enum CollectionActivityRewardType
  {
    [ProtoEnum(Name = "CollectionActivityRewardType_None", Value = 0)] CollectionActivityRewardType_None,
    [ProtoEnum(Name = "CollectionActivityRewardType_Exchange", Value = 1)] CollectionActivityRewardType_Exchange,
    [ProtoEnum(Name = "CollectionActivityRewardType_Score", Value = 2)] CollectionActivityRewardType_Score,
  }
}
