﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.BuyRuleType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "BuyRuleType")]
  public enum BuyRuleType
  {
    [ProtoEnum(Name = "BuyRuleType_FixedTime", Value = 1)] BuyRuleType_FixedTime = 1,
    [ProtoEnum(Name = "BuyRuleType_WeekTime", Value = 2)] BuyRuleType_WeekTime = 2,
    [ProtoEnum(Name = "BuyRuleType_MonthTime", Value = 3)] BuyRuleType_MonthTime = 3,
    [ProtoEnum(Name = "BuyRuleType_Forever", Value = 4)] BuyRuleType_Forever = 4,
    [ProtoEnum(Name = "BuyRuleType_Weekend", Value = 5)] BuyRuleType_Weekend = 5,
    [ProtoEnum(Name = "BuyRuleType_DiscontinuousFixedTime", Value = 6)] BuyRuleType_DiscontinuousFixedTime = 6,
  }
}
