﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ScenarioInfoUnlockCondition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ScenarioInfoUnlockCondition")]
  [Serializable]
  public class ScenarioInfoUnlockCondition : IExtensible
  {
    private ScenarioUnlockConditionType _ConditionType;
    private int _Param;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public ScenarioUnlockConditionType ConditionType
    {
      get
      {
        return this._ConditionType;
      }
      set
      {
        this._ConditionType = value;
      }
    }

    [ProtoMember(2)]
    public int Param
    {
      get
      {
        return this._Param;
      }
      set
      {
        this._Param = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
