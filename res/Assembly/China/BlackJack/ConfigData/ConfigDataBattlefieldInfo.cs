﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataBattlefieldInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataBattlefieldInfo")]
  [Serializable]
  public class ConfigDataBattlefieldInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _Width;
    private int _Height;
    private List<int> _Terrains;
    private string _BattleMap;
    private int _BackgroundWidth;
    private int _BackgroundHeight;
    private float _BackgroundOffsetX;
    private float _BackgroundOffsetY;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattlefieldInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(7)]
    public int Width
    {
      get
      {
        return this._Width;
      }
      set
      {
        this._Width = value;
      }
    }

    [ProtoMember(8)]
    public int Height
    {
      get
      {
        return this._Height;
      }
      set
      {
        this._Height = value;
      }
    }

    [ProtoMember(9)]
    public List<int> Terrains
    {
      get
      {
        return this._Terrains;
      }
      set
      {
        this._Terrains = value;
      }
    }

    [ProtoMember(10)]
    public string BattleMap
    {
      get
      {
        return this._BattleMap;
      }
      set
      {
        this._BattleMap = value;
      }
    }

    [ProtoMember(11)]
    public int BackgroundWidth
    {
      get
      {
        return this._BackgroundWidth;
      }
      set
      {
        this._BackgroundWidth = value;
      }
    }

    [ProtoMember(12)]
    public int BackgroundHeight
    {
      get
      {
        return this._BackgroundHeight;
      }
      set
      {
        this._BackgroundHeight = value;
      }
    }

    [ProtoMember(13)]
    public float BackgroundOffsetX
    {
      get
      {
        return this._BackgroundOffsetX;
      }
      set
      {
        this._BackgroundOffsetX = value;
      }
    }

    [ProtoMember(14)]
    public float BackgroundOffsetY
    {
      get
      {
        return this._BackgroundOffsetY;
      }
      set
      {
        this._BackgroundOffsetY = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
