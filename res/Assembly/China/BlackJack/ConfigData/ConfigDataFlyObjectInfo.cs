﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataFlyObjectInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataFlyObjectInfo")]
  [Serializable]
  public class ConfigDataFlyObjectInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private int _Speed;
    private int _Distance;
    private TrackType _TrackType;
    private int _Gravity;
    private int _CollisionRadius;
    private string _Model;
    private int _ModelScale;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(7)]
    public int Speed
    {
      get
      {
        return this._Speed;
      }
      set
      {
        this._Speed = value;
      }
    }

    [ProtoMember(8)]
    public int Distance
    {
      get
      {
        return this._Distance;
      }
      set
      {
        this._Distance = value;
      }
    }

    [ProtoMember(9)]
    public TrackType TrackType
    {
      get
      {
        return this._TrackType;
      }
      set
      {
        this._TrackType = value;
      }
    }

    [ProtoMember(10)]
    public int Gravity
    {
      get
      {
        return this._Gravity;
      }
      set
      {
        this._Gravity = value;
      }
    }

    [ProtoMember(11)]
    public int CollisionRadius
    {
      get
      {
        return this._CollisionRadius;
      }
      set
      {
        this._CollisionRadius = value;
      }
    }

    [ProtoMember(12)]
    public string Model
    {
      get
      {
        return this._Model;
      }
      set
      {
        this._Model = value;
      }
    }

    [ProtoMember(13)]
    public int ModelScale
    {
      get
      {
        return this._ModelScale;
      }
      set
      {
        this._ModelScale = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
