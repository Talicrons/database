﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.Rewards
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "Rewards")]
  [Serializable]
  public class Rewards : IExtensible
  {
    private int _Id;
    private int _Percent;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int Id
    {
      get
      {
        return this._Id;
      }
      set
      {
        this._Id = value;
      }
    }

    [ProtoMember(2)]
    public int Percent
    {
      get
      {
        return this._Percent;
      }
      set
      {
        this._Percent = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
