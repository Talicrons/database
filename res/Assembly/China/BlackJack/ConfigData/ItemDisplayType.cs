﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ItemDisplayType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ItemDisplayType")]
  public enum ItemDisplayType
  {
    [ProtoEnum(Name = "ItemDisplayType_None", Value = 0)] ItemDisplayType_None,
    [ProtoEnum(Name = "ItemDisplayType_Consumable", Value = 1)] ItemDisplayType_Consumable,
    [ProtoEnum(Name = "ItemDisplayType_HeroFragment", Value = 2)] ItemDisplayType_HeroFragment,
    [ProtoEnum(Name = "ItemDisplayType_JobMaterialFragment", Value = 3)] ItemDisplayType_JobMaterialFragment,
    [ProtoEnum(Name = "ItemDisplayType_Goblin", Value = 4)] ItemDisplayType_Goblin,
    [ProtoEnum(Name = "ItemDisplayType_UnchartedScore", Value = 5)] ItemDisplayType_UnchartedScore,
  }
}
