﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.FlushRuleType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "FlushRuleType")]
  public enum FlushRuleType
  {
    [ProtoEnum(Name = "FlushRuleType_None", Value = 0)] FlushRuleType_None,
    [ProtoEnum(Name = "FlushRuleType_Period", Value = 1)] FlushRuleType_Period,
    [ProtoEnum(Name = "FlushRuleType_FixedTime", Value = 2)] FlushRuleType_FixedTime,
  }
}
