﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataTowerLevelInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataTowerLevelInfo")]
  [Serializable]
  public class ConfigDataTowerLevelInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _LevelRes;
    private string _ThumbImage;
    private int _Battle_ID;
    private List<int> _BattleRuleList;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTowerLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string LevelRes
    {
      get
      {
        return this._LevelRes;
      }
      set
      {
        this._LevelRes = value;
      }
    }

    [ProtoMember(5)]
    public string ThumbImage
    {
      get
      {
        return this._ThumbImage;
      }
      set
      {
        this._ThumbImage = value;
      }
    }

    [ProtoMember(6)]
    public int Battle_ID
    {
      get
      {
        return this._Battle_ID;
      }
      set
      {
        this._Battle_ID = value;
      }
    }

    [ProtoMember(7)]
    public List<int> BattleRuleList
    {
      get
      {
        return this._BattleRuleList;
      }
      set
      {
        this._BattleRuleList = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    public ConfigDataBattleInfo BattleInfo { get; set; }
  }
}
