﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.WaypointInfoStateList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "WaypointInfoStateList")]
  [Serializable]
  public class WaypointInfoStateList : IExtensible
  {
    private int _ScenarioId;
    private string _StateName;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int ScenarioId
    {
      get
      {
        return this._ScenarioId;
      }
      set
      {
        this._ScenarioId = value;
      }
    }

    [ProtoMember(2)]
    public string StateName
    {
      get
      {
        return this._StateName;
      }
      set
      {
        this._StateName = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
