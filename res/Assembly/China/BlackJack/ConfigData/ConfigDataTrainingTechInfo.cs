﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataTrainingTechInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataTrainingTechInfo")]
  [Serializable]
  public class ConfigDataTrainingTechInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Resource;
    private List<int> _PreTechIDs;
    private List<int> _PreTechLevel;
    private int _RoomLevelRequired;
    private List<int> _SoldierIDRelated;
    private List<int> _ArmyIDRelated;
    private bool _IsSummon;
    private TechDisplayType _TechType;
    private List<int> _TechLevelupInfoList;
    private bool _IsLocked;
    private IExtension extensionObject;
    public List<ConfigDataTrainingTechLevelInfo> m_techLevelupInfo;
    public List<TrainingTechInfo> m_Infos;
    public int m_courseId;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTrainingTechInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string Resource
    {
      get
      {
        return this._Resource;
      }
      set
      {
        this._Resource = value;
      }
    }

    [ProtoMember(5)]
    public List<int> PreTechIDs
    {
      get
      {
        return this._PreTechIDs;
      }
      set
      {
        this._PreTechIDs = value;
      }
    }

    [ProtoMember(6)]
    public List<int> PreTechLevel
    {
      get
      {
        return this._PreTechLevel;
      }
      set
      {
        this._PreTechLevel = value;
      }
    }

    [ProtoMember(7)]
    public int RoomLevelRequired
    {
      get
      {
        return this._RoomLevelRequired;
      }
      set
      {
        this._RoomLevelRequired = value;
      }
    }

    [ProtoMember(8)]
    public List<int> SoldierIDRelated
    {
      get
      {
        return this._SoldierIDRelated;
      }
      set
      {
        this._SoldierIDRelated = value;
      }
    }

    [ProtoMember(9)]
    public List<int> ArmyIDRelated
    {
      get
      {
        return this._ArmyIDRelated;
      }
      set
      {
        this._ArmyIDRelated = value;
      }
    }

    [ProtoMember(10)]
    public bool IsSummon
    {
      get
      {
        return this._IsSummon;
      }
      set
      {
        this._IsSummon = value;
      }
    }

    [ProtoMember(11)]
    public TechDisplayType TechType
    {
      get
      {
        return this._TechType;
      }
      set
      {
        this._TechType = value;
      }
    }

    [ProtoMember(12)]
    public List<int> TechLevelupInfoList
    {
      get
      {
        return this._TechLevelupInfoList;
      }
      set
      {
        this._TechLevelupInfoList = value;
      }
    }

    [ProtoMember(13)]
    public bool IsLocked
    {
      get
      {
        return this._IsLocked;
      }
      set
      {
        this._IsLocked = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
