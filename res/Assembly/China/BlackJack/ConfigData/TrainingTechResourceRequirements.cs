﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.TrainingTechResourceRequirements
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  public class TrainingTechResourceRequirements
  {
    public int Gold;
    public int RoomLevel;
    public List<int> PreTechs;
    public List<int> PreTechLevels;
    public List<Goods> Materials;

    [MethodImpl((MethodImplOptions) 32768)]
    public static TrainingTechResourceRequirements operator +(
      TrainingTechResourceRequirements x,
      TrainingTechResourceRequirements y)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
