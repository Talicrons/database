﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CollectionActivityLevelType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "CollectionActivityLevelType")]
  public enum CollectionActivityLevelType
  {
    [ProtoEnum(Name = "CollectionActivityLevelType_None", Value = 0)] CollectionActivityLevelType_None,
    [ProtoEnum(Name = "CollectionActivityLevelType_Scenario", Value = 1)] CollectionActivityLevelType_Scenario,
    [ProtoEnum(Name = "CollectionActivityLevelType_Challenge", Value = 2)] CollectionActivityLevelType_Challenge,
    [ProtoEnum(Name = "CollectionActivityLevelType_Loot", Value = 3)] CollectionActivityLevelType_Loot,
  }
}
