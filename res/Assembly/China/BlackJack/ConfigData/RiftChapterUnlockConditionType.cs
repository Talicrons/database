﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RiftChapterUnlockConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RiftChapterUnlockConditionType")]
  public enum RiftChapterUnlockConditionType
  {
    [ProtoEnum(Name = "RiftChapterUnlockConditionType_None", Value = 0)] RiftChapterUnlockConditionType_None,
    [ProtoEnum(Name = "RiftChapterUnlockConditionType_PlayerLevel", Value = 1)] RiftChapterUnlockConditionType_PlayerLevel,
    [ProtoEnum(Name = "RiftChapterUnlockConditionType_Scenario", Value = 2)] RiftChapterUnlockConditionType_Scenario,
    [ProtoEnum(Name = "RiftChapterUnlockConditionType_ChapterStar", Value = 3)] RiftChapterUnlockConditionType_ChapterStar,
  }
}
