﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.EventDisappearConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "EventDisappearConditionType")]
  public enum EventDisappearConditionType
  {
    [ProtoEnum(Name = "EventDisappearConditionType_None", Value = 0)] EventDisappearConditionType_None,
    [ProtoEnum(Name = "EventDisappearConditionType_Complete", Value = 1)] EventDisappearConditionType_Complete,
    [ProtoEnum(Name = "EventDisappearConditionType_LifeTime", Value = 2)] EventDisappearConditionType_LifeTime,
  }
}
