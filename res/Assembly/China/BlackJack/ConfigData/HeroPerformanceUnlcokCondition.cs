﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroPerformanceUnlcokCondition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroPerformanceUnlcokCondition")]
  [Serializable]
  public class HeroPerformanceUnlcokCondition : IExtensible
  {
    private HeroPerformanceUnlockConditionType _ConditionType;
    private int _Param1;
    private int _Param2;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public HeroPerformanceUnlockConditionType ConditionType
    {
      get
      {
        return this._ConditionType;
      }
      set
      {
        this._ConditionType = value;
      }
    }

    [ProtoMember(2)]
    public int Param1
    {
      get
      {
        return this._Param1;
      }
      set
      {
        this._Param1 = value;
      }
    }

    [ProtoMember(3)]
    public int Param2
    {
      get
      {
        return this._Param2;
      }
      set
      {
        this._Param2 = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
