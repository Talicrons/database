﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.LevelAndMembers
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "LevelAndMembers")]
  [Serializable]
  public class LevelAndMembers : IExtensible
  {
    private int _Level;
    private int _Members;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int Level
    {
      get
      {
        return this._Level;
      }
      set
      {
        this._Level = value;
      }
    }

    [ProtoMember(2)]
    public int Members
    {
      get
      {
        return this._Members;
      }
      set
      {
        this._Members = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
