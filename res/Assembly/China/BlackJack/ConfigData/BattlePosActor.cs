﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.BattlePosActor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "BattlePosActor")]
  [Serializable]
  public class BattlePosActor : IExtensible
  {
    private int _X;
    private int _Y;
    private int _ID;
    private int _Level;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int X
    {
      get
      {
        return this._X;
      }
      set
      {
        this._X = value;
      }
    }

    [ProtoMember(2)]
    public int Y
    {
      get
      {
        return this._Y;
      }
      set
      {
        this._Y = value;
      }
    }

    [ProtoMember(3)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(4)]
    public int Level
    {
      get
      {
        return this._Level;
      }
      set
      {
        this._Level = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
