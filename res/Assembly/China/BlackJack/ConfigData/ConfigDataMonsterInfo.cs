﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataMonsterInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataMonsterInfo")]
  [Serializable]
  public class ConfigDataMonsterInfo : IExtensible
  {
    private int _ID;
    private bool _IsBoss;
    private bool _NoDie;
    private int _ActionLoop;
    private bool _AddDamage;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(4)]
    public bool IsBoss
    {
      get
      {
        return this._IsBoss;
      }
      set
      {
        this._IsBoss = value;
      }
    }

    [ProtoMember(5)]
    public bool NoDie
    {
      get
      {
        return this._NoDie;
      }
      set
      {
        this._NoDie = value;
      }
    }

    [ProtoMember(6)]
    public int ActionLoop
    {
      get
      {
        return this._ActionLoop;
      }
      set
      {
        this._ActionLoop = value;
      }
    }

    [ProtoMember(7)]
    public bool AddDamage
    {
      get
      {
        return this._AddDamage;
      }
      set
      {
        this._AddDamage = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
