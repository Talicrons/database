﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.AncientRecommendHeros
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "AncientRecommendHeros")]
  [Serializable]
  public class AncientRecommendHeros : IExtensible
  {
    private int _Hero;
    private int _DescId;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int Hero
    {
      get
      {
        return this._Hero;
      }
      set
      {
        this._Hero = value;
      }
    }

    [ProtoMember(2)]
    public int DescId
    {
      get
      {
        return this._DescId;
      }
      set
      {
        this._DescId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
