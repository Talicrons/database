﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataTrainingCourseInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataTrainingCourseInfo")]
  [Serializable]
  public class ConfigDataTrainingCourseInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private List<string> _Resource;
    private int _RoomID;
    private int _RoomLevel;
    private List<int> _Techs;
    private string _SkillTreeName;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTrainingCourseInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public List<string> Resource
    {
      get
      {
        return this._Resource;
      }
      set
      {
        this._Resource = value;
      }
    }

    [ProtoMember(5)]
    public int RoomID
    {
      get
      {
        return this._RoomID;
      }
      set
      {
        this._RoomID = value;
      }
    }

    [ProtoMember(6)]
    public int RoomLevel
    {
      get
      {
        return this._RoomLevel;
      }
      set
      {
        this._RoomLevel = value;
      }
    }

    [ProtoMember(7)]
    public List<int> Techs
    {
      get
      {
        return this._Techs;
      }
      set
      {
        this._Techs = value;
      }
    }

    [ProtoMember(8)]
    public string SkillTreeName
    {
      get
      {
        return this._SkillTreeName;
      }
      set
      {
        this._SkillTreeName = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
