﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataMissionExtRefluxInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataMissionExtRefluxInfo")]
  [Serializable]
  public class ConfigDataMissionExtRefluxInfo : IExtensible
  {
    private int _ID;
    private int _RefluxPoints;
    private int _ActivateTime;
    private int _DeactivateTime;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public int RefluxPoints
    {
      get
      {
        return this._RefluxPoints;
      }
      set
      {
        this._RefluxPoints = value;
      }
    }

    [ProtoMember(4)]
    public int ActivateTime
    {
      get
      {
        return this._ActivateTime;
      }
      set
      {
        this._ActivateTime = value;
      }
    }

    [ProtoMember(5)]
    public int DeactivateTime
    {
      get
      {
        return this._DeactivateTime;
      }
      set
      {
        this._DeactivateTime = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
