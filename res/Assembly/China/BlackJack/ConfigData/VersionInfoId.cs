﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.VersionInfoId
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "VersionInfoId")]
  public enum VersionInfoId
  {
    [ProtoEnum(Name = "VersionInfoId_ClientProgram", Value = 1)] VersionInfoId_ClientProgram = 1,
    [ProtoEnum(Name = "VersionInfoId_AssetBundle", Value = 2)] VersionInfoId_AssetBundle = 2,
    [ProtoEnum(Name = "VersionInfoId_BattleReport", Value = 3)] VersionInfoId_BattleReport = 3,
  }
}
