﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataBattleAchievementInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataBattleAchievementInfo")]
  [Serializable]
  public class ConfigDataBattleAchievementInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private BattleAchievementConditionType _ConditionType;
    private int _TurnCount;
    private List<int> _ActorID;
    private List<int> _ConditionParam1;
    private List<ParamPosition> _ConditionParam2;
    private List<int> _ConditionParam3;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleAchievementInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(7)]
    public BattleAchievementConditionType ConditionType
    {
      get
      {
        return this._ConditionType;
      }
      set
      {
        this._ConditionType = value;
      }
    }

    [ProtoMember(8)]
    public int TurnCount
    {
      get
      {
        return this._TurnCount;
      }
      set
      {
        this._TurnCount = value;
      }
    }

    [ProtoMember(9)]
    public List<int> ActorID
    {
      get
      {
        return this._ActorID;
      }
      set
      {
        this._ActorID = value;
      }
    }

    [ProtoMember(10)]
    public List<int> ConditionParam1
    {
      get
      {
        return this._ConditionParam1;
      }
      set
      {
        this._ConditionParam1 = value;
      }
    }

    [ProtoMember(11)]
    public List<ParamPosition> ConditionParam2
    {
      get
      {
        return this._ConditionParam2;
      }
      set
      {
        this._ConditionParam2 = value;
      }
    }

    [ProtoMember(12)]
    public List<int> ConditionParam3
    {
      get
      {
        return this._ConditionParam3;
      }
      set
      {
        this._ConditionParam3 = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Param1FirstValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Param2FirstValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Param3FirstValue()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
