﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroInfoStarToRank
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroInfoStarToRank")]
  [Serializable]
  public class HeroInfoStarToRank : IExtensible
  {
    private int _Star;
    private int _Rank;
    private int _CharImageId;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int Star
    {
      get
      {
        return this._Star;
      }
      set
      {
        this._Star = value;
      }
    }

    [ProtoMember(2)]
    public int Rank
    {
      get
      {
        return this._Rank;
      }
      set
      {
        this._Rank = value;
      }
    }

    [ProtoMember(3)]
    public int CharImageId
    {
      get
      {
        return this._CharImageId;
      }
      set
      {
        this._CharImageId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
