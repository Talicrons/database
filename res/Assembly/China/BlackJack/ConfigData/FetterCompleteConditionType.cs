﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.FetterCompleteConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "FetterCompleteConditionType")]
  public enum FetterCompleteConditionType
  {
    [ProtoEnum(Name = "FetterCompleteConditionType_None", Value = 0)] FetterCompleteConditionType_None,
    [ProtoEnum(Name = "FetterCompleteConditionType_HeroFavorabilityLevel", Value = 1)] FetterCompleteConditionType_HeroFavorabilityLevel,
    [ProtoEnum(Name = "FetterCompleteConditionType_Mission", Value = 2)] FetterCompleteConditionType_Mission,
  }
}
