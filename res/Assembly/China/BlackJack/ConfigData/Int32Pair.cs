﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.Int32Pair
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "Int32Pair")]
  [Serializable]
  public class Int32Pair : IExtensible
  {
    private int _Key;
    private int _Value;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int Key
    {
      get
      {
        return this._Key;
      }
      set
      {
        this._Key = value;
      }
    }

    [ProtoMember(2)]
    public int Value
    {
      get
      {
        return this._Value;
      }
      set
      {
        this._Value = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
