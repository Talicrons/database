﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataScoreLevelInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataScoreLevelInfo")]
  [Serializable]
  public class ConfigDataScoreLevelInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _TeamName;
    private int _PlayerLevelMin;
    private List<int> _UnlockDaysDelayGroup;
    private int _EnergySuccess;
    private int _EnergyFail;
    private int _EnergyTeam;
    private int _MonsterLevel;
    private int _Battle_ID;
    private int _PlayerExp;
    private int _HeroExp;
    private int _GoldReward;
    private int _ScoreBase;
    private int _DropID;
    private int _ItemDropCountDisplay;
    private int _TeamDrop_ID;
    private int _DayBonusDrop_ID;
    private string _Strategy;
    private IExtension extensionObject;
    public HashSet<int> UnlockDaysDelayGroupSet;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataScoreLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string TeamName
    {
      get
      {
        return this._TeamName;
      }
      set
      {
        this._TeamName = value;
      }
    }

    [ProtoMember(5)]
    public int PlayerLevelMin
    {
      get
      {
        return this._PlayerLevelMin;
      }
      set
      {
        this._PlayerLevelMin = value;
      }
    }

    [ProtoMember(6)]
    public List<int> UnlockDaysDelayGroup
    {
      get
      {
        return this._UnlockDaysDelayGroup;
      }
      set
      {
        this._UnlockDaysDelayGroup = value;
      }
    }

    [ProtoMember(7)]
    public int EnergySuccess
    {
      get
      {
        return this._EnergySuccess;
      }
      set
      {
        this._EnergySuccess = value;
      }
    }

    [ProtoMember(8)]
    public int EnergyFail
    {
      get
      {
        return this._EnergyFail;
      }
      set
      {
        this._EnergyFail = value;
      }
    }

    [ProtoMember(9)]
    public int EnergyTeam
    {
      get
      {
        return this._EnergyTeam;
      }
      set
      {
        this._EnergyTeam = value;
      }
    }

    [ProtoMember(10)]
    public int MonsterLevel
    {
      get
      {
        return this._MonsterLevel;
      }
      set
      {
        this._MonsterLevel = value;
      }
    }

    [ProtoMember(11)]
    public int Battle_ID
    {
      get
      {
        return this._Battle_ID;
      }
      set
      {
        this._Battle_ID = value;
      }
    }

    [ProtoMember(12)]
    public int PlayerExp
    {
      get
      {
        return this._PlayerExp;
      }
      set
      {
        this._PlayerExp = value;
      }
    }

    [ProtoMember(13)]
    public int HeroExp
    {
      get
      {
        return this._HeroExp;
      }
      set
      {
        this._HeroExp = value;
      }
    }

    [ProtoMember(14)]
    public int GoldReward
    {
      get
      {
        return this._GoldReward;
      }
      set
      {
        this._GoldReward = value;
      }
    }

    [ProtoMember(15)]
    public int ScoreBase
    {
      get
      {
        return this._ScoreBase;
      }
      set
      {
        this._ScoreBase = value;
      }
    }

    [ProtoMember(16)]
    public int DropID
    {
      get
      {
        return this._DropID;
      }
      set
      {
        this._DropID = value;
      }
    }

    [ProtoMember(17)]
    public int ItemDropCountDisplay
    {
      get
      {
        return this._ItemDropCountDisplay;
      }
      set
      {
        this._ItemDropCountDisplay = value;
      }
    }

    [ProtoMember(18)]
    public int TeamDrop_ID
    {
      get
      {
        return this._TeamDrop_ID;
      }
      set
      {
        this._TeamDrop_ID = value;
      }
    }

    [ProtoMember(19)]
    public int DayBonusDrop_ID
    {
      get
      {
        return this._DayBonusDrop_ID;
      }
      set
      {
        this._DayBonusDrop_ID = value;
      }
    }

    [ProtoMember(20)]
    public string Strategy
    {
      get
      {
        return this._Strategy;
      }
      set
      {
        this._Strategy = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    public ConfigDataBattleInfo BattleInfo { get; set; }

    public ConfigDataRandomDropRewardInfo RandomDropInfo { get; set; }

    public ConfigDataRandomDropRewardInfo TeamRandomDropInfo { get; set; }

    public ConfigDataRandomDropRewardInfo DailyRandomDropInfo { get; set; }

    public ConfigDataUnchartedScoreInfo UnchartedScoreInfo { get; set; }
  }
}
