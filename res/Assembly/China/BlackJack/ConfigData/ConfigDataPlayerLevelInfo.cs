﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataPlayerLevelInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataPlayerLevelInfo")]
  [Serializable]
  public class ConfigDataPlayerLevelInfo : IExtensible
  {
    private int _ID;
    private int _Exp;
    private int _Energy;
    private int _ArenaBuffID;
    private int _PVPBuffID;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public int Exp
    {
      get
      {
        return this._Exp;
      }
      set
      {
        this._Exp = value;
      }
    }

    [ProtoMember(4)]
    public int Energy
    {
      get
      {
        return this._Energy;
      }
      set
      {
        this._Energy = value;
      }
    }

    [ProtoMember(5)]
    public int ArenaBuffID
    {
      get
      {
        return this._ArenaBuffID;
      }
      set
      {
        this._ArenaBuffID = value;
      }
    }

    [ProtoMember(6)]
    public int PVPBuffID
    {
      get
      {
        return this._PVPBuffID;
      }
      set
      {
        this._PVPBuffID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
