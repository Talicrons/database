﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.EventFuncType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "EventFuncType")]
  public enum EventFuncType
  {
    [ProtoEnum(Name = "EventFuncType_None", Value = 0)] EventFuncType_None,
    [ProtoEnum(Name = "EventFuncType_Monster", Value = 1)] EventFuncType_Monster,
    [ProtoEnum(Name = "EventFuncType_Mission", Value = 2)] EventFuncType_Mission,
    [ProtoEnum(Name = "EventFuncType_Dialog", Value = 3)] EventFuncType_Dialog,
    [ProtoEnum(Name = "EventFuncType_Treasure", Value = 4)] EventFuncType_Treasure,
    [ProtoEnum(Name = "EventFuncType_Shop", Value = 5)] EventFuncType_Shop,
  }
}
