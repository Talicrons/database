﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RandomDropRewardGroup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  public class RandomDropRewardGroup
  {
    public int GroupIndex;
    public int DropCount;
    public Dictionary<int, WeightGoods> DropRewards;

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomDropRewardGroup()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
