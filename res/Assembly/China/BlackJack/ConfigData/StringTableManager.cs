﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.StringTableManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.ConfigData;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  public class StringTableManager : StringTableManagerBase
  {
    private Dictionary<int, string> m_ConfigDataST;

    [MethodImpl((MethodImplOptions) 32768)]
    public StringTableManager(ClientConfigDataLoaderBase configLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected override IEnumerator InitDefaultStringTable(Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearCurrentLocalizeion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string GetStringInDefaultStringTable(string key)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetStringInST(string key)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ParseStringKey(string key, out string stringTableName, out int stringTableId)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
