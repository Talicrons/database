﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataArenaDefendRuleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataArenaDefendRuleInfo")]
  [Serializable]
  public class ConfigDataArenaDefendRuleInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _DefendWinDesc;
    private string _DefendLoseDesc;
    private string _WinDesc;
    private string _LoseDesc;
    private int _TurnMax;
    private List<int> _WinConditions_ID;
    private List<int> _LoseConditions_ID;
    private string _Icon;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataArenaDefendRuleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5)]
    public string DefendWinDesc
    {
      get
      {
        return this._DefendWinDesc;
      }
      set
      {
        this._DefendWinDesc = value;
      }
    }

    [ProtoMember(7)]
    public string DefendLoseDesc
    {
      get
      {
        return this._DefendLoseDesc;
      }
      set
      {
        this._DefendLoseDesc = value;
      }
    }

    [ProtoMember(9)]
    public string WinDesc
    {
      get
      {
        return this._WinDesc;
      }
      set
      {
        this._WinDesc = value;
      }
    }

    [ProtoMember(11)]
    public string LoseDesc
    {
      get
      {
        return this._LoseDesc;
      }
      set
      {
        this._LoseDesc = value;
      }
    }

    [ProtoMember(13)]
    public int TurnMax
    {
      get
      {
        return this._TurnMax;
      }
      set
      {
        this._TurnMax = value;
      }
    }

    [ProtoMember(14)]
    public List<int> WinConditions_ID
    {
      get
      {
        return this._WinConditions_ID;
      }
      set
      {
        this._WinConditions_ID = value;
      }
    }

    [ProtoMember(15)]
    public List<int> LoseConditions_ID
    {
      get
      {
        return this._LoseConditions_ID;
      }
      set
      {
        this._LoseConditions_ID = value;
      }
    }

    [ProtoMember(16)]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
