﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataStoreInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataStoreInfo")]
  [Serializable]
  public class ConfigDataStoreInfo : IExtensible
  {
    private int _ID;
    private StoreId _StoreID;
    private StoreType _Type;
    private string _StoreName;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public StoreId StoreID
    {
      get
      {
        return this._StoreID;
      }
      set
      {
        this._StoreID = value;
      }
    }

    [ProtoMember(4)]
    public StoreType Type
    {
      get
      {
        return this._Type;
      }
      set
      {
        this._Type = value;
      }
    }

    [ProtoMember(5)]
    public string StoreName
    {
      get
      {
        return this._StoreName;
      }
      set
      {
        this._StoreName = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
