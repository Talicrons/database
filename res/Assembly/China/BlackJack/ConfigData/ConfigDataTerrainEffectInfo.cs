﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataTerrainEffectInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataTerrainEffectInfo")]
  [Serializable]
  public class ConfigDataTerrainEffectInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private int _Damage;
    private int _Time;
    private int _ReplacePriority;
    private string _Fx;
    private string _EnemyFx;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(5)]
    public int Damage
    {
      get
      {
        return this._Damage;
      }
      set
      {
        this._Damage = value;
      }
    }

    [ProtoMember(6)]
    public int Time
    {
      get
      {
        return this._Time;
      }
      set
      {
        this._Time = value;
      }
    }

    [ProtoMember(7)]
    public int ReplacePriority
    {
      get
      {
        return this._ReplacePriority;
      }
      set
      {
        this._ReplacePriority = value;
      }
    }

    [ProtoMember(8)]
    public string Fx
    {
      get
      {
        return this._Fx;
      }
      set
      {
        this._Fx = value;
      }
    }

    [ProtoMember(9)]
    public string EnemyFx
    {
      get
      {
        return this._EnemyFx;
      }
      set
      {
        this._EnemyFx = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInfiniteTime()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
