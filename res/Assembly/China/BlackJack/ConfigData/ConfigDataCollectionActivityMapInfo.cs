﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataCollectionActivityMapInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataCollectionActivityMapInfo")]
  [Serializable]
  public class ConfigDataCollectionActivityMapInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private List<int> _Waypoints_ID;
    private int _StartWaypoint_ID;
    private int _FirstScenario_ID;
    private string _Background;
    private int _BackgroundWidth;
    private int _BackgroundHeight;
    private int _CameraHeight;
    private int _CameraHeightMin;
    private int _CameraHeightMax;
    private int _CameraAngle;
    private bool _CameraFollow;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityMapInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(5)]
    public List<int> Waypoints_ID
    {
      get
      {
        return this._Waypoints_ID;
      }
      set
      {
        this._Waypoints_ID = value;
      }
    }

    [ProtoMember(6)]
    public int StartWaypoint_ID
    {
      get
      {
        return this._StartWaypoint_ID;
      }
      set
      {
        this._StartWaypoint_ID = value;
      }
    }

    [ProtoMember(7)]
    public int FirstScenario_ID
    {
      get
      {
        return this._FirstScenario_ID;
      }
      set
      {
        this._FirstScenario_ID = value;
      }
    }

    [ProtoMember(8)]
    public string Background
    {
      get
      {
        return this._Background;
      }
      set
      {
        this._Background = value;
      }
    }

    [ProtoMember(9)]
    public int BackgroundWidth
    {
      get
      {
        return this._BackgroundWidth;
      }
      set
      {
        this._BackgroundWidth = value;
      }
    }

    [ProtoMember(10)]
    public int BackgroundHeight
    {
      get
      {
        return this._BackgroundHeight;
      }
      set
      {
        this._BackgroundHeight = value;
      }
    }

    [ProtoMember(11)]
    public int CameraHeight
    {
      get
      {
        return this._CameraHeight;
      }
      set
      {
        this._CameraHeight = value;
      }
    }

    [ProtoMember(12)]
    public int CameraHeightMin
    {
      get
      {
        return this._CameraHeightMin;
      }
      set
      {
        this._CameraHeightMin = value;
      }
    }

    [ProtoMember(13)]
    public int CameraHeightMax
    {
      get
      {
        return this._CameraHeightMax;
      }
      set
      {
        this._CameraHeightMax = value;
      }
    }

    [ProtoMember(14)]
    public int CameraAngle
    {
      get
      {
        return this._CameraAngle;
      }
      set
      {
        this._CameraAngle = value;
      }
    }

    [ProtoMember(15)]
    public bool CameraFollow
    {
      get
      {
        return this._CameraFollow;
      }
      set
      {
        this._CameraFollow = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
