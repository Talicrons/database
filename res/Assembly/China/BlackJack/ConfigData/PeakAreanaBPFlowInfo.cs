﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.PeakAreanaBPFlowInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

namespace BlackJack.ConfigData
{
  public class PeakAreanaBPFlowInfo
  {
    public int BPType;
    public int PlayerIndex;

    public int Nums { get; set; }
  }
}
