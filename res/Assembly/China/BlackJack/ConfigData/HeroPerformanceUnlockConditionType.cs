﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroPerformanceUnlockConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroPerformanceUnlockConditionType")]
  public enum HeroPerformanceUnlockConditionType
  {
    [ProtoEnum(Name = "HeroPerformanceUnlockConditionType_None", Value = 0)] HeroPerformanceUnlockConditionType_None,
    [ProtoEnum(Name = "HeroPerformanceUnlockConditionType_HeroFavourabilityLevel", Value = 1)] HeroPerformanceUnlockConditionType_HeroFavourabilityLevel,
  }
}
