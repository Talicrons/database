﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.PoolContentItemData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "PoolContentItemData")]
  [Serializable]
  public class PoolContentItemData : IExtensible
  {
    private int _GoodsType;
    private int _GoodsId;
    private string _Rank;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int GoodsType
    {
      get
      {
        return this._GoodsType;
      }
      set
      {
        this._GoodsType = value;
      }
    }

    [ProtoMember(2)]
    public int GoodsId
    {
      get
      {
        return this._GoodsId;
      }
      set
      {
        this._GoodsId = value;
      }
    }

    [ProtoMember(3)]
    public string Rank
    {
      get
      {
        return this._Rank;
      }
      set
      {
        this._Rank = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
