﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataUnchartedScoreRewardGroupInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataUnchartedScoreRewardGroupInfo")]
  [Serializable]
  public class ConfigDataUnchartedScoreRewardGroupInfo : IExtensible
  {
    private int _ID;
    private int _GroupId;
    private int _Score;
    private string _Name;
    private List<Goods> _RewardList;
    private int _RewardDisplayCount;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataUnchartedScoreRewardGroupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public int GroupId
    {
      get
      {
        return this._GroupId;
      }
      set
      {
        this._GroupId = value;
      }
    }

    [ProtoMember(4)]
    public int Score
    {
      get
      {
        return this._Score;
      }
      set
      {
        this._Score = value;
      }
    }

    [ProtoMember(5)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(6)]
    public List<Goods> RewardList
    {
      get
      {
        return this._RewardList;
      }
      set
      {
        this._RewardList = value;
      }
    }

    [ProtoMember(7)]
    public int RewardDisplayCount
    {
      get
      {
        return this._RewardDisplayCount;
      }
      set
      {
        this._RewardDisplayCount = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
