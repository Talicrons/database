﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataMonthCardInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataMonthCardInfo")]
  [Serializable]
  public class ConfigDataMonthCardInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _ValidDays;
    private string _Desc;
    private List<Goods> _Reward;
    private int _OpenEverydayTaskId;
    private int _HeroExpAddRate;
    private int _HeroFavourabilityExpAddRate;
    private List<GameFuncDayBonus> _DayBonusAdd;
    private GameFunctionType _OpenGameFunc;
    private bool _IsAppleSubscribe;
    private int _FightFailEnergyDecreaseRate;
    private string _Icon;
    private List<GameFuncDayTimes> _UnchartedRaidTimesAdd;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataMonthCardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public int ValidDays
    {
      get
      {
        return this._ValidDays;
      }
      set
      {
        this._ValidDays = value;
      }
    }

    [ProtoMember(5)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(6)]
    public List<Goods> Reward
    {
      get
      {
        return this._Reward;
      }
      set
      {
        this._Reward = value;
      }
    }

    [ProtoMember(7)]
    public int OpenEverydayTaskId
    {
      get
      {
        return this._OpenEverydayTaskId;
      }
      set
      {
        this._OpenEverydayTaskId = value;
      }
    }

    [ProtoMember(8)]
    public int HeroExpAddRate
    {
      get
      {
        return this._HeroExpAddRate;
      }
      set
      {
        this._HeroExpAddRate = value;
      }
    }

    [ProtoMember(9)]
    public int HeroFavourabilityExpAddRate
    {
      get
      {
        return this._HeroFavourabilityExpAddRate;
      }
      set
      {
        this._HeroFavourabilityExpAddRate = value;
      }
    }

    [ProtoMember(10)]
    public List<GameFuncDayBonus> DayBonusAdd
    {
      get
      {
        return this._DayBonusAdd;
      }
      set
      {
        this._DayBonusAdd = value;
      }
    }

    [ProtoMember(11)]
    public GameFunctionType OpenGameFunc
    {
      get
      {
        return this._OpenGameFunc;
      }
      set
      {
        this._OpenGameFunc = value;
      }
    }

    [ProtoMember(12)]
    public bool IsAppleSubscribe
    {
      get
      {
        return this._IsAppleSubscribe;
      }
      set
      {
        this._IsAppleSubscribe = value;
      }
    }

    [ProtoMember(13)]
    public int FightFailEnergyDecreaseRate
    {
      get
      {
        return this._FightFailEnergyDecreaseRate;
      }
      set
      {
        this._FightFailEnergyDecreaseRate = value;
      }
    }

    [ProtoMember(14)]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(15)]
    public List<GameFuncDayTimes> UnchartedRaidTimesAdd
    {
      get
      {
        return this._UnchartedRaidTimesAdd;
      }
      set
      {
        this._UnchartedRaidTimesAdd = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
