﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ActivityRetrospectLevelType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ActivityRetrospectLevelType")]
  public enum ActivityRetrospectLevelType
  {
    [ProtoEnum(Name = "ActivityRetrospectLevelType_None", Value = 0)] ActivityRetrospectLevelType_None,
    [ProtoEnum(Name = "ActivityRetrospectLevelType_Scenario", Value = 1)] ActivityRetrospectLevelType_Scenario,
    [ProtoEnum(Name = "ActivityRetrospectLevelType_Challenge", Value = 2)] ActivityRetrospectLevelType_Challenge,
    [ProtoEnum(Name = "ActivityRetrospectLevelType_Loot", Value = 3)] ActivityRetrospectLevelType_Loot,
  }
}
