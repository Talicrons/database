﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataChallengeLevelInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataChallengeLevelInfo")]
  [Serializable]
  public class ConfigDataChallengeLevelInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _PrevLevelId;
    private int _UnlockDaysDelay;
    private int _EnergySuccess;
    private int _EnergyFail;
    private int _MonsterLevel;
    private int _Battle_ID;
    private int _DialogBefore_ID;
    private int _DialogAfter_ID;
    private int _PlayerExp;
    private int _HeroExp;
    private int _Gold;
    private int _ScoreBase;
    private List<Goods> _CommonRewardList;
    private int _ItemDropCountDisplay;
    private string _Strategy;
    private ScoreLevelType _LevelType;
    private IExtension extensionObject;
    public ConfigDataDialogInfo m_dialogInfoBefore;
    public ConfigDataDialogInfo m_dialogInfoAfter;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataChallengeLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public int PrevLevelId
    {
      get
      {
        return this._PrevLevelId;
      }
      set
      {
        this._PrevLevelId = value;
      }
    }

    [ProtoMember(5)]
    public int UnlockDaysDelay
    {
      get
      {
        return this._UnlockDaysDelay;
      }
      set
      {
        this._UnlockDaysDelay = value;
      }
    }

    [ProtoMember(6)]
    public int EnergySuccess
    {
      get
      {
        return this._EnergySuccess;
      }
      set
      {
        this._EnergySuccess = value;
      }
    }

    [ProtoMember(7)]
    public int EnergyFail
    {
      get
      {
        return this._EnergyFail;
      }
      set
      {
        this._EnergyFail = value;
      }
    }

    [ProtoMember(8)]
    public int MonsterLevel
    {
      get
      {
        return this._MonsterLevel;
      }
      set
      {
        this._MonsterLevel = value;
      }
    }

    [ProtoMember(9)]
    public int Battle_ID
    {
      get
      {
        return this._Battle_ID;
      }
      set
      {
        this._Battle_ID = value;
      }
    }

    [ProtoMember(10)]
    public int DialogBefore_ID
    {
      get
      {
        return this._DialogBefore_ID;
      }
      set
      {
        this._DialogBefore_ID = value;
      }
    }

    [ProtoMember(11)]
    public int DialogAfter_ID
    {
      get
      {
        return this._DialogAfter_ID;
      }
      set
      {
        this._DialogAfter_ID = value;
      }
    }

    [ProtoMember(12)]
    public int PlayerExp
    {
      get
      {
        return this._PlayerExp;
      }
      set
      {
        this._PlayerExp = value;
      }
    }

    [ProtoMember(13)]
    public int HeroExp
    {
      get
      {
        return this._HeroExp;
      }
      set
      {
        this._HeroExp = value;
      }
    }

    [ProtoMember(14)]
    public int Gold
    {
      get
      {
        return this._Gold;
      }
      set
      {
        this._Gold = value;
      }
    }

    [ProtoMember(15)]
    public int ScoreBase
    {
      get
      {
        return this._ScoreBase;
      }
      set
      {
        this._ScoreBase = value;
      }
    }

    [ProtoMember(16)]
    public List<Goods> CommonRewardList
    {
      get
      {
        return this._CommonRewardList;
      }
      set
      {
        this._CommonRewardList = value;
      }
    }

    [ProtoMember(17)]
    public int ItemDropCountDisplay
    {
      get
      {
        return this._ItemDropCountDisplay;
      }
      set
      {
        this._ItemDropCountDisplay = value;
      }
    }

    [ProtoMember(18)]
    public string Strategy
    {
      get
      {
        return this._Strategy;
      }
      set
      {
        this._Strategy = value;
      }
    }

    [ProtoMember(19)]
    public ScoreLevelType LevelType
    {
      get
      {
        return this._LevelType;
      }
      set
      {
        this._LevelType = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    public ConfigDataBattleInfo BattleInfo { get; set; }

    public ConfigDataUnchartedScoreInfo UnchartedScoreInfo { get; set; }
  }
}
