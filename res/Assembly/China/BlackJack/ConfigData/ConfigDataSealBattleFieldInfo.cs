﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataSealBattleFieldInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataSealBattleFieldInfo")]
  [Serializable]
  public class ConfigDataSealBattleFieldInfo : IExtensible
  {
    private int _ID;
    private List<int> _Group;
    private string _TabImage;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSealBattleFieldInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public List<int> Group
    {
      get
      {
        return this._Group;
      }
      set
      {
        this._Group = value;
      }
    }

    [ProtoMember(4)]
    public string TabImage
    {
      get
      {
        return this._TabImage;
      }
      set
      {
        this._TabImage = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
