﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataRegionInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataRegionInfo")]
  [Serializable]
  public class ConfigDataRegionInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private List<int> _Waypoints_ID;
    private int _OpenByScenario_ID;
    private IExtension extensionObject;
    public ConfigDataWorldMapInfo m_worldMapInfo;
    public ConfigDataWaypointInfo[] m_waypointInfos;
    public ConfigDataScenarioInfo m_openByScenarioInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRegionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(7)]
    public List<int> Waypoints_ID
    {
      get
      {
        return this._Waypoints_ID;
      }
      set
      {
        this._Waypoints_ID = value;
      }
    }

    [ProtoMember(8)]
    public int OpenByScenario_ID
    {
      get
      {
        return this._OpenByScenario_ID;
      }
      set
      {
        this._OpenByScenario_ID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
