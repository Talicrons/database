﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataGuildMassiveCombatStrongholdInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataGuildMassiveCombatStrongholdInfo")]
  [Serializable]
  public class ConfigDataGuildMassiveCombatStrongholdInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _UIName;
    private string _Resources;
    private string _TeamName;
    private List<int> _LevelList;
    private int _EnemyLevel;
    private int _EnemyCount;
    private int _EnemyPoints;
    private int _PerfectWinPointsBonus;
    private List<Goods> _Bonus;
    private List<int> _UpHeroTag_IDS;
    private int _UpSkill_ID;
    private IExtension extensionObject;
    public ConfigDataGuildMassiveCombatDifficultyInfo m_difficultyInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGuildMassiveCombatStrongholdInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string UIName
    {
      get
      {
        return this._UIName;
      }
      set
      {
        this._UIName = value;
      }
    }

    [ProtoMember(5)]
    public string Resources
    {
      get
      {
        return this._Resources;
      }
      set
      {
        this._Resources = value;
      }
    }

    [ProtoMember(6)]
    public string TeamName
    {
      get
      {
        return this._TeamName;
      }
      set
      {
        this._TeamName = value;
      }
    }

    [ProtoMember(7)]
    public List<int> LevelList
    {
      get
      {
        return this._LevelList;
      }
      set
      {
        this._LevelList = value;
      }
    }

    [ProtoMember(8)]
    public int EnemyLevel
    {
      get
      {
        return this._EnemyLevel;
      }
      set
      {
        this._EnemyLevel = value;
      }
    }

    [ProtoMember(9)]
    public int EnemyCount
    {
      get
      {
        return this._EnemyCount;
      }
      set
      {
        this._EnemyCount = value;
      }
    }

    [ProtoMember(10)]
    public int EnemyPoints
    {
      get
      {
        return this._EnemyPoints;
      }
      set
      {
        this._EnemyPoints = value;
      }
    }

    [ProtoMember(11)]
    public int PerfectWinPointsBonus
    {
      get
      {
        return this._PerfectWinPointsBonus;
      }
      set
      {
        this._PerfectWinPointsBonus = value;
      }
    }

    [ProtoMember(12)]
    public List<Goods> Bonus
    {
      get
      {
        return this._Bonus;
      }
      set
      {
        this._Bonus = value;
      }
    }

    [ProtoMember(13)]
    public List<int> UpHeroTag_IDS
    {
      get
      {
        return this._UpHeroTag_IDS;
      }
      set
      {
        this._UpHeroTag_IDS = value;
      }
    }

    [ProtoMember(14)]
    public int UpSkill_ID
    {
      get
      {
        return this._UpSkill_ID;
      }
      set
      {
        this._UpSkill_ID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
