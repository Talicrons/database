﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroSkinInfoSummonSkins
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroSkinInfoSummonSkins")]
  [Serializable]
  public class HeroSkinInfoSummonSkins : IExtensible
  {
    private int _HeroId;
    private int _SkinId;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int HeroId
    {
      get
      {
        return this._HeroId;
      }
      set
      {
        this._HeroId = value;
      }
    }

    [ProtoMember(2)]
    public int SkinId
    {
      get
      {
        return this._SkinId;
      }
      set
      {
        this._SkinId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
