﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.FavourabilityPerformance
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "FavourabilityPerformance")]
  [Serializable]
  public class FavourabilityPerformance : IExtensible
  {
    private int _FavourabilityLevel;
    private int _PerformanceId;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int FavourabilityLevel
    {
      get
      {
        return this._FavourabilityLevel;
      }
      set
      {
        this._FavourabilityLevel = value;
      }
    }

    [ProtoMember(2)]
    public int PerformanceId
    {
      get
      {
        return this._PerformanceId;
      }
      set
      {
        this._PerformanceId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
