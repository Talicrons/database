﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataArenaOpponentPointZoneInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataArenaOpponentPointZoneInfo")]
  [Serializable]
  public class ConfigDataArenaOpponentPointZoneInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _MinPercent;
    private int _MaxPercent;
    private int _VictoryPoint;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public int MinPercent
    {
      get
      {
        return this._MinPercent;
      }
      set
      {
        this._MinPercent = value;
      }
    }

    [ProtoMember(5)]
    public int MaxPercent
    {
      get
      {
        return this._MaxPercent;
      }
      set
      {
        this._MaxPercent = value;
      }
    }

    [ProtoMember(6)]
    public int VictoryPoint
    {
      get
      {
        return this._VictoryPoint;
      }
      set
      {
        this._VictoryPoint = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
