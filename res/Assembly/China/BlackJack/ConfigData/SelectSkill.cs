﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.SelectSkill
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "SelectSkill")]
  public enum SelectSkill
  {
    [ProtoEnum(Name = "SelectSkill_DefaultSelection", Value = 1)] SelectSkill_DefaultSelection = 1,
    [ProtoEnum(Name = "SelectSkill_DirectReachTargetSkill", Value = 2)] SelectSkill_DirectReachTargetSkill = 2,
    [ProtoEnum(Name = "SelectSkill_ExcludeSkillID", Value = 3)] SelectSkill_ExcludeSkillID = 3,
    [ProtoEnum(Name = "SelectSkill_IncludeSkillID", Value = 4)] SelectSkill_IncludeSkillID = 4,
  }
}
