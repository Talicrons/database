﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataHeroPhantomInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataHeroPhantomInfo")]
  [Serializable]
  public class ConfigDataHeroPhantomInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private List<int> _LevelList;
    private string _OpenDateTime;
    private string _CloseDateTime;
    private string _ShowDateTime;
    private string _HideDateTime;
    private string _Image;
    private string _Model;
    private int _UI_ModelScale;
    private int _UI_ModelOffsetX;
    private int _UI_ModelOffsetY;
    private IExtension extensionObject;
    public List<ConfigDataHeroPhantomLevelInfo> m_levels;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroPhantomInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(5)]
    public List<int> LevelList
    {
      get
      {
        return this._LevelList;
      }
      set
      {
        this._LevelList = value;
      }
    }

    [ProtoMember(6)]
    public string OpenDateTime
    {
      get
      {
        return this._OpenDateTime;
      }
      set
      {
        this._OpenDateTime = value;
      }
    }

    [ProtoMember(7)]
    public string CloseDateTime
    {
      get
      {
        return this._CloseDateTime;
      }
      set
      {
        this._CloseDateTime = value;
      }
    }

    [ProtoMember(8)]
    public string ShowDateTime
    {
      get
      {
        return this._ShowDateTime;
      }
      set
      {
        this._ShowDateTime = value;
      }
    }

    [ProtoMember(9)]
    public string HideDateTime
    {
      get
      {
        return this._HideDateTime;
      }
      set
      {
        this._HideDateTime = value;
      }
    }

    [ProtoMember(10)]
    public string Image
    {
      get
      {
        return this._Image;
      }
      set
      {
        this._Image = value;
      }
    }

    [ProtoMember(11)]
    public string Model
    {
      get
      {
        return this._Model;
      }
      set
      {
        this._Model = value;
      }
    }

    [ProtoMember(12)]
    public int UI_ModelScale
    {
      get
      {
        return this._UI_ModelScale;
      }
      set
      {
        this._UI_ModelScale = value;
      }
    }

    [ProtoMember(13)]
    public int UI_ModelOffsetX
    {
      get
      {
        return this._UI_ModelOffsetX;
      }
      set
      {
        this._UI_ModelOffsetX = value;
      }
    }

    [ProtoMember(14)]
    public int UI_ModelOffsetY
    {
      get
      {
        return this._UI_ModelOffsetY;
      }
      set
      {
        this._UI_ModelOffsetY = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
