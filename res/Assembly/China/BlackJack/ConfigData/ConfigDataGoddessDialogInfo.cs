﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataGoddessDialogInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataGoddessDialogInfo")]
  [Serializable]
  public class ConfigDataGoddessDialogInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _NextDialog_ID;
    private string _PreAnimation;
    private string _PreFacialAnimation;
    private string _IdleAnimation;
    private string _IdleFacialAnimation;
    private string _Voice;
    private string _Words;
    private string _ResetWords;
    private string _FeedbackVoice1;
    private string _FeedbackVoice2;
    private string _FeedbackVoice3;
    private string _FeedbackText1;
    private string _FeedbackText2;
    private string _FeedbackText3;
    private string _Choice1Text;
    private int _Choice1NextDialog_ID;
    private List<ChoiceValue> _Choice1Value;
    private string _Choice2Text;
    private int _Choice2NextDialog_ID;
    private List<ChoiceValue> _Choice2Value;
    private string _Choice3Text;
    private int _Choice3NextDialog_ID;
    private List<ChoiceValue> _Choice3Value;
    private GoddessDialogFuncType _FunctionType;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGoddessDialogInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5)]
    public int NextDialog_ID
    {
      get
      {
        return this._NextDialog_ID;
      }
      set
      {
        this._NextDialog_ID = value;
      }
    }

    [ProtoMember(6)]
    public string PreAnimation
    {
      get
      {
        return this._PreAnimation;
      }
      set
      {
        this._PreAnimation = value;
      }
    }

    [ProtoMember(7)]
    public string PreFacialAnimation
    {
      get
      {
        return this._PreFacialAnimation;
      }
      set
      {
        this._PreFacialAnimation = value;
      }
    }

    [ProtoMember(8)]
    public string IdleAnimation
    {
      get
      {
        return this._IdleAnimation;
      }
      set
      {
        this._IdleAnimation = value;
      }
    }

    [ProtoMember(9)]
    public string IdleFacialAnimation
    {
      get
      {
        return this._IdleFacialAnimation;
      }
      set
      {
        this._IdleFacialAnimation = value;
      }
    }

    [ProtoMember(10)]
    public string Voice
    {
      get
      {
        return this._Voice;
      }
      set
      {
        this._Voice = value;
      }
    }

    [ProtoMember(11)]
    public string Words
    {
      get
      {
        return this._Words;
      }
      set
      {
        this._Words = value;
      }
    }

    [ProtoMember(13)]
    public string ResetWords
    {
      get
      {
        return this._ResetWords;
      }
      set
      {
        this._ResetWords = value;
      }
    }

    [ProtoMember(14)]
    public string FeedbackVoice1
    {
      get
      {
        return this._FeedbackVoice1;
      }
      set
      {
        this._FeedbackVoice1 = value;
      }
    }

    [ProtoMember(15)]
    public string FeedbackVoice2
    {
      get
      {
        return this._FeedbackVoice2;
      }
      set
      {
        this._FeedbackVoice2 = value;
      }
    }

    [ProtoMember(16)]
    public string FeedbackVoice3
    {
      get
      {
        return this._FeedbackVoice3;
      }
      set
      {
        this._FeedbackVoice3 = value;
      }
    }

    [ProtoMember(17)]
    public string FeedbackText1
    {
      get
      {
        return this._FeedbackText1;
      }
      set
      {
        this._FeedbackText1 = value;
      }
    }

    [ProtoMember(18)]
    public string FeedbackText2
    {
      get
      {
        return this._FeedbackText2;
      }
      set
      {
        this._FeedbackText2 = value;
      }
    }

    [ProtoMember(19)]
    public string FeedbackText3
    {
      get
      {
        return this._FeedbackText3;
      }
      set
      {
        this._FeedbackText3 = value;
      }
    }

    [ProtoMember(20)]
    public string Choice1Text
    {
      get
      {
        return this._Choice1Text;
      }
      set
      {
        this._Choice1Text = value;
      }
    }

    [ProtoMember(22)]
    public int Choice1NextDialog_ID
    {
      get
      {
        return this._Choice1NextDialog_ID;
      }
      set
      {
        this._Choice1NextDialog_ID = value;
      }
    }

    [ProtoMember(23)]
    public List<ChoiceValue> Choice1Value
    {
      get
      {
        return this._Choice1Value;
      }
      set
      {
        this._Choice1Value = value;
      }
    }

    [ProtoMember(24)]
    public string Choice2Text
    {
      get
      {
        return this._Choice2Text;
      }
      set
      {
        this._Choice2Text = value;
      }
    }

    [ProtoMember(26)]
    public int Choice2NextDialog_ID
    {
      get
      {
        return this._Choice2NextDialog_ID;
      }
      set
      {
        this._Choice2NextDialog_ID = value;
      }
    }

    [ProtoMember(27)]
    public List<ChoiceValue> Choice2Value
    {
      get
      {
        return this._Choice2Value;
      }
      set
      {
        this._Choice2Value = value;
      }
    }

    [ProtoMember(28)]
    public string Choice3Text
    {
      get
      {
        return this._Choice3Text;
      }
      set
      {
        this._Choice3Text = value;
      }
    }

    [ProtoMember(30)]
    public int Choice3NextDialog_ID
    {
      get
      {
        return this._Choice3NextDialog_ID;
      }
      set
      {
        this._Choice3NextDialog_ID = value;
      }
    }

    [ProtoMember(31)]
    public List<ChoiceValue> Choice3Value
    {
      get
      {
        return this._Choice3Value;
      }
      set
      {
        this._Choice3Value = value;
      }
    }

    [ProtoMember(32)]
    public GoddessDialogFuncType FunctionType
    {
      get
      {
        return this._FunctionType;
      }
      set
      {
        this._FunctionType = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
