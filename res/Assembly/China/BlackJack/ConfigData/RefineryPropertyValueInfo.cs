﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RefineryPropertyValueInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RefineryPropertyValueInfo")]
  [Serializable]
  public class RefineryPropertyValueInfo : IExtensible
  {
    private int _Min;
    private int _Max;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int Min
    {
      get
      {
        return this._Min;
      }
      set
      {
        this._Min = value;
      }
    }

    [ProtoMember(2)]
    public int Max
    {
      get
      {
        return this._Max;
      }
      set
      {
        this._Max = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
