﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ExpressionPurposeType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ExpressionPurposeType")]
  public enum ExpressionPurposeType
  {
    [ProtoEnum(Name = "ExpressionPurposeType_Chat", Value = 1)] ExpressionPurposeType_Chat = 1,
    [ProtoEnum(Name = "ExpressionPurposeType_Combat", Value = 2)] ExpressionPurposeType_Combat = 2,
  }
}
