﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.PeakArenaLossScoreDiff
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "PeakArenaLossScoreDiff")]
  [Serializable]
  public class PeakArenaLossScoreDiff : IExtensible
  {
    private int _ScoreDiff;
    private int _ScoreBonus;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int ScoreDiff
    {
      get
      {
        return this._ScoreDiff;
      }
      set
      {
        this._ScoreDiff = value;
      }
    }

    [ProtoMember(2)]
    public int ScoreBonus
    {
      get
      {
        return this._ScoreBonus;
      }
      set
      {
        this._ScoreBonus = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
