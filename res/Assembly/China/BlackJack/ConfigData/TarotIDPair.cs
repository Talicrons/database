﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.TarotIDPair
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "TarotIDPair")]
  [Serializable]
  public class TarotIDPair : IExtensible
  {
    private int _RaffleID;
    private int _TarotID;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int RaffleID
    {
      get
      {
        return this._RaffleID;
      }
      set
      {
        this._RaffleID = value;
      }
    }

    [ProtoMember(2)]
    public int TarotID
    {
      get
      {
        return this._TarotID;
      }
      set
      {
        this._TarotID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
