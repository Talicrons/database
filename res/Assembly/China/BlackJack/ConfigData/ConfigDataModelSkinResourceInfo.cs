﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataModelSkinResourceInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataModelSkinResourceInfo")]
  [Serializable]
  public class ConfigDataModelSkinResourceInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Model;
    private string _CombatModel;
    private int _ModelScale;
    private int _BF_ModelScale;
    private int _UI_ModelScale;
    private int _UI_ModelOffsetX;
    private int _UI_ModelOffsetY;
    private int _MeleeATK_ID;
    private int _RangeATK_ID;
    private int _Skill_ID;
    private string _MoveSound;
    private IExtension extensionObject;
    public ConfigDataHeroSkinInfo m_heroSkinInfo;
    public ConfigDataSkillInfo m_meleeSkillInfo;
    public ConfigDataSkillInfo m_rangeSkillInfo;
    public ConfigDataSkillInfo m_skinSkillInfo;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string Model
    {
      get
      {
        return this._Model;
      }
      set
      {
        this._Model = value;
      }
    }

    [ProtoMember(5)]
    public string CombatModel
    {
      get
      {
        return this._CombatModel;
      }
      set
      {
        this._CombatModel = value;
      }
    }

    [ProtoMember(6)]
    public int ModelScale
    {
      get
      {
        return this._ModelScale;
      }
      set
      {
        this._ModelScale = value;
      }
    }

    [ProtoMember(7)]
    public int BF_ModelScale
    {
      get
      {
        return this._BF_ModelScale;
      }
      set
      {
        this._BF_ModelScale = value;
      }
    }

    [ProtoMember(8)]
    public int UI_ModelScale
    {
      get
      {
        return this._UI_ModelScale;
      }
      set
      {
        this._UI_ModelScale = value;
      }
    }

    [ProtoMember(9)]
    public int UI_ModelOffsetX
    {
      get
      {
        return this._UI_ModelOffsetX;
      }
      set
      {
        this._UI_ModelOffsetX = value;
      }
    }

    [ProtoMember(10)]
    public int UI_ModelOffsetY
    {
      get
      {
        return this._UI_ModelOffsetY;
      }
      set
      {
        this._UI_ModelOffsetY = value;
      }
    }

    [ProtoMember(11)]
    public int MeleeATK_ID
    {
      get
      {
        return this._MeleeATK_ID;
      }
      set
      {
        this._MeleeATK_ID = value;
      }
    }

    [ProtoMember(12)]
    public int RangeATK_ID
    {
      get
      {
        return this._RangeATK_ID;
      }
      set
      {
        this._RangeATK_ID = value;
      }
    }

    [ProtoMember(13)]
    public int Skill_ID
    {
      get
      {
        return this._Skill_ID;
      }
      set
      {
        this._Skill_ID = value;
      }
    }

    [ProtoMember(14)]
    public string MoveSound
    {
      get
      {
        return this._MoveSound;
      }
      set
      {
        this._MoveSound = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
