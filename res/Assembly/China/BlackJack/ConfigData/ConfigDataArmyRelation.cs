﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataArmyRelation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataArmyRelation")]
  [Serializable]
  public class ConfigDataArmyRelation : IExtensible
  {
    private int _ID;
    private ArmyTag _ArmyTagA;
    private ArmyTag _ArmyTagB;
    private int _Attack;
    private int _Defend;
    private int _Magic;
    private int _MagicDefend;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public ArmyTag ArmyTagA
    {
      get
      {
        return this._ArmyTagA;
      }
      set
      {
        this._ArmyTagA = value;
      }
    }

    [ProtoMember(4)]
    public ArmyTag ArmyTagB
    {
      get
      {
        return this._ArmyTagB;
      }
      set
      {
        this._ArmyTagB = value;
      }
    }

    [ProtoMember(5)]
    public int Attack
    {
      get
      {
        return this._Attack;
      }
      set
      {
        this._Attack = value;
      }
    }

    [ProtoMember(6)]
    public int Defend
    {
      get
      {
        return this._Defend;
      }
      set
      {
        this._Defend = value;
      }
    }

    [ProtoMember(7)]
    public int Magic
    {
      get
      {
        return this._Magic;
      }
      set
      {
        this._Magic = value;
      }
    }

    [ProtoMember(8)]
    public int MagicDefend
    {
      get
      {
        return this._MagicDefend;
      }
      set
      {
        this._MagicDefend = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
