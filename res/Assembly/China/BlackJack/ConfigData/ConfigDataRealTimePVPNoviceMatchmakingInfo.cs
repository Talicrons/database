﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataRealTimePVPNoviceMatchmakingInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataRealTimePVPNoviceMatchmakingInfo")]
  [Serializable]
  public class ConfigDataRealTimePVPNoviceMatchmakingInfo : IExtensible
  {
    private int _ID;
    private int _Battles;
    private int _LevelAdjustment;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public int Battles
    {
      get
      {
        return this._Battles;
      }
      set
      {
        this._Battles = value;
      }
    }

    [ProtoMember(4)]
    public int LevelAdjustment
    {
      get
      {
        return this._LevelAdjustment;
      }
      set
      {
        this._LevelAdjustment = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
