﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RiftLevelType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RiftLevelType")]
  public enum RiftLevelType
  {
    [ProtoEnum(Name = "RiftLevelType_None", Value = 0)] RiftLevelType_None,
    [ProtoEnum(Name = "RiftLevelType_Scenario", Value = 1)] RiftLevelType_Scenario,
    [ProtoEnum(Name = "RiftLevelType_Event", Value = 2)] RiftLevelType_Event,
  }
}
