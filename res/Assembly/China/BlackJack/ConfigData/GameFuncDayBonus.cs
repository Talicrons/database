﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.GameFuncDayBonus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "GameFuncDayBonus")]
  [Serializable]
  public class GameFuncDayBonus : IExtensible
  {
    private GameFunctionType _GameFuncId;
    private int _Nums;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public GameFunctionType GameFuncId
    {
      get
      {
        return this._GameFuncId;
      }
      set
      {
        this._GameFuncId = value;
      }
    }

    [ProtoMember(2)]
    public int Nums
    {
      get
      {
        return this._Nums;
      }
      set
      {
        this._Nums = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
