﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataScenarioInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataScenarioInfo")]
  [Serializable]
  public class ConfigDataScenarioInfo : IExtensible
  {
    private int _ID;
    private string _Chapter;
    private string _Name;
    private string _Desc;
    private int _Waypoint_ID;
    private int _NextScenario_ID;
    private int _MonsterLevel;
    private int _Battle_ID;
    private int _DialogBefore_ID;
    private int _DialogAfter_ID;
    private List<ScenarioInfoUnlockCondition> _UnlockCondition;
    private int _PlayerExpReward;
    private int _HeroExpReward;
    private int _GoldReward;
    private int _EnergySuccess;
    private int _EnergyFail;
    private List<Goods> _FirstReward;
    private int _Drop_ID;
    private int _Achievement1_ID;
    private List<Goods> _AchievementReward1;
    private int _Achievement2_ID;
    private List<Goods> _AchievementReward2;
    private int _Achievement3_ID;
    private List<Goods> _AchievementReward3;
    private string _Strategy;
    private bool _IsOpened;
    private int _TreeNode;
    private int _TreeDialog_ID;
    private string _MapImage;
    private TreeMemoryAgeType _TreeMemoryAge;
    private string _TreeMemoryImage;
    private IExtension extensionObject;
    public ConfigDataWaypointInfo m_waypointInfo;
    public ConfigDataScenarioInfo m_nextScenarioInfo;
    public ConfigDataBattleInfo m_battleInfo;
    public ConfigDataDialogInfo m_dialogInfoBefore;
    public ConfigDataDialogInfo m_dialogInfoAfter;
    public int m_scenarioDepth;
    public ConfigDataRegionInfo m_openRegionInfo;
    public BattleLevelAchievement[] m_achievements;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataScenarioInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Chapter
    {
      get
      {
        return this._Chapter;
      }
      set
      {
        this._Chapter = value;
      }
    }

    [ProtoMember(5)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(7)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(9)]
    public int Waypoint_ID
    {
      get
      {
        return this._Waypoint_ID;
      }
      set
      {
        this._Waypoint_ID = value;
      }
    }

    [ProtoMember(10)]
    public int NextScenario_ID
    {
      get
      {
        return this._NextScenario_ID;
      }
      set
      {
        this._NextScenario_ID = value;
      }
    }

    [ProtoMember(11)]
    public int MonsterLevel
    {
      get
      {
        return this._MonsterLevel;
      }
      set
      {
        this._MonsterLevel = value;
      }
    }

    [ProtoMember(12)]
    public int Battle_ID
    {
      get
      {
        return this._Battle_ID;
      }
      set
      {
        this._Battle_ID = value;
      }
    }

    [ProtoMember(13)]
    public int DialogBefore_ID
    {
      get
      {
        return this._DialogBefore_ID;
      }
      set
      {
        this._DialogBefore_ID = value;
      }
    }

    [ProtoMember(14)]
    public int DialogAfter_ID
    {
      get
      {
        return this._DialogAfter_ID;
      }
      set
      {
        this._DialogAfter_ID = value;
      }
    }

    [ProtoMember(15)]
    public List<ScenarioInfoUnlockCondition> UnlockCondition
    {
      get
      {
        return this._UnlockCondition;
      }
      set
      {
        this._UnlockCondition = value;
      }
    }

    [ProtoMember(16)]
    public int PlayerExpReward
    {
      get
      {
        return this._PlayerExpReward;
      }
      set
      {
        this._PlayerExpReward = value;
      }
    }

    [ProtoMember(17)]
    public int HeroExpReward
    {
      get
      {
        return this._HeroExpReward;
      }
      set
      {
        this._HeroExpReward = value;
      }
    }

    [ProtoMember(18)]
    public int GoldReward
    {
      get
      {
        return this._GoldReward;
      }
      set
      {
        this._GoldReward = value;
      }
    }

    [ProtoMember(19)]
    public int EnergySuccess
    {
      get
      {
        return this._EnergySuccess;
      }
      set
      {
        this._EnergySuccess = value;
      }
    }

    [ProtoMember(20)]
    public int EnergyFail
    {
      get
      {
        return this._EnergyFail;
      }
      set
      {
        this._EnergyFail = value;
      }
    }

    [ProtoMember(21)]
    public List<Goods> FirstReward
    {
      get
      {
        return this._FirstReward;
      }
      set
      {
        this._FirstReward = value;
      }
    }

    [ProtoMember(22)]
    public int Drop_ID
    {
      get
      {
        return this._Drop_ID;
      }
      set
      {
        this._Drop_ID = value;
      }
    }

    [ProtoMember(23)]
    public int Achievement1_ID
    {
      get
      {
        return this._Achievement1_ID;
      }
      set
      {
        this._Achievement1_ID = value;
      }
    }

    [ProtoMember(24)]
    public List<Goods> AchievementReward1
    {
      get
      {
        return this._AchievementReward1;
      }
      set
      {
        this._AchievementReward1 = value;
      }
    }

    [ProtoMember(25)]
    public int Achievement2_ID
    {
      get
      {
        return this._Achievement2_ID;
      }
      set
      {
        this._Achievement2_ID = value;
      }
    }

    [ProtoMember(26)]
    public List<Goods> AchievementReward2
    {
      get
      {
        return this._AchievementReward2;
      }
      set
      {
        this._AchievementReward2 = value;
      }
    }

    [ProtoMember(27)]
    public int Achievement3_ID
    {
      get
      {
        return this._Achievement3_ID;
      }
      set
      {
        this._Achievement3_ID = value;
      }
    }

    [ProtoMember(28)]
    public List<Goods> AchievementReward3
    {
      get
      {
        return this._AchievementReward3;
      }
      set
      {
        this._AchievementReward3 = value;
      }
    }

    [ProtoMember(29)]
    public string Strategy
    {
      get
      {
        return this._Strategy;
      }
      set
      {
        this._Strategy = value;
      }
    }

    [ProtoMember(30)]
    public bool IsOpened
    {
      get
      {
        return this._IsOpened;
      }
      set
      {
        this._IsOpened = value;
      }
    }

    [ProtoMember(31)]
    public int TreeNode
    {
      get
      {
        return this._TreeNode;
      }
      set
      {
        this._TreeNode = value;
      }
    }

    [ProtoMember(32)]
    public int TreeDialog_ID
    {
      get
      {
        return this._TreeDialog_ID;
      }
      set
      {
        this._TreeDialog_ID = value;
      }
    }

    [ProtoMember(33)]
    public string MapImage
    {
      get
      {
        return this._MapImage;
      }
      set
      {
        this._MapImage = value;
      }
    }

    [ProtoMember(34)]
    public TreeMemoryAgeType TreeMemoryAge
    {
      get
      {
        return this._TreeMemoryAge;
      }
      set
      {
        this._TreeMemoryAge = value;
      }
    }

    [ProtoMember(35)]
    public string TreeMemoryImage
    {
      get
      {
        return this._TreeMemoryImage;
      }
      set
      {
        this._TreeMemoryImage = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
