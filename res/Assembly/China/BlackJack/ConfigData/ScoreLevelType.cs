﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ScoreLevelType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ScoreLevelType")]
  public enum ScoreLevelType
  {
    [ProtoEnum(Name = "ScoreLevelType_None", Value = 0)] ScoreLevelType_None,
    [ProtoEnum(Name = "ScoreLevelType_Scenario", Value = 1)] ScoreLevelType_Scenario,
    [ProtoEnum(Name = "ScoreLevelType_Challenge", Value = 2)] ScoreLevelType_Challenge,
  }
}
