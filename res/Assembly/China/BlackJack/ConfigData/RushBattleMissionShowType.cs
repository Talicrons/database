﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RushBattleMissionShowType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RushBattleMissionShowType")]
  public enum RushBattleMissionShowType
  {
    [ProtoEnum(Name = "RushBattleMissionShowType_Period", Value = 1)] RushBattleMissionShowType_Period = 1,
    [ProtoEnum(Name = "RushBattleMissionShowType_Achievement", Value = 2)] RushBattleMissionShowType_Achievement = 2,
  }
}
