﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataPeakArenaDanRewardSettleTimeInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataPeakArenaDanRewardSettleTimeInfo")]
  [Serializable]
  public class ConfigDataPeakArenaDanRewardSettleTimeInfo : IExtensible
  {
    private int _ID;
    private string _Desc;
    private int _SettleStartTime;
    private int _SettleEndTime;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(4)]
    public int SettleStartTime
    {
      get
      {
        return this._SettleStartTime;
      }
      set
      {
        this._SettleStartTime = value;
      }
    }

    [ProtoMember(5)]
    public int SettleEndTime
    {
      get
      {
        return this._SettleEndTime;
      }
      set
      {
        this._SettleEndTime = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
