﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataHeroAssistantTaskScheduleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataHeroAssistantTaskScheduleInfo")]
  [Serializable]
  public class ConfigDataHeroAssistantTaskScheduleInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private List<string> _Resource;
    private List<int> _TaskIDs;
    private int _Weekday;
    private List<int> _RecommendHeroes;
    private List<int> _SoldierTypeList;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroAssistantTaskScheduleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public List<string> Resource
    {
      get
      {
        return this._Resource;
      }
      set
      {
        this._Resource = value;
      }
    }

    [ProtoMember(5)]
    public List<int> TaskIDs
    {
      get
      {
        return this._TaskIDs;
      }
      set
      {
        this._TaskIDs = value;
      }
    }

    [ProtoMember(6)]
    public int Weekday
    {
      get
      {
        return this._Weekday;
      }
      set
      {
        this._Weekday = value;
      }
    }

    [ProtoMember(7)]
    public List<int> RecommendHeroes
    {
      get
      {
        return this._RecommendHeroes;
      }
      set
      {
        this._RecommendHeroes = value;
      }
    }

    [ProtoMember(8)]
    public List<int> SoldierTypeList
    {
      get
      {
        return this._SoldierTypeList;
      }
      set
      {
        this._SoldierTypeList = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
