﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataUserGuideDialogInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataUserGuideDialogInfo")]
  [Serializable]
  public class ConfigDataUserGuideDialogInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _NextDialog_ID;
    private int _FrameType;
    private string _CharName;
    private int _CharImage_ID;
    private int _Position;
    private int _EnterType;
    private string _PreAnimation;
    private string _PreFacialAnimation;
    private string _IdleAnimation;
    private string _IdleFacialAnimation;
    private string _Voice;
    private string _Words;
    private string _WordsKey;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5)]
    public int NextDialog_ID
    {
      get
      {
        return this._NextDialog_ID;
      }
      set
      {
        this._NextDialog_ID = value;
      }
    }

    [ProtoMember(6)]
    public int FrameType
    {
      get
      {
        return this._FrameType;
      }
      set
      {
        this._FrameType = value;
      }
    }

    [ProtoMember(7)]
    public string CharName
    {
      get
      {
        return this._CharName;
      }
      set
      {
        this._CharName = value;
      }
    }

    [ProtoMember(8)]
    public int CharImage_ID
    {
      get
      {
        return this._CharImage_ID;
      }
      set
      {
        this._CharImage_ID = value;
      }
    }

    [ProtoMember(9)]
    public int Position
    {
      get
      {
        return this._Position;
      }
      set
      {
        this._Position = value;
      }
    }

    [ProtoMember(10)]
    public int EnterType
    {
      get
      {
        return this._EnterType;
      }
      set
      {
        this._EnterType = value;
      }
    }

    [ProtoMember(11)]
    public string PreAnimation
    {
      get
      {
        return this._PreAnimation;
      }
      set
      {
        this._PreAnimation = value;
      }
    }

    [ProtoMember(12)]
    public string PreFacialAnimation
    {
      get
      {
        return this._PreFacialAnimation;
      }
      set
      {
        this._PreFacialAnimation = value;
      }
    }

    [ProtoMember(13)]
    public string IdleAnimation
    {
      get
      {
        return this._IdleAnimation;
      }
      set
      {
        this._IdleAnimation = value;
      }
    }

    [ProtoMember(14)]
    public string IdleFacialAnimation
    {
      get
      {
        return this._IdleFacialAnimation;
      }
      set
      {
        this._IdleFacialAnimation = value;
      }
    }

    [ProtoMember(15)]
    public string Voice
    {
      get
      {
        return this._Voice;
      }
      set
      {
        this._Voice = value;
      }
    }

    [ProtoMember(16)]
    public string Words
    {
      get
      {
        return this._Words;
      }
      set
      {
        this._Words = value;
      }
    }

    [ProtoMember(17)]
    public string WordsKey
    {
      get
      {
        return this._WordsKey;
      }
      set
      {
        this._WordsKey = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
