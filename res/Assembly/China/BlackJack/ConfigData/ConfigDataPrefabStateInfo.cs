﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataPrefabStateInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataPrefabStateInfo")]
  [Serializable]
  public class ConfigDataPrefabStateInfo : IExtensible
  {
    private int _ID;
    private string _Prefab;
    private string _State;
    private bool _IsLoop;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Prefab
    {
      get
      {
        return this._Prefab;
      }
      set
      {
        this._Prefab = value;
      }
    }

    [ProtoMember(4)]
    public string State
    {
      get
      {
        return this._State;
      }
      set
      {
        this._State = value;
      }
    }

    [ProtoMember(5)]
    public bool IsLoop
    {
      get
      {
        return this._IsLoop;
      }
      set
      {
        this._IsLoop = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
