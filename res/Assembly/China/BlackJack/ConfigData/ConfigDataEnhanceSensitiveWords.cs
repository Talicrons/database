﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataEnhanceSensitiveWords
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataEnhanceSensitiveWords")]
  [Serializable]
  public class ConfigDataEnhanceSensitiveWords : IExtensible
  {
    private int _ID;
    private string _Text;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Text
    {
      get
      {
        return this._Text;
      }
      set
      {
        this._Text = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
