﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataTerrainInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataTerrainInfo")]
  [Serializable]
  public class ConfigDataTerrainInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _MovePoint_Ride;
    private int _MovePoint_Walk;
    private int _MovePoint_Water;
    private int _MovePoint_Fly;
    private int _MovePoint_FieldArmy;
    private int _BattleBonus;
    private int _Damage;
    private List<int> _TerrainTags;
    private string _Fx;
    private string _InfoImage;
    private string _MapTileImage;
    private string _Background;
    private int _ColorR;
    private int _ColorG;
    private int _ColorB;
    private string _Desc;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTerrainInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5)]
    public int MovePoint_Ride
    {
      get
      {
        return this._MovePoint_Ride;
      }
      set
      {
        this._MovePoint_Ride = value;
      }
    }

    [ProtoMember(6)]
    public int MovePoint_Walk
    {
      get
      {
        return this._MovePoint_Walk;
      }
      set
      {
        this._MovePoint_Walk = value;
      }
    }

    [ProtoMember(7)]
    public int MovePoint_Water
    {
      get
      {
        return this._MovePoint_Water;
      }
      set
      {
        this._MovePoint_Water = value;
      }
    }

    [ProtoMember(8)]
    public int MovePoint_Fly
    {
      get
      {
        return this._MovePoint_Fly;
      }
      set
      {
        this._MovePoint_Fly = value;
      }
    }

    [ProtoMember(9)]
    public int MovePoint_FieldArmy
    {
      get
      {
        return this._MovePoint_FieldArmy;
      }
      set
      {
        this._MovePoint_FieldArmy = value;
      }
    }

    [ProtoMember(10)]
    public int BattleBonus
    {
      get
      {
        return this._BattleBonus;
      }
      set
      {
        this._BattleBonus = value;
      }
    }

    [ProtoMember(11)]
    public int Damage
    {
      get
      {
        return this._Damage;
      }
      set
      {
        this._Damage = value;
      }
    }

    [ProtoMember(12)]
    public List<int> TerrainTags
    {
      get
      {
        return this._TerrainTags;
      }
      set
      {
        this._TerrainTags = value;
      }
    }

    [ProtoMember(13)]
    public string Fx
    {
      get
      {
        return this._Fx;
      }
      set
      {
        this._Fx = value;
      }
    }

    [ProtoMember(14)]
    public string InfoImage
    {
      get
      {
        return this._InfoImage;
      }
      set
      {
        this._InfoImage = value;
      }
    }

    [ProtoMember(15)]
    public string MapTileImage
    {
      get
      {
        return this._MapTileImage;
      }
      set
      {
        this._MapTileImage = value;
      }
    }

    [ProtoMember(16)]
    public string Background
    {
      get
      {
        return this._Background;
      }
      set
      {
        this._Background = value;
      }
    }

    [ProtoMember(17)]
    public int ColorR
    {
      get
      {
        return this._ColorR;
      }
      set
      {
        this._ColorR = value;
      }
    }

    [ProtoMember(18)]
    public int ColorG
    {
      get
      {
        return this._ColorG;
      }
      set
      {
        this._ColorG = value;
      }
    }

    [ProtoMember(19)]
    public int ColorB
    {
      get
      {
        return this._ColorB;
      }
      set
      {
        this._ColorB = value;
      }
    }

    [ProtoMember(20)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
