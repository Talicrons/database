﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataRushBattleMissionInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataRushBattleMissionInfo")]
  [Serializable]
  public class ConfigDataRushBattleMissionInfo : IExtensible
  {
    private int _ID;
    private string _Title;
    private string _Desc;
    private RushBattleMissionPeriodType _RushBattleMissionPeriodType;
    private RushBattleMissionShowType _RushBattleMissionShowType;
    private int _RushBattleActivityId;
    private RushBattleMissionType _RushBattleMissionType;
    private int _Param1;
    private int _Param2;
    private int _Param3;
    private int _Param4;
    private List<int> _Param5;
    private List<Goods> _Reward;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRushBattleMissionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Title
    {
      get
      {
        return this._Title;
      }
      set
      {
        this._Title = value;
      }
    }

    [ProtoMember(4)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(5)]
    public RushBattleMissionPeriodType RushBattleMissionPeriodType
    {
      get
      {
        return this._RushBattleMissionPeriodType;
      }
      set
      {
        this._RushBattleMissionPeriodType = value;
      }
    }

    [ProtoMember(6)]
    public RushBattleMissionShowType RushBattleMissionShowType
    {
      get
      {
        return this._RushBattleMissionShowType;
      }
      set
      {
        this._RushBattleMissionShowType = value;
      }
    }

    [ProtoMember(7)]
    public int RushBattleActivityId
    {
      get
      {
        return this._RushBattleActivityId;
      }
      set
      {
        this._RushBattleActivityId = value;
      }
    }

    [ProtoMember(8)]
    public RushBattleMissionType RushBattleMissionType
    {
      get
      {
        return this._RushBattleMissionType;
      }
      set
      {
        this._RushBattleMissionType = value;
      }
    }

    [ProtoMember(9)]
    public int Param1
    {
      get
      {
        return this._Param1;
      }
      set
      {
        this._Param1 = value;
      }
    }

    [ProtoMember(10)]
    public int Param2
    {
      get
      {
        return this._Param2;
      }
      set
      {
        this._Param2 = value;
      }
    }

    [ProtoMember(11)]
    public int Param3
    {
      get
      {
        return this._Param3;
      }
      set
      {
        this._Param3 = value;
      }
    }

    [ProtoMember(12)]
    public int Param4
    {
      get
      {
        return this._Param4;
      }
      set
      {
        this._Param4 = value;
      }
    }

    [ProtoMember(13)]
    public List<int> Param5
    {
      get
      {
        return this._Param5;
      }
      set
      {
        this._Param5 = value;
      }
    }

    [ProtoMember(14)]
    public List<Goods> Reward
    {
      get
      {
        return this._Reward;
      }
      set
      {
        this._Reward = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
