﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataWorldMapInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataWorldMapInfo")]
  [Serializable]
  public class ConfigDataWorldMapInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private List<int> _Regions_ID;
    private int _StartWaypoint_ID;
    private string _WorldMap;
    private int _BackgroundWidth;
    private int _BackgroundHeight;
    private string _Music;
    private IExtension extensionObject;
    public ConfigDataRegionInfo[] m_regionInfos;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataWorldMapInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(6)]
    public List<int> Regions_ID
    {
      get
      {
        return this._Regions_ID;
      }
      set
      {
        this._Regions_ID = value;
      }
    }

    [ProtoMember(8)]
    public int StartWaypoint_ID
    {
      get
      {
        return this._StartWaypoint_ID;
      }
      set
      {
        this._StartWaypoint_ID = value;
      }
    }

    [ProtoMember(9)]
    public string WorldMap
    {
      get
      {
        return this._WorldMap;
      }
      set
      {
        this._WorldMap = value;
      }
    }

    [ProtoMember(10)]
    public int BackgroundWidth
    {
      get
      {
        return this._BackgroundWidth;
      }
      set
      {
        this._BackgroundWidth = value;
      }
    }

    [ProtoMember(11)]
    public int BackgroundHeight
    {
      get
      {
        return this._BackgroundHeight;
      }
      set
      {
        this._BackgroundHeight = value;
      }
    }

    [ProtoMember(12)]
    public string Music
    {
      get
      {
        return this._Music;
      }
      set
      {
        this._Music = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
