﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataRefineryStonePropertyTemplateInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataRefineryStonePropertyTemplateInfo")]
  [Serializable]
  public class ConfigDataRefineryStonePropertyTemplateInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private PropertyModifyType _Property1_ID;
    private List<RefineryPropertyValueInfo> _ValueRange1;
    private PropertyModifyType _Property2_ID;
    private List<RefineryPropertyValueInfo> _ValueRange2;
    private PropertyModifyType _Property3_ID;
    private List<RefineryPropertyValueInfo> _ValueRange3;
    private PropertyModifyType _Property4_ID;
    private List<RefineryPropertyValueInfo> _ValueRange4;
    private PropertyModifyType _Property5_ID;
    private List<RefineryPropertyValueInfo> _ValueRange5;
    private PropertyModifyType _Property6_ID;
    private List<RefineryPropertyValueInfo> _ValueRange6;
    private PropertyModifyType _Property7_ID;
    private List<RefineryPropertyValueInfo> _ValueRange7;
    private PropertyModifyType _Property8_ID;
    private List<RefineryPropertyValueInfo> _ValueRange8;
    private PropertyModifyType _Property9_ID;
    private List<RefineryPropertyValueInfo> _ValueRange9;
    private PropertyModifyType _Property10_ID;
    private List<RefineryPropertyValueInfo> _ValueRange10;
    private IExtension extensionObject;
    public Dictionary<PropertyModifyType, List<RefineryPropertyValueInfo>> PropertyValues;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRefineryStonePropertyTemplateInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public PropertyModifyType Property1_ID
    {
      get
      {
        return this._Property1_ID;
      }
      set
      {
        this._Property1_ID = value;
      }
    }

    [ProtoMember(5)]
    public List<RefineryPropertyValueInfo> ValueRange1
    {
      get
      {
        return this._ValueRange1;
      }
      set
      {
        this._ValueRange1 = value;
      }
    }

    [ProtoMember(6)]
    public PropertyModifyType Property2_ID
    {
      get
      {
        return this._Property2_ID;
      }
      set
      {
        this._Property2_ID = value;
      }
    }

    [ProtoMember(7)]
    public List<RefineryPropertyValueInfo> ValueRange2
    {
      get
      {
        return this._ValueRange2;
      }
      set
      {
        this._ValueRange2 = value;
      }
    }

    [ProtoMember(8)]
    public PropertyModifyType Property3_ID
    {
      get
      {
        return this._Property3_ID;
      }
      set
      {
        this._Property3_ID = value;
      }
    }

    [ProtoMember(9)]
    public List<RefineryPropertyValueInfo> ValueRange3
    {
      get
      {
        return this._ValueRange3;
      }
      set
      {
        this._ValueRange3 = value;
      }
    }

    [ProtoMember(10)]
    public PropertyModifyType Property4_ID
    {
      get
      {
        return this._Property4_ID;
      }
      set
      {
        this._Property4_ID = value;
      }
    }

    [ProtoMember(11)]
    public List<RefineryPropertyValueInfo> ValueRange4
    {
      get
      {
        return this._ValueRange4;
      }
      set
      {
        this._ValueRange4 = value;
      }
    }

    [ProtoMember(12)]
    public PropertyModifyType Property5_ID
    {
      get
      {
        return this._Property5_ID;
      }
      set
      {
        this._Property5_ID = value;
      }
    }

    [ProtoMember(13)]
    public List<RefineryPropertyValueInfo> ValueRange5
    {
      get
      {
        return this._ValueRange5;
      }
      set
      {
        this._ValueRange5 = value;
      }
    }

    [ProtoMember(14)]
    public PropertyModifyType Property6_ID
    {
      get
      {
        return this._Property6_ID;
      }
      set
      {
        this._Property6_ID = value;
      }
    }

    [ProtoMember(15)]
    public List<RefineryPropertyValueInfo> ValueRange6
    {
      get
      {
        return this._ValueRange6;
      }
      set
      {
        this._ValueRange6 = value;
      }
    }

    [ProtoMember(16)]
    public PropertyModifyType Property7_ID
    {
      get
      {
        return this._Property7_ID;
      }
      set
      {
        this._Property7_ID = value;
      }
    }

    [ProtoMember(17)]
    public List<RefineryPropertyValueInfo> ValueRange7
    {
      get
      {
        return this._ValueRange7;
      }
      set
      {
        this._ValueRange7 = value;
      }
    }

    [ProtoMember(18)]
    public PropertyModifyType Property8_ID
    {
      get
      {
        return this._Property8_ID;
      }
      set
      {
        this._Property8_ID = value;
      }
    }

    [ProtoMember(19)]
    public List<RefineryPropertyValueInfo> ValueRange8
    {
      get
      {
        return this._ValueRange8;
      }
      set
      {
        this._ValueRange8 = value;
      }
    }

    [ProtoMember(20)]
    public PropertyModifyType Property9_ID
    {
      get
      {
        return this._Property9_ID;
      }
      set
      {
        this._Property9_ID = value;
      }
    }

    [ProtoMember(21)]
    public List<RefineryPropertyValueInfo> ValueRange9
    {
      get
      {
        return this._ValueRange9;
      }
      set
      {
        this._ValueRange9 = value;
      }
    }

    [ProtoMember(22)]
    public PropertyModifyType Property10_ID
    {
      get
      {
        return this._Property10_ID;
      }
      set
      {
        this._Property10_ID = value;
      }
    }

    [ProtoMember(23)]
    public List<RefineryPropertyValueInfo> ValueRange10
    {
      get
      {
        return this._ValueRange10;
      }
      set
      {
        this._ValueRange10 = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
