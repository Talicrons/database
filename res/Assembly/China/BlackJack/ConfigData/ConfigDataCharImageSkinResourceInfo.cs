﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataCharImageSkinResourceInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataCharImageSkinResourceInfo")]
  [Serializable]
  public class ConfigDataCharImageSkinResourceInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Image;
    private string _SpineAssetPath;
    private IExtension extensionObject;
    public ConfigDataHeroSkinInfo m_heroSkinInfo;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string Image
    {
      get
      {
        return this._Image;
      }
      set
      {
        this._Image = value;
      }
    }

    [ProtoMember(5)]
    public string SpineAssetPath
    {
      get
      {
        return this._SpineAssetPath;
      }
      set
      {
        this._SpineAssetPath = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
