﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CardSelectType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "CardSelectType")]
  public enum CardSelectType
  {
    [ProtoEnum(Name = "CardSelectType_SingleSelect", Value = 1)] CardSelectType_SingleSelect = 1,
    [ProtoEnum(Name = "CardSelectType_TenSelect", Value = 2)] CardSelectType_TenSelect = 2,
    [ProtoEnum(Name = "CardSelectType_BothSelect", Value = 3)] CardSelectType_BothSelect = 3,
  }
}
