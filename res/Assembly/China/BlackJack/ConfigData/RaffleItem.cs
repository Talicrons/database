﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RaffleItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RaffleItem")]
  [Serializable]
  public class RaffleItem : IExtensible
  {
    private int _RaffleID;
    private int _RaffleLevel;
    private GoodsType _GoodsType;
    private int _ItemID;
    private int _ItemCount;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int RaffleID
    {
      get
      {
        return this._RaffleID;
      }
      set
      {
        this._RaffleID = value;
      }
    }

    [ProtoMember(2)]
    public int RaffleLevel
    {
      get
      {
        return this._RaffleLevel;
      }
      set
      {
        this._RaffleLevel = value;
      }
    }

    [ProtoMember(3)]
    public GoodsType GoodsType
    {
      get
      {
        return this._GoodsType;
      }
      set
      {
        this._GoodsType = value;
      }
    }

    [ProtoMember(4)]
    public int ItemID
    {
      get
      {
        return this._ItemID;
      }
      set
      {
        this._ItemID = value;
      }
    }

    [ProtoMember(5)]
    public int ItemCount
    {
      get
      {
        return this._ItemCount;
      }
      set
      {
        this._ItemCount = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
