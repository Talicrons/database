﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.SkillType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "SkillType")]
  public enum SkillType
  {
    [ProtoEnum(Name = "SkillType_None", Value = 0)] SkillType_None,
    [ProtoEnum(Name = "SkillType_Slash", Value = 1)] SkillType_Slash,
    [ProtoEnum(Name = "SkillType_Shoot", Value = 2)] SkillType_Shoot,
    [ProtoEnum(Name = "SkillType_Charge", Value = 3)] SkillType_Charge,
    [ProtoEnum(Name = "SkillType_MagicDamage", Value = 4)] SkillType_MagicDamage,
    [ProtoEnum(Name = "SkillType_BF_Heal", Value = 5)] SkillType_BF_Heal,
    [ProtoEnum(Name = "SkillType_BF_HealRemoveCD", Value = 6)] SkillType_BF_HealRemoveCD,
    [ProtoEnum(Name = "SkillType_BF_HealNewTurn", Value = 7)] SkillType_BF_HealNewTurn,
    [ProtoEnum(Name = "SkillType_BF_Teleport", Value = 8)] SkillType_BF_Teleport,
    [ProtoEnum(Name = "SkillType_BF_Summon", Value = 9)] SkillType_BF_Summon,
    [ProtoEnum(Name = "SkillType_BF_MagicDamage", Value = 10)] SkillType_BF_MagicDamage,
    [ProtoEnum(Name = "SkillType_Passive", Value = 11)] SkillType_Passive,
    [ProtoEnum(Name = "SkillType_BF_DamageHeal", Value = 12)] SkillType_BF_DamageHeal,
    [ProtoEnum(Name = "SkillType_BF_HealPercent", Value = 13)] SkillType_BF_HealPercent,
    [ProtoEnum(Name = "SkillType_BF_AddBuff", Value = 14)] SkillType_BF_AddBuff,
    [ProtoEnum(Name = "SkillType_BF_TeleportAOE", Value = 15)] SkillType_BF_TeleportAOE,
    [ProtoEnum(Name = "SkillType_BF_HealNewTurn2", Value = 16)] SkillType_BF_HealNewTurn2,
    [ProtoEnum(Name = "SkillType_BF_SuperMagicDamage", Value = 17)] SkillType_BF_SuperMagicDamage,
    [ProtoEnum(Name = "SkillType_BF_DamageSummon", Value = 18)] SkillType_BF_DamageSummon,
    [ProtoEnum(Name = "SkillType_BF_TeleportAOE2", Value = 19)] SkillType_BF_TeleportAOE2,
    [ProtoEnum(Name = "SkillType_BF_DamageTeleport", Value = 20)] SkillType_BF_DamageTeleport,
    [ProtoEnum(Name = "SkillType_BF_DamageTeleport2", Value = 21)] SkillType_BF_DamageTeleport2,
    [ProtoEnum(Name = "SkillType_BF_HealMagicDamage", Value = 22)] SkillType_BF_HealMagicDamage,
    [ProtoEnum(Name = "SkillType_BF_DamageSummon2", Value = 23)] SkillType_BF_DamageSummon2,
    [ProtoEnum(Name = "SkillType_BF_HealRemoveCD2", Value = 24)] SkillType_BF_HealRemoveCD2,
    [ProtoEnum(Name = "SkillType_BF_Heal2", Value = 25)] SkillType_BF_Heal2,
    [ProtoEnum(Name = "SkillType_BF_KillSummon", Value = 26)] SkillType_BF_KillSummon,
    [ProtoEnum(Name = "SkillType_BF_KillSummonTeleport", Value = 27)] SkillType_BF_KillSummonTeleport,
    [ProtoEnum(Name = "SkillType_BF_DieBFSkill", Value = 28)] SkillType_BF_DieBFSkill,
    [ProtoEnum(Name = "SkillType_BF_TeleportAOE3", Value = 29)] SkillType_BF_TeleportAOE3,
    [ProtoEnum(Name = "SkillType_BF_TeleportAOE4", Value = 30)] SkillType_BF_TeleportAOE4,
    [ProtoEnum(Name = "SkillType_BF_Teleport2", Value = 31)] SkillType_BF_Teleport2,
  }
}
