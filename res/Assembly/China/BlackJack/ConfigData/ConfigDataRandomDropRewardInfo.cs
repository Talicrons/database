﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataRandomDropRewardInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataRandomDropRewardInfo")]
  [Serializable]
  public class ConfigDataRandomDropRewardInfo : IExtensible
  {
    private int _ID;
    private int _DropID;
    private int _GroupIndex;
    private int _DropCount;
    private List<WeightGoods> _DropRewards;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRandomDropRewardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public int DropID
    {
      get
      {
        return this._DropID;
      }
      set
      {
        this._DropID = value;
      }
    }

    [ProtoMember(4)]
    public int GroupIndex
    {
      get
      {
        return this._GroupIndex;
      }
      set
      {
        this._GroupIndex = value;
      }
    }

    [ProtoMember(5)]
    public int DropCount
    {
      get
      {
        return this._DropCount;
      }
      set
      {
        this._DropCount = value;
      }
    }

    [ProtoMember(6)]
    public List<WeightGoods> DropRewards
    {
      get
      {
        return this._DropRewards;
      }
      set
      {
        this._DropRewards = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
