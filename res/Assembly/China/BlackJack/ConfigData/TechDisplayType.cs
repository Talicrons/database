﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.TechDisplayType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "TechDisplayType")]
  public enum TechDisplayType
  {
    [ProtoEnum(Name = "TechDisplayType_None", Value = 0)] TechDisplayType_None,
    [ProtoEnum(Name = "TechDisplayType_SoldierTypeLevelUp", Value = 1)] TechDisplayType_SoldierTypeLevelUp,
    [ProtoEnum(Name = "TechDisplayType_SoldierLevelUp", Value = 2)] TechDisplayType_SoldierLevelUp,
    [ProtoEnum(Name = "TechDisplayType_SkillLevelUp", Value = 3)] TechDisplayType_SkillLevelUp,
  }
}
