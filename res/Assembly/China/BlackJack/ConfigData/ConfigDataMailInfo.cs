﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataMailInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataMailInfo")]
  [Serializable]
  public class ConfigDataMailInfo : IExtensible
  {
    private int _ID;
    private string _Title;
    private string _Content;
    private List<Goods> _Attachments;
    private uint _ExpiredTime;
    private int _ReadedExpiredTime;
    private bool _GotDeleted;
    private GameFunctionType _GameFunctionType;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataMailInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Title
    {
      get
      {
        return this._Title;
      }
      set
      {
        this._Title = value;
      }
    }

    [ProtoMember(4)]
    public string Content
    {
      get
      {
        return this._Content;
      }
      set
      {
        this._Content = value;
      }
    }

    [ProtoMember(5)]
    public List<Goods> Attachments
    {
      get
      {
        return this._Attachments;
      }
      set
      {
        this._Attachments = value;
      }
    }

    [ProtoMember(6)]
    public uint ExpiredTime
    {
      get
      {
        return this._ExpiredTime;
      }
      set
      {
        this._ExpiredTime = value;
      }
    }

    [ProtoMember(7)]
    public int ReadedExpiredTime
    {
      get
      {
        return this._ReadedExpiredTime;
      }
      set
      {
        this._ReadedExpiredTime = value;
      }
    }

    [ProtoMember(8)]
    public bool GotDeleted
    {
      get
      {
        return this._GotDeleted;
      }
      set
      {
        this._GotDeleted = value;
      }
    }

    [ProtoMember(9)]
    public GameFunctionType GameFunctionType
    {
      get
      {
        return this._GameFunctionType;
      }
      set
      {
        this._GameFunctionType = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
