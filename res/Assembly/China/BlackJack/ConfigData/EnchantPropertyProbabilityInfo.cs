﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.EnchantPropertyProbabilityInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using System.Collections.Generic;

namespace BlackJack.ConfigData
{
  public class EnchantPropertyProbabilityInfo
  {
    public PropertyModifyType Id { get; set; }

    public List<EnchantPropertyValueInfo> PropertyValues { get; set; }

    public int Weight { get; set; }
  }
}
