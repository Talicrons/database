﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RandomTalent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RandomTalent")]
  [Serializable]
  public class RandomTalent : IExtensible
  {
    private int _SkillId;
    private int _Weight;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int SkillId
    {
      get
      {
        return this._SkillId;
      }
      set
      {
        this._SkillId = value;
      }
    }

    [ProtoMember(2)]
    public int Weight
    {
      get
      {
        return this._Weight;
      }
      set
      {
        this._Weight = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
