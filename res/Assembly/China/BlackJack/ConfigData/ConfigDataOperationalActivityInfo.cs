﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataOperationalActivityInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataOperationalActivityInfo")]
  [Serializable]
  public class ConfigDataOperationalActivityInfo : IExtensible
  {
    private int _ID;
    private string _Title;
    private string _Content;
    private OperationalActivityType _ActivityType;
    private List<ActivityParam> _OperationalActivityParms;
    private int _SkipPageID;
    private int _SortID;
    private string _AdvertisingImage;
    private string _AdvertisingImage2;
    private string _Desc;
    private int _DaysAfterServerOpen;
    private int _DaysAfterPlayerCreated;
    private int _NoEffectLastDays;
    private int _AfterRefluxOpenLastDays;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataOperationalActivityInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Title
    {
      get
      {
        return this._Title;
      }
      set
      {
        this._Title = value;
      }
    }

    [ProtoMember(4)]
    public string Content
    {
      get
      {
        return this._Content;
      }
      set
      {
        this._Content = value;
      }
    }

    [ProtoMember(5)]
    public OperationalActivityType ActivityType
    {
      get
      {
        return this._ActivityType;
      }
      set
      {
        this._ActivityType = value;
      }
    }

    [ProtoMember(6)]
    public List<ActivityParam> OperationalActivityParms
    {
      get
      {
        return this._OperationalActivityParms;
      }
      set
      {
        this._OperationalActivityParms = value;
      }
    }

    [ProtoMember(7)]
    public int SkipPageID
    {
      get
      {
        return this._SkipPageID;
      }
      set
      {
        this._SkipPageID = value;
      }
    }

    [ProtoMember(8)]
    public int SortID
    {
      get
      {
        return this._SortID;
      }
      set
      {
        this._SortID = value;
      }
    }

    [ProtoMember(9)]
    public string AdvertisingImage
    {
      get
      {
        return this._AdvertisingImage;
      }
      set
      {
        this._AdvertisingImage = value;
      }
    }

    [ProtoMember(10)]
    public string AdvertisingImage2
    {
      get
      {
        return this._AdvertisingImage2;
      }
      set
      {
        this._AdvertisingImage2 = value;
      }
    }

    [ProtoMember(11)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(12)]
    public int DaysAfterServerOpen
    {
      get
      {
        return this._DaysAfterServerOpen;
      }
      set
      {
        this._DaysAfterServerOpen = value;
      }
    }

    [ProtoMember(13)]
    public int DaysAfterPlayerCreated
    {
      get
      {
        return this._DaysAfterPlayerCreated;
      }
      set
      {
        this._DaysAfterPlayerCreated = value;
      }
    }

    [ProtoMember(14)]
    public int NoEffectLastDays
    {
      get
      {
        return this._NoEffectLastDays;
      }
      set
      {
        this._NoEffectLastDays = value;
      }
    }

    [ProtoMember(15)]
    public int AfterRefluxOpenLastDays
    {
      get
      {
        return this._AfterRefluxOpenLastDays;
      }
      set
      {
        this._AfterRefluxOpenLastDays = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
