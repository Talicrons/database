﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.BattleArmyRandomRuleType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "BattleArmyRandomRuleType")]
  public enum BattleArmyRandomRuleType
  {
    [ProtoEnum(Name = "BattleArmyRandomRuleType_None", Value = 0)] BattleArmyRandomRuleType_None,
    [ProtoEnum(Name = "BattleArmyRandomRuleType_EveryTime", Value = 1)] BattleArmyRandomRuleType_EveryTime,
    [ProtoEnum(Name = "BattleArmyRandomRuleType_DailyTime", Value = 2)] BattleArmyRandomRuleType_DailyTime,
    [ProtoEnum(Name = "BattleArmyRandomRuleType_ClimbTower", Value = 3)] BattleArmyRandomRuleType_ClimbTower,
  }
}
