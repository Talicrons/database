﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RealTimePVPConsecutiveWins
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RealTimePVPConsecutiveWins")]
  [Serializable]
  public class RealTimePVPConsecutiveWins : IExtensible
  {
    private int _ConsecutiveWins;
    private int _ScoreBonus;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int ConsecutiveWins
    {
      get
      {
        return this._ConsecutiveWins;
      }
      set
      {
        this._ConsecutiveWins = value;
      }
    }

    [ProtoMember(2)]
    public int ScoreBonus
    {
      get
      {
        return this._ScoreBonus;
      }
      set
      {
        this._ScoreBonus = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
