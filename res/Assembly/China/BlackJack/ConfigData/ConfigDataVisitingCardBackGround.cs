﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataVisitingCardBackGround
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataVisitingCardBackGround")]
  [Serializable]
  public class ConfigDataVisitingCardBackGround : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _GetPathExplain;
    private string _BackGroundIcon;
    private string _FloorIcon;
    private string _MarkIcon;
    private string _Describe;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string GetPathExplain
    {
      get
      {
        return this._GetPathExplain;
      }
      set
      {
        this._GetPathExplain = value;
      }
    }

    [ProtoMember(5)]
    public string BackGroundIcon
    {
      get
      {
        return this._BackGroundIcon;
      }
      set
      {
        this._BackGroundIcon = value;
      }
    }

    [ProtoMember(6)]
    public string FloorIcon
    {
      get
      {
        return this._FloorIcon;
      }
      set
      {
        this._FloorIcon = value;
      }
    }

    [ProtoMember(7)]
    public string MarkIcon
    {
      get
      {
        return this._MarkIcon;
      }
      set
      {
        this._MarkIcon = value;
      }
    }

    [ProtoMember(8)]
    public string Describe
    {
      get
      {
        return this._Describe;
      }
      set
      {
        this._Describe = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
