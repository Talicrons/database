﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataStoryOutlineInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataStoryOutlineInfo")]
  [Serializable]
  public class ConfigDataStoryOutlineInfo : IExtensible
  {
    private int _ID;
    private string _Desc;
    private string _Music;
    private string _Image;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(4)]
    public string Music
    {
      get
      {
        return this._Music;
      }
      set
      {
        this._Music = value;
      }
    }

    [ProtoMember(5)]
    public string Image
    {
      get
      {
        return this._Image;
      }
      set
      {
        this._Image = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
