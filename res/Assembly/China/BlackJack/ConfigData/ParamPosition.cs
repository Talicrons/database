﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ParamPosition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ParamPosition")]
  [Serializable]
  public class ParamPosition : IExtensible
  {
    private int _X;
    private int _Y;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int X
    {
      get
      {
        return this._X;
      }
      set
      {
        this._X = value;
      }
    }

    [ProtoMember(2)]
    public int Y
    {
      get
      {
        return this._Y;
      }
      set
      {
        this._Y = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
