﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.MissionCardPoolType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "MissionCardPoolType")]
  public enum MissionCardPoolType
  {
    [ProtoEnum(Name = "MissionCardPoolType_None", Value = 0)] MissionCardPoolType_None,
    [ProtoEnum(Name = "MissionCardPoolType_Free", Value = 1)] MissionCardPoolType_Free,
    [ProtoEnum(Name = "MissionCardPoolType_Hero", Value = 2)] MissionCardPoolType_Hero,
    [ProtoEnum(Name = "MissionCardPoolType_Equipment", Value = 3)] MissionCardPoolType_Equipment,
  }
}
