﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataSealBattleFieldLevelGroupInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataSealBattleFieldLevelGroupInfo")]
  [Serializable]
  public class ConfigDataSealBattleFieldLevelGroupInfo : IExtensible
  {
    private int _ID;
    private string _Describe;
    private int _OpenTime;
    private List<int> _IncludeLevel;
    private List<int> _AchievementMission;
    private IExtension extensionObject;
    public ConfigDataSealBattleFieldInfo m_sealBattleFieldInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSealBattleFieldLevelGroupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Describe
    {
      get
      {
        return this._Describe;
      }
      set
      {
        this._Describe = value;
      }
    }

    [ProtoMember(4)]
    public int OpenTime
    {
      get
      {
        return this._OpenTime;
      }
      set
      {
        this._OpenTime = value;
      }
    }

    [ProtoMember(5)]
    public List<int> IncludeLevel
    {
      get
      {
        return this._IncludeLevel;
      }
      set
      {
        this._IncludeLevel = value;
      }
    }

    [ProtoMember(6)]
    public List<int> AchievementMission
    {
      get
      {
        return this._AchievementMission;
      }
      set
      {
        this._AchievementMission = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
