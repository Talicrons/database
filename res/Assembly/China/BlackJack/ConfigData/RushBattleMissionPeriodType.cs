﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RushBattleMissionPeriodType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RushBattleMissionPeriodType")]
  public enum RushBattleMissionPeriodType
  {
    [ProtoEnum(Name = "RushBattleMissionPeriodType_Week", Value = 1)] RushBattleMissionPeriodType_Week = 1,
    [ProtoEnum(Name = "RushBattleMissionPeriodType_OneOff", Value = 2)] RushBattleMissionPeriodType_OneOff = 2,
  }
}
