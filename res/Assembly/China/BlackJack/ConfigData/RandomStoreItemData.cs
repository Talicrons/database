﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RandomStoreItemData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

namespace BlackJack.ConfigData
{
  public class RandomStoreItemData
  {
    public int Id { get; set; }

    public int Weight { get; set; }
  }
}
