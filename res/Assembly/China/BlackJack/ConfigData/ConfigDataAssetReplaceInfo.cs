﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataAssetReplaceInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataAssetReplaceInfo")]
  [Serializable]
  public class ConfigDataAssetReplaceInfo : IExtensible
  {
    private int _ID;
    private string _OldPath;
    private string _NewPath;
    private string _Desc;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string OldPath
    {
      get
      {
        return this._OldPath;
      }
      set
      {
        this._OldPath = value;
      }
    }

    [ProtoMember(4)]
    public string NewPath
    {
      get
      {
        return this._NewPath;
      }
      set
      {
        this._NewPath = value;
      }
    }

    [ProtoMember(5)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
