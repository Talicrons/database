﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataPeakArenaKnockoutMatchTime
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataPeakArenaKnockoutMatchTime")]
  [Serializable]
  public class ConfigDataPeakArenaKnockoutMatchTime : IExtensible
  {
    private int _ID;
    private int _Season;
    private int _Round;
    private string _Date;
    private string _Time;
    private string _Match;
    private string _MatchType;
    private string _TitleName;
    private string _EndTime;
    private string _PushNotificationTime;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public int Season
    {
      get
      {
        return this._Season;
      }
      set
      {
        this._Season = value;
      }
    }

    [ProtoMember(4)]
    public int Round
    {
      get
      {
        return this._Round;
      }
      set
      {
        this._Round = value;
      }
    }

    [ProtoMember(5)]
    public string Date
    {
      get
      {
        return this._Date;
      }
      set
      {
        this._Date = value;
      }
    }

    [ProtoMember(6)]
    public string Time
    {
      get
      {
        return this._Time;
      }
      set
      {
        this._Time = value;
      }
    }

    [ProtoMember(7)]
    public string Match
    {
      get
      {
        return this._Match;
      }
      set
      {
        this._Match = value;
      }
    }

    [ProtoMember(8)]
    public string MatchType
    {
      get
      {
        return this._MatchType;
      }
      set
      {
        this._MatchType = value;
      }
    }

    [ProtoMember(9)]
    public string TitleName
    {
      get
      {
        return this._TitleName;
      }
      set
      {
        this._TitleName = value;
      }
    }

    [ProtoMember(10)]
    public string EndTime
    {
      get
      {
        return this._EndTime;
      }
      set
      {
        this._EndTime = value;
      }
    }

    [ProtoMember(11)]
    public string PushNotificationTime
    {
      get
      {
        return this._PushNotificationTime;
      }
      set
      {
        this._PushNotificationTime = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
