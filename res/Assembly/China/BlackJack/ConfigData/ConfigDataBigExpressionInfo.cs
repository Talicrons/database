﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataBigExpressionInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataBigExpressionInfo")]
  [Serializable]
  public class ConfigDataBigExpressionInfo : IExtensible
  {
    private int _ID;
    private string _ExpressionIconPath;
    private int _Group;
    private ExpressionPurposeType _ExpressionPurposeType;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string ExpressionIconPath
    {
      get
      {
        return this._ExpressionIconPath;
      }
      set
      {
        this._ExpressionIconPath = value;
      }
    }

    [ProtoMember(5)]
    public int Group
    {
      get
      {
        return this._Group;
      }
      set
      {
        this._Group = value;
      }
    }

    [ProtoMember(6)]
    public ExpressionPurposeType ExpressionPurposeType
    {
      get
      {
        return this._ExpressionPurposeType;
      }
      set
      {
        this._ExpressionPurposeType = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
