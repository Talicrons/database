﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataPreloadAssetInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataPreloadAssetInfo")]
  [Serializable]
  public class ConfigDataPreloadAssetInfo : IExtensible
  {
    private int _ID;
    private string _Path;
    private bool _IsSprite;
    private int _MemoryLevel;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Path
    {
      get
      {
        return this._Path;
      }
      set
      {
        this._Path = value;
      }
    }

    [ProtoMember(4)]
    public bool IsSprite
    {
      get
      {
        return this._IsSprite;
      }
      set
      {
        this._IsSprite = value;
      }
    }

    [ProtoMember(5)]
    public int MemoryLevel
    {
      get
      {
        return this._MemoryLevel;
      }
      set
      {
        this._MemoryLevel = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
