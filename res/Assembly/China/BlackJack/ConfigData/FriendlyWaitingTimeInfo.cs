﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.FriendlyWaitingTimeInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "FriendlyWaitingTimeInfo")]
  [Serializable]
  public class FriendlyWaitingTimeInfo : IExtensible
  {
    private int _WaitingTime;
    private int _TopBattlePowerMin;
    private int _TopBattlePowerMax;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int WaitingTime
    {
      get
      {
        return this._WaitingTime;
      }
      set
      {
        this._WaitingTime = value;
      }
    }

    [ProtoMember(2)]
    public int TopBattlePowerMin
    {
      get
      {
        return this._TopBattlePowerMin;
      }
      set
      {
        this._TopBattlePowerMin = value;
      }
    }

    [ProtoMember(3)]
    public int TopBattlePowerMax
    {
      get
      {
        return this._TopBattlePowerMax;
      }
      set
      {
        this._TopBattlePowerMax = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
