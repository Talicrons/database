﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataTitleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataTitleInfo")]
  [Serializable]
  public class ConfigDataTitleInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private int _Rarity;
    private string _ResourceLocation;
    private string _FrameLocation;
    private string _Icon;
    private string _EndTime;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(5)]
    public int Rarity
    {
      get
      {
        return this._Rarity;
      }
      set
      {
        this._Rarity = value;
      }
    }

    [ProtoMember(6)]
    public string ResourceLocation
    {
      get
      {
        return this._ResourceLocation;
      }
      set
      {
        this._ResourceLocation = value;
      }
    }

    [ProtoMember(7)]
    public string FrameLocation
    {
      get
      {
        return this._FrameLocation;
      }
      set
      {
        this._FrameLocation = value;
      }
    }

    [ProtoMember(8)]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(9)]
    public string EndTime
    {
      get
      {
        return this._EndTime;
      }
      set
      {
        this._EndTime = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
