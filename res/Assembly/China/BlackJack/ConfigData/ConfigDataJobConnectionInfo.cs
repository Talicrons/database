﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataJobConnectionInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataJobConnectionInfo")]
  [Serializable]
  public class ConfigDataJobConnectionInfo : IExtensible
  {
    private int _ID;
    private int _Job_ID;
    private List<int> _JobLevels_ID;
    private List<int> _TalentSkill_IDs;
    private int _UISort;
    private string _PropertyRating;
    private bool _IsRecommend;
    private string _Model;
    private string _CombatModel;
    private List<ReplaceAnim> _ReplaceAnims;
    private int _ModelScale;
    private int _BF_ModelScale;
    private int _UI_ModelScale;
    private int _UI_ModelOffsetX;
    private int _UI_ModelOffsetY;
    private int _Radius;
    private int _Height;
    private int _FootHeight;
    private List<int> _PreJobConnectionList;
    private List<int> _JobUnlockConditioList;
    private bool _IsJobOpen;
    private List<int> _Skins_ID;
    private IExtension extensionObject;
    public ConfigDataJobInfo m_jobInfo;
    public ConfigDataJobLevelInfo[] m_jobLevelInfos;
    public List<ConfigDataJobUnlockConditionInfo> m_unlockConditions;
    public List<ConfigDataJobConnectionInfo> m_preJobConnectionInfos;
    public List<ConfigDataJobConnectionInfo> m_nextJobConnectionInfos;
    public List<ConfigDataSkillInfo> m_talentSkillInfos;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataJobConnectionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(6)]
    public int Job_ID
    {
      get
      {
        return this._Job_ID;
      }
      set
      {
        this._Job_ID = value;
      }
    }

    [ProtoMember(7)]
    public List<int> JobLevels_ID
    {
      get
      {
        return this._JobLevels_ID;
      }
      set
      {
        this._JobLevels_ID = value;
      }
    }

    [ProtoMember(8)]
    public List<int> TalentSkill_IDs
    {
      get
      {
        return this._TalentSkill_IDs;
      }
      set
      {
        this._TalentSkill_IDs = value;
      }
    }

    [ProtoMember(9)]
    public int UISort
    {
      get
      {
        return this._UISort;
      }
      set
      {
        this._UISort = value;
      }
    }

    [ProtoMember(10)]
    public string PropertyRating
    {
      get
      {
        return this._PropertyRating;
      }
      set
      {
        this._PropertyRating = value;
      }
    }

    [ProtoMember(11)]
    public bool IsRecommend
    {
      get
      {
        return this._IsRecommend;
      }
      set
      {
        this._IsRecommend = value;
      }
    }

    [ProtoMember(12)]
    public string Model
    {
      get
      {
        return this._Model;
      }
      set
      {
        this._Model = value;
      }
    }

    [ProtoMember(13)]
    public string CombatModel
    {
      get
      {
        return this._CombatModel;
      }
      set
      {
        this._CombatModel = value;
      }
    }

    [ProtoMember(14)]
    public List<ReplaceAnim> ReplaceAnims
    {
      get
      {
        return this._ReplaceAnims;
      }
      set
      {
        this._ReplaceAnims = value;
      }
    }

    [ProtoMember(15)]
    public int ModelScale
    {
      get
      {
        return this._ModelScale;
      }
      set
      {
        this._ModelScale = value;
      }
    }

    [ProtoMember(16)]
    public int BF_ModelScale
    {
      get
      {
        return this._BF_ModelScale;
      }
      set
      {
        this._BF_ModelScale = value;
      }
    }

    [ProtoMember(17)]
    public int UI_ModelScale
    {
      get
      {
        return this._UI_ModelScale;
      }
      set
      {
        this._UI_ModelScale = value;
      }
    }

    [ProtoMember(18)]
    public int UI_ModelOffsetX
    {
      get
      {
        return this._UI_ModelOffsetX;
      }
      set
      {
        this._UI_ModelOffsetX = value;
      }
    }

    [ProtoMember(19)]
    public int UI_ModelOffsetY
    {
      get
      {
        return this._UI_ModelOffsetY;
      }
      set
      {
        this._UI_ModelOffsetY = value;
      }
    }

    [ProtoMember(20)]
    public int Radius
    {
      get
      {
        return this._Radius;
      }
      set
      {
        this._Radius = value;
      }
    }

    [ProtoMember(21)]
    public int Height
    {
      get
      {
        return this._Height;
      }
      set
      {
        this._Height = value;
      }
    }

    [ProtoMember(22)]
    public int FootHeight
    {
      get
      {
        return this._FootHeight;
      }
      set
      {
        this._FootHeight = value;
      }
    }

    [ProtoMember(23)]
    public List<int> PreJobConnectionList
    {
      get
      {
        return this._PreJobConnectionList;
      }
      set
      {
        this._PreJobConnectionList = value;
      }
    }

    [ProtoMember(24)]
    public List<int> JobUnlockConditioList
    {
      get
      {
        return this._JobUnlockConditioList;
      }
      set
      {
        this._JobUnlockConditioList = value;
      }
    }

    [ProtoMember(25)]
    public bool IsJobOpen
    {
      get
      {
        return this._IsJobOpen;
      }
      set
      {
        this._IsJobOpen = value;
      }
    }

    [ProtoMember(26)]
    public List<int> Skins_ID
    {
      get
      {
        return this._Skins_ID;
      }
      set
      {
        this._Skins_ID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSkillInfo GetTalentSkillInfo(int heroStar)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsJobLevelMax(int jobLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetJobRankForProgrammer(ConfigDataJobConnectionInfo info)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetJobRankForProgrammer()
    {
      return this.GetJobRankForProgrammer(this);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsUltimateJob()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
