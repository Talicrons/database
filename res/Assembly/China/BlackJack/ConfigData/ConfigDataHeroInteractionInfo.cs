﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataHeroInteractionInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataHeroInteractionInfo")]
  [Serializable]
  public class ConfigDataHeroInteractionInfo : IExtensible
  {
    private int _ID;
    private List<HeroInteractionWeightResult> _Results;
    private int _NormalResultPerformance;
    private int _SmallUpResultPerformance;
    private int _BigUpResultPerformance;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroInteractionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public List<HeroInteractionWeightResult> Results
    {
      get
      {
        return this._Results;
      }
      set
      {
        this._Results = value;
      }
    }

    [ProtoMember(4)]
    public int NormalResultPerformance
    {
      get
      {
        return this._NormalResultPerformance;
      }
      set
      {
        this._NormalResultPerformance = value;
      }
    }

    [ProtoMember(5)]
    public int SmallUpResultPerformance
    {
      get
      {
        return this._SmallUpResultPerformance;
      }
      set
      {
        this._SmallUpResultPerformance = value;
      }
    }

    [ProtoMember(6)]
    public int BigUpResultPerformance
    {
      get
      {
        return this._BigUpResultPerformance;
      }
      set
      {
        this._BigUpResultPerformance = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
