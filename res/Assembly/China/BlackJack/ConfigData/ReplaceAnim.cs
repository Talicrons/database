﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ReplaceAnim
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ReplaceAnim")]
  [Serializable]
  public class ReplaceAnim : IExtensible
  {
    private string _DefaultName;
    private string _ReplaceName;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public string DefaultName
    {
      get
      {
        return this._DefaultName;
      }
      set
      {
        this._DefaultName = value;
      }
    }

    [ProtoMember(2)]
    public string ReplaceName
    {
      get
      {
        return this._ReplaceName;
      }
      set
      {
        this._ReplaceName = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
