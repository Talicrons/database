﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataPVPBattleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataPVPBattleInfo")]
  [Serializable]
  public class ConfigDataPVPBattleInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private string _WinDesc;
    private string _LoseDesc;
    private int _Battlefield_ID;
    private int _CameraX;
    private int _CameraY;
    private int _DefendCameraX;
    private int _DefendCameraY;
    private string _PrepareMusic;
    private string _BattleMusic;
    private string _DefendBattleMusic;
    private int _TurnMax;
    private List<int> _WinConditions_ID;
    private List<int> _LoseConditions_ID;
    private List<int> _EventTriggers_ID;
    private int _AttackNumber;
    private List<ParamPosition> _AttackPositions;
    private List<int> _AttackDirs;
    private int _DefendNumber;
    private List<ParamPosition> _DefendPositions;
    private List<int> _DefendDirs;
    private IExtension extensionObject;
    public ConfigDataBattlefieldInfo m_battlefieldInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPVPBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(7)]
    public string WinDesc
    {
      get
      {
        return this._WinDesc;
      }
      set
      {
        this._WinDesc = value;
      }
    }

    [ProtoMember(9)]
    public string LoseDesc
    {
      get
      {
        return this._LoseDesc;
      }
      set
      {
        this._LoseDesc = value;
      }
    }

    [ProtoMember(11)]
    public int Battlefield_ID
    {
      get
      {
        return this._Battlefield_ID;
      }
      set
      {
        this._Battlefield_ID = value;
      }
    }

    [ProtoMember(12)]
    public int CameraX
    {
      get
      {
        return this._CameraX;
      }
      set
      {
        this._CameraX = value;
      }
    }

    [ProtoMember(13)]
    public int CameraY
    {
      get
      {
        return this._CameraY;
      }
      set
      {
        this._CameraY = value;
      }
    }

    [ProtoMember(14)]
    public int DefendCameraX
    {
      get
      {
        return this._DefendCameraX;
      }
      set
      {
        this._DefendCameraX = value;
      }
    }

    [ProtoMember(15)]
    public int DefendCameraY
    {
      get
      {
        return this._DefendCameraY;
      }
      set
      {
        this._DefendCameraY = value;
      }
    }

    [ProtoMember(16)]
    public string PrepareMusic
    {
      get
      {
        return this._PrepareMusic;
      }
      set
      {
        this._PrepareMusic = value;
      }
    }

    [ProtoMember(17)]
    public string BattleMusic
    {
      get
      {
        return this._BattleMusic;
      }
      set
      {
        this._BattleMusic = value;
      }
    }

    [ProtoMember(18)]
    public string DefendBattleMusic
    {
      get
      {
        return this._DefendBattleMusic;
      }
      set
      {
        this._DefendBattleMusic = value;
      }
    }

    [ProtoMember(19)]
    public int TurnMax
    {
      get
      {
        return this._TurnMax;
      }
      set
      {
        this._TurnMax = value;
      }
    }

    [ProtoMember(20)]
    public List<int> WinConditions_ID
    {
      get
      {
        return this._WinConditions_ID;
      }
      set
      {
        this._WinConditions_ID = value;
      }
    }

    [ProtoMember(21)]
    public List<int> LoseConditions_ID
    {
      get
      {
        return this._LoseConditions_ID;
      }
      set
      {
        this._LoseConditions_ID = value;
      }
    }

    [ProtoMember(22)]
    public List<int> EventTriggers_ID
    {
      get
      {
        return this._EventTriggers_ID;
      }
      set
      {
        this._EventTriggers_ID = value;
      }
    }

    [ProtoMember(23)]
    public int AttackNumber
    {
      get
      {
        return this._AttackNumber;
      }
      set
      {
        this._AttackNumber = value;
      }
    }

    [ProtoMember(24)]
    public List<ParamPosition> AttackPositions
    {
      get
      {
        return this._AttackPositions;
      }
      set
      {
        this._AttackPositions = value;
      }
    }

    [ProtoMember(25)]
    public List<int> AttackDirs
    {
      get
      {
        return this._AttackDirs;
      }
      set
      {
        this._AttackDirs = value;
      }
    }

    [ProtoMember(26)]
    public int DefendNumber
    {
      get
      {
        return this._DefendNumber;
      }
      set
      {
        this._DefendNumber = value;
      }
    }

    [ProtoMember(27)]
    public List<ParamPosition> DefendPositions
    {
      get
      {
        return this._DefendPositions;
      }
      set
      {
        this._DefendPositions = value;
      }
    }

    [ProtoMember(28)]
    public List<int> DefendDirs
    {
      get
      {
        return this._DefendDirs;
      }
      set
      {
        this._DefendDirs = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
