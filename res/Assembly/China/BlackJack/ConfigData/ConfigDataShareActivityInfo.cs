﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataShareActivityInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataShareActivityInfo")]
  [Serializable]
  public class ConfigDataShareActivityInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _StaticImage;
    private string _PublicizeImage;
    private int _ShareRewardMailTemplateId;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string StaticImage
    {
      get
      {
        return this._StaticImage;
      }
      set
      {
        this._StaticImage = value;
      }
    }

    [ProtoMember(5)]
    public string PublicizeImage
    {
      get
      {
        return this._PublicizeImage;
      }
      set
      {
        this._PublicizeImage = value;
      }
    }

    [ProtoMember(6)]
    public int ShareRewardMailTemplateId
    {
      get
      {
        return this._ShareRewardMailTemplateId;
      }
      set
      {
        this._ShareRewardMailTemplateId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
