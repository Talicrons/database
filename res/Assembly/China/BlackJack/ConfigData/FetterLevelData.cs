﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.FetterLevelData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "FetterLevelData")]
  [Serializable]
  public class FetterLevelData : IExtensible
  {
    private int _FetterSortId;
    private int _FetterLv;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int FetterSortId
    {
      get
      {
        return this._FetterSortId;
      }
      set
      {
        this._FetterSortId = value;
      }
    }

    [ProtoMember(2)]
    public int FetterLv
    {
      get
      {
        return this._FetterLv;
      }
      set
      {
        this._FetterLv = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
