﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataAwakenInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataAwakenInfo")]
  [Serializable]
  public class ConfigDataAwakenInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private List<Goods> _Awaken1Material;
    private int _Awaken1LevelID;
    private bool _Awaken2Unlock;
    private List<Goods> _Awaken2Material;
    private int _Awaken2LevelID;
    private int _Level2SkillID;
    private string _Anim;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataAwakenInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public List<Goods> Awaken1Material
    {
      get
      {
        return this._Awaken1Material;
      }
      set
      {
        this._Awaken1Material = value;
      }
    }

    [ProtoMember(5)]
    public int Awaken1LevelID
    {
      get
      {
        return this._Awaken1LevelID;
      }
      set
      {
        this._Awaken1LevelID = value;
      }
    }

    [ProtoMember(6)]
    public bool Awaken2Unlock
    {
      get
      {
        return this._Awaken2Unlock;
      }
      set
      {
        this._Awaken2Unlock = value;
      }
    }

    [ProtoMember(7)]
    public List<Goods> Awaken2Material
    {
      get
      {
        return this._Awaken2Material;
      }
      set
      {
        this._Awaken2Material = value;
      }
    }

    [ProtoMember(8)]
    public int Awaken2LevelID
    {
      get
      {
        return this._Awaken2LevelID;
      }
      set
      {
        this._Awaken2LevelID = value;
      }
    }

    [ProtoMember(9)]
    public int Level2SkillID
    {
      get
      {
        return this._Level2SkillID;
      }
      set
      {
        this._Level2SkillID = value;
      }
    }

    [ProtoMember(10)]
    public string Anim
    {
      get
      {
        return this._Anim;
      }
      set
      {
        this._Anim = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
