﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ActivityRetrospectType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 6051A05C-4743-4658-929B-A78C1745C3F3
// Assembly location: C:\Users\TR\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ActivityRetrospectType")]
  public enum ActivityRetrospectType
  {
    [ProtoEnum(Name = "ActivityRetrospectType_None", Value = 0)] ActivityRetrospectType_None,
    [ProtoEnum(Name = "ActivityRetrospectType_UnchartedScore", Value = 1)] ActivityRetrospectType_UnchartedScore,
    [ProtoEnum(Name = "ActivityRetrospectType_CollectionActivity", Value = 2)] ActivityRetrospectType_CollectionActivity,
  }
}
