﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.MissionPeriodType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "MissionPeriodType")]
  public enum MissionPeriodType
  {
    [ProtoEnum(Name = "MissionPeriodType_Everyday", Value = 1)] MissionPeriodType_Everyday = 1,
    [ProtoEnum(Name = "MissionPeriodType_OneOff", Value = 2)] MissionPeriodType_OneOff = 2,
  }
}
