﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroBiographyUnlockConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroBiographyUnlockConditionType")]
  public enum HeroBiographyUnlockConditionType
  {
    [ProtoEnum(Name = "HeroBiographyUnlockConditionType_None", Value = 0)] HeroBiographyUnlockConditionType_None,
    [ProtoEnum(Name = "HeroBiographyUnlockConditionType_HeroFavorabilityLevel", Value = 1)] HeroBiographyUnlockConditionType_HeroFavorabilityLevel,
  }
}
