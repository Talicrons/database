﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.PerformanceEffect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "PerformanceEffect")]
  [Serializable]
  public class PerformanceEffect : IExtensible
  {
    private string _Anim;
    private int _Word;
    private string _Voice;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public string Anim
    {
      get
      {
        return this._Anim;
      }
      set
      {
        this._Anim = value;
      }
    }

    [ProtoMember(2)]
    public int Word
    {
      get
      {
        return this._Word;
      }
      set
      {
        this._Word = value;
      }
    }

    [ProtoMember(3)]
    public string Voice
    {
      get
      {
        return this._Voice;
      }
      set
      {
        this._Voice = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
