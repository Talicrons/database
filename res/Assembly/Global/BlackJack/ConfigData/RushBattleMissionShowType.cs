﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RushBattleMissionShowType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RushBattleMissionShowType")]
  public enum RushBattleMissionShowType
  {
    [ProtoEnum(Name = "RushBattleMissionShowType_Period", Value = 1)] RushBattleMissionShowType_Period = 1,
    [ProtoEnum(Name = "RushBattleMissionShowType_Achievement", Value = 2)] RushBattleMissionShowType_Achievement = 2,
  }
}
