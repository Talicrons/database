﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataCardPoolExchangeItemInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataCardPoolExchangeItemInfo")]
  [Serializable]
  public class ConfigDataCardPoolExchangeItemInfo : IExtensible
  {
    private int _ID;
    private int _CardPoolID;
    private List<Goods> _BeforeExchangeItems;
    private int _ExchangeCount;
    private List<Goods> _AfterExchangeItems;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCardPoolExchangeItemInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public int CardPoolID
    {
      get
      {
        return this._CardPoolID;
      }
      set
      {
        this._CardPoolID = value;
      }
    }

    [ProtoMember(4)]
    public List<Goods> BeforeExchangeItems
    {
      get
      {
        return this._BeforeExchangeItems;
      }
      set
      {
        this._BeforeExchangeItems = value;
      }
    }

    [ProtoMember(5)]
    public int ExchangeCount
    {
      get
      {
        return this._ExchangeCount;
      }
      set
      {
        this._ExchangeCount = value;
      }
    }

    [ProtoMember(6)]
    public List<Goods> AfterExchangeItems
    {
      get
      {
        return this._AfterExchangeItems;
      }
      set
      {
        this._AfterExchangeItems = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
