﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.LevelInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "LevelInfo")]
  [Serializable]
  public class LevelInfo : IExtensible
  {
    private CollectionActivityLevelType _LevelType;
    private int _ID;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public CollectionActivityLevelType LevelType
    {
      get
      {
        return this._LevelType;
      }
      set
      {
        this._LevelType = value;
      }
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
