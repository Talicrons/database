﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataHeroHeartFetterInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataHeroHeartFetterInfo")]
  [Serializable]
  public class ConfigDataHeroHeartFetterInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _MaxLevel;
    private List<HeroHeartFetterUnlockCondition> _UnlockConditions;
    private List<Goods> _UnlockReward;
    private List<int> _HeroHeartFetterSkills;
    private List<HeroFetterLevelUpCost> _LevelUpMaterials;
    private List<int> _LevelUpGold;
    private List<HeroHeartFetterUnlockSkill> _UnlockSkills_ID;
    private int _ShownSkillCount;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroHeartFetterInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5)]
    public int MaxLevel
    {
      get
      {
        return this._MaxLevel;
      }
      set
      {
        this._MaxLevel = value;
      }
    }

    [ProtoMember(6)]
    public List<HeroHeartFetterUnlockCondition> UnlockConditions
    {
      get
      {
        return this._UnlockConditions;
      }
      set
      {
        this._UnlockConditions = value;
      }
    }

    [ProtoMember(7)]
    public List<Goods> UnlockReward
    {
      get
      {
        return this._UnlockReward;
      }
      set
      {
        this._UnlockReward = value;
      }
    }

    [ProtoMember(8)]
    public List<int> HeroHeartFetterSkills
    {
      get
      {
        return this._HeroHeartFetterSkills;
      }
      set
      {
        this._HeroHeartFetterSkills = value;
      }
    }

    [ProtoMember(9)]
    public List<HeroFetterLevelUpCost> LevelUpMaterials
    {
      get
      {
        return this._LevelUpMaterials;
      }
      set
      {
        this._LevelUpMaterials = value;
      }
    }

    [ProtoMember(10)]
    public List<int> LevelUpGold
    {
      get
      {
        return this._LevelUpGold;
      }
      set
      {
        this._LevelUpGold = value;
      }
    }

    [ProtoMember(11)]
    public List<HeroHeartFetterUnlockSkill> UnlockSkills_ID
    {
      get
      {
        return this._UnlockSkills_ID;
      }
      set
      {
        this._UnlockSkills_ID = value;
      }
    }

    [ProtoMember(12)]
    public int ShownSkillCount
    {
      get
      {
        return this._ShownSkillCount;
      }
      set
      {
        this._ShownSkillCount = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
