﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataTowerBonusHeroGroupInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataTowerBonusHeroGroupInfo")]
  [Serializable]
  public class ConfigDataTowerBonusHeroGroupInfo : IExtensible
  {
    private int _ID;
    private List<int> _BonusHeroIdList;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTowerBonusHeroGroupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public List<int> BonusHeroIdList
    {
      get
      {
        return this._BonusHeroIdList;
      }
      set
      {
        this._BonusHeroIdList = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
