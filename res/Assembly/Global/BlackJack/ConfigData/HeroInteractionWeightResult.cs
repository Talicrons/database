﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroInteractionWeightResult
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroInteractionWeightResult")]
  [Serializable]
  public class HeroInteractionWeightResult : IExtensible
  {
    private HeroInteractionResultType _ResultType;
    private int _Weight;
    private int _FavourabilityExp;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public HeroInteractionResultType ResultType
    {
      get
      {
        return this._ResultType;
      }
      set
      {
        this._ResultType = value;
      }
    }

    [ProtoMember(2)]
    public int Weight
    {
      get
      {
        return this._Weight;
      }
      set
      {
        this._Weight = value;
      }
    }

    [ProtoMember(3)]
    public int FavourabilityExp
    {
      get
      {
        return this._FavourabilityExp;
      }
      set
      {
        this._FavourabilityExp = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
