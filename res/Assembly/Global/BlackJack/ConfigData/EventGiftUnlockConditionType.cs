﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.EventGiftUnlockConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "EventGiftUnlockConditionType")]
  public enum EventGiftUnlockConditionType
  {
    [ProtoEnum(Name = "EventGiftUnlockConditionType_None", Value = 0)] EventGiftUnlockConditionType_None,
    [ProtoEnum(Name = "EventGiftUnlockConditionType_PlayerLevel", Value = 1)] EventGiftUnlockConditionType_PlayerLevel,
    [ProtoEnum(Name = "EventGiftUnlockConditionType_SummonRankHero", Value = 2)] EventGiftUnlockConditionType_SummonRankHero,
    [ProtoEnum(Name = "EventGiftUnlockConditionType_CompleteLevel", Value = 3)] EventGiftUnlockConditionType_CompleteLevel,
    [ProtoEnum(Name = "EventGiftUnlockConditionType_ReachOfflineArenaScore", Value = 4)] EventGiftUnlockConditionType_ReachOfflineArenaScore,
  }
}
