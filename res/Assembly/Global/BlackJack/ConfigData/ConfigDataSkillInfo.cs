﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataSkillInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataSkillInfo")]
  [Serializable]
  public class ConfigDataSkillInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private bool _IsMagic;
    private bool _IsActiveSkill;
    private bool _IsSupport;
    private bool _IsHeal;
    private bool _IsRangeSkill;
    private bool _AIIsRangeSkill;
    private bool _IsNewTurn;
    private int _NewTurnMovePoint;
    private int _NewTurnRemainMovePoint;
    private List<int> _LimitArmys_ID;
    private List<int> _PassiveBuffs_ID;
    private List<int> _SelfBuffs_ID;
    private SkillAIType _SkillAIType;
    private int _TargetWithNParam;
    private int _SkillTargetAIType;
    private SkillType _SkillType;
    private int _SkillTypeParam1;
    private int _SkillTypeParam2;
    private int _SkillTypeParam3;
    private List<int> _SkillTypeParam4;
    private int _Time_Sing;
    private int _Power;
    private int _AttackDistance;
    private int _HitCountMax;
    private int _AttackCount;
    private int _Time_NextAttack;
    private List<int> _Buffs_ID;
    private int _BuffRate;
    private int _BuffNum;
    private int _HeroMoveDelay;
    private int _Time_EffCast1;
    private int _Time_EffCast2;
    private int _Time_EffMagic1;
    private int _Time_EffMagic2;
    private int _Time_Hit;
    private int _Time_End;
    private List<int> _Time_MultiHit;
    private string _Anim_Cast;
    private string _Anim_Sing;
    private string _Effect_Sing;
    private string _Effect_Cast1;
    private string _Effect_Cast1_Far;
    private string _Effect_Cast2;
    private string _Effect_Cast2_Far;
    private string _Effect_Magic1;
    private string _Effect_Magic1_Far;
    private string _Effect_Magic2;
    private string _Effect_Magic2_Far;
    private string _Effect_Hit;
    private string _Effect_PreCast;
    private int _CutsceneType;
    private int _DeadAnimType;
    private int _BF_InitCooldown;
    private int _BF_Cooldown;
    private int _BF_Distance;
    private int _BF_Range;
    private int _BF_RangeShape;
    private SkillTargetType _BF_TargetFaction;
    private SkillTargetType _BF_TargetType;
    private int _CastSkillShape;
    private string _Icon;
    private int _SkillCost;
    private string _TypeText;
    private string _CDText;
    private string _DistanceText;
    private string _RangeText;
    private int _BattlePower;
    private int _RushFortressUseCount;
    private IExtension extensionObject;
    public ConfigDataBuffInfo[] m_buffInfos;
    public ConfigDataBuffInfo[] m_passiveBuffInfos;
    public ConfigDataBuffInfo[] m_selfBuffInfos;
    public bool m_isNormalAttack;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSkillInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(7)]
    public bool IsMagic
    {
      get
      {
        return this._IsMagic;
      }
      set
      {
        this._IsMagic = value;
      }
    }

    [ProtoMember(8)]
    public bool IsActiveSkill
    {
      get
      {
        return this._IsActiveSkill;
      }
      set
      {
        this._IsActiveSkill = value;
      }
    }

    [ProtoMember(9)]
    public bool IsSupport
    {
      get
      {
        return this._IsSupport;
      }
      set
      {
        this._IsSupport = value;
      }
    }

    [ProtoMember(10)]
    public bool IsHeal
    {
      get
      {
        return this._IsHeal;
      }
      set
      {
        this._IsHeal = value;
      }
    }

    [ProtoMember(11)]
    public bool IsRangeSkill
    {
      get
      {
        return this._IsRangeSkill;
      }
      set
      {
        this._IsRangeSkill = value;
      }
    }

    [ProtoMember(12)]
    public bool AIIsRangeSkill
    {
      get
      {
        return this._AIIsRangeSkill;
      }
      set
      {
        this._AIIsRangeSkill = value;
      }
    }

    [ProtoMember(13)]
    public bool IsNewTurn
    {
      get
      {
        return this._IsNewTurn;
      }
      set
      {
        this._IsNewTurn = value;
      }
    }

    [ProtoMember(14)]
    public int NewTurnMovePoint
    {
      get
      {
        return this._NewTurnMovePoint;
      }
      set
      {
        this._NewTurnMovePoint = value;
      }
    }

    [ProtoMember(15)]
    public int NewTurnRemainMovePoint
    {
      get
      {
        return this._NewTurnRemainMovePoint;
      }
      set
      {
        this._NewTurnRemainMovePoint = value;
      }
    }

    [ProtoMember(16)]
    public List<int> LimitArmys_ID
    {
      get
      {
        return this._LimitArmys_ID;
      }
      set
      {
        this._LimitArmys_ID = value;
      }
    }

    [ProtoMember(17)]
    public List<int> PassiveBuffs_ID
    {
      get
      {
        return this._PassiveBuffs_ID;
      }
      set
      {
        this._PassiveBuffs_ID = value;
      }
    }

    [ProtoMember(18)]
    public List<int> SelfBuffs_ID
    {
      get
      {
        return this._SelfBuffs_ID;
      }
      set
      {
        this._SelfBuffs_ID = value;
      }
    }

    [ProtoMember(19)]
    public SkillAIType SkillAIType
    {
      get
      {
        return this._SkillAIType;
      }
      set
      {
        this._SkillAIType = value;
      }
    }

    [ProtoMember(20)]
    public int TargetWithNParam
    {
      get
      {
        return this._TargetWithNParam;
      }
      set
      {
        this._TargetWithNParam = value;
      }
    }

    [ProtoMember(21)]
    public int SkillTargetAIType
    {
      get
      {
        return this._SkillTargetAIType;
      }
      set
      {
        this._SkillTargetAIType = value;
      }
    }

    [ProtoMember(22)]
    public SkillType SkillType
    {
      get
      {
        return this._SkillType;
      }
      set
      {
        this._SkillType = value;
      }
    }

    [ProtoMember(23)]
    public int SkillTypeParam1
    {
      get
      {
        return this._SkillTypeParam1;
      }
      set
      {
        this._SkillTypeParam1 = value;
      }
    }

    [ProtoMember(24)]
    public int SkillTypeParam2
    {
      get
      {
        return this._SkillTypeParam2;
      }
      set
      {
        this._SkillTypeParam2 = value;
      }
    }

    [ProtoMember(25)]
    public int SkillTypeParam3
    {
      get
      {
        return this._SkillTypeParam3;
      }
      set
      {
        this._SkillTypeParam3 = value;
      }
    }

    [ProtoMember(26)]
    public List<int> SkillTypeParam4
    {
      get
      {
        return this._SkillTypeParam4;
      }
      set
      {
        this._SkillTypeParam4 = value;
      }
    }

    [ProtoMember(27)]
    public int Time_Sing
    {
      get
      {
        return this._Time_Sing;
      }
      set
      {
        this._Time_Sing = value;
      }
    }

    [ProtoMember(28)]
    public int Power
    {
      get
      {
        return this._Power;
      }
      set
      {
        this._Power = value;
      }
    }

    [ProtoMember(29)]
    public int AttackDistance
    {
      get
      {
        return this._AttackDistance;
      }
      set
      {
        this._AttackDistance = value;
      }
    }

    [ProtoMember(30)]
    public int HitCountMax
    {
      get
      {
        return this._HitCountMax;
      }
      set
      {
        this._HitCountMax = value;
      }
    }

    [ProtoMember(31)]
    public int AttackCount
    {
      get
      {
        return this._AttackCount;
      }
      set
      {
        this._AttackCount = value;
      }
    }

    [ProtoMember(32)]
    public int Time_NextAttack
    {
      get
      {
        return this._Time_NextAttack;
      }
      set
      {
        this._Time_NextAttack = value;
      }
    }

    [ProtoMember(33)]
    public List<int> Buffs_ID
    {
      get
      {
        return this._Buffs_ID;
      }
      set
      {
        this._Buffs_ID = value;
      }
    }

    [ProtoMember(34)]
    public int BuffRate
    {
      get
      {
        return this._BuffRate;
      }
      set
      {
        this._BuffRate = value;
      }
    }

    [ProtoMember(35)]
    public int BuffNum
    {
      get
      {
        return this._BuffNum;
      }
      set
      {
        this._BuffNum = value;
      }
    }

    [ProtoMember(36)]
    public int HeroMoveDelay
    {
      get
      {
        return this._HeroMoveDelay;
      }
      set
      {
        this._HeroMoveDelay = value;
      }
    }

    [ProtoMember(37)]
    public int Time_EffCast1
    {
      get
      {
        return this._Time_EffCast1;
      }
      set
      {
        this._Time_EffCast1 = value;
      }
    }

    [ProtoMember(38)]
    public int Time_EffCast2
    {
      get
      {
        return this._Time_EffCast2;
      }
      set
      {
        this._Time_EffCast2 = value;
      }
    }

    [ProtoMember(39)]
    public int Time_EffMagic1
    {
      get
      {
        return this._Time_EffMagic1;
      }
      set
      {
        this._Time_EffMagic1 = value;
      }
    }

    [ProtoMember(40)]
    public int Time_EffMagic2
    {
      get
      {
        return this._Time_EffMagic2;
      }
      set
      {
        this._Time_EffMagic2 = value;
      }
    }

    [ProtoMember(41)]
    public int Time_Hit
    {
      get
      {
        return this._Time_Hit;
      }
      set
      {
        this._Time_Hit = value;
      }
    }

    [ProtoMember(42)]
    public int Time_End
    {
      get
      {
        return this._Time_End;
      }
      set
      {
        this._Time_End = value;
      }
    }

    [ProtoMember(43)]
    public List<int> Time_MultiHit
    {
      get
      {
        return this._Time_MultiHit;
      }
      set
      {
        this._Time_MultiHit = value;
      }
    }

    [ProtoMember(44)]
    public string Anim_Cast
    {
      get
      {
        return this._Anim_Cast;
      }
      set
      {
        this._Anim_Cast = value;
      }
    }

    [ProtoMember(45)]
    public string Anim_Sing
    {
      get
      {
        return this._Anim_Sing;
      }
      set
      {
        this._Anim_Sing = value;
      }
    }

    [ProtoMember(46)]
    public string Effect_Sing
    {
      get
      {
        return this._Effect_Sing;
      }
      set
      {
        this._Effect_Sing = value;
      }
    }

    [ProtoMember(47)]
    public string Effect_Cast1
    {
      get
      {
        return this._Effect_Cast1;
      }
      set
      {
        this._Effect_Cast1 = value;
      }
    }

    [ProtoMember(48)]
    public string Effect_Cast1_Far
    {
      get
      {
        return this._Effect_Cast1_Far;
      }
      set
      {
        this._Effect_Cast1_Far = value;
      }
    }

    [ProtoMember(49)]
    public string Effect_Cast2
    {
      get
      {
        return this._Effect_Cast2;
      }
      set
      {
        this._Effect_Cast2 = value;
      }
    }

    [ProtoMember(50)]
    public string Effect_Cast2_Far
    {
      get
      {
        return this._Effect_Cast2_Far;
      }
      set
      {
        this._Effect_Cast2_Far = value;
      }
    }

    [ProtoMember(51)]
    public string Effect_Magic1
    {
      get
      {
        return this._Effect_Magic1;
      }
      set
      {
        this._Effect_Magic1 = value;
      }
    }

    [ProtoMember(52)]
    public string Effect_Magic1_Far
    {
      get
      {
        return this._Effect_Magic1_Far;
      }
      set
      {
        this._Effect_Magic1_Far = value;
      }
    }

    [ProtoMember(53)]
    public string Effect_Magic2
    {
      get
      {
        return this._Effect_Magic2;
      }
      set
      {
        this._Effect_Magic2 = value;
      }
    }

    [ProtoMember(54)]
    public string Effect_Magic2_Far
    {
      get
      {
        return this._Effect_Magic2_Far;
      }
      set
      {
        this._Effect_Magic2_Far = value;
      }
    }

    [ProtoMember(55)]
    public string Effect_Hit
    {
      get
      {
        return this._Effect_Hit;
      }
      set
      {
        this._Effect_Hit = value;
      }
    }

    [ProtoMember(56)]
    public string Effect_PreCast
    {
      get
      {
        return this._Effect_PreCast;
      }
      set
      {
        this._Effect_PreCast = value;
      }
    }

    [ProtoMember(57)]
    public int CutsceneType
    {
      get
      {
        return this._CutsceneType;
      }
      set
      {
        this._CutsceneType = value;
      }
    }

    [ProtoMember(58)]
    public int DeadAnimType
    {
      get
      {
        return this._DeadAnimType;
      }
      set
      {
        this._DeadAnimType = value;
      }
    }

    [ProtoMember(59)]
    public int BF_InitCooldown
    {
      get
      {
        return this._BF_InitCooldown;
      }
      set
      {
        this._BF_InitCooldown = value;
      }
    }

    [ProtoMember(60)]
    public int BF_Cooldown
    {
      get
      {
        return this._BF_Cooldown;
      }
      set
      {
        this._BF_Cooldown = value;
      }
    }

    [ProtoMember(61)]
    public int BF_Distance
    {
      get
      {
        return this._BF_Distance;
      }
      set
      {
        this._BF_Distance = value;
      }
    }

    [ProtoMember(62)]
    public int BF_Range
    {
      get
      {
        return this._BF_Range;
      }
      set
      {
        this._BF_Range = value;
      }
    }

    [ProtoMember(63)]
    public int BF_RangeShape
    {
      get
      {
        return this._BF_RangeShape;
      }
      set
      {
        this._BF_RangeShape = value;
      }
    }

    [ProtoMember(64)]
    public SkillTargetType BF_TargetFaction
    {
      get
      {
        return this._BF_TargetFaction;
      }
      set
      {
        this._BF_TargetFaction = value;
      }
    }

    [ProtoMember(65)]
    public SkillTargetType BF_TargetType
    {
      get
      {
        return this._BF_TargetType;
      }
      set
      {
        this._BF_TargetType = value;
      }
    }

    [ProtoMember(66)]
    public int CastSkillShape
    {
      get
      {
        return this._CastSkillShape;
      }
      set
      {
        this._CastSkillShape = value;
      }
    }

    [ProtoMember(67)]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(68)]
    public int SkillCost
    {
      get
      {
        return this._SkillCost;
      }
      set
      {
        this._SkillCost = value;
      }
    }

    [ProtoMember(69)]
    public string TypeText
    {
      get
      {
        return this._TypeText;
      }
      set
      {
        this._TypeText = value;
      }
    }

    [ProtoMember(70)]
    public string CDText
    {
      get
      {
        return this._CDText;
      }
      set
      {
        this._CDText = value;
      }
    }

    [ProtoMember(71)]
    public string DistanceText
    {
      get
      {
        return this._DistanceText;
      }
      set
      {
        this._DistanceText = value;
      }
    }

    [ProtoMember(72)]
    public string RangeText
    {
      get
      {
        return this._RangeText;
      }
      set
      {
        this._RangeText = value;
      }
    }

    [ProtoMember(73)]
    public int BattlePower
    {
      get
      {
        return this._BattlePower;
      }
      set
      {
        this._BattlePower = value;
      }
    }

    [ProtoMember(74)]
    public int RushFortressUseCount
    {
      get
      {
        return this._RushFortressUseCount;
      }
      set
      {
        this._RushFortressUseCount = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    public bool IsNormalAttack()
    {
      return this.m_isNormalAttack;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCombatSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBattlefieldSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsDamageSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPhysicalDamageSkill(bool isMagic)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMagicDamageSkill(bool isMagic)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHealSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsDamageAndHealSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBuffSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsSummonSkill()
    {
      return this.SkillType == SkillType.SkillType_BF_Summon;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAnySummonSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSummonSkillAllowOnlyOneSummonActorMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAnyTeleportSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsPassiveSkill()
    {
      return this.SkillType == SkillType.SkillType_Passive;
    }

    public bool IsCombineSkill()
    {
      return this.SkillType == SkillType.SkillType_BF_SuperMagicDamage;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanDispelBuff()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
