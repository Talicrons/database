﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ActivityRetrospectType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ActivityRetrospectType")]
  public enum ActivityRetrospectType
  {
    [ProtoEnum(Name = "ActivityRetrospectType_None", Value = 0)] ActivityRetrospectType_None,
    [ProtoEnum(Name = "ActivityRetrospectType_UnchartedScore", Value = 1)] ActivityRetrospectType_UnchartedScore,
    [ProtoEnum(Name = "ActivityRetrospectType_CollectionActivity", Value = 2)] ActivityRetrospectType_CollectionActivity,
  }
}
