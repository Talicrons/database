﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.Goods
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "Goods")]
  [Serializable]
  public class Goods : IExtensible
  {
    private GoodsType _GoodsType;
    private int _Id;
    private int _Count;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public GoodsType GoodsType
    {
      get
      {
        return this._GoodsType;
      }
      set
      {
        this._GoodsType = value;
      }
    }

    [ProtoMember(2)]
    public int Id
    {
      get
      {
        return this._Id;
      }
      set
      {
        this._Id = value;
      }
    }

    [ProtoMember(3)]
    public int Count
    {
      get
      {
        return this._Count;
      }
      set
      {
        this._Count = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
