﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.EnchantPropertyProbabilityInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using System.Collections.Generic;

namespace BlackJack.ConfigData
{
  public class EnchantPropertyProbabilityInfo
  {
    public PropertyModifyType Id { get; set; }

    public List<EnchantPropertyValueInfo> PropertyValues { get; set; }

    public int Weight { get; set; }
  }
}
