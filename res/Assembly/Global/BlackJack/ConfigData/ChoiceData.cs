﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ChoiceData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ChoiceData")]
  [Serializable]
  public class ChoiceData : IExtensible
  {
    private int _ChoiceID;
    private int _NextDialogID;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int ChoiceID
    {
      get
      {
        return this._ChoiceID;
      }
      set
      {
        this._ChoiceID = value;
      }
    }

    [ProtoMember(2)]
    public int NextDialogID
    {
      get
      {
        return this._NextDialogID;
      }
      set
      {
        this._NextDialogID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
