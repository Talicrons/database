﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CollectionEventAppearConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "CollectionEventAppearConditionType")]
  public enum CollectionEventAppearConditionType
  {
    [ProtoEnum(Name = "CollectionEventAppearConditionType_None", Value = 0)] CollectionEventAppearConditionType_None,
    [ProtoEnum(Name = "CollectionEventAppearConditionType_CompleteScenario", Value = 1)] CollectionEventAppearConditionType_CompleteScenario,
    [ProtoEnum(Name = "CollectionEventAppearConditionType_CompleteChallengeLevel", Value = 2)] CollectionEventAppearConditionType_CompleteChallengeLevel,
  }
}
