﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataUnchartedScoreInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataUnchartedScoreInfo")]
  [Serializable]
  public class ConfigDataUnchartedScoreInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _PageImage;
    private string _UIState;
    private string _BackgroundImage;
    private string _ScoreName;
    private List<Int32Pair> _BonusHeroIdList;
    private int _UnchartedScoreRewardGroupId;
    private int _ScoreItemId;
    private List<int> _ChallengeLevelIdList;
    private List<int> _ScoreLevelIdList;
    private int _ScoreLevelBonusCount;
    private int _ScoreLevelBonus;
    private List<int> _ModelIdList;
    private IExtension extensionObject;
    public Dictionary<int, int> HeroId2Bonus;
    public Dictionary<int, ConfigDataChallengeLevelInfo> Id2ChallengeLevelInfos;
    public Dictionary<int, ConfigDataScoreLevelInfo> Id2ScoreLevelInfos;
    public Dictionary<int, List<Goods>> Score2GoodsList;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataUnchartedScoreInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string PageImage
    {
      get
      {
        return this._PageImage;
      }
      set
      {
        this._PageImage = value;
      }
    }

    [ProtoMember(5)]
    public string UIState
    {
      get
      {
        return this._UIState;
      }
      set
      {
        this._UIState = value;
      }
    }

    [ProtoMember(6)]
    public string BackgroundImage
    {
      get
      {
        return this._BackgroundImage;
      }
      set
      {
        this._BackgroundImage = value;
      }
    }

    [ProtoMember(7)]
    public string ScoreName
    {
      get
      {
        return this._ScoreName;
      }
      set
      {
        this._ScoreName = value;
      }
    }

    [ProtoMember(8)]
    public List<Int32Pair> BonusHeroIdList
    {
      get
      {
        return this._BonusHeroIdList;
      }
      set
      {
        this._BonusHeroIdList = value;
      }
    }

    [ProtoMember(9)]
    public int UnchartedScoreRewardGroupId
    {
      get
      {
        return this._UnchartedScoreRewardGroupId;
      }
      set
      {
        this._UnchartedScoreRewardGroupId = value;
      }
    }

    [ProtoMember(10)]
    public int ScoreItemId
    {
      get
      {
        return this._ScoreItemId;
      }
      set
      {
        this._ScoreItemId = value;
      }
    }

    [ProtoMember(11)]
    public List<int> ChallengeLevelIdList
    {
      get
      {
        return this._ChallengeLevelIdList;
      }
      set
      {
        this._ChallengeLevelIdList = value;
      }
    }

    [ProtoMember(12)]
    public List<int> ScoreLevelIdList
    {
      get
      {
        return this._ScoreLevelIdList;
      }
      set
      {
        this._ScoreLevelIdList = value;
      }
    }

    [ProtoMember(13)]
    public int ScoreLevelBonusCount
    {
      get
      {
        return this._ScoreLevelBonusCount;
      }
      set
      {
        this._ScoreLevelBonusCount = value;
      }
    }

    [ProtoMember(14)]
    public int ScoreLevelBonus
    {
      get
      {
        return this._ScoreLevelBonus;
      }
      set
      {
        this._ScoreLevelBonus = value;
      }
    }

    [ProtoMember(15)]
    public List<int> ModelIdList
    {
      get
      {
        return this._ModelIdList;
      }
      set
      {
        this._ModelIdList = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
