﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataJobInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataJobInfo")]
  [Serializable]
  public class ConfigDataJobInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private string _Name_Eng;
    private int _Rank;
    private int _ChangeJobGold;
    private string _UnlockText;
    private int _Army_ID;
    private bool _IsMelee;
    private MoveType _MoveType;
    private int _BF_AttackDistance;
    private int _MeleeATK_ID;
    private int _RangeATK_ID;
    private int _MoveSPD_INI;
    private int _BF_MovePoint;
    private int _BF_ActionValue;
    private int _Behavior;
    private int _DieFlyDistanceMin;
    private int _DieFlyDistanceMax;
    private PropertyModifyType _Property1_ID;
    private int _Property1_Value;
    private PropertyModifyType _Property2_ID;
    private int _Property2_Value;
    private PropertyModifyType _Property3_ID;
    private int _Property3_Value;
    private List<int> _AdvantagePropertyIds;
    private string _JobIcon;
    private int _BattlePowerHP;
    private int _BattlePowerAT;
    private int _BattlePowerMagic;
    private int _BattlePowerDF;
    private int _BattlePowerMagicDF;
    private int _BattlePowerDEX;
    private List<int> _Skills_ID;
    private IExtension extensionObject;
    public ConfigDataArmyInfo m_armyInfo;
    public ConfigDataSkillInfo m_meleeSkillInfo;
    public ConfigDataSkillInfo m_rangeSkillInfo;
    public ConfigDataSkillInfo[] m_jobSkillInfos;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataJobInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(7)]
    public string Name_Eng
    {
      get
      {
        return this._Name_Eng;
      }
      set
      {
        this._Name_Eng = value;
      }
    }

    [ProtoMember(8)]
    public int Rank
    {
      get
      {
        return this._Rank;
      }
      set
      {
        this._Rank = value;
      }
    }

    [ProtoMember(10)]
    public int ChangeJobGold
    {
      get
      {
        return this._ChangeJobGold;
      }
      set
      {
        this._ChangeJobGold = value;
      }
    }

    [ProtoMember(11)]
    public string UnlockText
    {
      get
      {
        return this._UnlockText;
      }
      set
      {
        this._UnlockText = value;
      }
    }

    [ProtoMember(12)]
    public int Army_ID
    {
      get
      {
        return this._Army_ID;
      }
      set
      {
        this._Army_ID = value;
      }
    }

    [ProtoMember(13)]
    public bool IsMelee
    {
      get
      {
        return this._IsMelee;
      }
      set
      {
        this._IsMelee = value;
      }
    }

    [ProtoMember(14)]
    public MoveType MoveType
    {
      get
      {
        return this._MoveType;
      }
      set
      {
        this._MoveType = value;
      }
    }

    [ProtoMember(15)]
    public int BF_AttackDistance
    {
      get
      {
        return this._BF_AttackDistance;
      }
      set
      {
        this._BF_AttackDistance = value;
      }
    }

    [ProtoMember(16)]
    public int MeleeATK_ID
    {
      get
      {
        return this._MeleeATK_ID;
      }
      set
      {
        this._MeleeATK_ID = value;
      }
    }

    [ProtoMember(17)]
    public int RangeATK_ID
    {
      get
      {
        return this._RangeATK_ID;
      }
      set
      {
        this._RangeATK_ID = value;
      }
    }

    [ProtoMember(18)]
    public int MoveSPD_INI
    {
      get
      {
        return this._MoveSPD_INI;
      }
      set
      {
        this._MoveSPD_INI = value;
      }
    }

    [ProtoMember(19)]
    public int BF_MovePoint
    {
      get
      {
        return this._BF_MovePoint;
      }
      set
      {
        this._BF_MovePoint = value;
      }
    }

    [ProtoMember(20)]
    public int BF_ActionValue
    {
      get
      {
        return this._BF_ActionValue;
      }
      set
      {
        this._BF_ActionValue = value;
      }
    }

    [ProtoMember(21)]
    public int Behavior
    {
      get
      {
        return this._Behavior;
      }
      set
      {
        this._Behavior = value;
      }
    }

    [ProtoMember(22)]
    public int DieFlyDistanceMin
    {
      get
      {
        return this._DieFlyDistanceMin;
      }
      set
      {
        this._DieFlyDistanceMin = value;
      }
    }

    [ProtoMember(23)]
    public int DieFlyDistanceMax
    {
      get
      {
        return this._DieFlyDistanceMax;
      }
      set
      {
        this._DieFlyDistanceMax = value;
      }
    }

    [ProtoMember(24)]
    public PropertyModifyType Property1_ID
    {
      get
      {
        return this._Property1_ID;
      }
      set
      {
        this._Property1_ID = value;
      }
    }

    [ProtoMember(25)]
    public int Property1_Value
    {
      get
      {
        return this._Property1_Value;
      }
      set
      {
        this._Property1_Value = value;
      }
    }

    [ProtoMember(26)]
    public PropertyModifyType Property2_ID
    {
      get
      {
        return this._Property2_ID;
      }
      set
      {
        this._Property2_ID = value;
      }
    }

    [ProtoMember(27)]
    public int Property2_Value
    {
      get
      {
        return this._Property2_Value;
      }
      set
      {
        this._Property2_Value = value;
      }
    }

    [ProtoMember(28)]
    public PropertyModifyType Property3_ID
    {
      get
      {
        return this._Property3_ID;
      }
      set
      {
        this._Property3_ID = value;
      }
    }

    [ProtoMember(29)]
    public int Property3_Value
    {
      get
      {
        return this._Property3_Value;
      }
      set
      {
        this._Property3_Value = value;
      }
    }

    [ProtoMember(31)]
    public List<int> AdvantagePropertyIds
    {
      get
      {
        return this._AdvantagePropertyIds;
      }
      set
      {
        this._AdvantagePropertyIds = value;
      }
    }

    [ProtoMember(32)]
    public string JobIcon
    {
      get
      {
        return this._JobIcon;
      }
      set
      {
        this._JobIcon = value;
      }
    }

    [ProtoMember(33)]
    public int BattlePowerHP
    {
      get
      {
        return this._BattlePowerHP;
      }
      set
      {
        this._BattlePowerHP = value;
      }
    }

    [ProtoMember(34)]
    public int BattlePowerAT
    {
      get
      {
        return this._BattlePowerAT;
      }
      set
      {
        this._BattlePowerAT = value;
      }
    }

    [ProtoMember(35)]
    public int BattlePowerMagic
    {
      get
      {
        return this._BattlePowerMagic;
      }
      set
      {
        this._BattlePowerMagic = value;
      }
    }

    [ProtoMember(36)]
    public int BattlePowerDF
    {
      get
      {
        return this._BattlePowerDF;
      }
      set
      {
        this._BattlePowerDF = value;
      }
    }

    [ProtoMember(37)]
    public int BattlePowerMagicDF
    {
      get
      {
        return this._BattlePowerMagicDF;
      }
      set
      {
        this._BattlePowerMagicDF = value;
      }
    }

    [ProtoMember(38)]
    public int BattlePowerDEX
    {
      get
      {
        return this._BattlePowerDEX;
      }
      set
      {
        this._BattlePowerDEX = value;
      }
    }

    [ProtoMember(39)]
    public List<int> Skills_ID
    {
      get
      {
        return this._Skills_ID;
      }
      set
      {
        this._Skills_ID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
