﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataCollectionActivityWaypointInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataCollectionActivityWaypointInfo")]
  [Serializable]
  public class ConfigDataCollectionActivityWaypointInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private CollectionActivityWaypointFuncType _FuncType;
    private List<int> _Waypoints_ID;
    private string _Model;
    private CollectionActivityWaypointStateType _InitState;
    private List<int> _ScenarioLevelIdList;
    private List<int> _ChallengeLevelIdList;
    private List<int> _LootLevelIdList;
    private List<int> _EventIdList;
    private IExtension extensionObject;
    public ConfigDataCollectionActivityInfo CollectionActivity;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityWaypointInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(5)]
    public CollectionActivityWaypointFuncType FuncType
    {
      get
      {
        return this._FuncType;
      }
      set
      {
        this._FuncType = value;
      }
    }

    [ProtoMember(6)]
    public List<int> Waypoints_ID
    {
      get
      {
        return this._Waypoints_ID;
      }
      set
      {
        this._Waypoints_ID = value;
      }
    }

    [ProtoMember(7)]
    public string Model
    {
      get
      {
        return this._Model;
      }
      set
      {
        this._Model = value;
      }
    }

    [ProtoMember(8)]
    public CollectionActivityWaypointStateType InitState
    {
      get
      {
        return this._InitState;
      }
      set
      {
        this._InitState = value;
      }
    }

    [ProtoMember(9)]
    public List<int> ScenarioLevelIdList
    {
      get
      {
        return this._ScenarioLevelIdList;
      }
      set
      {
        this._ScenarioLevelIdList = value;
      }
    }

    [ProtoMember(10)]
    public List<int> ChallengeLevelIdList
    {
      get
      {
        return this._ChallengeLevelIdList;
      }
      set
      {
        this._ChallengeLevelIdList = value;
      }
    }

    [ProtoMember(11)]
    public List<int> LootLevelIdList
    {
      get
      {
        return this._LootLevelIdList;
      }
      set
      {
        this._LootLevelIdList = value;
      }
    }

    [ProtoMember(12)]
    public List<int> EventIdList
    {
      get
      {
        return this._EventIdList;
      }
      set
      {
        this._EventIdList = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
