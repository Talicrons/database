﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataVersionInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataVersionInfo")]
  [Serializable]
  public class ConfigDataVersionInfo : IExtensible
  {
    private int _ID;
    private int _Value;
    private string _StringValue;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(5)]
    public int Value
    {
      get
      {
        return this._Value;
      }
      set
      {
        this._Value = value;
      }
    }

    [ProtoMember(6)]
    public string StringValue
    {
      get
      {
        return this._StringValue;
      }
      set
      {
        this._StringValue = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
