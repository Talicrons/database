﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataEternalShrineLevelInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataEternalShrineLevelInfo")]
  [Serializable]
  public class ConfigDataEternalShrineLevelInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _TeamName;
    private int _PreLevel;
    private int _TicketCost;
    private int _EnergySuccess;
    private int _EnergyFail;
    private int _TeamEnergyCost;
    private int _MonsterLevel;
    private int _Battle_ID;
    private int _PlayerExp;
    private int _HeroExp;
    private int _Gold;
    private int _Drop_ID;
    private int _TeamDrop_ID;
    private int _OperationalActivityDrop_ID;
    private int _DayBonusDrop_ID;
    private int _DisplayRewardCount;
    private string _Strategy;
    private IExtension extensionObject;
    public ConfigDataBattleInfo m_battleInfo;
    public ConfigDataEternalShrineInfo m_groupInfo;
    public ConfigDataRandomDropRewardInfo m_randomDropInfo;
    public ConfigDataRandomDropRewardInfo m_activityRandomDropInfo;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string TeamName
    {
      get
      {
        return this._TeamName;
      }
      set
      {
        this._TeamName = value;
      }
    }

    [ProtoMember(5)]
    public int PreLevel
    {
      get
      {
        return this._PreLevel;
      }
      set
      {
        this._PreLevel = value;
      }
    }

    [ProtoMember(6)]
    public int TicketCost
    {
      get
      {
        return this._TicketCost;
      }
      set
      {
        this._TicketCost = value;
      }
    }

    [ProtoMember(7)]
    public int EnergySuccess
    {
      get
      {
        return this._EnergySuccess;
      }
      set
      {
        this._EnergySuccess = value;
      }
    }

    [ProtoMember(8)]
    public int EnergyFail
    {
      get
      {
        return this._EnergyFail;
      }
      set
      {
        this._EnergyFail = value;
      }
    }

    [ProtoMember(9)]
    public int TeamEnergyCost
    {
      get
      {
        return this._TeamEnergyCost;
      }
      set
      {
        this._TeamEnergyCost = value;
      }
    }

    [ProtoMember(10)]
    public int MonsterLevel
    {
      get
      {
        return this._MonsterLevel;
      }
      set
      {
        this._MonsterLevel = value;
      }
    }

    [ProtoMember(11)]
    public int Battle_ID
    {
      get
      {
        return this._Battle_ID;
      }
      set
      {
        this._Battle_ID = value;
      }
    }

    [ProtoMember(12)]
    public int PlayerExp
    {
      get
      {
        return this._PlayerExp;
      }
      set
      {
        this._PlayerExp = value;
      }
    }

    [ProtoMember(13)]
    public int HeroExp
    {
      get
      {
        return this._HeroExp;
      }
      set
      {
        this._HeroExp = value;
      }
    }

    [ProtoMember(14)]
    public int Gold
    {
      get
      {
        return this._Gold;
      }
      set
      {
        this._Gold = value;
      }
    }

    [ProtoMember(15)]
    public int Drop_ID
    {
      get
      {
        return this._Drop_ID;
      }
      set
      {
        this._Drop_ID = value;
      }
    }

    [ProtoMember(16)]
    public int TeamDrop_ID
    {
      get
      {
        return this._TeamDrop_ID;
      }
      set
      {
        this._TeamDrop_ID = value;
      }
    }

    [ProtoMember(17)]
    public int OperationalActivityDrop_ID
    {
      get
      {
        return this._OperationalActivityDrop_ID;
      }
      set
      {
        this._OperationalActivityDrop_ID = value;
      }
    }

    [ProtoMember(18)]
    public int DayBonusDrop_ID
    {
      get
      {
        return this._DayBonusDrop_ID;
      }
      set
      {
        this._DayBonusDrop_ID = value;
      }
    }

    [ProtoMember(19)]
    public int DisplayRewardCount
    {
      get
      {
        return this._DisplayRewardCount;
      }
      set
      {
        this._DisplayRewardCount = value;
      }
    }

    [ProtoMember(21)]
    public string Strategy
    {
      get
      {
        return this._Strategy;
      }
      set
      {
        this._Strategy = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
