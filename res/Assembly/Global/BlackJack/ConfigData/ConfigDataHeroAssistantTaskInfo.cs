﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataHeroAssistantTaskInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataHeroAssistantTaskInfo")]
  [Serializable]
  public class ConfigDataHeroAssistantTaskInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private List<string> _Resource;
    private int _RequiredUserLevel;
    private int _CompletePoints;
    private List<CompleteValueDropID> _Rewards;
    private IExtension extensionObject;
    public ConfigDataHeroAssistantTaskScheduleInfo m_schedule;
    public ConfigDataHeroAssistantTaskGeneralInfo m_general;
    public List<int> m_rewardCompleteRate;
    public List<int> m_rewardDropId;
    public List<int> m_rewardWorkSeconds;
    public List<int> m_rewardDropCount;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroAssistantTaskInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public List<string> Resource
    {
      get
      {
        return this._Resource;
      }
      set
      {
        this._Resource = value;
      }
    }

    [ProtoMember(5)]
    public int RequiredUserLevel
    {
      get
      {
        return this._RequiredUserLevel;
      }
      set
      {
        this._RequiredUserLevel = value;
      }
    }

    [ProtoMember(6)]
    public int CompletePoints
    {
      get
      {
        return this._CompletePoints;
      }
      set
      {
        this._CompletePoints = value;
      }
    }

    [ProtoMember(7)]
    public List<CompleteValueDropID> Rewards
    {
      get
      {
        return this._Rewards;
      }
      set
      {
        this._Rewards = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
