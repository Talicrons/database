﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataBehavior
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using BlackJack.ProjectL.Common;
using FixMath.NET;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataBehavior")]
  [Serializable]
  public class ConfigDataBehavior : IExtensible
  {
    private int _ID;
    private string _Desc;
    private SelectTarget _MoveTarget;
    private string _MTParam;
    private BehaviorCondition _StartAttackCondition;
    private string _StartACParam;
    private SelectTarget _SelectAttackTarget;
    private string _SATParam;
    private BehaviorCondition _ReselectAttackTargetCondition;
    private string _RATCParam;
    private SelectSkill _SelectSkill;
    private string _SSParam;
    private BehaviorCondition _StopAttackCondition;
    private string _StopACParam;
    private List<int> _BehaviorChangeRules;
    private IExtension extensionObject;
    public ConfigDataBehavior.ParamData RATCParamData;
    public ConfigDataBehavior.ParamData MTParamData;
    public ConfigDataBehavior.ParamData StartACParamData;
    public ConfigDataBehavior.ParamData StopACParamData;
    public ConfigDataBehavior.ParamData SATParamData;
    public ConfigDataBehavior.ParamData SSParamData;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBehavior()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(4)]
    public SelectTarget MoveTarget
    {
      get
      {
        return this._MoveTarget;
      }
      set
      {
        this._MoveTarget = value;
      }
    }

    [ProtoMember(5)]
    public string MTParam
    {
      get
      {
        return this._MTParam;
      }
      set
      {
        this._MTParam = value;
      }
    }

    [ProtoMember(6)]
    public BehaviorCondition StartAttackCondition
    {
      get
      {
        return this._StartAttackCondition;
      }
      set
      {
        this._StartAttackCondition = value;
      }
    }

    [ProtoMember(7)]
    public string StartACParam
    {
      get
      {
        return this._StartACParam;
      }
      set
      {
        this._StartACParam = value;
      }
    }

    [ProtoMember(8)]
    public SelectTarget SelectAttackTarget
    {
      get
      {
        return this._SelectAttackTarget;
      }
      set
      {
        this._SelectAttackTarget = value;
      }
    }

    [ProtoMember(9)]
    public string SATParam
    {
      get
      {
        return this._SATParam;
      }
      set
      {
        this._SATParam = value;
      }
    }

    [ProtoMember(10)]
    public BehaviorCondition ReselectAttackTargetCondition
    {
      get
      {
        return this._ReselectAttackTargetCondition;
      }
      set
      {
        this._ReselectAttackTargetCondition = value;
      }
    }

    [ProtoMember(11)]
    public string RATCParam
    {
      get
      {
        return this._RATCParam;
      }
      set
      {
        this._RATCParam = value;
      }
    }

    [ProtoMember(12)]
    public SelectSkill SelectSkill
    {
      get
      {
        return this._SelectSkill;
      }
      set
      {
        this._SelectSkill = value;
      }
    }

    [ProtoMember(13)]
    public string SSParam
    {
      get
      {
        return this._SSParam;
      }
      set
      {
        this._SSParam = value;
      }
    }

    [ProtoMember(14)]
    public BehaviorCondition StopAttackCondition
    {
      get
      {
        return this._StopAttackCondition;
      }
      set
      {
        this._StopAttackCondition = value;
      }
    }

    [ProtoMember(15)]
    public string StopACParam
    {
      get
      {
        return this._StopACParam;
      }
      set
      {
        this._StopACParam = value;
      }
    }

    [ProtoMember(16)]
    public List<int> BehaviorChangeRules
    {
      get
      {
        return this._BehaviorChangeRules;
      }
      set
      {
        this._BehaviorChangeRules = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataBehavior.ParamData InitParamData(
      BehaviorCondition c,
      string param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool TryParsePositionList(string param, out List<GridPosition> positions)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataBehavior.ParamData InitParamData(
      SelectTarget t,
      string param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool TryParseIntArray(string s, out int[] ret)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataBehavior.ParamData InitParamData(
      SelectSkill s,
      string param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Initialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public class ParamData
    {
      public float _float;
      public int _int;
      public List<GridPosition> _GridPositions;
      public int[] _ints;
      public int[] _ints2;

      public Fix64 _fix64
      {
        get
        {
          return (Fix64) this._float;
        }
      }
    }
  }
}
