﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataStoreCurrencyInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataStoreCurrencyInfo")]
  [Serializable]
  public class ConfigDataStoreCurrencyInfo : IExtensible
  {
    private int _ID;
    private string _CurrencyType;
    private string _CurrencyImage;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string CurrencyType
    {
      get
      {
        return this._CurrencyType;
      }
      set
      {
        this._CurrencyType = value;
      }
    }

    [ProtoMember(4)]
    public string CurrencyImage
    {
      get
      {
        return this._CurrencyImage;
      }
      set
      {
        this._CurrencyImage = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
