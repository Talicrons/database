﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.TrainingTechResourceRequirements
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  public class TrainingTechResourceRequirements
  {
    public int Gold;
    public int RoomLevel;
    public List<int> PreTechs;
    public List<int> PreTechLevels;
    public List<Goods> Materials;

    [MethodImpl((MethodImplOptions) 32768)]
    public static TrainingTechResourceRequirements operator +(
      TrainingTechResourceRequirements x,
      TrainingTechResourceRequirements y)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
