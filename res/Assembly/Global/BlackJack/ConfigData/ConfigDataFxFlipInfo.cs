﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataFxFlipInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataFxFlipInfo")]
  [Serializable]
  public class ConfigDataFxFlipInfo : IExtensible
  {
    private int _ID;
    private string _DefaultName;
    private string _FlipName;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string DefaultName
    {
      get
      {
        return this._DefaultName;
      }
      set
      {
        this._DefaultName = value;
      }
    }

    [ProtoMember(4)]
    public string FlipName
    {
      get
      {
        return this._FlipName;
      }
      set
      {
        this._FlipName = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
