﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroHeartFetterUnlockCondition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroHeartFetterUnlockCondition")]
  [Serializable]
  public class HeroHeartFetterUnlockCondition : IExtensible
  {
    private HeroHeartFetterUnlockConditionType _ConditionType;
    private int _Parm1;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public HeroHeartFetterUnlockConditionType ConditionType
    {
      get
      {
        return this._ConditionType;
      }
      set
      {
        this._ConditionType = value;
      }
    }

    [ProtoMember(2)]
    public int Parm1
    {
      get
      {
        return this._Parm1;
      }
      set
      {
        this._Parm1 = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
