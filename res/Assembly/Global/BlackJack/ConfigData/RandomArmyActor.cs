﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RandomArmyActor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RandomArmyActor")]
  [Serializable]
  public class RandomArmyActor : IExtensible
  {
    private int _HeroID;
    private int _Level;
    private int _AI;
    private int _Weight;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int HeroID
    {
      get
      {
        return this._HeroID;
      }
      set
      {
        this._HeroID = value;
      }
    }

    [ProtoMember(2)]
    public int Level
    {
      get
      {
        return this._Level;
      }
      set
      {
        this._Level = value;
      }
    }

    [ProtoMember(3)]
    public int AI
    {
      get
      {
        return this._AI;
      }
      set
      {
        this._AI = value;
      }
    }

    [ProtoMember(4)]
    public int Weight
    {
      get
      {
        return this._Weight;
      }
      set
      {
        this._Weight = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
