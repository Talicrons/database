﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataEventGiftInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataEventGiftInfo")]
  [Serializable]
  public class ConfigDataEventGiftInfo : IExtensible
  {
    private int _ID;
    private int _GiftId;
    private EventGiftUnlockConditionType _UnlockConditionType;
    private int _Param1;
    private int _Param2;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public int GiftId
    {
      get
      {
        return this._GiftId;
      }
      set
      {
        this._GiftId = value;
      }
    }

    [ProtoMember(4)]
    public EventGiftUnlockConditionType UnlockConditionType
    {
      get
      {
        return this._UnlockConditionType;
      }
      set
      {
        this._UnlockConditionType = value;
      }
    }

    [ProtoMember(5)]
    public int Param1
    {
      get
      {
        return this._Param1;
      }
      set
      {
        this._Param1 = value;
      }
    }

    [ProtoMember(6)]
    public int Param2
    {
      get
      {
        return this._Param2;
      }
      set
      {
        this._Param2 = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
