﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataSelectProbabilityInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataSelectProbabilityInfo")]
  [Serializable]
  public class ConfigDataSelectProbabilityInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _SSRCardProbability;
    private string _SRCardProbability;
    private string _RCardProbability;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string SSRCardProbability
    {
      get
      {
        return this._SSRCardProbability;
      }
      set
      {
        this._SSRCardProbability = value;
      }
    }

    [ProtoMember(5)]
    public string SRCardProbability
    {
      get
      {
        return this._SRCardProbability;
      }
      set
      {
        this._SRCardProbability = value;
      }
    }

    [ProtoMember(6)]
    public string RCardProbability
    {
      get
      {
        return this._RCardProbability;
      }
      set
      {
        this._RCardProbability = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
