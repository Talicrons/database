﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataCutsceneInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataCutsceneInfo")]
  [Serializable]
  public class ConfigDataCutsceneInfo : IExtensible
  {
    private int _ID;
    private string _Head;
    private string _Sound1;
    private string _Sound2;
    private string _Sound3;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(7)]
    public string Head
    {
      get
      {
        return this._Head;
      }
      set
      {
        this._Head = value;
      }
    }

    [ProtoMember(9)]
    public string Sound1
    {
      get
      {
        return this._Sound1;
      }
      set
      {
        this._Sound1 = value;
      }
    }

    [ProtoMember(10)]
    public string Sound2
    {
      get
      {
        return this._Sound2;
      }
      set
      {
        this._Sound2 = value;
      }
    }

    [ProtoMember(11)]
    public string Sound3
    {
      get
      {
        return this._Sound3;
      }
      set
      {
        this._Sound3 = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
