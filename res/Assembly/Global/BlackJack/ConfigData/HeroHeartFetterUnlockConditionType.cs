﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroHeartFetterUnlockConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroHeartFetterUnlockConditionType")]
  public enum HeroHeartFetterUnlockConditionType
  {
    [ProtoEnum(Name = "HeroHeartFetterUnlockConditionType_None", Value = 0)] HeroHeartFetterUnlockConditionType_None,
    [ProtoEnum(Name = "HeroHeartFetterUnlockConditionType_HeroFetterLevel", Value = 1)] HeroHeartFetterUnlockConditionType_HeroFetterLevel,
    [ProtoEnum(Name = "HeroHeartFetterUnlockConditionType_HeroLevel", Value = 2)] HeroHeartFetterUnlockConditionType_HeroLevel,
  }
}
