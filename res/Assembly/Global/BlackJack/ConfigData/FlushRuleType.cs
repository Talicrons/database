﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.FlushRuleType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "FlushRuleType")]
  public enum FlushRuleType
  {
    [ProtoEnum(Name = "FlushRuleType_None", Value = 0)] FlushRuleType_None,
    [ProtoEnum(Name = "FlushRuleType_Period", Value = 1)] FlushRuleType_Period,
    [ProtoEnum(Name = "FlushRuleType_FixedTime", Value = 2)] FlushRuleType_FixedTime,
  }
}
