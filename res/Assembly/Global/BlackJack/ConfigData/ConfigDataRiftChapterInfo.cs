﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataRiftChapterInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataRiftChapterInfo")]
  [Serializable]
  public class ConfigDataRiftChapterInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _TitleName;
    private string _Desc;
    private string _Place;
    private List<RiftChapterInfoUnlockConditions> _UnlockConditions;
    private int _Hard;
    private int _HardChapter;
    private List<int> _RiftLevels_ID;
    private int _Reward1Star;
    private List<Goods> _Reward1;
    private int _Reward2Star;
    private List<Goods> _Reward2;
    private int _Reward3Star;
    private List<Goods> _Reward3;
    private string _Image;
    private string _ChapterBGPrefabName;
    private bool _IsOpened;
    private int _StoryOutlineInfoID;
    private IExtension extensionObject;
    public ConfigDataRiftChapterInfo m_hardChapterInfo;
    public ConfigDataRiftLevelInfo[] m_levelInfos;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRiftChapterInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5)]
    public string TitleName
    {
      get
      {
        return this._TitleName;
      }
      set
      {
        this._TitleName = value;
      }
    }

    [ProtoMember(6)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(8)]
    public string Place
    {
      get
      {
        return this._Place;
      }
      set
      {
        this._Place = value;
      }
    }

    [ProtoMember(10)]
    public List<RiftChapterInfoUnlockConditions> UnlockConditions
    {
      get
      {
        return this._UnlockConditions;
      }
      set
      {
        this._UnlockConditions = value;
      }
    }

    [ProtoMember(11)]
    public int Hard
    {
      get
      {
        return this._Hard;
      }
      set
      {
        this._Hard = value;
      }
    }

    [ProtoMember(12)]
    public int HardChapter
    {
      get
      {
        return this._HardChapter;
      }
      set
      {
        this._HardChapter = value;
      }
    }

    [ProtoMember(13)]
    public List<int> RiftLevels_ID
    {
      get
      {
        return this._RiftLevels_ID;
      }
      set
      {
        this._RiftLevels_ID = value;
      }
    }

    [ProtoMember(14)]
    public int Reward1Star
    {
      get
      {
        return this._Reward1Star;
      }
      set
      {
        this._Reward1Star = value;
      }
    }

    [ProtoMember(15)]
    public List<Goods> Reward1
    {
      get
      {
        return this._Reward1;
      }
      set
      {
        this._Reward1 = value;
      }
    }

    [ProtoMember(16)]
    public int Reward2Star
    {
      get
      {
        return this._Reward2Star;
      }
      set
      {
        this._Reward2Star = value;
      }
    }

    [ProtoMember(17)]
    public List<Goods> Reward2
    {
      get
      {
        return this._Reward2;
      }
      set
      {
        this._Reward2 = value;
      }
    }

    [ProtoMember(18)]
    public int Reward3Star
    {
      get
      {
        return this._Reward3Star;
      }
      set
      {
        this._Reward3Star = value;
      }
    }

    [ProtoMember(19)]
    public List<Goods> Reward3
    {
      get
      {
        return this._Reward3;
      }
      set
      {
        this._Reward3 = value;
      }
    }

    [ProtoMember(20)]
    public string Image
    {
      get
      {
        return this._Image;
      }
      set
      {
        this._Image = value;
      }
    }

    [ProtoMember(21)]
    public string ChapterBGPrefabName
    {
      get
      {
        return this._ChapterBGPrefabName;
      }
      set
      {
        this._ChapterBGPrefabName = value;
      }
    }

    [ProtoMember(22)]
    public bool IsOpened
    {
      get
      {
        return this._IsOpened;
      }
      set
      {
        this._IsOpened = value;
      }
    }

    [ProtoMember(23)]
    public int StoryOutlineInfoID
    {
      get
      {
        return this._StoryOutlineInfoID;
      }
      set
      {
        this._StoryOutlineInfoID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
