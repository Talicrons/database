﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ExpressionPurposeType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ExpressionPurposeType")]
  public enum ExpressionPurposeType
  {
    [ProtoEnum(Name = "ExpressionPurposeType_Chat", Value = 1)] ExpressionPurposeType_Chat = 1,
    [ProtoEnum(Name = "ExpressionPurposeType_Combat", Value = 2)] ExpressionPurposeType_Combat = 2,
  }
}
