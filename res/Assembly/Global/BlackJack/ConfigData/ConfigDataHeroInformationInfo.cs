﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataHeroInformationInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataHeroInformationInfo")]
  [Serializable]
  public class ConfigDataHeroInformationInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private List<int> _HeroBiographies_ID;
    private List<int> _HeroFetters_ID;
    private int _HeroConfession;
    private int _HeroPerformance_ID;
    private bool _IsDungeonLevelsUnLock;
    private List<int> _DungeonLevels_ID;
    private int _Reward1Star;
    private List<Goods> _Reward1;
    private int _Reward2Star;
    private List<Goods> _Reward2;
    private int _Reward3Star;
    private List<Goods> _Reward3;
    private int _FavorabilityMaxLevel;
    private int _FavourabilityLevelUpReward;
    private List<HeroInteractor> _HeroTouch;
    private int _SendRewardsPerformance;
    private List<int> _InteractHeroPerformances_ID;
    private int _HeroHeartFetterId;
    private IExtension extensionObject;
    public List<ConfigDataHeroFetterInfo> m_fetterInfos;
    public ConfigDataHeroConfessionInfo m_confessionInfo;
    public ConfigDataHeroInfo m_heroInfo;
    public ConfigDataHeroHeartFetterInfo m_heartFetterInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroInformationInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public List<int> HeroBiographies_ID
    {
      get
      {
        return this._HeroBiographies_ID;
      }
      set
      {
        this._HeroBiographies_ID = value;
      }
    }

    [ProtoMember(5)]
    public List<int> HeroFetters_ID
    {
      get
      {
        return this._HeroFetters_ID;
      }
      set
      {
        this._HeroFetters_ID = value;
      }
    }

    [ProtoMember(6)]
    public int HeroConfession
    {
      get
      {
        return this._HeroConfession;
      }
      set
      {
        this._HeroConfession = value;
      }
    }

    [ProtoMember(7)]
    public int HeroPerformance_ID
    {
      get
      {
        return this._HeroPerformance_ID;
      }
      set
      {
        this._HeroPerformance_ID = value;
      }
    }

    [ProtoMember(8)]
    public bool IsDungeonLevelsUnLock
    {
      get
      {
        return this._IsDungeonLevelsUnLock;
      }
      set
      {
        this._IsDungeonLevelsUnLock = value;
      }
    }

    [ProtoMember(9)]
    public List<int> DungeonLevels_ID
    {
      get
      {
        return this._DungeonLevels_ID;
      }
      set
      {
        this._DungeonLevels_ID = value;
      }
    }

    [ProtoMember(10)]
    public int Reward1Star
    {
      get
      {
        return this._Reward1Star;
      }
      set
      {
        this._Reward1Star = value;
      }
    }

    [ProtoMember(11)]
    public List<Goods> Reward1
    {
      get
      {
        return this._Reward1;
      }
      set
      {
        this._Reward1 = value;
      }
    }

    [ProtoMember(12)]
    public int Reward2Star
    {
      get
      {
        return this._Reward2Star;
      }
      set
      {
        this._Reward2Star = value;
      }
    }

    [ProtoMember(13)]
    public List<Goods> Reward2
    {
      get
      {
        return this._Reward2;
      }
      set
      {
        this._Reward2 = value;
      }
    }

    [ProtoMember(14)]
    public int Reward3Star
    {
      get
      {
        return this._Reward3Star;
      }
      set
      {
        this._Reward3Star = value;
      }
    }

    [ProtoMember(15)]
    public List<Goods> Reward3
    {
      get
      {
        return this._Reward3;
      }
      set
      {
        this._Reward3 = value;
      }
    }

    [ProtoMember(16)]
    public int FavorabilityMaxLevel
    {
      get
      {
        return this._FavorabilityMaxLevel;
      }
      set
      {
        this._FavorabilityMaxLevel = value;
      }
    }

    [ProtoMember(17)]
    public int FavourabilityLevelUpReward
    {
      get
      {
        return this._FavourabilityLevelUpReward;
      }
      set
      {
        this._FavourabilityLevelUpReward = value;
      }
    }

    [ProtoMember(18)]
    public List<HeroInteractor> HeroTouch
    {
      get
      {
        return this._HeroTouch;
      }
      set
      {
        this._HeroTouch = value;
      }
    }

    [ProtoMember(19)]
    public int SendRewardsPerformance
    {
      get
      {
        return this._SendRewardsPerformance;
      }
      set
      {
        this._SendRewardsPerformance = value;
      }
    }

    [ProtoMember(20)]
    public List<int> InteractHeroPerformances_ID
    {
      get
      {
        return this._InteractHeroPerformances_ID;
      }
      set
      {
        this._InteractHeroPerformances_ID = value;
      }
    }

    [ProtoMember(21)]
    public int HeroHeartFetterId
    {
      get
      {
        return this._HeroHeartFetterId;
      }
      set
      {
        this._HeroHeartFetterId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
