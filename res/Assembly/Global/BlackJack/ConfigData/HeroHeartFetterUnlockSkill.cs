﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroHeartFetterUnlockSkill
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroHeartFetterUnlockSkill")]
  [Serializable]
  public class HeroHeartFetterUnlockSkill : IExtensible
  {
    private int _SkillId;
    private int _HeartFetterLevel;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int SkillId
    {
      get
      {
        return this._SkillId;
      }
      set
      {
        this._SkillId = value;
      }
    }

    [ProtoMember(2)]
    public int HeartFetterLevel
    {
      get
      {
        return this._HeartFetterLevel;
      }
      set
      {
        this._HeartFetterLevel = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
