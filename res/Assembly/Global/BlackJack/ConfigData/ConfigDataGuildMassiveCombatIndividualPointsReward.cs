﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataGuildMassiveCombatIndividualPointsRewardsInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataGuildMassiveCombatIndividualPointsRewardsInfo")]
  [Serializable]
  public class ConfigDataGuildMassiveCombatIndividualPointsRewardsInfo : IExtensible
  {
    private int _ID;
    private int _RewardGroupID;
    private List<Goods> _RewardItems;
    private int _Points;
    private string _Name;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGuildMassiveCombatIndividualPointsRewardsInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public int RewardGroupID
    {
      get
      {
        return this._RewardGroupID;
      }
      set
      {
        this._RewardGroupID = value;
      }
    }

    [ProtoMember(4)]
    public List<Goods> RewardItems
    {
      get
      {
        return this._RewardItems;
      }
      set
      {
        this._RewardItems = value;
      }
    }

    [ProtoMember(5)]
    public int Points
    {
      get
      {
        return this._Points;
      }
      set
      {
        this._Points = value;
      }
    }

    [ProtoMember(6)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
