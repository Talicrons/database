﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.PointRaceRewardInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "PointRaceRewardInfo")]
  [Serializable]
  public class PointRaceRewardInfo : IExtensible
  {
    private int _BattleNum;
    private int _DanId;
    private int _TemplateMailId;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int BattleNum
    {
      get
      {
        return this._BattleNum;
      }
      set
      {
        this._BattleNum = value;
      }
    }

    [ProtoMember(2)]
    public int DanId
    {
      get
      {
        return this._DanId;
      }
      set
      {
        this._DanId = value;
      }
    }

    [ProtoMember(3)]
    public int TemplateMailId
    {
      get
      {
        return this._TemplateMailId;
      }
      set
      {
        this._TemplateMailId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
