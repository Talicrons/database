﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.Behavioral
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "Behavioral")]
  [Serializable]
  public class Behavioral : IExtensible
  {
    private int _Boss;
    private int _BehavioralDesc;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int Boss
    {
      get
      {
        return this._Boss;
      }
      set
      {
        this._Boss = value;
      }
    }

    [ProtoMember(2)]
    public int BehavioralDesc
    {
      get
      {
        return this._BehavioralDesc;
      }
      set
      {
        this._BehavioralDesc = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
