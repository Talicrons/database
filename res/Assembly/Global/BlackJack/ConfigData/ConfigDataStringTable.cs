﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataStringTable
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataStringTable")]
  [Serializable]
  public class ConfigDataStringTable : IExtensible
  {
    private int _ID;
    private string _Value;
    private string _ValueStrKey;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(4)]
    public string Value
    {
      get
      {
        return this._Value;
      }
      set
      {
        this._Value = value;
      }
    }

    [ProtoMember(5)]
    public string ValueStrKey
    {
      get
      {
        return this._ValueStrKey;
      }
      set
      {
        this._ValueStrKey = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
