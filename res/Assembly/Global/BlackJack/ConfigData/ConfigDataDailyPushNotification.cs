﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataDailyPushNotification
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataDailyPushNotification")]
  [Serializable]
  public class ConfigDataDailyPushNotification : IExtensible
  {
    private int _ID;
    private string _Title;
    private string _Content;
    private int _Hour;
    private int _Minute;
    private UserGuideCondition _PushCondition;
    private string _PCParam;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Title
    {
      get
      {
        return this._Title;
      }
      set
      {
        this._Title = value;
      }
    }

    [ProtoMember(4)]
    public string Content
    {
      get
      {
        return this._Content;
      }
      set
      {
        this._Content = value;
      }
    }

    [ProtoMember(5)]
    public int Hour
    {
      get
      {
        return this._Hour;
      }
      set
      {
        this._Hour = value;
      }
    }

    [ProtoMember(6)]
    public int Minute
    {
      get
      {
        return this._Minute;
      }
      set
      {
        this._Minute = value;
      }
    }

    [ProtoMember(7)]
    public UserGuideCondition PushCondition
    {
      get
      {
        return this._PushCondition;
      }
      set
      {
        this._PushCondition = value;
      }
    }

    [ProtoMember(8)]
    public string PCParam
    {
      get
      {
        return this._PCParam;
      }
      set
      {
        this._PCParam = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
