﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataTreasureLevelInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataTreasureLevelInfo")]
  [Serializable]
  public class ConfigDataTreasureLevelInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _IconResource;
    private int _OpenPlayerLevel;
    private int _EnergyFail;
    private int _EnergySuccess;
    private int _TicketCost;
    private int _Battle_ID;
    private int _MonsterLevel;
    private int _PlayerExp;
    private int _HeroExp;
    private List<int> _Bandits_ID;
    private List<Goods> _RewardList;
    private string _Strategy;
    private IExtension extensionObject;
    public List<ConfigDataBanditInfo> m_banditInfos;
    public ConfigDataBattleInfo m_battleInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTreasureLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string IconResource
    {
      get
      {
        return this._IconResource;
      }
      set
      {
        this._IconResource = value;
      }
    }

    [ProtoMember(5)]
    public int OpenPlayerLevel
    {
      get
      {
        return this._OpenPlayerLevel;
      }
      set
      {
        this._OpenPlayerLevel = value;
      }
    }

    [ProtoMember(6)]
    public int EnergyFail
    {
      get
      {
        return this._EnergyFail;
      }
      set
      {
        this._EnergyFail = value;
      }
    }

    [ProtoMember(7)]
    public int EnergySuccess
    {
      get
      {
        return this._EnergySuccess;
      }
      set
      {
        this._EnergySuccess = value;
      }
    }

    [ProtoMember(8)]
    public int TicketCost
    {
      get
      {
        return this._TicketCost;
      }
      set
      {
        this._TicketCost = value;
      }
    }

    [ProtoMember(9)]
    public int Battle_ID
    {
      get
      {
        return this._Battle_ID;
      }
      set
      {
        this._Battle_ID = value;
      }
    }

    [ProtoMember(10)]
    public int MonsterLevel
    {
      get
      {
        return this._MonsterLevel;
      }
      set
      {
        this._MonsterLevel = value;
      }
    }

    [ProtoMember(11)]
    public int PlayerExp
    {
      get
      {
        return this._PlayerExp;
      }
      set
      {
        this._PlayerExp = value;
      }
    }

    [ProtoMember(12)]
    public int HeroExp
    {
      get
      {
        return this._HeroExp;
      }
      set
      {
        this._HeroExp = value;
      }
    }

    [ProtoMember(13)]
    public List<int> Bandits_ID
    {
      get
      {
        return this._Bandits_ID;
      }
      set
      {
        this._Bandits_ID = value;
      }
    }

    [ProtoMember(14)]
    public List<Goods> RewardList
    {
      get
      {
        return this._RewardList;
      }
      set
      {
        this._RewardList = value;
      }
    }

    [ProtoMember(15)]
    public string Strategy
    {
      get
      {
        return this._Strategy;
      }
      set
      {
        this._Strategy = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
