﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataDialogInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataDialogInfo")]
  [Serializable]
  public class ConfigDataDialogInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _NextDialog_ID;
    private int _FrameType;
    private string _PlaceName;
    private string _CharName;
    private int _CharImage_ID;
    private int _Position;
    private int _EnterType;
    private string _BGM;
    private int _LeaveType;
    private int _LeaveTime;
    private string _PreAnimation;
    private string _PreFacialAnimation;
    private string _IdleAnimation;
    private string _IdleFacialAnimation;
    private string _Background;
    private string _BackgroundImage;
    private int _BackgroundX;
    private string _Voice;
    private string _Words;
    private List<int> _BeforeEnterEffectGroup1;
    private List<int> _BeforeEnterEffectGroup2;
    private List<int> _BeforeEnterEffectGroup3;
    private int _BeforeEnterInsertEffect;
    private List<int> _BeforeTalkEffectGroup1;
    private List<int> _BeforeTalkEffectGroup2;
    private List<int> _BeforeTalkEffectGroup3;
    private int _BeforeTalkInsertEffect;
    private List<int> _AfterTalkEffectGroup1;
    private List<int> _AfterTalkEffectGroup2;
    private List<int> _AfterTalkEffectGroup3;
    private int _AfterTalkInsertEffect;
    private int _DialogGroupID;
    private List<ChoiceData> _ChoiceList;
    private int _CountdownTime;
    private List<Goods> _DialogRewards;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataDialogInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5)]
    public int NextDialog_ID
    {
      get
      {
        return this._NextDialog_ID;
      }
      set
      {
        this._NextDialog_ID = value;
      }
    }

    [ProtoMember(6)]
    public int FrameType
    {
      get
      {
        return this._FrameType;
      }
      set
      {
        this._FrameType = value;
      }
    }

    [ProtoMember(7)]
    public string PlaceName
    {
      get
      {
        return this._PlaceName;
      }
      set
      {
        this._PlaceName = value;
      }
    }

    [ProtoMember(9)]
    public string CharName
    {
      get
      {
        return this._CharName;
      }
      set
      {
        this._CharName = value;
      }
    }

    [ProtoMember(11)]
    public int CharImage_ID
    {
      get
      {
        return this._CharImage_ID;
      }
      set
      {
        this._CharImage_ID = value;
      }
    }

    [ProtoMember(12)]
    public int Position
    {
      get
      {
        return this._Position;
      }
      set
      {
        this._Position = value;
      }
    }

    [ProtoMember(13)]
    public int EnterType
    {
      get
      {
        return this._EnterType;
      }
      set
      {
        this._EnterType = value;
      }
    }

    [ProtoMember(14)]
    public string BGM
    {
      get
      {
        return this._BGM;
      }
      set
      {
        this._BGM = value;
      }
    }

    [ProtoMember(15)]
    public int LeaveType
    {
      get
      {
        return this._LeaveType;
      }
      set
      {
        this._LeaveType = value;
      }
    }

    [ProtoMember(16)]
    public int LeaveTime
    {
      get
      {
        return this._LeaveTime;
      }
      set
      {
        this._LeaveTime = value;
      }
    }

    [ProtoMember(17)]
    public string PreAnimation
    {
      get
      {
        return this._PreAnimation;
      }
      set
      {
        this._PreAnimation = value;
      }
    }

    [ProtoMember(18)]
    public string PreFacialAnimation
    {
      get
      {
        return this._PreFacialAnimation;
      }
      set
      {
        this._PreFacialAnimation = value;
      }
    }

    [ProtoMember(19)]
    public string IdleAnimation
    {
      get
      {
        return this._IdleAnimation;
      }
      set
      {
        this._IdleAnimation = value;
      }
    }

    [ProtoMember(20)]
    public string IdleFacialAnimation
    {
      get
      {
        return this._IdleFacialAnimation;
      }
      set
      {
        this._IdleFacialAnimation = value;
      }
    }

    [ProtoMember(21)]
    public string Background
    {
      get
      {
        return this._Background;
      }
      set
      {
        this._Background = value;
      }
    }

    [ProtoMember(22)]
    public string BackgroundImage
    {
      get
      {
        return this._BackgroundImage;
      }
      set
      {
        this._BackgroundImage = value;
      }
    }

    [ProtoMember(23)]
    public int BackgroundX
    {
      get
      {
        return this._BackgroundX;
      }
      set
      {
        this._BackgroundX = value;
      }
    }

    [ProtoMember(24)]
    public string Voice
    {
      get
      {
        return this._Voice;
      }
      set
      {
        this._Voice = value;
      }
    }

    [ProtoMember(25)]
    public string Words
    {
      get
      {
        return this._Words;
      }
      set
      {
        this._Words = value;
      }
    }

    [ProtoMember(27)]
    public List<int> BeforeEnterEffectGroup1
    {
      get
      {
        return this._BeforeEnterEffectGroup1;
      }
      set
      {
        this._BeforeEnterEffectGroup1 = value;
      }
    }

    [ProtoMember(28)]
    public List<int> BeforeEnterEffectGroup2
    {
      get
      {
        return this._BeforeEnterEffectGroup2;
      }
      set
      {
        this._BeforeEnterEffectGroup2 = value;
      }
    }

    [ProtoMember(29)]
    public List<int> BeforeEnterEffectGroup3
    {
      get
      {
        return this._BeforeEnterEffectGroup3;
      }
      set
      {
        this._BeforeEnterEffectGroup3 = value;
      }
    }

    [ProtoMember(30)]
    public int BeforeEnterInsertEffect
    {
      get
      {
        return this._BeforeEnterInsertEffect;
      }
      set
      {
        this._BeforeEnterInsertEffect = value;
      }
    }

    [ProtoMember(31)]
    public List<int> BeforeTalkEffectGroup1
    {
      get
      {
        return this._BeforeTalkEffectGroup1;
      }
      set
      {
        this._BeforeTalkEffectGroup1 = value;
      }
    }

    [ProtoMember(32)]
    public List<int> BeforeTalkEffectGroup2
    {
      get
      {
        return this._BeforeTalkEffectGroup2;
      }
      set
      {
        this._BeforeTalkEffectGroup2 = value;
      }
    }

    [ProtoMember(33)]
    public List<int> BeforeTalkEffectGroup3
    {
      get
      {
        return this._BeforeTalkEffectGroup3;
      }
      set
      {
        this._BeforeTalkEffectGroup3 = value;
      }
    }

    [ProtoMember(34)]
    public int BeforeTalkInsertEffect
    {
      get
      {
        return this._BeforeTalkInsertEffect;
      }
      set
      {
        this._BeforeTalkInsertEffect = value;
      }
    }

    [ProtoMember(35)]
    public List<int> AfterTalkEffectGroup1
    {
      get
      {
        return this._AfterTalkEffectGroup1;
      }
      set
      {
        this._AfterTalkEffectGroup1 = value;
      }
    }

    [ProtoMember(36)]
    public List<int> AfterTalkEffectGroup2
    {
      get
      {
        return this._AfterTalkEffectGroup2;
      }
      set
      {
        this._AfterTalkEffectGroup2 = value;
      }
    }

    [ProtoMember(37)]
    public List<int> AfterTalkEffectGroup3
    {
      get
      {
        return this._AfterTalkEffectGroup3;
      }
      set
      {
        this._AfterTalkEffectGroup3 = value;
      }
    }

    [ProtoMember(38)]
    public int AfterTalkInsertEffect
    {
      get
      {
        return this._AfterTalkInsertEffect;
      }
      set
      {
        this._AfterTalkInsertEffect = value;
      }
    }

    [ProtoMember(39)]
    public int DialogGroupID
    {
      get
      {
        return this._DialogGroupID;
      }
      set
      {
        this._DialogGroupID = value;
      }
    }

    [ProtoMember(40)]
    public List<ChoiceData> ChoiceList
    {
      get
      {
        return this._ChoiceList;
      }
      set
      {
        this._ChoiceList = value;
      }
    }

    [ProtoMember(41)]
    public int CountdownTime
    {
      get
      {
        return this._CountdownTime;
      }
      set
      {
        this._CountdownTime = value;
      }
    }

    [ProtoMember(42)]
    public List<Goods> DialogRewards
    {
      get
      {
        return this._DialogRewards;
      }
      set
      {
        this._DialogRewards = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
