﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.WaypointStyleType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "WaypointStyleType")]
  public enum WaypointStyleType
  {
    [ProtoEnum(Name = "WaypointStyleType_None", Value = 0)] WaypointStyleType_None,
    [ProtoEnum(Name = "WaypointStyleType_Forest", Value = 1)] WaypointStyleType_Forest,
    [ProtoEnum(Name = "WaypointStyleType_Mountain", Value = 2)] WaypointStyleType_Mountain,
    [ProtoEnum(Name = "WaypointStyleType_Cave", Value = 3)] WaypointStyleType_Cave,
    [ProtoEnum(Name = "WaypointStyleType_Village", Value = 4)] WaypointStyleType_Village,
    [ProtoEnum(Name = "WaypointStyleType_ActivityEvent", Value = 5)] WaypointStyleType_ActivityEvent,
  }
}
