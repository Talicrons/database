﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.LevelAndMembers
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "LevelAndMembers")]
  [Serializable]
  public class LevelAndMembers : IExtensible
  {
    private int _Level;
    private int _Members;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int Level
    {
      get
      {
        return this._Level;
      }
      set
      {
        this._Level = value;
      }
    }

    [ProtoMember(2)]
    public int Members
    {
      get
      {
        return this._Members;
      }
      set
      {
        this._Members = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
