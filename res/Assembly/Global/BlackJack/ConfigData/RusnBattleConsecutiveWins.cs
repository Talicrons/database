﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RusnBattleConsecutiveWins
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RusnBattleConsecutiveWins")]
  [Serializable]
  public class RusnBattleConsecutiveWins : IExtensible
  {
    private int _ConsecutiveWins;
    private int _ScoreBonus;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int ConsecutiveWins
    {
      get
      {
        return this._ConsecutiveWins;
      }
      set
      {
        this._ConsecutiveWins = value;
      }
    }

    [ProtoMember(2)]
    public int ScoreBonus
    {
      get
      {
        return this._ScoreBonus;
      }
      set
      {
        this._ScoreBonus = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
