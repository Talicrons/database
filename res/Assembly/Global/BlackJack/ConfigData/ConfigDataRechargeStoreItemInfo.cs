﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataRechargeStoreItemInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataRechargeStoreItemInfo")]
  [Serializable]
  public class ConfigDataRechargeStoreItemInfo : IExtensible
  {
    private int _ID;
    private double _Price;
    private int _GotCrystalNums;
    private int _FirstBoughtReward;
    private int _RepeatlyBoughtReward;
    private string _Icon;
    private bool _IsBestValue;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public double Price
    {
      get
      {
        return this._Price;
      }
      set
      {
        this._Price = value;
      }
    }

    [ProtoMember(4)]
    public int GotCrystalNums
    {
      get
      {
        return this._GotCrystalNums;
      }
      set
      {
        this._GotCrystalNums = value;
      }
    }

    [ProtoMember(5)]
    public int FirstBoughtReward
    {
      get
      {
        return this._FirstBoughtReward;
      }
      set
      {
        this._FirstBoughtReward = value;
      }
    }

    [ProtoMember(6)]
    public int RepeatlyBoughtReward
    {
      get
      {
        return this._RepeatlyBoughtReward;
      }
      set
      {
        this._RepeatlyBoughtReward = value;
      }
    }

    [ProtoMember(7)]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(8)]
    public bool IsBestValue
    {
      get
      {
        return this._IsBestValue;
      }
      set
      {
        this._IsBestValue = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
