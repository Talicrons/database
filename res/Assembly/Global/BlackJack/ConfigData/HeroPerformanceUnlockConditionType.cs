﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroPerformanceUnlockConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroPerformanceUnlockConditionType")]
  public enum HeroPerformanceUnlockConditionType
  {
    [ProtoEnum(Name = "HeroPerformanceUnlockConditionType_None", Value = 0)] HeroPerformanceUnlockConditionType_None,
    [ProtoEnum(Name = "HeroPerformanceUnlockConditionType_HeroFavourabilityLevel", Value = 1)] HeroPerformanceUnlockConditionType_HeroFavourabilityLevel,
  }
}
