﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataTrainingRoomLevelInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataTrainingRoomLevelInfo")]
  [Serializable]
  public class ConfigDataTrainingRoomLevelInfo : IExtensible
  {
    private int _ID;
    private int _CurrentLevel;
    private int _ExpToNextLevel;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public int CurrentLevel
    {
      get
      {
        return this._CurrentLevel;
      }
      set
      {
        this._CurrentLevel = value;
      }
    }

    [ProtoMember(4)]
    public int ExpToNextLevel
    {
      get
      {
        return this._ExpToNextLevel;
      }
      set
      {
        this._ExpToNextLevel = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
