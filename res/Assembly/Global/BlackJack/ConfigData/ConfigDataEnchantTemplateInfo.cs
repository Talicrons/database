﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataEnchantTemplateInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataEnchantTemplateInfo")]
  [Serializable]
  public class ConfigDataEnchantTemplateInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private PropertyModifyType _Property1_ID;
    private List<EnchantPropertyValueInfo> _ValueRange1;
    private PropertyModifyType _Property2_ID;
    private List<EnchantPropertyValueInfo> _ValueRange2;
    private PropertyModifyType _Property3_ID;
    private List<EnchantPropertyValueInfo> _ValueRange3;
    private PropertyModifyType _Property4_ID;
    private List<EnchantPropertyValueInfo> _ValueRange4;
    private PropertyModifyType _Property5_ID;
    private List<EnchantPropertyValueInfo> _ValueRange5;
    private PropertyModifyType _Property6_ID;
    private List<EnchantPropertyValueInfo> _ValueRange6;
    private PropertyModifyType _Property7_ID;
    private List<EnchantPropertyValueInfo> _ValueRange7;
    private PropertyModifyType _Property8_ID;
    private List<EnchantPropertyValueInfo> _ValueRange8;
    private PropertyModifyType _Property9_ID;
    private List<EnchantPropertyValueInfo> _ValueRange9;
    private PropertyModifyType _Property10_ID;
    private List<EnchantPropertyValueInfo> _ValueRange10;
    private PropertyModifyType _Property11_ID;
    private List<EnchantPropertyValueInfo> _ValueRange11;
    private PropertyModifyType _Property12_ID;
    private List<EnchantPropertyValueInfo> _ValueRange12;
    private PropertyModifyType _Property13_ID;
    private List<EnchantPropertyValueInfo> _ValueRange13;
    private PropertyModifyType _Property14_ID;
    private List<EnchantPropertyValueInfo> _ValueRange14;
    private PropertyModifyType _Property15_ID;
    private List<EnchantPropertyValueInfo> _ValueRange15;
    private PropertyModifyType _Property16_ID;
    private List<EnchantPropertyValueInfo> _ValueRange16;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEnchantTemplateInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public PropertyModifyType Property1_ID
    {
      get
      {
        return this._Property1_ID;
      }
      set
      {
        this._Property1_ID = value;
      }
    }

    [ProtoMember(5)]
    public List<EnchantPropertyValueInfo> ValueRange1
    {
      get
      {
        return this._ValueRange1;
      }
      set
      {
        this._ValueRange1 = value;
      }
    }

    [ProtoMember(7)]
    public PropertyModifyType Property2_ID
    {
      get
      {
        return this._Property2_ID;
      }
      set
      {
        this._Property2_ID = value;
      }
    }

    [ProtoMember(8)]
    public List<EnchantPropertyValueInfo> ValueRange2
    {
      get
      {
        return this._ValueRange2;
      }
      set
      {
        this._ValueRange2 = value;
      }
    }

    [ProtoMember(10)]
    public PropertyModifyType Property3_ID
    {
      get
      {
        return this._Property3_ID;
      }
      set
      {
        this._Property3_ID = value;
      }
    }

    [ProtoMember(11)]
    public List<EnchantPropertyValueInfo> ValueRange3
    {
      get
      {
        return this._ValueRange3;
      }
      set
      {
        this._ValueRange3 = value;
      }
    }

    [ProtoMember(13)]
    public PropertyModifyType Property4_ID
    {
      get
      {
        return this._Property4_ID;
      }
      set
      {
        this._Property4_ID = value;
      }
    }

    [ProtoMember(14)]
    public List<EnchantPropertyValueInfo> ValueRange4
    {
      get
      {
        return this._ValueRange4;
      }
      set
      {
        this._ValueRange4 = value;
      }
    }

    [ProtoMember(16)]
    public PropertyModifyType Property5_ID
    {
      get
      {
        return this._Property5_ID;
      }
      set
      {
        this._Property5_ID = value;
      }
    }

    [ProtoMember(17)]
    public List<EnchantPropertyValueInfo> ValueRange5
    {
      get
      {
        return this._ValueRange5;
      }
      set
      {
        this._ValueRange5 = value;
      }
    }

    [ProtoMember(19)]
    public PropertyModifyType Property6_ID
    {
      get
      {
        return this._Property6_ID;
      }
      set
      {
        this._Property6_ID = value;
      }
    }

    [ProtoMember(20)]
    public List<EnchantPropertyValueInfo> ValueRange6
    {
      get
      {
        return this._ValueRange6;
      }
      set
      {
        this._ValueRange6 = value;
      }
    }

    [ProtoMember(22)]
    public PropertyModifyType Property7_ID
    {
      get
      {
        return this._Property7_ID;
      }
      set
      {
        this._Property7_ID = value;
      }
    }

    [ProtoMember(23)]
    public List<EnchantPropertyValueInfo> ValueRange7
    {
      get
      {
        return this._ValueRange7;
      }
      set
      {
        this._ValueRange7 = value;
      }
    }

    [ProtoMember(25)]
    public PropertyModifyType Property8_ID
    {
      get
      {
        return this._Property8_ID;
      }
      set
      {
        this._Property8_ID = value;
      }
    }

    [ProtoMember(26)]
    public List<EnchantPropertyValueInfo> ValueRange8
    {
      get
      {
        return this._ValueRange8;
      }
      set
      {
        this._ValueRange8 = value;
      }
    }

    [ProtoMember(28)]
    public PropertyModifyType Property9_ID
    {
      get
      {
        return this._Property9_ID;
      }
      set
      {
        this._Property9_ID = value;
      }
    }

    [ProtoMember(29)]
    public List<EnchantPropertyValueInfo> ValueRange9
    {
      get
      {
        return this._ValueRange9;
      }
      set
      {
        this._ValueRange9 = value;
      }
    }

    [ProtoMember(31)]
    public PropertyModifyType Property10_ID
    {
      get
      {
        return this._Property10_ID;
      }
      set
      {
        this._Property10_ID = value;
      }
    }

    [ProtoMember(32)]
    public List<EnchantPropertyValueInfo> ValueRange10
    {
      get
      {
        return this._ValueRange10;
      }
      set
      {
        this._ValueRange10 = value;
      }
    }

    [ProtoMember(34)]
    public PropertyModifyType Property11_ID
    {
      get
      {
        return this._Property11_ID;
      }
      set
      {
        this._Property11_ID = value;
      }
    }

    [ProtoMember(35)]
    public List<EnchantPropertyValueInfo> ValueRange11
    {
      get
      {
        return this._ValueRange11;
      }
      set
      {
        this._ValueRange11 = value;
      }
    }

    [ProtoMember(37)]
    public PropertyModifyType Property12_ID
    {
      get
      {
        return this._Property12_ID;
      }
      set
      {
        this._Property12_ID = value;
      }
    }

    [ProtoMember(38)]
    public List<EnchantPropertyValueInfo> ValueRange12
    {
      get
      {
        return this._ValueRange12;
      }
      set
      {
        this._ValueRange12 = value;
      }
    }

    [ProtoMember(40)]
    public PropertyModifyType Property13_ID
    {
      get
      {
        return this._Property13_ID;
      }
      set
      {
        this._Property13_ID = value;
      }
    }

    [ProtoMember(41)]
    public List<EnchantPropertyValueInfo> ValueRange13
    {
      get
      {
        return this._ValueRange13;
      }
      set
      {
        this._ValueRange13 = value;
      }
    }

    [ProtoMember(43)]
    public PropertyModifyType Property14_ID
    {
      get
      {
        return this._Property14_ID;
      }
      set
      {
        this._Property14_ID = value;
      }
    }

    [ProtoMember(44)]
    public List<EnchantPropertyValueInfo> ValueRange14
    {
      get
      {
        return this._ValueRange14;
      }
      set
      {
        this._ValueRange14 = value;
      }
    }

    [ProtoMember(46)]
    public PropertyModifyType Property15_ID
    {
      get
      {
        return this._Property15_ID;
      }
      set
      {
        this._Property15_ID = value;
      }
    }

    [ProtoMember(47)]
    public List<EnchantPropertyValueInfo> ValueRange15
    {
      get
      {
        return this._ValueRange15;
      }
      set
      {
        this._ValueRange15 = value;
      }
    }

    [ProtoMember(49)]
    public PropertyModifyType Property16_ID
    {
      get
      {
        return this._Property16_ID;
      }
      set
      {
        this._Property16_ID = value;
      }
    }

    [ProtoMember(50)]
    public List<EnchantPropertyValueInfo> ValueRange16
    {
      get
      {
        return this._ValueRange16;
      }
      set
      {
        this._ValueRange16 = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
