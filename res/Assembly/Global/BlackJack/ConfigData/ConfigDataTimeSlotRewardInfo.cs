﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataTimeSlotRewardInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataTimeSlotRewardInfo")]
  [Serializable]
  public class ConfigDataTimeSlotRewardInfo : IExtensible
  {
    private int _ID;
    private int _NUMS;
    private List<Goods> _REWARD;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTimeSlotRewardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public int NUMS
    {
      get
      {
        return this._NUMS;
      }
      set
      {
        this._NUMS = value;
      }
    }

    [ProtoMember(4)]
    public List<Goods> REWARD
    {
      get
      {
        return this._REWARD;
      }
      set
      {
        this._REWARD = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
