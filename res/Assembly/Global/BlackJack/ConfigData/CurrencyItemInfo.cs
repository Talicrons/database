﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CurrencyItemInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "CurrencyItemInfo")]
  [Serializable]
  public class CurrencyItemInfo : IExtensible
  {
    private int _ItemId;
    private int _Multiplier;
    private int _ExtInfoID;
    private IExtension extensionObject;
    public ConfigDataCollectionActivityCurrencyItemExtInfo m_extInfo;

    [ProtoMember(1)]
    public int ItemId
    {
      get
      {
        return this._ItemId;
      }
      set
      {
        this._ItemId = value;
      }
    }

    [ProtoMember(2)]
    public int Multiplier
    {
      get
      {
        return this._Multiplier;
      }
      set
      {
        this._Multiplier = value;
      }
    }

    [ProtoMember(3)]
    public int ExtInfoID
    {
      get
      {
        return this._ExtInfoID;
      }
      set
      {
        this._ExtInfoID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
