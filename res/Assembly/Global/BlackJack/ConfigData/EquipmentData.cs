﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.EquipmentData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "EquipmentData")]
  [Serializable]
  public class EquipmentData : IExtensible
  {
    private int _EquipmentId;
    private int _EquipmentLv;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int EquipmentId
    {
      get
      {
        return this._EquipmentId;
      }
      set
      {
        this._EquipmentId = value;
      }
    }

    [ProtoMember(2)]
    public int EquipmentLv
    {
      get
      {
        return this._EquipmentLv;
      }
      set
      {
        this._EquipmentLv = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
