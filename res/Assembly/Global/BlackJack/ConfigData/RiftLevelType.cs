﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RiftLevelType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RiftLevelType")]
  public enum RiftLevelType
  {
    [ProtoEnum(Name = "RiftLevelType_None", Value = 0)] RiftLevelType_None,
    [ProtoEnum(Name = "RiftLevelType_Scenario", Value = 1)] RiftLevelType_Scenario,
    [ProtoEnum(Name = "RiftLevelType_Event", Value = 2)] RiftLevelType_Event,
  }
}
