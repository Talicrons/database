﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataArenaLevelInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataArenaLevelInfo")]
  [Serializable]
  public class ConfigDataArenaLevelInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _UpgradePoints;
    private int _LevelMaxPoints;
    private int _DowngradePoints;
    private int _LevelRewardMailTemplateId;
    private int _DecreasePointsPerWeek;
    private string _Icon;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public int UpgradePoints
    {
      get
      {
        return this._UpgradePoints;
      }
      set
      {
        this._UpgradePoints = value;
      }
    }

    [ProtoMember(5)]
    public int LevelMaxPoints
    {
      get
      {
        return this._LevelMaxPoints;
      }
      set
      {
        this._LevelMaxPoints = value;
      }
    }

    [ProtoMember(6)]
    public int DowngradePoints
    {
      get
      {
        return this._DowngradePoints;
      }
      set
      {
        this._DowngradePoints = value;
      }
    }

    [ProtoMember(7)]
    public int LevelRewardMailTemplateId
    {
      get
      {
        return this._LevelRewardMailTemplateId;
      }
      set
      {
        this._LevelRewardMailTemplateId = value;
      }
    }

    [ProtoMember(8)]
    public int DecreasePointsPerWeek
    {
      get
      {
        return this._DecreasePointsPerWeek;
      }
      set
      {
        this._DecreasePointsPerWeek = value;
      }
    }

    [ProtoMember(11)]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
