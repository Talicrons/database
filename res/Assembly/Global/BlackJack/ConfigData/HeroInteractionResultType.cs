﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroInteractionResultType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroInteractionResultType")]
  public enum HeroInteractionResultType
  {
    [ProtoEnum(Name = "HeroInteractionResultType_None", Value = 0)] HeroInteractionResultType_None,
    [ProtoEnum(Name = "HeroInteractionResultType_Norml", Value = 1)] HeroInteractionResultType_Norml,
    [ProtoEnum(Name = "HeroInteractionResultType_SmallUp", Value = 2)] HeroInteractionResultType_SmallUp,
    [ProtoEnum(Name = "HeroInteractionResultType_BigUp", Value = 3)] HeroInteractionResultType_BigUp,
  }
}
