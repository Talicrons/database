﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.Rewards
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "Rewards")]
  [Serializable]
  public class Rewards : IExtensible
  {
    private int _Id;
    private int _Percent;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int Id
    {
      get
      {
        return this._Id;
      }
      set
      {
        this._Id = value;
      }
    }

    [ProtoMember(2)]
    public int Percent
    {
      get
      {
        return this._Percent;
      }
      set
      {
        this._Percent = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
