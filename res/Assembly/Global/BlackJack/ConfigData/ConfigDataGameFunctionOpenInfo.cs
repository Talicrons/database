﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataGameFunctionOpenInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataGameFunctionOpenInfo")]
  [Serializable]
  public class ConfigDataGameFunctionOpenInfo : IExtensible
  {
    private int _ID;
    private GameFunctionType _GameFunctionType;
    private string _Name;
    private GameFunctionOpenConditionType _OpenCondition;
    private int _OpenConditionParam1;
    private string _Message;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public GameFunctionType GameFunctionType
    {
      get
      {
        return this._GameFunctionType;
      }
      set
      {
        this._GameFunctionType = value;
      }
    }

    [ProtoMember(4)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5)]
    public GameFunctionOpenConditionType OpenCondition
    {
      get
      {
        return this._OpenCondition;
      }
      set
      {
        this._OpenCondition = value;
      }
    }

    [ProtoMember(6)]
    public int OpenConditionParam1
    {
      get
      {
        return this._OpenConditionParam1;
      }
      set
      {
        this._OpenConditionParam1 = value;
      }
    }

    [ProtoMember(7)]
    public string Message
    {
      get
      {
        return this._Message;
      }
      set
      {
        this._Message = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
