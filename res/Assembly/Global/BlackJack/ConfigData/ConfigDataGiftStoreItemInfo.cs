﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataGiftStoreItemInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataGiftStoreItemInfo")]
  [Serializable]
  public class ConfigDataGiftStoreItemInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private StoreId _StoreId;
    private GoodsType _ItemType;
    private int _ItemId;
    private int _Nums;
    private string _ShowStartTime;
    private string _ShowEndTime;
    private int _RefluxBeginDays;
    private int _RefluxEndDays;
    private bool _IsOperateGoods;
    private BuyRuleType _BuyLimitType;
    private int _Param;
    private int _Count;
    private List<Goods> _FirstReward;
    private double _FirstPrice;
    private double _NormalPrice;
    private LabelType _Lable;
    private bool _IsAppleSubscribe;
    private int _FirstBuyCompensation;
    private int _NormalBuyCompensation;
    private int _PlayerBuyMinLevel;
    private int _Sort;
    private int _IsRecommendDisplay;
    private string _RecommendDisplayStartTime;
    private string _DiscountText;
    private string _BigAdvertisingImage;
    private string _Desc;
    private int _IsStrongRecommend;
    private string _SmallAdvertisingImage;
    private string _RecommendDesc;
    private bool _IsShowIOSSpecificGo;
    private StoreRedMarkType _RedMark;
    private int _PeriodGiftId;
    private int _PreUnlockGoodsId;
    private int _DollGroup;
    private bool _NotShowInStore;
    private int _OpenActivityIdAfterPurchase;
    private bool _IsHideItemList;
    private GiftType _GiftType;
    private IExtension extensionObject;
    public DateTime ShowStartDateTime;
    public DateTime ShowEndDateTime;
    public DateTime RecommendDisplayStartDateTime;
    public int SubPeriodGiftId;
    public int PostUnlockItemId;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGiftStoreItemInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public StoreId StoreId
    {
      get
      {
        return this._StoreId;
      }
      set
      {
        this._StoreId = value;
      }
    }

    [ProtoMember(5)]
    public GoodsType ItemType
    {
      get
      {
        return this._ItemType;
      }
      set
      {
        this._ItemType = value;
      }
    }

    [ProtoMember(6)]
    public int ItemId
    {
      get
      {
        return this._ItemId;
      }
      set
      {
        this._ItemId = value;
      }
    }

    [ProtoMember(7)]
    public int Nums
    {
      get
      {
        return this._Nums;
      }
      set
      {
        this._Nums = value;
      }
    }

    [ProtoMember(8)]
    public string ShowStartTime
    {
      get
      {
        return this._ShowStartTime;
      }
      set
      {
        this._ShowStartTime = value;
      }
    }

    [ProtoMember(9)]
    public string ShowEndTime
    {
      get
      {
        return this._ShowEndTime;
      }
      set
      {
        this._ShowEndTime = value;
      }
    }

    [ProtoMember(10)]
    public int RefluxBeginDays
    {
      get
      {
        return this._RefluxBeginDays;
      }
      set
      {
        this._RefluxBeginDays = value;
      }
    }

    [ProtoMember(11)]
    public int RefluxEndDays
    {
      get
      {
        return this._RefluxEndDays;
      }
      set
      {
        this._RefluxEndDays = value;
      }
    }

    [ProtoMember(12)]
    public bool IsOperateGoods
    {
      get
      {
        return this._IsOperateGoods;
      }
      set
      {
        this._IsOperateGoods = value;
      }
    }

    [ProtoMember(13)]
    public BuyRuleType BuyLimitType
    {
      get
      {
        return this._BuyLimitType;
      }
      set
      {
        this._BuyLimitType = value;
      }
    }

    [ProtoMember(14)]
    public int Param
    {
      get
      {
        return this._Param;
      }
      set
      {
        this._Param = value;
      }
    }

    [ProtoMember(15)]
    public int Count
    {
      get
      {
        return this._Count;
      }
      set
      {
        this._Count = value;
      }
    }

    [ProtoMember(16)]
    public List<Goods> FirstReward
    {
      get
      {
        return this._FirstReward;
      }
      set
      {
        this._FirstReward = value;
      }
    }

    [ProtoMember(17)]
    public double FirstPrice
    {
      get
      {
        return this._FirstPrice;
      }
      set
      {
        this._FirstPrice = value;
      }
    }

    [ProtoMember(18)]
    public double NormalPrice
    {
      get
      {
        return this._NormalPrice;
      }
      set
      {
        this._NormalPrice = value;
      }
    }

    [ProtoMember(19)]
    public LabelType Lable
    {
      get
      {
        return this._Lable;
      }
      set
      {
        this._Lable = value;
      }
    }

    [ProtoMember(20)]
    public bool IsAppleSubscribe
    {
      get
      {
        return this._IsAppleSubscribe;
      }
      set
      {
        this._IsAppleSubscribe = value;
      }
    }

    [ProtoMember(21)]
    public int FirstBuyCompensation
    {
      get
      {
        return this._FirstBuyCompensation;
      }
      set
      {
        this._FirstBuyCompensation = value;
      }
    }

    [ProtoMember(22)]
    public int NormalBuyCompensation
    {
      get
      {
        return this._NormalBuyCompensation;
      }
      set
      {
        this._NormalBuyCompensation = value;
      }
    }

    [ProtoMember(23)]
    public int PlayerBuyMinLevel
    {
      get
      {
        return this._PlayerBuyMinLevel;
      }
      set
      {
        this._PlayerBuyMinLevel = value;
      }
    }

    [ProtoMember(24)]
    public int Sort
    {
      get
      {
        return this._Sort;
      }
      set
      {
        this._Sort = value;
      }
    }

    [ProtoMember(25)]
    public int IsRecommendDisplay
    {
      get
      {
        return this._IsRecommendDisplay;
      }
      set
      {
        this._IsRecommendDisplay = value;
      }
    }

    [ProtoMember(26)]
    public string RecommendDisplayStartTime
    {
      get
      {
        return this._RecommendDisplayStartTime;
      }
      set
      {
        this._RecommendDisplayStartTime = value;
      }
    }

    [ProtoMember(27)]
    public string DiscountText
    {
      get
      {
        return this._DiscountText;
      }
      set
      {
        this._DiscountText = value;
      }
    }

    [ProtoMember(28)]
    public string BigAdvertisingImage
    {
      get
      {
        return this._BigAdvertisingImage;
      }
      set
      {
        this._BigAdvertisingImage = value;
      }
    }

    [ProtoMember(29)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(30)]
    public int IsStrongRecommend
    {
      get
      {
        return this._IsStrongRecommend;
      }
      set
      {
        this._IsStrongRecommend = value;
      }
    }

    [ProtoMember(31)]
    public string SmallAdvertisingImage
    {
      get
      {
        return this._SmallAdvertisingImage;
      }
      set
      {
        this._SmallAdvertisingImage = value;
      }
    }

    [ProtoMember(32)]
    public string RecommendDesc
    {
      get
      {
        return this._RecommendDesc;
      }
      set
      {
        this._RecommendDesc = value;
      }
    }

    [ProtoMember(33)]
    public bool IsShowIOSSpecificGo
    {
      get
      {
        return this._IsShowIOSSpecificGo;
      }
      set
      {
        this._IsShowIOSSpecificGo = value;
      }
    }

    [ProtoMember(34)]
    public StoreRedMarkType RedMark
    {
      get
      {
        return this._RedMark;
      }
      set
      {
        this._RedMark = value;
      }
    }

    [ProtoMember(35)]
    public int PeriodGiftId
    {
      get
      {
        return this._PeriodGiftId;
      }
      set
      {
        this._PeriodGiftId = value;
      }
    }

    [ProtoMember(36)]
    public int PreUnlockGoodsId
    {
      get
      {
        return this._PreUnlockGoodsId;
      }
      set
      {
        this._PreUnlockGoodsId = value;
      }
    }

    [ProtoMember(37)]
    public int DollGroup
    {
      get
      {
        return this._DollGroup;
      }
      set
      {
        this._DollGroup = value;
      }
    }

    [ProtoMember(38)]
    public bool NotShowInStore
    {
      get
      {
        return this._NotShowInStore;
      }
      set
      {
        this._NotShowInStore = value;
      }
    }

    [ProtoMember(39)]
    public int OpenActivityIdAfterPurchase
    {
      get
      {
        return this._OpenActivityIdAfterPurchase;
      }
      set
      {
        this._OpenActivityIdAfterPurchase = value;
      }
    }

    [ProtoMember(40)]
    public bool IsHideItemList
    {
      get
      {
        return this._IsHideItemList;
      }
      set
      {
        this._IsHideItemList = value;
      }
    }

    [ProtoMember(41)]
    public GiftType GiftType
    {
      get
      {
        return this._GiftType;
      }
      set
      {
        this._GiftType = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
