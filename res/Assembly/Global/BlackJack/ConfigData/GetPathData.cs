﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.GetPathData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "GetPathData")]
  [Serializable]
  public class GetPathData : IExtensible
  {
    private GetPathType _PathType;
    private int _ID;
    private int _Name;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public GetPathType PathType
    {
      get
      {
        return this._PathType;
      }
      set
      {
        this._PathType = value;
      }
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public int Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
