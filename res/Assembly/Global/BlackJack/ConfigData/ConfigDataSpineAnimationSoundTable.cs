﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataSpineAnimationSoundTable
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataSpineAnimationSoundTable")]
  [Serializable]
  public class ConfigDataSpineAnimationSoundTable : IExtensible
  {
    private int _ID;
    private string _SpineDataName;
    private string _AnimationName;
    private string _EventName;
    private string _SoundName;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string SpineDataName
    {
      get
      {
        return this._SpineDataName;
      }
      set
      {
        this._SpineDataName = value;
      }
    }

    [ProtoMember(4)]
    public string AnimationName
    {
      get
      {
        return this._AnimationName;
      }
      set
      {
        this._AnimationName = value;
      }
    }

    [ProtoMember(5)]
    public string EventName
    {
      get
      {
        return this._EventName;
      }
      set
      {
        this._EventName = value;
      }
    }

    [ProtoMember(6)]
    public string SoundName
    {
      get
      {
        return this._SoundName;
      }
      set
      {
        this._SoundName = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
