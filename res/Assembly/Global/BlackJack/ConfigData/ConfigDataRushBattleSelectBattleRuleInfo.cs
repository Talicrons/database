﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataRushBattleSelectBattleRuleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataRushBattleSelectBattleRuleInfo")]
  [Serializable]
  public class ConfigDataRushBattleSelectBattleRuleInfo : IExtensible
  {
    private int _ID;
    private int _StartDays;
    private List<int> _Battle_ID;
    private List<int> _CommanderSkill_ID;
    private IExtension extensionObject;
    public List<ConfigDataRushBattleInfo> m_battleInfos;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRushBattleSelectBattleRuleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public int StartDays
    {
      get
      {
        return this._StartDays;
      }
      set
      {
        this._StartDays = value;
      }
    }

    [ProtoMember(4)]
    public List<int> Battle_ID
    {
      get
      {
        return this._Battle_ID;
      }
      set
      {
        this._Battle_ID = value;
      }
    }

    [ProtoMember(5)]
    public List<int> CommanderSkill_ID
    {
      get
      {
        return this._CommanderSkill_ID;
      }
      set
      {
        this._CommanderSkill_ID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
