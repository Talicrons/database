﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataHeroAssistantTaskGeneralInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataHeroAssistantTaskGeneralInfo")]
  [Serializable]
  public class ConfigDataHeroAssistantTaskGeneralInfo : IExtensible
  {
    private int _ID;
    private int _WorkSeconds1;
    private int _DropCount1;
    private int _WorkSeconds2;
    private int _DropCount2;
    private int _WorkSeconds3;
    private int _DropCount3;
    private int _RecommandHeroMultiply;
    private int _RecommandHeroAdd;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public int WorkSeconds1
    {
      get
      {
        return this._WorkSeconds1;
      }
      set
      {
        this._WorkSeconds1 = value;
      }
    }

    [ProtoMember(4)]
    public int DropCount1
    {
      get
      {
        return this._DropCount1;
      }
      set
      {
        this._DropCount1 = value;
      }
    }

    [ProtoMember(5)]
    public int WorkSeconds2
    {
      get
      {
        return this._WorkSeconds2;
      }
      set
      {
        this._WorkSeconds2 = value;
      }
    }

    [ProtoMember(6)]
    public int DropCount2
    {
      get
      {
        return this._DropCount2;
      }
      set
      {
        this._DropCount2 = value;
      }
    }

    [ProtoMember(7)]
    public int WorkSeconds3
    {
      get
      {
        return this._WorkSeconds3;
      }
      set
      {
        this._WorkSeconds3 = value;
      }
    }

    [ProtoMember(8)]
    public int DropCount3
    {
      get
      {
        return this._DropCount3;
      }
      set
      {
        this._DropCount3 = value;
      }
    }

    [ProtoMember(9)]
    public int RecommandHeroMultiply
    {
      get
      {
        return this._RecommandHeroMultiply;
      }
      set
      {
        this._RecommandHeroMultiply = value;
      }
    }

    [ProtoMember(10)]
    public int RecommandHeroAdd
    {
      get
      {
        return this._RecommandHeroAdd;
      }
      set
      {
        this._RecommandHeroAdd = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
