﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataMissionExtNoviceInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataMissionExtNoviceInfo")]
  [Serializable]
  public class ConfigDataMissionExtNoviceInfo : IExtensible
  {
    private int _ID;
    private int _NovicePoints;
    private int _ActivateTime;
    private int _DeactivateTime;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public int NovicePoints
    {
      get
      {
        return this._NovicePoints;
      }
      set
      {
        this._NovicePoints = value;
      }
    }

    [ProtoMember(4)]
    public int ActivateTime
    {
      get
      {
        return this._ActivateTime;
      }
      set
      {
        this._ActivateTime = value;
      }
    }

    [ProtoMember(5)]
    public int DeactivateTime
    {
      get
      {
        return this._DeactivateTime;
      }
      set
      {
        this._DeactivateTime = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
