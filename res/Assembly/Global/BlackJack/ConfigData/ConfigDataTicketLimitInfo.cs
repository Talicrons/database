﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataTicketLimitInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataTicketLimitInfo")]
  [Serializable]
  public class ConfigDataTicketLimitInfo : IExtensible
  {
    private int _ID;
    private GameFunctionType _GameFunctionTypeID;
    private int _MaxNums;
    private int _TicketId;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public GameFunctionType GameFunctionTypeID
    {
      get
      {
        return this._GameFunctionTypeID;
      }
      set
      {
        this._GameFunctionTypeID = value;
      }
    }

    [ProtoMember(4)]
    public int MaxNums
    {
      get
      {
        return this._MaxNums;
      }
      set
      {
        this._MaxNums = value;
      }
    }

    [ProtoMember(5)]
    public int TicketId
    {
      get
      {
        return this._TicketId;
      }
      set
      {
        this._TicketId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
