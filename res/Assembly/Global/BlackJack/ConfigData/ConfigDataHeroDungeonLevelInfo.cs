﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataHeroDungeonLevelInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataHeroDungeonLevelInfo")]
  [Serializable]
  public class ConfigDataHeroDungeonLevelInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private string _NameNum;
    private string _Resource;
    private int _ChallengeCount;
    private int _EnergySuccess;
    private int _EnergyFail;
    private int _MonsterLevel;
    private int _Battle_ID;
    private int _DialogBefore_ID;
    private int _DialogAfter_ID;
    private int _PreLevel_ID;
    private int _PlayerLevel;
    private List<HeroDungeonLevelUnlcokCondition> _UnlockConditions;
    private int _StarTurnMax;
    private int _StarDeadMax;
    private int _Achievement1_ID;
    private List<Goods> _AchievementReward1;
    private int _Achievement2_ID;
    private List<Goods> _AchievementReward2;
    private int _Achievement3_ID;
    private List<Goods> _AchievementReward3;
    private int _PlayerExpReward;
    private int _HeroExpReward;
    private int _GoldReward;
    private List<Goods> _FirstReward;
    private List<Goods> _RaidReward;
    private int _Drop_ID;
    private int _OperationalActivityDrop_ID;
    private int _DisplayRewardCount;
    private string _Strategy;
    private int _HeroFragment_ID;
    private IExtension extensionObject;
    public int m_chapterId;
    public ConfigDataBattleInfo m_battleInfo;
    public BattleLevelAchievement[] m_achievements;
    public ConfigDataDialogInfo m_dialogInfoBefore;
    public ConfigDataDialogInfo m_dialogInfoAfter;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroDungeonLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(5)]
    public string NameNum
    {
      get
      {
        return this._NameNum;
      }
      set
      {
        this._NameNum = value;
      }
    }

    [ProtoMember(6)]
    public string Resource
    {
      get
      {
        return this._Resource;
      }
      set
      {
        this._Resource = value;
      }
    }

    [ProtoMember(7)]
    public int ChallengeCount
    {
      get
      {
        return this._ChallengeCount;
      }
      set
      {
        this._ChallengeCount = value;
      }
    }

    [ProtoMember(8)]
    public int EnergySuccess
    {
      get
      {
        return this._EnergySuccess;
      }
      set
      {
        this._EnergySuccess = value;
      }
    }

    [ProtoMember(9)]
    public int EnergyFail
    {
      get
      {
        return this._EnergyFail;
      }
      set
      {
        this._EnergyFail = value;
      }
    }

    [ProtoMember(10)]
    public int MonsterLevel
    {
      get
      {
        return this._MonsterLevel;
      }
      set
      {
        this._MonsterLevel = value;
      }
    }

    [ProtoMember(11)]
    public int Battle_ID
    {
      get
      {
        return this._Battle_ID;
      }
      set
      {
        this._Battle_ID = value;
      }
    }

    [ProtoMember(12)]
    public int DialogBefore_ID
    {
      get
      {
        return this._DialogBefore_ID;
      }
      set
      {
        this._DialogBefore_ID = value;
      }
    }

    [ProtoMember(13)]
    public int DialogAfter_ID
    {
      get
      {
        return this._DialogAfter_ID;
      }
      set
      {
        this._DialogAfter_ID = value;
      }
    }

    [ProtoMember(14)]
    public int PreLevel_ID
    {
      get
      {
        return this._PreLevel_ID;
      }
      set
      {
        this._PreLevel_ID = value;
      }
    }

    [ProtoMember(15)]
    public int PlayerLevel
    {
      get
      {
        return this._PlayerLevel;
      }
      set
      {
        this._PlayerLevel = value;
      }
    }

    [ProtoMember(16)]
    public List<HeroDungeonLevelUnlcokCondition> UnlockConditions
    {
      get
      {
        return this._UnlockConditions;
      }
      set
      {
        this._UnlockConditions = value;
      }
    }

    [ProtoMember(17)]
    public int StarTurnMax
    {
      get
      {
        return this._StarTurnMax;
      }
      set
      {
        this._StarTurnMax = value;
      }
    }

    [ProtoMember(18)]
    public int StarDeadMax
    {
      get
      {
        return this._StarDeadMax;
      }
      set
      {
        this._StarDeadMax = value;
      }
    }

    [ProtoMember(19)]
    public int Achievement1_ID
    {
      get
      {
        return this._Achievement1_ID;
      }
      set
      {
        this._Achievement1_ID = value;
      }
    }

    [ProtoMember(20)]
    public List<Goods> AchievementReward1
    {
      get
      {
        return this._AchievementReward1;
      }
      set
      {
        this._AchievementReward1 = value;
      }
    }

    [ProtoMember(21)]
    public int Achievement2_ID
    {
      get
      {
        return this._Achievement2_ID;
      }
      set
      {
        this._Achievement2_ID = value;
      }
    }

    [ProtoMember(22)]
    public List<Goods> AchievementReward2
    {
      get
      {
        return this._AchievementReward2;
      }
      set
      {
        this._AchievementReward2 = value;
      }
    }

    [ProtoMember(23)]
    public int Achievement3_ID
    {
      get
      {
        return this._Achievement3_ID;
      }
      set
      {
        this._Achievement3_ID = value;
      }
    }

    [ProtoMember(24)]
    public List<Goods> AchievementReward3
    {
      get
      {
        return this._AchievementReward3;
      }
      set
      {
        this._AchievementReward3 = value;
      }
    }

    [ProtoMember(25)]
    public int PlayerExpReward
    {
      get
      {
        return this._PlayerExpReward;
      }
      set
      {
        this._PlayerExpReward = value;
      }
    }

    [ProtoMember(26)]
    public int HeroExpReward
    {
      get
      {
        return this._HeroExpReward;
      }
      set
      {
        this._HeroExpReward = value;
      }
    }

    [ProtoMember(27)]
    public int GoldReward
    {
      get
      {
        return this._GoldReward;
      }
      set
      {
        this._GoldReward = value;
      }
    }

    [ProtoMember(28)]
    public List<Goods> FirstReward
    {
      get
      {
        return this._FirstReward;
      }
      set
      {
        this._FirstReward = value;
      }
    }

    [ProtoMember(29)]
    public List<Goods> RaidReward
    {
      get
      {
        return this._RaidReward;
      }
      set
      {
        this._RaidReward = value;
      }
    }

    [ProtoMember(30)]
    public int Drop_ID
    {
      get
      {
        return this._Drop_ID;
      }
      set
      {
        this._Drop_ID = value;
      }
    }

    [ProtoMember(31)]
    public int OperationalActivityDrop_ID
    {
      get
      {
        return this._OperationalActivityDrop_ID;
      }
      set
      {
        this._OperationalActivityDrop_ID = value;
      }
    }

    [ProtoMember(32)]
    public int DisplayRewardCount
    {
      get
      {
        return this._DisplayRewardCount;
      }
      set
      {
        this._DisplayRewardCount = value;
      }
    }

    [ProtoMember(33)]
    public string Strategy
    {
      get
      {
        return this._Strategy;
      }
      set
      {
        this._Strategy = value;
      }
    }

    [ProtoMember(34)]
    public int HeroFragment_ID
    {
      get
      {
        return this._HeroFragment_ID;
      }
      set
      {
        this._HeroFragment_ID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
