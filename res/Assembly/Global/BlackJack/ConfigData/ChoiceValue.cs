﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ChoiceValue
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ChoiceValue")]
  [Serializable]
  public class ChoiceValue : IExtensible
  {
    private int _Id;
    private int _Value;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int Id
    {
      get
      {
        return this._Id;
      }
      set
      {
        this._Id = value;
      }
    }

    [ProtoMember(2)]
    public int Value
    {
      get
      {
        return this._Value;
      }
      set
      {
        this._Value = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
