﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataRushBattleScoreInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataRushBattleScoreInfo")]
  [Serializable]
  public class ConfigDataRushBattleScoreInfo : IExtensible
  {
    private int _ID;
    private int _ScoreSectionStart;
    private int _ScoreSectionEnd;
    private int _WinScoreBase;
    private int _LoseScoreBase;
    private int _WinScoreMax;
    private int _WinScoreMin;
    private int _LoseScoreMax;
    private int _LoseScoreMin;
    private List<RusnBattleConsecutiveWins> _ConsecutiveWinScoreBonus;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRushBattleScoreInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public int ScoreSectionStart
    {
      get
      {
        return this._ScoreSectionStart;
      }
      set
      {
        this._ScoreSectionStart = value;
      }
    }

    [ProtoMember(4)]
    public int ScoreSectionEnd
    {
      get
      {
        return this._ScoreSectionEnd;
      }
      set
      {
        this._ScoreSectionEnd = value;
      }
    }

    [ProtoMember(5)]
    public int WinScoreBase
    {
      get
      {
        return this._WinScoreBase;
      }
      set
      {
        this._WinScoreBase = value;
      }
    }

    [ProtoMember(6)]
    public int LoseScoreBase
    {
      get
      {
        return this._LoseScoreBase;
      }
      set
      {
        this._LoseScoreBase = value;
      }
    }

    [ProtoMember(7)]
    public int WinScoreMax
    {
      get
      {
        return this._WinScoreMax;
      }
      set
      {
        this._WinScoreMax = value;
      }
    }

    [ProtoMember(8)]
    public int WinScoreMin
    {
      get
      {
        return this._WinScoreMin;
      }
      set
      {
        this._WinScoreMin = value;
      }
    }

    [ProtoMember(9)]
    public int LoseScoreMax
    {
      get
      {
        return this._LoseScoreMax;
      }
      set
      {
        this._LoseScoreMax = value;
      }
    }

    [ProtoMember(10)]
    public int LoseScoreMin
    {
      get
      {
        return this._LoseScoreMin;
      }
      set
      {
        this._LoseScoreMin = value;
      }
    }

    [ProtoMember(11)]
    public List<RusnBattleConsecutiveWins> ConsecutiveWinScoreBonus
    {
      get
      {
        return this._ConsecutiveWinScoreBonus;
      }
      set
      {
        this._ConsecutiveWinScoreBonus = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
