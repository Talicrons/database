﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.Soldier2SkinResource
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "Soldier2SkinResource")]
  [Serializable]
  public class Soldier2SkinResource : IExtensible
  {
    private int _SoldierId;
    private int _SkinResourceId;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int SoldierId
    {
      get
      {
        return this._SoldierId;
      }
      set
      {
        this._SoldierId = value;
      }
    }

    [ProtoMember(2)]
    public int SkinResourceId
    {
      get
      {
        return this._SkinResourceId;
      }
      set
      {
        this._SkinResourceId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
