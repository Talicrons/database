﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.WaitingTimeInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "WaitingTimeInfo")]
  [Serializable]
  public class WaitingTimeInfo : IExtensible
  {
    private int _WaitingTime;
    private int _Min;
    private int _Max;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int WaitingTime
    {
      get
      {
        return this._WaitingTime;
      }
      set
      {
        this._WaitingTime = value;
      }
    }

    [ProtoMember(2)]
    public int Min
    {
      get
      {
        return this._Min;
      }
      set
      {
        this._Min = value;
      }
    }

    [ProtoMember(3)]
    public int Max
    {
      get
      {
        return this._Max;
      }
      set
      {
        this._Max = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
