﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.GoddessDialogFuncType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "GoddessDialogFuncType")]
  public enum GoddessDialogFuncType
  {
    [ProtoEnum(Name = "GoddessDialogFuncType_None", Value = 0)] GoddessDialogFuncType_None,
    [ProtoEnum(Name = "GoddessDialogFuncType_Start", Value = 1)] GoddessDialogFuncType_Start,
    [ProtoEnum(Name = "GoddessDialogFuncType_QuestionStart", Value = 2)] GoddessDialogFuncType_QuestionStart,
    [ProtoEnum(Name = "GoddessDialogFuncType_Result", Value = 3)] GoddessDialogFuncType_Result,
    [ProtoEnum(Name = "GoddessDialogFuncType_Select", Value = 4)] GoddessDialogFuncType_Select,
    [ProtoEnum(Name = "GoddessDialogFuncType_Final", Value = 5)] GoddessDialogFuncType_Final,
  }
}
