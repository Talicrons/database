﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataBehaviorChangeRule
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataBehaviorChangeRule")]
  [Serializable]
  public class ConfigDataBehaviorChangeRule : IExtensible
  {
    private int _ID;
    private BehaviorCondition _ChangeCondition;
    private string _CCParam;
    private int _NextBehaviorID;
    private IExtension extensionObject;
    public ConfigDataBehavior.ParamData CCParamData;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBehaviorChangeRule()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(4)]
    public BehaviorCondition ChangeCondition
    {
      get
      {
        return this._ChangeCondition;
      }
      set
      {
        this._ChangeCondition = value;
      }
    }

    [ProtoMember(5)]
    public string CCParam
    {
      get
      {
        return this._CCParam;
      }
      set
      {
        this._CCParam = value;
      }
    }

    [ProtoMember(6)]
    public int NextBehaviorID
    {
      get
      {
        return this._NextBehaviorID;
      }
      set
      {
        this._NextBehaviorID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Initialize()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
