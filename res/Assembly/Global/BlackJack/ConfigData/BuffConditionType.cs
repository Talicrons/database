﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.BuffConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "BuffConditionType")]
  public enum BuffConditionType
  {
    [ProtoEnum(Name = "BuffConditionType_None", Value = 0)] BuffConditionType_None,
    [ProtoEnum(Name = "BuffConditionType_IsAlone", Value = 1)] BuffConditionType_IsAlone,
    [ProtoEnum(Name = "BuffConditionType_NotAlone", Value = 2)] BuffConditionType_NotAlone,
    [ProtoEnum(Name = "BuffConditionType_Terrain", Value = 3)] BuffConditionType_Terrain,
    [ProtoEnum(Name = "BuffConditionType_HeroArmy", Value = 4)] BuffConditionType_HeroArmy,
    [ProtoEnum(Name = "BuffConditionType_TerrainIsDF", Value = 5)] BuffConditionType_TerrainIsDF,
    [ProtoEnum(Name = "BuffConditionType_ArmyCombination", Value = 6)] BuffConditionType_ArmyCombination,
    [ProtoEnum(Name = "BuffConditionType_HeroJob", Value = 7)] BuffConditionType_HeroJob,
    [ProtoEnum(Name = "BuffConditionType_HeroInfo", Value = 8)] BuffConditionType_HeroInfo,
    [ProtoEnum(Name = "BuffConditionType_IsMove", Value = 9)] BuffConditionType_IsMove,
    [ProtoEnum(Name = "BuffConditionType_BuffID", Value = 10)] BuffConditionType_BuffID,
    [ProtoEnum(Name = "BuffConditionType_NotAloneHeroInfo", Value = 11)] BuffConditionType_NotAloneHeroInfo,
    [ProtoEnum(Name = "BuffConditionType_BuffID2", Value = 12)] BuffConditionType_BuffID2,
    [ProtoEnum(Name = "BuffConditionType_BuffTime", Value = 13)] BuffConditionType_BuffTime,
    [ProtoEnum(Name = "BuffConditionType_IsPVP", Value = 14)] BuffConditionType_IsPVP,
    [ProtoEnum(Name = "BuffConditionType_EnhanceBuff", Value = 15)] BuffConditionType_EnhanceBuff,
    [ProtoEnum(Name = "BuffConditionType_AttackRange", Value = 16)] BuffConditionType_AttackRange,
    [ProtoEnum(Name = "BuffConditionType_BuffSubType", Value = 17)] BuffConditionType_BuffSubType,
    [ProtoEnum(Name = "BuffConditionType_Solider", Value = 18)] BuffConditionType_Solider,
    [ProtoEnum(Name = "BuffConditionType_SoliderAttackDistance", Value = 19)] BuffConditionType_SoliderAttackDistance,
    [ProtoEnum(Name = "BuffConditionType_NotAlone2", Value = 20)] BuffConditionType_NotAlone2,
    [ProtoEnum(Name = "BuffConditionType_BuffSubType2", Value = 21)] BuffConditionType_BuffSubType2,
  }
}
