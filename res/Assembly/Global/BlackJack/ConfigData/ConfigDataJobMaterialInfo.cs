﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataJobMaterialInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataJobMaterialInfo")]
  [Serializable]
  public class ConfigDataJobMaterialInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private int _Rank;
    private int _SellGold;
    private int _ComposeGold;
    private List<Goods> _ComposeMaterials;
    private PropertyModifyType _Property1_ID;
    private int _Property1_Value;
    private PropertyModifyType _Property2_ID;
    private int _Property2_Value;
    private PropertyModifyType _Property3_ID;
    private int _Property3_Value;
    private PropertyModifyType _Property4_ID;
    private int _Property4_Value;
    private PropertyModifyType _Property5_ID;
    private int _Property5_Value;
    private string _Icon;
    private List<GetPathData> _GetPathList;
    private string _GetPathDesc;
    private List<Goods> _AlchemyReward;
    private int _RandomDropRewardID;
    private int _DisplayRewardCount;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataJobMaterialInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(7)]
    public int Rank
    {
      get
      {
        return this._Rank;
      }
      set
      {
        this._Rank = value;
      }
    }

    [ProtoMember(8)]
    public int SellGold
    {
      get
      {
        return this._SellGold;
      }
      set
      {
        this._SellGold = value;
      }
    }

    [ProtoMember(9)]
    public int ComposeGold
    {
      get
      {
        return this._ComposeGold;
      }
      set
      {
        this._ComposeGold = value;
      }
    }

    [ProtoMember(10)]
    public List<Goods> ComposeMaterials
    {
      get
      {
        return this._ComposeMaterials;
      }
      set
      {
        this._ComposeMaterials = value;
      }
    }

    [ProtoMember(11)]
    public PropertyModifyType Property1_ID
    {
      get
      {
        return this._Property1_ID;
      }
      set
      {
        this._Property1_ID = value;
      }
    }

    [ProtoMember(12)]
    public int Property1_Value
    {
      get
      {
        return this._Property1_Value;
      }
      set
      {
        this._Property1_Value = value;
      }
    }

    [ProtoMember(13)]
    public PropertyModifyType Property2_ID
    {
      get
      {
        return this._Property2_ID;
      }
      set
      {
        this._Property2_ID = value;
      }
    }

    [ProtoMember(14)]
    public int Property2_Value
    {
      get
      {
        return this._Property2_Value;
      }
      set
      {
        this._Property2_Value = value;
      }
    }

    [ProtoMember(15)]
    public PropertyModifyType Property3_ID
    {
      get
      {
        return this._Property3_ID;
      }
      set
      {
        this._Property3_ID = value;
      }
    }

    [ProtoMember(16)]
    public int Property3_Value
    {
      get
      {
        return this._Property3_Value;
      }
      set
      {
        this._Property3_Value = value;
      }
    }

    [ProtoMember(17)]
    public PropertyModifyType Property4_ID
    {
      get
      {
        return this._Property4_ID;
      }
      set
      {
        this._Property4_ID = value;
      }
    }

    [ProtoMember(18)]
    public int Property4_Value
    {
      get
      {
        return this._Property4_Value;
      }
      set
      {
        this._Property4_Value = value;
      }
    }

    [ProtoMember(19)]
    public PropertyModifyType Property5_ID
    {
      get
      {
        return this._Property5_ID;
      }
      set
      {
        this._Property5_ID = value;
      }
    }

    [ProtoMember(20)]
    public int Property5_Value
    {
      get
      {
        return this._Property5_Value;
      }
      set
      {
        this._Property5_Value = value;
      }
    }

    [ProtoMember(21)]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(23)]
    public List<GetPathData> GetPathList
    {
      get
      {
        return this._GetPathList;
      }
      set
      {
        this._GetPathList = value;
      }
    }

    [ProtoMember(24)]
    public string GetPathDesc
    {
      get
      {
        return this._GetPathDesc;
      }
      set
      {
        this._GetPathDesc = value;
      }
    }

    [ProtoMember(25)]
    public List<Goods> AlchemyReward
    {
      get
      {
        return this._AlchemyReward;
      }
      set
      {
        this._AlchemyReward = value;
      }
    }

    [ProtoMember(26)]
    public int RandomDropRewardID
    {
      get
      {
        return this._RandomDropRewardID;
      }
      set
      {
        this._RandomDropRewardID = value;
      }
    }

    [ProtoMember(27)]
    public int DisplayRewardCount
    {
      get
      {
        return this._DisplayRewardCount;
      }
      set
      {
        this._DisplayRewardCount = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
