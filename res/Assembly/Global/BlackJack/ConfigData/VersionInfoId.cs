﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.VersionInfoId
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "VersionInfoId")]
  public enum VersionInfoId
  {
    [ProtoEnum(Name = "VersionInfoId_ClientProgram", Value = 1)] VersionInfoId_ClientProgram = 1,
    [ProtoEnum(Name = "VersionInfoId_AssetBundle", Value = 2)] VersionInfoId_AssetBundle = 2,
    [ProtoEnum(Name = "VersionInfoId_BattleReport", Value = 3)] VersionInfoId_BattleReport = 3,
  }
}
