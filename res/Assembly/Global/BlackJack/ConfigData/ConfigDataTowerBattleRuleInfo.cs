﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataTowerBattleRuleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataTowerBattleRuleInfo")]
  [Serializable]
  public class ConfigDataTowerBattleRuleInfo : IExtensible
  {
    private int _ID;
    private int _Skill_ID;
    private int _Target;
    private string _Desc;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public int Skill_ID
    {
      get
      {
        return this._Skill_ID;
      }
      set
      {
        this._Skill_ID = value;
      }
    }

    [ProtoMember(4)]
    public int Target
    {
      get
      {
        return this._Target;
      }
      set
      {
        this._Target = value;
      }
    }

    [ProtoMember(5)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    public ConfigDataSkillInfo SkillInfo { get; set; }
  }
}
