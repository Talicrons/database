﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.MonthSignRewardInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  public class MonthSignRewardInfo
  {
    public int Month;
    public Dictionary<int, List<Goods>> SignRewards;

    [MethodImpl((MethodImplOptions) 32768)]
    public MonthSignRewardInfo()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
