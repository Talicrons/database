﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataEventInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataEventInfo")]
  [Serializable]
  public class ConfigDataEventInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private EventFuncType _FuncType;
    private int _EnergySuccess;
    private int _EnergyFail;
    private List<EventInfoDisappearCondition> _DisappearCondition;
    private int _WorldMap_ID;
    private int _MonsterLevel;
    private int _Battle_ID;
    private int _DialogBefore_ID;
    private int _DialogAfter_ID;
    private int _PlayerExpReward;
    private int _HeroExpReward;
    private int _GoldReward;
    private List<Goods> _Reward;
    private int _Drop_ID;
    private WaypointStyleType _StyleType;
    private string _Model;
    private int _ModelScale;
    private string _MapIcon;
    private int _MapIconY;
    private string _Icon;
    private bool _IsDanger;
    private int _CharImage_ID;
    private string _Strategy;
    private int _OperationalActivityDrop_ID;
    private IExtension extensionObject;
    public ConfigDataBattleInfo m_battleInfo;
    public ConfigDataDialogInfo m_dialogInfoBefore;
    public ConfigDataDialogInfo m_dialogInfoAfter;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEventInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(7)]
    public EventFuncType FuncType
    {
      get
      {
        return this._FuncType;
      }
      set
      {
        this._FuncType = value;
      }
    }

    [ProtoMember(8)]
    public int EnergySuccess
    {
      get
      {
        return this._EnergySuccess;
      }
      set
      {
        this._EnergySuccess = value;
      }
    }

    [ProtoMember(9)]
    public int EnergyFail
    {
      get
      {
        return this._EnergyFail;
      }
      set
      {
        this._EnergyFail = value;
      }
    }

    [ProtoMember(10)]
    public List<EventInfoDisappearCondition> DisappearCondition
    {
      get
      {
        return this._DisappearCondition;
      }
      set
      {
        this._DisappearCondition = value;
      }
    }

    [ProtoMember(11)]
    public int WorldMap_ID
    {
      get
      {
        return this._WorldMap_ID;
      }
      set
      {
        this._WorldMap_ID = value;
      }
    }

    [ProtoMember(12)]
    public int MonsterLevel
    {
      get
      {
        return this._MonsterLevel;
      }
      set
      {
        this._MonsterLevel = value;
      }
    }

    [ProtoMember(13)]
    public int Battle_ID
    {
      get
      {
        return this._Battle_ID;
      }
      set
      {
        this._Battle_ID = value;
      }
    }

    [ProtoMember(14)]
    public int DialogBefore_ID
    {
      get
      {
        return this._DialogBefore_ID;
      }
      set
      {
        this._DialogBefore_ID = value;
      }
    }

    [ProtoMember(15)]
    public int DialogAfter_ID
    {
      get
      {
        return this._DialogAfter_ID;
      }
      set
      {
        this._DialogAfter_ID = value;
      }
    }

    [ProtoMember(16)]
    public int PlayerExpReward
    {
      get
      {
        return this._PlayerExpReward;
      }
      set
      {
        this._PlayerExpReward = value;
      }
    }

    [ProtoMember(17)]
    public int HeroExpReward
    {
      get
      {
        return this._HeroExpReward;
      }
      set
      {
        this._HeroExpReward = value;
      }
    }

    [ProtoMember(18)]
    public int GoldReward
    {
      get
      {
        return this._GoldReward;
      }
      set
      {
        this._GoldReward = value;
      }
    }

    [ProtoMember(19)]
    public List<Goods> Reward
    {
      get
      {
        return this._Reward;
      }
      set
      {
        this._Reward = value;
      }
    }

    [ProtoMember(20)]
    public int Drop_ID
    {
      get
      {
        return this._Drop_ID;
      }
      set
      {
        this._Drop_ID = value;
      }
    }

    [ProtoMember(21)]
    public WaypointStyleType StyleType
    {
      get
      {
        return this._StyleType;
      }
      set
      {
        this._StyleType = value;
      }
    }

    [ProtoMember(22)]
    public string Model
    {
      get
      {
        return this._Model;
      }
      set
      {
        this._Model = value;
      }
    }

    [ProtoMember(23)]
    public int ModelScale
    {
      get
      {
        return this._ModelScale;
      }
      set
      {
        this._ModelScale = value;
      }
    }

    [ProtoMember(24)]
    public string MapIcon
    {
      get
      {
        return this._MapIcon;
      }
      set
      {
        this._MapIcon = value;
      }
    }

    [ProtoMember(25)]
    public int MapIconY
    {
      get
      {
        return this._MapIconY;
      }
      set
      {
        this._MapIconY = value;
      }
    }

    [ProtoMember(26)]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(27)]
    public bool IsDanger
    {
      get
      {
        return this._IsDanger;
      }
      set
      {
        this._IsDanger = value;
      }
    }

    [ProtoMember(28)]
    public int CharImage_ID
    {
      get
      {
        return this._CharImage_ID;
      }
      set
      {
        this._CharImage_ID = value;
      }
    }

    [ProtoMember(29)]
    public string Strategy
    {
      get
      {
        return this._Strategy;
      }
      set
      {
        this._Strategy = value;
      }
    }

    [ProtoMember(30)]
    public int OperationalActivityDrop_ID
    {
      get
      {
        return this._OperationalActivityDrop_ID;
      }
      set
      {
        this._OperationalActivityDrop_ID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
