﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataRushBattleActivityInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataRushBattleActivityInfo")]
  [Serializable]
  public class ConfigDataRushBattleActivityInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _TabImage;
    private string _BackgroundImage;
    private List<int> _SelectBattleRule_ID;
    private IExtension extensionObject;
    public List<ConfigDataRushBattleSelectBattleRuleInfo> m_selectBattleRules;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRushBattleActivityInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string TabImage
    {
      get
      {
        return this._TabImage;
      }
      set
      {
        this._TabImage = value;
      }
    }

    [ProtoMember(5)]
    public string BackgroundImage
    {
      get
      {
        return this._BackgroundImage;
      }
      set
      {
        this._BackgroundImage = value;
      }
    }

    [ProtoMember(6)]
    public List<int> SelectBattleRule_ID
    {
      get
      {
        return this._SelectBattleRule_ID;
      }
      set
      {
        this._SelectBattleRule_ID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
