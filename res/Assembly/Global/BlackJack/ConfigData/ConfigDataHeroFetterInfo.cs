﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataHeroFetterInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataHeroFetterInfo")]
  [Serializable]
  public class ConfigDataHeroFetterInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Icon;
    private int _MaxLevel;
    private List<HeroFetterCompletionCondition> _CompletionConditions;
    private List<Goods> _Reward;
    private List<int> _GotSkills_ID;
    private List<HeroFetterLevelUpCost> _LevelUpMaterials;
    private List<int> _LevelUpGold;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroFetterInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(5)]
    public int MaxLevel
    {
      get
      {
        return this._MaxLevel;
      }
      set
      {
        this._MaxLevel = value;
      }
    }

    [ProtoMember(6)]
    public List<HeroFetterCompletionCondition> CompletionConditions
    {
      get
      {
        return this._CompletionConditions;
      }
      set
      {
        this._CompletionConditions = value;
      }
    }

    [ProtoMember(7)]
    public List<Goods> Reward
    {
      get
      {
        return this._Reward;
      }
      set
      {
        this._Reward = value;
      }
    }

    [ProtoMember(8)]
    public List<int> GotSkills_ID
    {
      get
      {
        return this._GotSkills_ID;
      }
      set
      {
        this._GotSkills_ID = value;
      }
    }

    [ProtoMember(9)]
    public List<HeroFetterLevelUpCost> LevelUpMaterials
    {
      get
      {
        return this._LevelUpMaterials;
      }
      set
      {
        this._LevelUpMaterials = value;
      }
    }

    [ProtoMember(10)]
    public List<int> LevelUpGold
    {
      get
      {
        return this._LevelUpGold;
      }
      set
      {
        this._LevelUpGold = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
