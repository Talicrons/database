﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RealTimePVPMatchmakingFailInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RealTimePVPMatchmakingFailInfo")]
  [Serializable]
  public class RealTimePVPMatchmakingFailInfo : IExtensible
  {
    private int _DanMin;
    private int _DanMax;
    private bool _IsBot;
    private int _BotLevelAdjustment;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int DanMin
    {
      get
      {
        return this._DanMin;
      }
      set
      {
        this._DanMin = value;
      }
    }

    [ProtoMember(2)]
    public int DanMax
    {
      get
      {
        return this._DanMax;
      }
      set
      {
        this._DanMax = value;
      }
    }

    [ProtoMember(3)]
    public bool IsBot
    {
      get
      {
        return this._IsBot;
      }
      set
      {
        this._IsBot = value;
      }
    }

    [ProtoMember(4)]
    public int BotLevelAdjustment
    {
      get
      {
        return this._BotLevelAdjustment;
      }
      set
      {
        this._BotLevelAdjustment = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
