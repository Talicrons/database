﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataHeroPhantomLevelInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataHeroPhantomLevelInfo")]
  [Serializable]
  public class ConfigDataHeroPhantomLevelInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _PreLevel;
    private int _EnergySuccess;
    private int _EnergyFail;
    private int _MonsterLevel;
    private int _Battle_ID;
    private int _PlayerExp;
    private int _HeroExp;
    private int _Gold;
    private int _DropID;
    private List<Goods> _FirstClearDropItems;
    private int _DisplayRewardCount;
    private int _Achievement1ID;
    private List<Goods> _Achievement1BonusItem;
    private int _Achievement2ID;
    private List<Goods> _Achievement2BonusItem;
    private int _Achievement3ID;
    private List<Goods> _Achievement3BonusItem;
    private string _Strategy;
    private IExtension extensionObject;
    public BattleLevelAchievement[] m_achievements;
    public ConfigDataBattleInfo m_battleInfo;
    public ConfigDataHeroPhantomInfo m_groupInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroPhantomLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public int PreLevel
    {
      get
      {
        return this._PreLevel;
      }
      set
      {
        this._PreLevel = value;
      }
    }

    [ProtoMember(5)]
    public int EnergySuccess
    {
      get
      {
        return this._EnergySuccess;
      }
      set
      {
        this._EnergySuccess = value;
      }
    }

    [ProtoMember(6)]
    public int EnergyFail
    {
      get
      {
        return this._EnergyFail;
      }
      set
      {
        this._EnergyFail = value;
      }
    }

    [ProtoMember(7)]
    public int MonsterLevel
    {
      get
      {
        return this._MonsterLevel;
      }
      set
      {
        this._MonsterLevel = value;
      }
    }

    [ProtoMember(8)]
    public int Battle_ID
    {
      get
      {
        return this._Battle_ID;
      }
      set
      {
        this._Battle_ID = value;
      }
    }

    [ProtoMember(9)]
    public int PlayerExp
    {
      get
      {
        return this._PlayerExp;
      }
      set
      {
        this._PlayerExp = value;
      }
    }

    [ProtoMember(10)]
    public int HeroExp
    {
      get
      {
        return this._HeroExp;
      }
      set
      {
        this._HeroExp = value;
      }
    }

    [ProtoMember(11)]
    public int Gold
    {
      get
      {
        return this._Gold;
      }
      set
      {
        this._Gold = value;
      }
    }

    [ProtoMember(12)]
    public int DropID
    {
      get
      {
        return this._DropID;
      }
      set
      {
        this._DropID = value;
      }
    }

    [ProtoMember(13)]
    public List<Goods> FirstClearDropItems
    {
      get
      {
        return this._FirstClearDropItems;
      }
      set
      {
        this._FirstClearDropItems = value;
      }
    }

    [ProtoMember(14)]
    public int DisplayRewardCount
    {
      get
      {
        return this._DisplayRewardCount;
      }
      set
      {
        this._DisplayRewardCount = value;
      }
    }

    [ProtoMember(15)]
    public int Achievement1ID
    {
      get
      {
        return this._Achievement1ID;
      }
      set
      {
        this._Achievement1ID = value;
      }
    }

    [ProtoMember(16)]
    public List<Goods> Achievement1BonusItem
    {
      get
      {
        return this._Achievement1BonusItem;
      }
      set
      {
        this._Achievement1BonusItem = value;
      }
    }

    [ProtoMember(17)]
    public int Achievement2ID
    {
      get
      {
        return this._Achievement2ID;
      }
      set
      {
        this._Achievement2ID = value;
      }
    }

    [ProtoMember(18)]
    public List<Goods> Achievement2BonusItem
    {
      get
      {
        return this._Achievement2BonusItem;
      }
      set
      {
        this._Achievement2BonusItem = value;
      }
    }

    [ProtoMember(19)]
    public int Achievement3ID
    {
      get
      {
        return this._Achievement3ID;
      }
      set
      {
        this._Achievement3ID = value;
      }
    }

    [ProtoMember(20)]
    public List<Goods> Achievement3BonusItem
    {
      get
      {
        return this._Achievement3BonusItem;
      }
      set
      {
        this._Achievement3BonusItem = value;
      }
    }

    [ProtoMember(22)]
    public string Strategy
    {
      get
      {
        return this._Strategy;
      }
      set
      {
        this._Strategy = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
