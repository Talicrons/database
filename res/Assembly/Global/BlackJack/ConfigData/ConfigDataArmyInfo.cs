﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataArmyInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataArmyInfo")]
  [Serializable]
  public class ConfigDataArmyInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private ArmyTag _ArmyTag;
    private string _Icon;
    private string _Icon_NoBack;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(7)]
    public ArmyTag ArmyTag
    {
      get
      {
        return this._ArmyTag;
      }
      set
      {
        this._ArmyTag = value;
      }
    }

    [ProtoMember(8)]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(9)]
    public string Icon_NoBack
    {
      get
      {
        return this._Icon_NoBack;
      }
      set
      {
        this._Icon_NoBack = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
