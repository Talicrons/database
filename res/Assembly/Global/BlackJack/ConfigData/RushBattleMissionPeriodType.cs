﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RushBattleMissionPeriodType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RushBattleMissionPeriodType")]
  public enum RushBattleMissionPeriodType
  {
    [ProtoEnum(Name = "RushBattleMissionPeriodType_Week", Value = 1)] RushBattleMissionPeriodType_Week = 1,
    [ProtoEnum(Name = "RushBattleMissionPeriodType_OneOff", Value = 2)] RushBattleMissionPeriodType_OneOff = 2,
  }
}
