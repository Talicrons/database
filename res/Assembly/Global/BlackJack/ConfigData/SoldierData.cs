﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.SoldierData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "SoldierData")]
  [Serializable]
  public class SoldierData : IExtensible
  {
    private int _TechId;
    private int _TechLv;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int TechId
    {
      get
      {
        return this._TechId;
      }
      set
      {
        this._TechId = value;
      }
    }

    [ProtoMember(2)]
    public int TechLv
    {
      get
      {
        return this._TechLv;
      }
      set
      {
        this._TechLv = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
