﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RandomDropRewardGroup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  public class RandomDropRewardGroup
  {
    public int GroupIndex;
    public int DropCount;
    public Dictionary<int, WeightGoods> DropRewards;

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomDropRewardGroup()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
