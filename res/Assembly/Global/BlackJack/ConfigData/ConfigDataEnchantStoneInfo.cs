﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataEnchantStoneInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataEnchantStoneInfo")]
  [Serializable]
  public class ConfigDataEnchantStoneInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private int _Rank;
    private int _SellGold;
    private ItemDisplayType _DisplayType;
    private string _Icon;
    private List<GetPathData> _GetPathList;
    private string _GetPathDesc;
    private List<Goods> _AlchemyReward;
    private int _RandomDropRewardID;
    private int _DisplayRewardCount;
    private int _Resonance_ID;
    private int _CostGold;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEnchantStoneInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(5)]
    public int Rank
    {
      get
      {
        return this._Rank;
      }
      set
      {
        this._Rank = value;
      }
    }

    [ProtoMember(6)]
    public int SellGold
    {
      get
      {
        return this._SellGold;
      }
      set
      {
        this._SellGold = value;
      }
    }

    [ProtoMember(7)]
    public ItemDisplayType DisplayType
    {
      get
      {
        return this._DisplayType;
      }
      set
      {
        this._DisplayType = value;
      }
    }

    [ProtoMember(8)]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(9)]
    public List<GetPathData> GetPathList
    {
      get
      {
        return this._GetPathList;
      }
      set
      {
        this._GetPathList = value;
      }
    }

    [ProtoMember(10)]
    public string GetPathDesc
    {
      get
      {
        return this._GetPathDesc;
      }
      set
      {
        this._GetPathDesc = value;
      }
    }

    [ProtoMember(11)]
    public List<Goods> AlchemyReward
    {
      get
      {
        return this._AlchemyReward;
      }
      set
      {
        this._AlchemyReward = value;
      }
    }

    [ProtoMember(12)]
    public int RandomDropRewardID
    {
      get
      {
        return this._RandomDropRewardID;
      }
      set
      {
        this._RandomDropRewardID = value;
      }
    }

    [ProtoMember(13)]
    public int DisplayRewardCount
    {
      get
      {
        return this._DisplayRewardCount;
      }
      set
      {
        this._DisplayRewardCount = value;
      }
    }

    [ProtoMember(15)]
    public int Resonance_ID
    {
      get
      {
        return this._Resonance_ID;
      }
      set
      {
        this._Resonance_ID = value;
      }
    }

    [ProtoMember(17)]
    public int CostGold
    {
      get
      {
        return this._CostGold;
      }
      set
      {
        this._CostGold = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
