﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RafflePoolType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RafflePoolType")]
  public enum RafflePoolType
  {
    [ProtoEnum(Name = "RafflePoolType_None", Value = 0)] RafflePoolType_None,
    [ProtoEnum(Name = "RafflePoolType_Raffle", Value = 1)] RafflePoolType_Raffle,
    [ProtoEnum(Name = "RafflePoolType_Tarot", Value = 2)] RafflePoolType_Tarot,
  }
}
