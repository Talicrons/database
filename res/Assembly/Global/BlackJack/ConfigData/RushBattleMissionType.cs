﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RushBattleMissionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RushBattleMissionType")]
  public enum RushBattleMissionType
  {
    [ProtoEnum(Name = "RushBattleMissionType_WinningNums", Value = 1)] RushBattleMissionType_WinningNums = 1,
    [ProtoEnum(Name = "RushBattleMissionType_EnemyHeroesKilledNums", Value = 2)] RushBattleMissionType_EnemyHeroesKilledNums = 2,
    [ProtoEnum(Name = "RushBattleMissionType_TowersRemovedNums", Value = 3)] RushBattleMissionType_TowersRemovedNums = 3,
    [ProtoEnum(Name = "RushBattleMissionType_BattleNums", Value = 4)] RushBattleMissionType_BattleNums = 4,
    [ProtoEnum(Name = "RushBattleMissionType_CreditUp2", Value = 5)] RushBattleMissionType_CreditUp2 = 5,
    [ProtoEnum(Name = "RushBattleMissionType_LadderRankingDown2", Value = 6)] RushBattleMissionType_LadderRankingDown2 = 6,
  }
}
