﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.PeakArenaConsecutiveWinsMatchmakingScoreInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "PeakArenaConsecutiveWinsMatchmakingScoreInfo")]
  [Serializable]
  public class PeakArenaConsecutiveWinsMatchmakingScoreInfo : IExtensible
  {
    private int _Count;
    private int _ScoreAdjust;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int Count
    {
      get
      {
        return this._Count;
      }
      set
      {
        this._Count = value;
      }
    }

    [ProtoMember(2)]
    public int ScoreAdjust
    {
      get
      {
        return this._ScoreAdjust;
      }
      set
      {
        this._ScoreAdjust = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
