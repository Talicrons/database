﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.JobConnection2SkinResource
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "JobConnection2SkinResource")]
  [Serializable]
  public class JobConnection2SkinResource : IExtensible
  {
    private int _JobConnectionId;
    private int _SkinResourceId;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int JobConnectionId
    {
      get
      {
        return this._JobConnectionId;
      }
      set
      {
        this._JobConnectionId = value;
      }
    }

    [ProtoMember(2)]
    public int SkinResourceId
    {
      get
      {
        return this._SkinResourceId;
      }
      set
      {
        this._SkinResourceId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
