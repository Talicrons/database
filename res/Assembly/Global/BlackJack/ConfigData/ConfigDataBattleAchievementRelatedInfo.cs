﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataBattleAchievementRelatedInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataBattleAchievementRelatedInfo")]
  [Serializable]
  public class ConfigDataBattleAchievementRelatedInfo : IExtensible
  {
    private int _ID;
    private int _Achievement_ID;
    private IExtension extensionObject;
    public ConfigDataBattleAchievementInfo m_achievementInfo;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public int Achievement_ID
    {
      get
      {
        return this._Achievement_ID;
      }
      set
      {
        this._Achievement_ID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
