﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.GiftType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "GiftType")]
  public enum GiftType
  {
    [ProtoEnum(Name = "GiftType_Normal", Value = 0)] GiftType_Normal,
    [ProtoEnum(Name = "GiftType_Event", Value = 1)] GiftType_Event,
  }
}
