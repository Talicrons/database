﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ScenarioUnlockConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ScenarioUnlockConditionType")]
  public enum ScenarioUnlockConditionType
  {
    [ProtoEnum(Name = "ScenarioUnlockConditionType_None", Value = 0)] ScenarioUnlockConditionType_None,
    [ProtoEnum(Name = "ScenarioUnlockConditionType_PlayerLevel", Value = 1)] ScenarioUnlockConditionType_PlayerLevel,
    [ProtoEnum(Name = "ScenarioUnlockConditionType_RiftLevel", Value = 2)] ScenarioUnlockConditionType_RiftLevel,
    [ProtoEnum(Name = "ScenarioUnlockConditionType_BattleAchievement", Value = 3)] ScenarioUnlockConditionType_BattleAchievement,
    [ProtoEnum(Name = "ScenarioUnlockConditionType_Count", Value = 4)] ScenarioUnlockConditionType_Count,
  }
}
