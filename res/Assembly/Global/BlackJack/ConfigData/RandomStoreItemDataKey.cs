﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RandomStoreItemDataKey
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  public class RandomStoreItemDataKey
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public RandomStoreItemDataKey(int storeId, int levelZoneId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int GetHashCode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool Equals(object obj)
    {
      // ISSUE: unable to decompile the method.
    }

    public int StoreId { get; set; }

    public int LevelZoneId { get; set; }
  }
}
