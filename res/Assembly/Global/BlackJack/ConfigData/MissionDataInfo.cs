﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.MissionDataInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  public class MissionDataInfo
  {
    public List<ConfigDataMissionInfo> EverydayMissions;
    public List<ConfigDataMissionInfo> OneOffMissions;

    [MethodImpl((MethodImplOptions) 32768)]
    public MissionDataInfo()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
