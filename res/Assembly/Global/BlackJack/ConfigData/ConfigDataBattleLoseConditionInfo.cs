﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataBattleLoseConditionInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataBattleLoseConditionInfo")]
  [Serializable]
  public class ConfigDataBattleLoseConditionInfo : IExtensible
  {
    private int _ID;
    private BattleLoseConditionType _LoseConditionType;
    private List<int> _Param1;
    private List<int> _Param2;
    private List<ParamPosition> _Param3;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleLoseConditionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public BattleLoseConditionType LoseConditionType
    {
      get
      {
        return this._LoseConditionType;
      }
      set
      {
        this._LoseConditionType = value;
      }
    }

    [ProtoMember(4)]
    public List<int> Param1
    {
      get
      {
        return this._Param1;
      }
      set
      {
        this._Param1 = value;
      }
    }

    [ProtoMember(5)]
    public List<int> Param2
    {
      get
      {
        return this._Param2;
      }
      set
      {
        this._Param2 = value;
      }
    }

    [ProtoMember(6)]
    public List<ParamPosition> Param3
    {
      get
      {
        return this._Param3;
      }
      set
      {
        this._Param3 = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Param1FirstValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Param2FirstValue()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
