﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataTowerFloorInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataTowerFloorInfo")]
  [Serializable]
  public class ConfigDataTowerFloorInfo : IExtensible
  {
    private int _ID;
    private string _BigFloorName;
    private string _Name;
    private List<Goods> _RewardList;
    private List<int> _RandomLevelIdList;
    private int _EnergySuccess;
    private int _EnergyFail;
    private int _MonsterLevel;
    private int _PlayerExp;
    private int _BonusSkill_ID;
    private int _HasBonusHero;
    private string _BonusHeroDesc;
    private List<string> _BonusHeroDescParam;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTowerFloorInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string BigFloorName
    {
      get
      {
        return this._BigFloorName;
      }
      set
      {
        this._BigFloorName = value;
      }
    }

    [ProtoMember(4)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5)]
    public List<Goods> RewardList
    {
      get
      {
        return this._RewardList;
      }
      set
      {
        this._RewardList = value;
      }
    }

    [ProtoMember(6)]
    public List<int> RandomLevelIdList
    {
      get
      {
        return this._RandomLevelIdList;
      }
      set
      {
        this._RandomLevelIdList = value;
      }
    }

    [ProtoMember(7)]
    public int EnergySuccess
    {
      get
      {
        return this._EnergySuccess;
      }
      set
      {
        this._EnergySuccess = value;
      }
    }

    [ProtoMember(8)]
    public int EnergyFail
    {
      get
      {
        return this._EnergyFail;
      }
      set
      {
        this._EnergyFail = value;
      }
    }

    [ProtoMember(9)]
    public int MonsterLevel
    {
      get
      {
        return this._MonsterLevel;
      }
      set
      {
        this._MonsterLevel = value;
      }
    }

    [ProtoMember(10)]
    public int PlayerExp
    {
      get
      {
        return this._PlayerExp;
      }
      set
      {
        this._PlayerExp = value;
      }
    }

    [ProtoMember(11)]
    public int BonusSkill_ID
    {
      get
      {
        return this._BonusSkill_ID;
      }
      set
      {
        this._BonusSkill_ID = value;
      }
    }

    [ProtoMember(12)]
    public int HasBonusHero
    {
      get
      {
        return this._HasBonusHero;
      }
      set
      {
        this._HasBonusHero = value;
      }
    }

    [ProtoMember(13)]
    public string BonusHeroDesc
    {
      get
      {
        return this._BonusHeroDesc;
      }
      set
      {
        this._BonusHeroDesc = value;
      }
    }

    [ProtoMember(14)]
    public List<string> BonusHeroDescParam
    {
      get
      {
        return this._BonusHeroDescParam;
      }
      set
      {
        this._BonusHeroDescParam = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
