﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataUserGuide
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataUserGuide")]
  [Serializable]
  public class ConfigDataUserGuide : IExtensible
  {
    private int _ID;
    private string _Name;
    private List<UserGuideTrigger> _OpenTrigger;
    private List<string> _OTParam;
    private UserGuideCondition _OpenCondition;
    private string _OCParam;
    private int _FirstStepID;
    private UserGuideTrigger _CompleteTrigger;
    private string _CTParam;
    private List<int> _EnforceHeros;
    private UserGuideCondition _OpenCondition2;
    private string _OCParam2;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataUserGuide()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public List<UserGuideTrigger> OpenTrigger
    {
      get
      {
        return this._OpenTrigger;
      }
      set
      {
        this._OpenTrigger = value;
      }
    }

    [ProtoMember(5)]
    public List<string> OTParam
    {
      get
      {
        return this._OTParam;
      }
      set
      {
        this._OTParam = value;
      }
    }

    [ProtoMember(6)]
    public UserGuideCondition OpenCondition
    {
      get
      {
        return this._OpenCondition;
      }
      set
      {
        this._OpenCondition = value;
      }
    }

    [ProtoMember(7)]
    public string OCParam
    {
      get
      {
        return this._OCParam;
      }
      set
      {
        this._OCParam = value;
      }
    }

    [ProtoMember(8)]
    public int FirstStepID
    {
      get
      {
        return this._FirstStepID;
      }
      set
      {
        this._FirstStepID = value;
      }
    }

    [ProtoMember(9)]
    public UserGuideTrigger CompleteTrigger
    {
      get
      {
        return this._CompleteTrigger;
      }
      set
      {
        this._CompleteTrigger = value;
      }
    }

    [ProtoMember(10)]
    public string CTParam
    {
      get
      {
        return this._CTParam;
      }
      set
      {
        this._CTParam = value;
      }
    }

    [ProtoMember(11)]
    public List<int> EnforceHeros
    {
      get
      {
        return this._EnforceHeros;
      }
      set
      {
        this._EnforceHeros = value;
      }
    }

    [ProtoMember(12)]
    public UserGuideCondition OpenCondition2
    {
      get
      {
        return this._OpenCondition2;
      }
      set
      {
        this._OpenCondition2 = value;
      }
    }

    [ProtoMember(13)]
    public string OCParam2
    {
      get
      {
        return this._OCParam2;
      }
      set
      {
        this._OCParam2 = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
