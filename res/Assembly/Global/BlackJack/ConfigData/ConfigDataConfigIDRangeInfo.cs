﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataConfigIDRangeInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataConfigIDRangeInfo")]
  [Serializable]
  public class ConfigDataConfigIDRangeInfo : IExtensible
  {
    private int _ID;
    private string _ConfigObjectName;
    private List<int> _ConfigIDRange;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataConfigIDRangeInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(2)]
    public string ConfigObjectName
    {
      get
      {
        return this._ConfigObjectName;
      }
      set
      {
        this._ConfigObjectName = value;
      }
    }

    [ProtoMember(3)]
    public List<int> ConfigIDRange
    {
      get
      {
        return this._ConfigIDRange;
      }
      set
      {
        this._ConfigIDRange = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
