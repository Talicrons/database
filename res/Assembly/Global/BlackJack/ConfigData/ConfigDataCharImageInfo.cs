﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataCharImageInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataCharImageInfo")]
  [Serializable]
  public class ConfigDataCharImageInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Spine;
    private string _HeroPainting;
    private string _Image;
    private int _Direction;
    private string _CVName;
    private int _HeroDetailScale;
    private int _HeroDetailYOffset;
    private int _HeroFetterYOffset;
    private int _HeroConfessionYOffset;
    private int _HeroConfessionEndingYOffset;
    private int _HeroConfessionEndingScaleOffset;
    private int _CombatScale;
    private int _CombatYOffset;
    private int _BattleDialogScale;
    private int _BattleDialogYOffset;
    private int _DialogScale;
    private int _DialogYOffset;
    private string _RoundHeadImage;
    private string _SummonHeadImage;
    private string _CardHeadImage;
    private string _SmallHeadImage;
    private int _SkillCutscene_ID;
    private string _Sound_Die;
    private List<PerformanceEffect> _Performances;
    private List<PerformanceEffect> _BreakPerformances;
    private List<PerformanceEffect> _SummonPerformances;
    private List<PerformanceEffect> _JobTransferPerformances;
    private string _BattleActionVoice1;
    private string _BattleActionVoice2;
    private string _BattleActionVoice3;
    private List<FavourabilityPerformance> _NewPerformances;
    private List<FavourabilityPerformance> _NewBreakPerformances;
    private List<FavourabilityPerformance> _NewSummonPerformances;
    private List<FavourabilityPerformance> _NewJobTransferPerformances;
    private IExtension extensionObject;
    public ConfigDataCutsceneInfo m_skillCutsceneInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCharImageInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string Spine
    {
      get
      {
        return this._Spine;
      }
      set
      {
        this._Spine = value;
      }
    }

    [ProtoMember(5)]
    public string HeroPainting
    {
      get
      {
        return this._HeroPainting;
      }
      set
      {
        this._HeroPainting = value;
      }
    }

    [ProtoMember(6)]
    public string Image
    {
      get
      {
        return this._Image;
      }
      set
      {
        this._Image = value;
      }
    }

    [ProtoMember(7)]
    public int Direction
    {
      get
      {
        return this._Direction;
      }
      set
      {
        this._Direction = value;
      }
    }

    [ProtoMember(8)]
    public string CVName
    {
      get
      {
        return this._CVName;
      }
      set
      {
        this._CVName = value;
      }
    }

    [ProtoMember(9)]
    public int HeroDetailScale
    {
      get
      {
        return this._HeroDetailScale;
      }
      set
      {
        this._HeroDetailScale = value;
      }
    }

    [ProtoMember(10)]
    public int HeroDetailYOffset
    {
      get
      {
        return this._HeroDetailYOffset;
      }
      set
      {
        this._HeroDetailYOffset = value;
      }
    }

    [ProtoMember(11)]
    public int HeroFetterYOffset
    {
      get
      {
        return this._HeroFetterYOffset;
      }
      set
      {
        this._HeroFetterYOffset = value;
      }
    }

    [ProtoMember(12)]
    public int HeroConfessionYOffset
    {
      get
      {
        return this._HeroConfessionYOffset;
      }
      set
      {
        this._HeroConfessionYOffset = value;
      }
    }

    [ProtoMember(13)]
    public int HeroConfessionEndingYOffset
    {
      get
      {
        return this._HeroConfessionEndingYOffset;
      }
      set
      {
        this._HeroConfessionEndingYOffset = value;
      }
    }

    [ProtoMember(14)]
    public int HeroConfessionEndingScaleOffset
    {
      get
      {
        return this._HeroConfessionEndingScaleOffset;
      }
      set
      {
        this._HeroConfessionEndingScaleOffset = value;
      }
    }

    [ProtoMember(15)]
    public int CombatScale
    {
      get
      {
        return this._CombatScale;
      }
      set
      {
        this._CombatScale = value;
      }
    }

    [ProtoMember(16)]
    public int CombatYOffset
    {
      get
      {
        return this._CombatYOffset;
      }
      set
      {
        this._CombatYOffset = value;
      }
    }

    [ProtoMember(17)]
    public int BattleDialogScale
    {
      get
      {
        return this._BattleDialogScale;
      }
      set
      {
        this._BattleDialogScale = value;
      }
    }

    [ProtoMember(18)]
    public int BattleDialogYOffset
    {
      get
      {
        return this._BattleDialogYOffset;
      }
      set
      {
        this._BattleDialogYOffset = value;
      }
    }

    [ProtoMember(19)]
    public int DialogScale
    {
      get
      {
        return this._DialogScale;
      }
      set
      {
        this._DialogScale = value;
      }
    }

    [ProtoMember(20)]
    public int DialogYOffset
    {
      get
      {
        return this._DialogYOffset;
      }
      set
      {
        this._DialogYOffset = value;
      }
    }

    [ProtoMember(21)]
    public string RoundHeadImage
    {
      get
      {
        return this._RoundHeadImage;
      }
      set
      {
        this._RoundHeadImage = value;
      }
    }

    [ProtoMember(22)]
    public string SummonHeadImage
    {
      get
      {
        return this._SummonHeadImage;
      }
      set
      {
        this._SummonHeadImage = value;
      }
    }

    [ProtoMember(23)]
    public string CardHeadImage
    {
      get
      {
        return this._CardHeadImage;
      }
      set
      {
        this._CardHeadImage = value;
      }
    }

    [ProtoMember(24)]
    public string SmallHeadImage
    {
      get
      {
        return this._SmallHeadImage;
      }
      set
      {
        this._SmallHeadImage = value;
      }
    }

    [ProtoMember(25)]
    public int SkillCutscene_ID
    {
      get
      {
        return this._SkillCutscene_ID;
      }
      set
      {
        this._SkillCutscene_ID = value;
      }
    }

    [ProtoMember(26)]
    public string Sound_Die
    {
      get
      {
        return this._Sound_Die;
      }
      set
      {
        this._Sound_Die = value;
      }
    }

    [ProtoMember(27)]
    public List<PerformanceEffect> Performances
    {
      get
      {
        return this._Performances;
      }
      set
      {
        this._Performances = value;
      }
    }

    [ProtoMember(28)]
    public List<PerformanceEffect> BreakPerformances
    {
      get
      {
        return this._BreakPerformances;
      }
      set
      {
        this._BreakPerformances = value;
      }
    }

    [ProtoMember(29)]
    public List<PerformanceEffect> SummonPerformances
    {
      get
      {
        return this._SummonPerformances;
      }
      set
      {
        this._SummonPerformances = value;
      }
    }

    [ProtoMember(30)]
    public List<PerformanceEffect> JobTransferPerformances
    {
      get
      {
        return this._JobTransferPerformances;
      }
      set
      {
        this._JobTransferPerformances = value;
      }
    }

    [ProtoMember(31)]
    public string BattleActionVoice1
    {
      get
      {
        return this._BattleActionVoice1;
      }
      set
      {
        this._BattleActionVoice1 = value;
      }
    }

    [ProtoMember(32)]
    public string BattleActionVoice2
    {
      get
      {
        return this._BattleActionVoice2;
      }
      set
      {
        this._BattleActionVoice2 = value;
      }
    }

    [ProtoMember(33)]
    public string BattleActionVoice3
    {
      get
      {
        return this._BattleActionVoice3;
      }
      set
      {
        this._BattleActionVoice3 = value;
      }
    }

    [ProtoMember(34)]
    public List<FavourabilityPerformance> NewPerformances
    {
      get
      {
        return this._NewPerformances;
      }
      set
      {
        this._NewPerformances = value;
      }
    }

    [ProtoMember(35)]
    public List<FavourabilityPerformance> NewBreakPerformances
    {
      get
      {
        return this._NewBreakPerformances;
      }
      set
      {
        this._NewBreakPerformances = value;
      }
    }

    [ProtoMember(36)]
    public List<FavourabilityPerformance> NewSummonPerformances
    {
      get
      {
        return this._NewSummonPerformances;
      }
      set
      {
        this._NewSummonPerformances = value;
      }
    }

    [ProtoMember(37)]
    public List<FavourabilityPerformance> NewJobTransferPerformances
    {
      get
      {
        return this._NewJobTransferPerformances;
      }
      set
      {
        this._NewJobTransferPerformances = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
