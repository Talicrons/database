﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataBuffInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataBuffInfo")]
  [Serializable]
  public class ConfigDataBuffInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private bool _IsDebuff;
    private bool _IsEnhance;
    private bool _CanDispel;
    private int _Time;
    private int _Cond_HP_Target;
    private int _Cond_HP_Operator;
    private int _Cond_HP_Value;
    private BuffConditionType _ConditionType;
    private List<int> _ConditionParam;
    private bool _OnlyEnemy;
    private BuffType _BuffType;
    private int _BuffTypeParam1;
    private int _BuffTypeParam2;
    private int _BuffTypeParam3;
    private List<int> _BuffTypeParam4;
    private PropertyModifyType _BuffTypeParam5;
    private List<int> _BuffTypeParam6;
    private bool _SelfNoExtraTime;
    private int _SubType;
    private int _ReplaceRule;
    private int _ReplacePriority;
    private List<FightTag> _FightTags;
    private PropertyModifyType _Property1_ID;
    private int _Property1_Value;
    private PropertyModifyType _Property2_ID;
    private int _Property2_Value;
    private PropertyModifyType _Property3_ID;
    private int _Property3_Value;
    private PropertyModifyType _Property4_ID;
    private int _Property4_Value;
    private int _CDBuff_ID;
    private string _Effect_Attach;
    private string _Effect_Process;
    private string _Effect_Acting;
    private string _Effect_ActingTarget;
    private string _Icon;
    private bool _IconDisplay;
    private bool _TopDisplay;
    private IExtension extensionObject;
    public ConfigDataBuffInfo m_cdBuffInfo;
    public bool m_isNeedCollectPropertyModifiersAndFightTags;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBuffInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(5)]
    public bool IsDebuff
    {
      get
      {
        return this._IsDebuff;
      }
      set
      {
        this._IsDebuff = value;
      }
    }

    [ProtoMember(6)]
    public bool IsEnhance
    {
      get
      {
        return this._IsEnhance;
      }
      set
      {
        this._IsEnhance = value;
      }
    }

    [ProtoMember(7)]
    public bool CanDispel
    {
      get
      {
        return this._CanDispel;
      }
      set
      {
        this._CanDispel = value;
      }
    }

    [ProtoMember(8)]
    public int Time
    {
      get
      {
        return this._Time;
      }
      set
      {
        this._Time = value;
      }
    }

    [ProtoMember(9)]
    public int Cond_HP_Target
    {
      get
      {
        return this._Cond_HP_Target;
      }
      set
      {
        this._Cond_HP_Target = value;
      }
    }

    [ProtoMember(10)]
    public int Cond_HP_Operator
    {
      get
      {
        return this._Cond_HP_Operator;
      }
      set
      {
        this._Cond_HP_Operator = value;
      }
    }

    [ProtoMember(11)]
    public int Cond_HP_Value
    {
      get
      {
        return this._Cond_HP_Value;
      }
      set
      {
        this._Cond_HP_Value = value;
      }
    }

    [ProtoMember(12)]
    public BuffConditionType ConditionType
    {
      get
      {
        return this._ConditionType;
      }
      set
      {
        this._ConditionType = value;
      }
    }

    [ProtoMember(13)]
    public List<int> ConditionParam
    {
      get
      {
        return this._ConditionParam;
      }
      set
      {
        this._ConditionParam = value;
      }
    }

    [ProtoMember(14)]
    public bool OnlyEnemy
    {
      get
      {
        return this._OnlyEnemy;
      }
      set
      {
        this._OnlyEnemy = value;
      }
    }

    [ProtoMember(15)]
    public BuffType BuffType
    {
      get
      {
        return this._BuffType;
      }
      set
      {
        this._BuffType = value;
      }
    }

    [ProtoMember(16)]
    public int BuffTypeParam1
    {
      get
      {
        return this._BuffTypeParam1;
      }
      set
      {
        this._BuffTypeParam1 = value;
      }
    }

    [ProtoMember(17)]
    public int BuffTypeParam2
    {
      get
      {
        return this._BuffTypeParam2;
      }
      set
      {
        this._BuffTypeParam2 = value;
      }
    }

    [ProtoMember(18)]
    public int BuffTypeParam3
    {
      get
      {
        return this._BuffTypeParam3;
      }
      set
      {
        this._BuffTypeParam3 = value;
      }
    }

    [ProtoMember(19)]
    public List<int> BuffTypeParam4
    {
      get
      {
        return this._BuffTypeParam4;
      }
      set
      {
        this._BuffTypeParam4 = value;
      }
    }

    [ProtoMember(20)]
    public PropertyModifyType BuffTypeParam5
    {
      get
      {
        return this._BuffTypeParam5;
      }
      set
      {
        this._BuffTypeParam5 = value;
      }
    }

    [ProtoMember(21)]
    public List<int> BuffTypeParam6
    {
      get
      {
        return this._BuffTypeParam6;
      }
      set
      {
        this._BuffTypeParam6 = value;
      }
    }

    [ProtoMember(22)]
    public bool SelfNoExtraTime
    {
      get
      {
        return this._SelfNoExtraTime;
      }
      set
      {
        this._SelfNoExtraTime = value;
      }
    }

    [ProtoMember(23)]
    public int SubType
    {
      get
      {
        return this._SubType;
      }
      set
      {
        this._SubType = value;
      }
    }

    [ProtoMember(24)]
    public int ReplaceRule
    {
      get
      {
        return this._ReplaceRule;
      }
      set
      {
        this._ReplaceRule = value;
      }
    }

    [ProtoMember(25)]
    public int ReplacePriority
    {
      get
      {
        return this._ReplacePriority;
      }
      set
      {
        this._ReplacePriority = value;
      }
    }

    [ProtoMember(26)]
    public List<FightTag> FightTags
    {
      get
      {
        return this._FightTags;
      }
      set
      {
        this._FightTags = value;
      }
    }

    [ProtoMember(27)]
    public PropertyModifyType Property1_ID
    {
      get
      {
        return this._Property1_ID;
      }
      set
      {
        this._Property1_ID = value;
      }
    }

    [ProtoMember(28)]
    public int Property1_Value
    {
      get
      {
        return this._Property1_Value;
      }
      set
      {
        this._Property1_Value = value;
      }
    }

    [ProtoMember(29)]
    public PropertyModifyType Property2_ID
    {
      get
      {
        return this._Property2_ID;
      }
      set
      {
        this._Property2_ID = value;
      }
    }

    [ProtoMember(30)]
    public int Property2_Value
    {
      get
      {
        return this._Property2_Value;
      }
      set
      {
        this._Property2_Value = value;
      }
    }

    [ProtoMember(31)]
    public PropertyModifyType Property3_ID
    {
      get
      {
        return this._Property3_ID;
      }
      set
      {
        this._Property3_ID = value;
      }
    }

    [ProtoMember(32)]
    public int Property3_Value
    {
      get
      {
        return this._Property3_Value;
      }
      set
      {
        this._Property3_Value = value;
      }
    }

    [ProtoMember(33)]
    public PropertyModifyType Property4_ID
    {
      get
      {
        return this._Property4_ID;
      }
      set
      {
        this._Property4_ID = value;
      }
    }

    [ProtoMember(34)]
    public int Property4_Value
    {
      get
      {
        return this._Property4_Value;
      }
      set
      {
        this._Property4_Value = value;
      }
    }

    [ProtoMember(35)]
    public int CDBuff_ID
    {
      get
      {
        return this._CDBuff_ID;
      }
      set
      {
        this._CDBuff_ID = value;
      }
    }

    [ProtoMember(36)]
    public string Effect_Attach
    {
      get
      {
        return this._Effect_Attach;
      }
      set
      {
        this._Effect_Attach = value;
      }
    }

    [ProtoMember(37)]
    public string Effect_Process
    {
      get
      {
        return this._Effect_Process;
      }
      set
      {
        this._Effect_Process = value;
      }
    }

    [ProtoMember(38)]
    public string Effect_Acting
    {
      get
      {
        return this._Effect_Acting;
      }
      set
      {
        this._Effect_Acting = value;
      }
    }

    [ProtoMember(39)]
    public string Effect_ActingTarget
    {
      get
      {
        return this._Effect_ActingTarget;
      }
      set
      {
        this._Effect_ActingTarget = value;
      }
    }

    [ProtoMember(40)]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(41)]
    public bool IconDisplay
    {
      get
      {
        return this._IconDisplay;
      }
      set
      {
        this._IconDisplay = value;
      }
    }

    [ProtoMember(42)]
    public bool TopDisplay
    {
      get
      {
        return this._TopDisplay;
      }
      set
      {
        this._TopDisplay = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAnyAuraBuff()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsApplyBuffAuraBuff()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsModifyAuraBuff()
    {
      return this.BuffType == BuffType.BuffType_HeroAuraModify;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInfiniteTime()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool Is99Time()
    {
      return this.Time == 99;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsDisplayTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsDisplayLayer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsDisplayLayerNumber(int layer)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEffectiveDependOnOthers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPropertyModifyDependOnTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool HasFightTag(FightTag fightTag)
    {
      return this.FightTags.Contains(fightTag);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPropertyModifiersBuffType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasPropertyModifiers()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
