﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroSkinInfoSummonSkins
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroSkinInfoSummonSkins")]
  [Serializable]
  public class HeroSkinInfoSummonSkins : IExtensible
  {
    private int _HeroId;
    private int _SkinId;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int HeroId
    {
      get
      {
        return this._HeroId;
      }
      set
      {
        this._HeroId = value;
      }
    }

    [ProtoMember(2)]
    public int SkinId
    {
      get
      {
        return this._SkinId;
      }
      set
      {
        this._SkinId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
