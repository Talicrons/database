﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.FightTag
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "FightTag")]
  public enum FightTag
  {
    [ProtoEnum(Name = "FightTag_None", Value = 0)] FightTag_None,
    [ProtoEnum(Name = "FightTag_IngoreGuard", Value = 1)] FightTag_IngoreGuard,
    [ProtoEnum(Name = "FightTag_BanGuard", Value = 2)] FightTag_BanGuard,
    [ProtoEnum(Name = "FightTag_BanActiveSkill", Value = 3)] FightTag_BanActiveSkill,
    [ProtoEnum(Name = "FightTag_BanPassiveSkill", Value = 4)] FightTag_BanPassiveSkill,
    [ProtoEnum(Name = "FightTag_BanHeal", Value = 5)] FightTag_BanHeal,
    [ProtoEnum(Name = "FightTag_BanDamage", Value = 6)] FightTag_BanDamage,
    [ProtoEnum(Name = "FightTag_BanBuff", Value = 7)] FightTag_BanBuff,
    [ProtoEnum(Name = "FightTag_BanDeBuff", Value = 8)] FightTag_BanDeBuff,
    [ProtoEnum(Name = "FightTag_BanPunch", Value = 9)] FightTag_BanPunch,
    [ProtoEnum(Name = "FightTag_Stun", Value = 10)] FightTag_Stun,
    [ProtoEnum(Name = "FightTag_Hide", Value = 11)] FightTag_Hide,
    [ProtoEnum(Name = "FightTag_BanPercentDamage", Value = 12)] FightTag_BanPercentDamage,
    [ProtoEnum(Name = "FightTag_BanMeleePunish", Value = 13)] FightTag_BanMeleePunish,
    [ProtoEnum(Name = "FightTag_OnlyAttackHero", Value = 14)] FightTag_OnlyAttackHero,
    [ProtoEnum(Name = "FightTag_BanSoldierHeal", Value = 15)] FightTag_BanSoldierHeal,
    [ProtoEnum(Name = "FightTag_BanNewTurn", Value = 16)] FightTag_BanNewTurn,
    [ProtoEnum(Name = "FightTag_IgnoreCost", Value = 17)] FightTag_IgnoreCost,
    [ProtoEnum(Name = "FightTag_BanHPPercentDamage", Value = 18)] FightTag_BanHPPercentDamage,
    [ProtoEnum(Name = "FightTag_FirstStrike", Value = 19)] FightTag_FirstStrike,
    [ProtoEnum(Name = "FightTag_BanBFDamageEdge", Value = 20)] FightTag_BanBFDamageEdge,
    [ProtoEnum(Name = "FightTag_DamageLine", Value = 21)] FightTag_DamageLine,
    [ProtoEnum(Name = "FightTag_BanTeleport", Value = 22)] FightTag_BanTeleport,
    [ProtoEnum(Name = "FightTag_BanNerverDie", Value = 23)] FightTag_BanNerverDie,
    [ProtoEnum(Name = "FightTag_IsMagic", Value = 24)] FightTag_IsMagic,
    [ProtoEnum(Name = "FightTag_IsPsy", Value = 25)] FightTag_IsPsy,
    [ProtoEnum(Name = "FightTag_IgnoreBanPercentDamage", Value = 26)] FightTag_IgnoreBanPercentDamage,
    [ProtoEnum(Name = "FightTag_BanDoubleMove", Value = 27)] FightTag_BanDoubleMove,
    [ProtoEnum(Name = "FightTag_MoveLine", Value = 28)] FightTag_MoveLine,
    [ProtoEnum(Name = "FightTag_BanRemovecd", Value = 29)] FightTag_BanRemovecd,
    [ProtoEnum(Name = "FightTag_BanTerrainDamage", Value = 30)] FightTag_BanTerrainDamage,
    [ProtoEnum(Name = "FightTag_ThroughEnemy", Value = 31)] FightTag_ThroughEnemy,
    [ProtoEnum(Name = "FightTag_ForceTriggerKill", Value = 32)] FightTag_ForceTriggerKill,
    [ProtoEnum(Name = "FightTag_Sleep", Value = 33)] FightTag_Sleep,
  }
}
