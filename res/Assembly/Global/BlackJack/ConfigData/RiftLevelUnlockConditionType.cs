﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RiftLevelUnlockConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RiftLevelUnlockConditionType")]
  public enum RiftLevelUnlockConditionType
  {
    [ProtoEnum(Name = "RiftLevelUnlockConditionType_None", Value = 0)] RiftLevelUnlockConditionType_None,
    [ProtoEnum(Name = "RiftLevelUnlockConditionType_Scenario", Value = 1)] RiftLevelUnlockConditionType_Scenario,
    [ProtoEnum(Name = "RiftLevelUnlockConditionType_Achievement", Value = 2)] RiftLevelUnlockConditionType_Achievement,
    [ProtoEnum(Name = "RiftLevelUnlockConditionType_Hero", Value = 3)] RiftLevelUnlockConditionType_Hero,
    [ProtoEnum(Name = "RiftLevelUnlockConditionType_RiftLevel", Value = 4)] RiftLevelUnlockConditionType_RiftLevel,
  }
}
