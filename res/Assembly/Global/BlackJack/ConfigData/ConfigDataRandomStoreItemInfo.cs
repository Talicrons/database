﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataRandomStoreItemInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataRandomStoreItemInfo")]
  [Serializable]
  public class ConfigDataRandomStoreItemInfo : IExtensible
  {
    private int _ID;
    private int _StoreID;
    private int _LevelZoneID;
    private string _Name;
    private GoodsType _ItemType;
    private int _ItemID;
    private int _Count;
    private GoodsType _CurrencyType;
    private int _Price;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public int StoreID
    {
      get
      {
        return this._StoreID;
      }
      set
      {
        this._StoreID = value;
      }
    }

    [ProtoMember(4)]
    public int LevelZoneID
    {
      get
      {
        return this._LevelZoneID;
      }
      set
      {
        this._LevelZoneID = value;
      }
    }

    [ProtoMember(5)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(6)]
    public GoodsType ItemType
    {
      get
      {
        return this._ItemType;
      }
      set
      {
        this._ItemType = value;
      }
    }

    [ProtoMember(7)]
    public int ItemID
    {
      get
      {
        return this._ItemID;
      }
      set
      {
        this._ItemID = value;
      }
    }

    [ProtoMember(8)]
    public int Count
    {
      get
      {
        return this._Count;
      }
      set
      {
        this._Count = value;
      }
    }

    [ProtoMember(9)]
    public GoodsType CurrencyType
    {
      get
      {
        return this._CurrencyType;
      }
      set
      {
        this._CurrencyType = value;
      }
    }

    [ProtoMember(10)]
    public int Price
    {
      get
      {
        return this._Price;
      }
      set
      {
        this._Price = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
