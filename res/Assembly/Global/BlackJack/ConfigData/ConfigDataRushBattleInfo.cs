﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataRushBattleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataRushBattleInfo")]
  [Serializable]
  public class ConfigDataRushBattleInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private string _WinDesc;
    private string _LoseDesc;
    private int _Battlefield_ID;
    private int _CameraX;
    private int _CameraY;
    private int _DefendCameraX;
    private int _DefendCameraY;
    private string _PrepareMusic;
    private string _BattleMusic;
    private string _DefendBattleMusic;
    private int _TurnMax;
    private int _FastKillTurn;
    private List<int> _WinConditions_ID;
    private List<int> _LoseConditions_ID;
    private List<int> _EventTriggers_ID;
    private List<int> _Buff_ID;
    private List<ParamPosition> _AttackPickPositions;
    private List<int> _AttackPickDirs;
    private int _AttackPickCount;
    private List<ParamPosition> _DefendPickPositions;
    private List<int> _DefendPickDirs;
    private int _DefendPickCount;
    private List<BattlePosActor> _AttackActors;
    private List<int> _AttackDirs;
    private List<BattlePosActor> _DefendActors;
    private List<int> _DefendDirs;
    private string _MapName;
    private string _MapImage;
    private IExtension extensionObject;
    public ConfigDataBattlefieldInfo m_battlefieldInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRushBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(5)]
    public string WinDesc
    {
      get
      {
        return this._WinDesc;
      }
      set
      {
        this._WinDesc = value;
      }
    }

    [ProtoMember(6)]
    public string LoseDesc
    {
      get
      {
        return this._LoseDesc;
      }
      set
      {
        this._LoseDesc = value;
      }
    }

    [ProtoMember(7)]
    public int Battlefield_ID
    {
      get
      {
        return this._Battlefield_ID;
      }
      set
      {
        this._Battlefield_ID = value;
      }
    }

    [ProtoMember(8)]
    public int CameraX
    {
      get
      {
        return this._CameraX;
      }
      set
      {
        this._CameraX = value;
      }
    }

    [ProtoMember(9)]
    public int CameraY
    {
      get
      {
        return this._CameraY;
      }
      set
      {
        this._CameraY = value;
      }
    }

    [ProtoMember(10)]
    public int DefendCameraX
    {
      get
      {
        return this._DefendCameraX;
      }
      set
      {
        this._DefendCameraX = value;
      }
    }

    [ProtoMember(11)]
    public int DefendCameraY
    {
      get
      {
        return this._DefendCameraY;
      }
      set
      {
        this._DefendCameraY = value;
      }
    }

    [ProtoMember(12)]
    public string PrepareMusic
    {
      get
      {
        return this._PrepareMusic;
      }
      set
      {
        this._PrepareMusic = value;
      }
    }

    [ProtoMember(13)]
    public string BattleMusic
    {
      get
      {
        return this._BattleMusic;
      }
      set
      {
        this._BattleMusic = value;
      }
    }

    [ProtoMember(14)]
    public string DefendBattleMusic
    {
      get
      {
        return this._DefendBattleMusic;
      }
      set
      {
        this._DefendBattleMusic = value;
      }
    }

    [ProtoMember(15)]
    public int TurnMax
    {
      get
      {
        return this._TurnMax;
      }
      set
      {
        this._TurnMax = value;
      }
    }

    [ProtoMember(16)]
    public int FastKillTurn
    {
      get
      {
        return this._FastKillTurn;
      }
      set
      {
        this._FastKillTurn = value;
      }
    }

    [ProtoMember(17)]
    public List<int> WinConditions_ID
    {
      get
      {
        return this._WinConditions_ID;
      }
      set
      {
        this._WinConditions_ID = value;
      }
    }

    [ProtoMember(18)]
    public List<int> LoseConditions_ID
    {
      get
      {
        return this._LoseConditions_ID;
      }
      set
      {
        this._LoseConditions_ID = value;
      }
    }

    [ProtoMember(19)]
    public List<int> EventTriggers_ID
    {
      get
      {
        return this._EventTriggers_ID;
      }
      set
      {
        this._EventTriggers_ID = value;
      }
    }

    [ProtoMember(20)]
    public List<int> Buff_ID
    {
      get
      {
        return this._Buff_ID;
      }
      set
      {
        this._Buff_ID = value;
      }
    }

    [ProtoMember(21)]
    public List<ParamPosition> AttackPickPositions
    {
      get
      {
        return this._AttackPickPositions;
      }
      set
      {
        this._AttackPickPositions = value;
      }
    }

    [ProtoMember(22)]
    public List<int> AttackPickDirs
    {
      get
      {
        return this._AttackPickDirs;
      }
      set
      {
        this._AttackPickDirs = value;
      }
    }

    [ProtoMember(23)]
    public int AttackPickCount
    {
      get
      {
        return this._AttackPickCount;
      }
      set
      {
        this._AttackPickCount = value;
      }
    }

    [ProtoMember(24)]
    public List<ParamPosition> DefendPickPositions
    {
      get
      {
        return this._DefendPickPositions;
      }
      set
      {
        this._DefendPickPositions = value;
      }
    }

    [ProtoMember(25)]
    public List<int> DefendPickDirs
    {
      get
      {
        return this._DefendPickDirs;
      }
      set
      {
        this._DefendPickDirs = value;
      }
    }

    [ProtoMember(26)]
    public int DefendPickCount
    {
      get
      {
        return this._DefendPickCount;
      }
      set
      {
        this._DefendPickCount = value;
      }
    }

    [ProtoMember(27)]
    public List<BattlePosActor> AttackActors
    {
      get
      {
        return this._AttackActors;
      }
      set
      {
        this._AttackActors = value;
      }
    }

    [ProtoMember(28)]
    public List<int> AttackDirs
    {
      get
      {
        return this._AttackDirs;
      }
      set
      {
        this._AttackDirs = value;
      }
    }

    [ProtoMember(29)]
    public List<BattlePosActor> DefendActors
    {
      get
      {
        return this._DefendActors;
      }
      set
      {
        this._DefendActors = value;
      }
    }

    [ProtoMember(30)]
    public List<int> DefendDirs
    {
      get
      {
        return this._DefendDirs;
      }
      set
      {
        this._DefendDirs = value;
      }
    }

    [ProtoMember(31)]
    public string MapName
    {
      get
      {
        return this._MapName;
      }
      set
      {
        this._MapName = value;
      }
    }

    [ProtoMember(32)]
    public string MapImage
    {
      get
      {
        return this._MapImage;
      }
      set
      {
        this._MapImage = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
