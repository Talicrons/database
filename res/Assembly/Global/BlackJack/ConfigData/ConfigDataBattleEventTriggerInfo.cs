﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataBattleEventTriggerInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataBattleEventTriggerInfo")]
  [Serializable]
  public class ConfigDataBattleEventTriggerInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private BattleEventTriggerType _TriggerType;
    private List<int> _Param1;
    private List<int> _Param2;
    private List<ParamPosition> _Param3;
    private List<int> _Actions_ID;
    private int _TriggerCountMax;
    private int _TriggerTurnCountMax;
    private IExtension extensionObject;
    public ConfigDataBattleEventActionInfo[] m_battleEventActionInfos;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleEventTriggerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5)]
    public BattleEventTriggerType TriggerType
    {
      get
      {
        return this._TriggerType;
      }
      set
      {
        this._TriggerType = value;
      }
    }

    [ProtoMember(6)]
    public List<int> Param1
    {
      get
      {
        return this._Param1;
      }
      set
      {
        this._Param1 = value;
      }
    }

    [ProtoMember(7)]
    public List<int> Param2
    {
      get
      {
        return this._Param2;
      }
      set
      {
        this._Param2 = value;
      }
    }

    [ProtoMember(8)]
    public List<ParamPosition> Param3
    {
      get
      {
        return this._Param3;
      }
      set
      {
        this._Param3 = value;
      }
    }

    [ProtoMember(9)]
    public List<int> Actions_ID
    {
      get
      {
        return this._Actions_ID;
      }
      set
      {
        this._Actions_ID = value;
      }
    }

    [ProtoMember(10)]
    public int TriggerCountMax
    {
      get
      {
        return this._TriggerCountMax;
      }
      set
      {
        this._TriggerCountMax = value;
      }
    }

    [ProtoMember(11)]
    public int TriggerTurnCountMax
    {
      get
      {
        return this._TriggerTurnCountMax;
      }
      set
      {
        this._TriggerTurnCountMax = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Param1FirstValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Param2FirstValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Param3FirstValue()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
