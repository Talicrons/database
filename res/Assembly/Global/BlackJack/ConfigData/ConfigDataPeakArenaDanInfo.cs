﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataPeakArenaDanInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataPeakArenaDanInfo")]
  [Serializable]
  public class ConfigDataPeakArenaDanInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _DisplayDan;
    private int _DanGroup;
    private bool _IsDanGroupProtected;
    private int _BaselineScore;
    private int _NextSeasonDan;
    private List<PeakArenaWinScoreDiff> _WinScoreDiffBonus;
    private List<PeakArenaLossScoreDiff> _LossScoreDiffBonus;
    private List<PeakArenaConsecutiveWins> _ConsecutiveWinScoreBonus;
    private int _WinBasicScore;
    private int _LossBasicScore;
    private int _DanDiffUpperBound;
    private int _DanDiffLowerBound;
    private List<WaitingTimeInfo> _WaitingTimeAdjustment;
    private List<PeakArenaConsecutiveWinsMatchmakingScoreInfo> _ConsecutiveWinsMatchmakingScoreAdjustment;
    private List<PeakArenaConsecutiveLossesMatchmakingInfo> _ConsecutiveLossesMatchmakingAdjustment;
    private List<PeakArenaMatchmakingFailInfo> _MatchmakingFailAdjustment;
    private string _Icon;
    private List<FriendlyWaitingTimeInfo> _FriendlyWaitingTimeAdjustment;
    private int _DanRewardMailTemplateId;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaDanInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public int DisplayDan
    {
      get
      {
        return this._DisplayDan;
      }
      set
      {
        this._DisplayDan = value;
      }
    }

    [ProtoMember(5)]
    public int DanGroup
    {
      get
      {
        return this._DanGroup;
      }
      set
      {
        this._DanGroup = value;
      }
    }

    [ProtoMember(6)]
    public bool IsDanGroupProtected
    {
      get
      {
        return this._IsDanGroupProtected;
      }
      set
      {
        this._IsDanGroupProtected = value;
      }
    }

    [ProtoMember(7)]
    public int BaselineScore
    {
      get
      {
        return this._BaselineScore;
      }
      set
      {
        this._BaselineScore = value;
      }
    }

    [ProtoMember(8)]
    public int NextSeasonDan
    {
      get
      {
        return this._NextSeasonDan;
      }
      set
      {
        this._NextSeasonDan = value;
      }
    }

    [ProtoMember(9)]
    public List<PeakArenaWinScoreDiff> WinScoreDiffBonus
    {
      get
      {
        return this._WinScoreDiffBonus;
      }
      set
      {
        this._WinScoreDiffBonus = value;
      }
    }

    [ProtoMember(10)]
    public List<PeakArenaLossScoreDiff> LossScoreDiffBonus
    {
      get
      {
        return this._LossScoreDiffBonus;
      }
      set
      {
        this._LossScoreDiffBonus = value;
      }
    }

    [ProtoMember(11)]
    public List<PeakArenaConsecutiveWins> ConsecutiveWinScoreBonus
    {
      get
      {
        return this._ConsecutiveWinScoreBonus;
      }
      set
      {
        this._ConsecutiveWinScoreBonus = value;
      }
    }

    [ProtoMember(12)]
    public int WinBasicScore
    {
      get
      {
        return this._WinBasicScore;
      }
      set
      {
        this._WinBasicScore = value;
      }
    }

    [ProtoMember(13)]
    public int LossBasicScore
    {
      get
      {
        return this._LossBasicScore;
      }
      set
      {
        this._LossBasicScore = value;
      }
    }

    [ProtoMember(14)]
    public int DanDiffUpperBound
    {
      get
      {
        return this._DanDiffUpperBound;
      }
      set
      {
        this._DanDiffUpperBound = value;
      }
    }

    [ProtoMember(15)]
    public int DanDiffLowerBound
    {
      get
      {
        return this._DanDiffLowerBound;
      }
      set
      {
        this._DanDiffLowerBound = value;
      }
    }

    [ProtoMember(16)]
    public List<WaitingTimeInfo> WaitingTimeAdjustment
    {
      get
      {
        return this._WaitingTimeAdjustment;
      }
      set
      {
        this._WaitingTimeAdjustment = value;
      }
    }

    [ProtoMember(17)]
    public List<PeakArenaConsecutiveWinsMatchmakingScoreInfo> ConsecutiveWinsMatchmakingScoreAdjustment
    {
      get
      {
        return this._ConsecutiveWinsMatchmakingScoreAdjustment;
      }
      set
      {
        this._ConsecutiveWinsMatchmakingScoreAdjustment = value;
      }
    }

    [ProtoMember(18)]
    public List<PeakArenaConsecutiveLossesMatchmakingInfo> ConsecutiveLossesMatchmakingAdjustment
    {
      get
      {
        return this._ConsecutiveLossesMatchmakingAdjustment;
      }
      set
      {
        this._ConsecutiveLossesMatchmakingAdjustment = value;
      }
    }

    [ProtoMember(19)]
    public List<PeakArenaMatchmakingFailInfo> MatchmakingFailAdjustment
    {
      get
      {
        return this._MatchmakingFailAdjustment;
      }
      set
      {
        this._MatchmakingFailAdjustment = value;
      }
    }

    [ProtoMember(20)]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(21)]
    public List<FriendlyWaitingTimeInfo> FriendlyWaitingTimeAdjustment
    {
      get
      {
        return this._FriendlyWaitingTimeAdjustment;
      }
      set
      {
        this._FriendlyWaitingTimeAdjustment = value;
      }
    }

    [ProtoMember(22)]
    public int DanRewardMailTemplateId
    {
      get
      {
        return this._DanRewardMailTemplateId;
      }
      set
      {
        this._DanRewardMailTemplateId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
