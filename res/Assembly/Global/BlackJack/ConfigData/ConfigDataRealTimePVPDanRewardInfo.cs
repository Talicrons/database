﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataRealTimePVPDanRewardInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataRealTimePVPDanRewardInfo")]
  [Serializable]
  public class ConfigDataRealTimePVPDanRewardInfo : IExtensible
  {
    private int _ID;
    private int _Dan;
    private int _DanRewardMailTemplateId;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public int Dan
    {
      get
      {
        return this._Dan;
      }
      set
      {
        this._Dan = value;
      }
    }

    [ProtoMember(4)]
    public int DanRewardMailTemplateId
    {
      get
      {
        return this._DanRewardMailTemplateId;
      }
      set
      {
        this._DanRewardMailTemplateId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
