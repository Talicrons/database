﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataMusicAppreciateTable
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataMusicAppreciateTable")]
  [Serializable]
  public class ConfigDataMusicAppreciateTable : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _Tab;
    private int _SortOrder;
    private string _SoundName;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public int Tab
    {
      get
      {
        return this._Tab;
      }
      set
      {
        this._Tab = value;
      }
    }

    [ProtoMember(5)]
    public int SortOrder
    {
      get
      {
        return this._SortOrder;
      }
      set
      {
        this._SortOrder = value;
      }
    }

    [ProtoMember(6)]
    public string SoundName
    {
      get
      {
        return this._SoundName;
      }
      set
      {
        this._SoundName = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
