﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataPeakArenaRankingRewardInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataPeakArenaRankingRewardInfo")]
  [Serializable]
  public class ConfigDataPeakArenaRankingRewardInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _Rank;
    private int _RankingRewardMailTemplateId;
    private int _SeasonId;
    private int _SeasonRankTitleId;
    private string _RankListName;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public int Rank
    {
      get
      {
        return this._Rank;
      }
      set
      {
        this._Rank = value;
      }
    }

    [ProtoMember(5)]
    public int RankingRewardMailTemplateId
    {
      get
      {
        return this._RankingRewardMailTemplateId;
      }
      set
      {
        this._RankingRewardMailTemplateId = value;
      }
    }

    [ProtoMember(6)]
    public int SeasonId
    {
      get
      {
        return this._SeasonId;
      }
      set
      {
        this._SeasonId = value;
      }
    }

    [ProtoMember(7)]
    public int SeasonRankTitleId
    {
      get
      {
        return this._SeasonRankTitleId;
      }
      set
      {
        this._SeasonRankTitleId = value;
      }
    }

    [ProtoMember(8)]
    public string RankListName
    {
      get
      {
        return this._RankListName;
      }
      set
      {
        this._RankListName = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
