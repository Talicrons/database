﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.FetterCompleteConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "FetterCompleteConditionType")]
  public enum FetterCompleteConditionType
  {
    [ProtoEnum(Name = "FetterCompleteConditionType_None", Value = 0)] FetterCompleteConditionType_None,
    [ProtoEnum(Name = "FetterCompleteConditionType_HeroFavorabilityLevel", Value = 1)] FetterCompleteConditionType_HeroFavorabilityLevel,
    [ProtoEnum(Name = "FetterCompleteConditionType_Mission", Value = 2)] FetterCompleteConditionType_Mission,
  }
}
