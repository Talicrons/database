﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataMissionInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataMissionInfo")]
  [Serializable]
  public class ConfigDataMissionInfo : IExtensible
  {
    private int _ID;
    private string _Title;
    private string _Desc;
    private MissionPeriodType _MissionPeriod;
    private MissionColumnType _MissionColumn;
    private MissionType _MissionType;
    private int _Param1;
    private int _Param2;
    private int _Param3;
    private int _Param4;
    private List<int> _Param5;
    private int _MissionUnlockPlayerLvl;
    private int _MissionUnlockPreTaskID;
    private int _MissionUnlockScenarioID;
    private int _SortID;
    private List<Goods> _Reward;
    private int _NoviceExt;
    private List<GetPathData> _GetPathList;
    private bool _IsMonthCardMission;
    private bool _Obsoleted;
    private List<Goods> _LevelMaxReward;
    private bool _InEveryDay;
    private int _RefluxExt;
    private List<Goods> _GrowUpSpeedUpExt;
    private IExtension extensionObject;
    public ConfigDataMissionExtNoviceInfo m_Novice;
    public ConfigDataMissionExtRefluxInfo m_Reflux;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataMissionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Title
    {
      get
      {
        return this._Title;
      }
      set
      {
        this._Title = value;
      }
    }

    [ProtoMember(4)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(5)]
    public MissionPeriodType MissionPeriod
    {
      get
      {
        return this._MissionPeriod;
      }
      set
      {
        this._MissionPeriod = value;
      }
    }

    [ProtoMember(6)]
    public MissionColumnType MissionColumn
    {
      get
      {
        return this._MissionColumn;
      }
      set
      {
        this._MissionColumn = value;
      }
    }

    [ProtoMember(7)]
    public MissionType MissionType
    {
      get
      {
        return this._MissionType;
      }
      set
      {
        this._MissionType = value;
      }
    }

    [ProtoMember(8)]
    public int Param1
    {
      get
      {
        return this._Param1;
      }
      set
      {
        this._Param1 = value;
      }
    }

    [ProtoMember(9)]
    public int Param2
    {
      get
      {
        return this._Param2;
      }
      set
      {
        this._Param2 = value;
      }
    }

    [ProtoMember(10)]
    public int Param3
    {
      get
      {
        return this._Param3;
      }
      set
      {
        this._Param3 = value;
      }
    }

    [ProtoMember(11)]
    public int Param4
    {
      get
      {
        return this._Param4;
      }
      set
      {
        this._Param4 = value;
      }
    }

    [ProtoMember(12)]
    public List<int> Param5
    {
      get
      {
        return this._Param5;
      }
      set
      {
        this._Param5 = value;
      }
    }

    [ProtoMember(13)]
    public int MissionUnlockPlayerLvl
    {
      get
      {
        return this._MissionUnlockPlayerLvl;
      }
      set
      {
        this._MissionUnlockPlayerLvl = value;
      }
    }

    [ProtoMember(14)]
    public int MissionUnlockPreTaskID
    {
      get
      {
        return this._MissionUnlockPreTaskID;
      }
      set
      {
        this._MissionUnlockPreTaskID = value;
      }
    }

    [ProtoMember(15)]
    public int MissionUnlockScenarioID
    {
      get
      {
        return this._MissionUnlockScenarioID;
      }
      set
      {
        this._MissionUnlockScenarioID = value;
      }
    }

    [ProtoMember(16)]
    public int SortID
    {
      get
      {
        return this._SortID;
      }
      set
      {
        this._SortID = value;
      }
    }

    [ProtoMember(17)]
    public List<Goods> Reward
    {
      get
      {
        return this._Reward;
      }
      set
      {
        this._Reward = value;
      }
    }

    [ProtoMember(18)]
    public int NoviceExt
    {
      get
      {
        return this._NoviceExt;
      }
      set
      {
        this._NoviceExt = value;
      }
    }

    [ProtoMember(20)]
    public List<GetPathData> GetPathList
    {
      get
      {
        return this._GetPathList;
      }
      set
      {
        this._GetPathList = value;
      }
    }

    [ProtoMember(21)]
    public bool IsMonthCardMission
    {
      get
      {
        return this._IsMonthCardMission;
      }
      set
      {
        this._IsMonthCardMission = value;
      }
    }

    [ProtoMember(22)]
    public bool Obsoleted
    {
      get
      {
        return this._Obsoleted;
      }
      set
      {
        this._Obsoleted = value;
      }
    }

    [ProtoMember(23)]
    public List<Goods> LevelMaxReward
    {
      get
      {
        return this._LevelMaxReward;
      }
      set
      {
        this._LevelMaxReward = value;
      }
    }

    [ProtoMember(24)]
    public bool InEveryDay
    {
      get
      {
        return this._InEveryDay;
      }
      set
      {
        this._InEveryDay = value;
      }
    }

    [ProtoMember(25)]
    public int RefluxExt
    {
      get
      {
        return this._RefluxExt;
      }
      set
      {
        this._RefluxExt = value;
      }
    }

    [ProtoMember(26)]
    public List<Goods> GrowUpSpeedUpExt
    {
      get
      {
        return this._GrowUpSpeedUpExt;
      }
      set
      {
        this._GrowUpSpeedUpExt = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Goods> GetRealRewards(bool isLevelMax)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
