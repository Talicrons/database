﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataGiftCDKeyInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataGiftCDKeyInfo")]
  [Serializable]
  public class ConfigDataGiftCDKeyInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _RankRewardMailTemplateId;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public int RankRewardMailTemplateId
    {
      get
      {
        return this._RankRewardMailTemplateId;
      }
      set
      {
        this._RankRewardMailTemplateId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
