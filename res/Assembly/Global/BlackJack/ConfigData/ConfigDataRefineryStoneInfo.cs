﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataRefineryStoneInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataRefineryStoneInfo")]
  [Serializable]
  public class ConfigDataRefineryStoneInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Description;
    private int _Rank;
    private string _IconLocation;
    private List<int> _LimitArmyType;
    private int _ConsumeGold;
    private List<int> _RefineryTemplateIds;
    private PropertyModifyType _RefineryPropertyType;
    private List<int> _SlotIds;
    private List<GetPathData> _GetPathList;
    private string _GetPathDesc;
    private List<Goods> _AlchemyReward;
    private int _RandomDropRewardID;
    private int _DisplayRewardCount;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRefineryStoneInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string Description
    {
      get
      {
        return this._Description;
      }
      set
      {
        this._Description = value;
      }
    }

    [ProtoMember(5)]
    public int Rank
    {
      get
      {
        return this._Rank;
      }
      set
      {
        this._Rank = value;
      }
    }

    [ProtoMember(6)]
    public string IconLocation
    {
      get
      {
        return this._IconLocation;
      }
      set
      {
        this._IconLocation = value;
      }
    }

    [ProtoMember(7)]
    public List<int> LimitArmyType
    {
      get
      {
        return this._LimitArmyType;
      }
      set
      {
        this._LimitArmyType = value;
      }
    }

    [ProtoMember(8)]
    public int ConsumeGold
    {
      get
      {
        return this._ConsumeGold;
      }
      set
      {
        this._ConsumeGold = value;
      }
    }

    [ProtoMember(9)]
    public List<int> RefineryTemplateIds
    {
      get
      {
        return this._RefineryTemplateIds;
      }
      set
      {
        this._RefineryTemplateIds = value;
      }
    }

    [ProtoMember(10)]
    public PropertyModifyType RefineryPropertyType
    {
      get
      {
        return this._RefineryPropertyType;
      }
      set
      {
        this._RefineryPropertyType = value;
      }
    }

    [ProtoMember(11)]
    public List<int> SlotIds
    {
      get
      {
        return this._SlotIds;
      }
      set
      {
        this._SlotIds = value;
      }
    }

    [ProtoMember(12)]
    public List<GetPathData> GetPathList
    {
      get
      {
        return this._GetPathList;
      }
      set
      {
        this._GetPathList = value;
      }
    }

    [ProtoMember(13)]
    public string GetPathDesc
    {
      get
      {
        return this._GetPathDesc;
      }
      set
      {
        this._GetPathDesc = value;
      }
    }

    [ProtoMember(14)]
    public List<Goods> AlchemyReward
    {
      get
      {
        return this._AlchemyReward;
      }
      set
      {
        this._AlchemyReward = value;
      }
    }

    [ProtoMember(15)]
    public int RandomDropRewardID
    {
      get
      {
        return this._RandomDropRewardID;
      }
      set
      {
        this._RandomDropRewardID = value;
      }
    }

    [ProtoMember(16)]
    public int DisplayRewardCount
    {
      get
      {
        return this._DisplayRewardCount;
      }
      set
      {
        this._DisplayRewardCount = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
