﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.StoreRedMarkType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "StoreRedMarkType")]
  public enum StoreRedMarkType
  {
    [ProtoEnum(Name = "StoreRedMarkType_None", Value = 0)] StoreRedMarkType_None,
    [ProtoEnum(Name = "StoreRedMarkType_Everyone", Value = 1)] StoreRedMarkType_Everyone,
    [ProtoEnum(Name = "StoreRedMarkType_PayingUser", Value = 2)] StoreRedMarkType_PayingUser,
  }
}
