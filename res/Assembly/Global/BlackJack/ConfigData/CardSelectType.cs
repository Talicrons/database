﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CardSelectType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "CardSelectType")]
  public enum CardSelectType
  {
    [ProtoEnum(Name = "CardSelectType_SingleSelect", Value = 1)] CardSelectType_SingleSelect = 1,
    [ProtoEnum(Name = "CardSelectType_TenSelect", Value = 2)] CardSelectType_TenSelect = 2,
    [ProtoEnum(Name = "CardSelectType_BothSelect", Value = 3)] CardSelectType_BothSelect = 3,
  }
}
