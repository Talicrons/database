﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataFetterCreateInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataFetterCreateInfo")]
  [Serializable]
  public class ConfigDataFetterCreateInfo : IExtensible
  {
    private int _ID;
    private string _Desc;
    private List<FetterLevelData> _FetterLevelList;
    private int _FetterHeartLevel;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataFetterCreateInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(4)]
    public List<FetterLevelData> FetterLevelList
    {
      get
      {
        return this._FetterLevelList;
      }
      set
      {
        this._FetterLevelList = value;
      }
    }

    [ProtoMember(5)]
    public int FetterHeartLevel
    {
      get
      {
        return this._FetterHeartLevel;
      }
      set
      {
        this._FetterHeartLevel = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
