﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ScoreLevelType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ScoreLevelType")]
  public enum ScoreLevelType
  {
    [ProtoEnum(Name = "ScoreLevelType_None", Value = 0)] ScoreLevelType_None,
    [ProtoEnum(Name = "ScoreLevelType_Scenario", Value = 1)] ScoreLevelType_Scenario,
    [ProtoEnum(Name = "ScoreLevelType_Challenge", Value = 2)] ScoreLevelType_Challenge,
  }
}
