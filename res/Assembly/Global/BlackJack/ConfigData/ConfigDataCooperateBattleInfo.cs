﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataCooperateBattleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataCooperateBattleInfo")]
  [Serializable]
  public class ConfigDataCooperateBattleInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private string _TeamName;
    private string _Image;
    private string _BriefView;
    private List<int> _LevelList;
    private int _RelatedLimitTimeActivityID;
    private string _OpenHour;
    private string _CloseHour;
    private string _OpenHour2;
    private string _CloseHour2;
    private List<int> _OpenWeekDays;
    private IExtension extensionObject;
    public List<ConfigDataCooperateBattleLevelInfo> m_levelInfos;
    public TimeSpan OpenTimeSpan;
    public TimeSpan CloseTimeSpan;
    public TimeSpan OpenTimeSpan2;
    public TimeSpan CloseTimeSpan2;
    public List<DayOfWeek> OpenDaysOfWeek;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCooperateBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(5)]
    public string TeamName
    {
      get
      {
        return this._TeamName;
      }
      set
      {
        this._TeamName = value;
      }
    }

    [ProtoMember(6)]
    public string Image
    {
      get
      {
        return this._Image;
      }
      set
      {
        this._Image = value;
      }
    }

    [ProtoMember(7)]
    public string BriefView
    {
      get
      {
        return this._BriefView;
      }
      set
      {
        this._BriefView = value;
      }
    }

    [ProtoMember(8)]
    public List<int> LevelList
    {
      get
      {
        return this._LevelList;
      }
      set
      {
        this._LevelList = value;
      }
    }

    [ProtoMember(9)]
    public int RelatedLimitTimeActivityID
    {
      get
      {
        return this._RelatedLimitTimeActivityID;
      }
      set
      {
        this._RelatedLimitTimeActivityID = value;
      }
    }

    [ProtoMember(10)]
    public string OpenHour
    {
      get
      {
        return this._OpenHour;
      }
      set
      {
        this._OpenHour = value;
      }
    }

    [ProtoMember(11)]
    public string CloseHour
    {
      get
      {
        return this._CloseHour;
      }
      set
      {
        this._CloseHour = value;
      }
    }

    [ProtoMember(12)]
    public string OpenHour2
    {
      get
      {
        return this._OpenHour2;
      }
      set
      {
        this._OpenHour2 = value;
      }
    }

    [ProtoMember(13)]
    public string CloseHour2
    {
      get
      {
        return this._CloseHour2;
      }
      set
      {
        this._CloseHour2 = value;
      }
    }

    [ProtoMember(14)]
    public List<int> OpenWeekDays
    {
      get
      {
        return this._OpenWeekDays;
      }
      set
      {
        this._OpenWeekDays = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
