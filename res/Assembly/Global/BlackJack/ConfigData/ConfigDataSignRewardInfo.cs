﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataSignRewardInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataSignRewardInfo")]
  [Serializable]
  public class ConfigDataSignRewardInfo : IExtensible
  {
    private int _ID;
    private MonthType _Month;
    private int _Day;
    private List<Goods> _Reward;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSignRewardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public MonthType Month
    {
      get
      {
        return this._Month;
      }
      set
      {
        this._Month = value;
      }
    }

    [ProtoMember(4)]
    public int Day
    {
      get
      {
        return this._Day;
      }
      set
      {
        this._Day = value;
      }
    }

    [ProtoMember(5)]
    public List<Goods> Reward
    {
      get
      {
        return this._Reward;
      }
      set
      {
        this._Reward = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
