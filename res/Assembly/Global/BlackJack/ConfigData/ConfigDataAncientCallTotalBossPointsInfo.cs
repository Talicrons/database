﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataAncientCallTotalBossPointsInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataAncientCallTotalBossPointsInfo")]
  [Serializable]
  public class ConfigDataAncientCallTotalBossPointsInfo : IExtensible
  {
    private int _ID;
    private int _Ranking;
    private int _TotalRankingRewardMailTemplateId;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public int Ranking
    {
      get
      {
        return this._Ranking;
      }
      set
      {
        this._Ranking = value;
      }
    }

    [ProtoMember(4)]
    public int TotalRankingRewardMailTemplateId
    {
      get
      {
        return this._TotalRankingRewardMailTemplateId;
      }
      set
      {
        this._TotalRankingRewardMailTemplateId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
