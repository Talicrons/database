﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataActivityRetrospectInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataActivityRetrospectInfo")]
  [Serializable]
  public class ConfigDataActivityRetrospectInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private ActivityRetrospectType _ActivityType;
    private int _ActivityID;
    private List<ActivityRetrospectInfoLevels> _Levels;
    private List<int> _ModelIdList;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataActivityRetrospectInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public ActivityRetrospectType ActivityType
    {
      get
      {
        return this._ActivityType;
      }
      set
      {
        this._ActivityType = value;
      }
    }

    [ProtoMember(5)]
    public int ActivityID
    {
      get
      {
        return this._ActivityID;
      }
      set
      {
        this._ActivityID = value;
      }
    }

    [ProtoMember(6)]
    public List<ActivityRetrospectInfoLevels> Levels
    {
      get
      {
        return this._Levels;
      }
      set
      {
        this._Levels = value;
      }
    }

    [ProtoMember(7)]
    public List<int> ModelIdList
    {
      get
      {
        return this._ModelIdList;
      }
      set
      {
        this._ModelIdList = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
