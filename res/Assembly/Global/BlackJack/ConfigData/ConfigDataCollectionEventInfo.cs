﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataCollectionEventInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataCollectionEventInfo")]
  [Serializable]
  public class ConfigDataCollectionEventInfo : IExtensible
  {
    private int _ID;
    private int _EventID;
    private List<CollectionEventInfoAppearCondition> _AppearCondition;
    private List<CollectionEventInfoDisappearCondition> _DisappearCondition;
    private IExtension extensionObject;
    public ConfigDataEventInfo EventInfo;
    public ConfigDataCollectionActivityWaypointInfo WaypointInfo;
    public ConfigDataCollectionActivityInfo CollectionActivity;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionEventInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public int EventID
    {
      get
      {
        return this._EventID;
      }
      set
      {
        this._EventID = value;
      }
    }

    [ProtoMember(4)]
    public List<CollectionEventInfoAppearCondition> AppearCondition
    {
      get
      {
        return this._AppearCondition;
      }
      set
      {
        this._AppearCondition = value;
      }
    }

    [ProtoMember(5)]
    public List<CollectionEventInfoDisappearCondition> DisappearCondition
    {
      get
      {
        return this._DisappearCondition;
      }
      set
      {
        this._DisappearCondition = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
