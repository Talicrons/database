﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroInteractor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroInteractor")]
  [Serializable]
  public class HeroInteractor : IExtensible
  {
    private int _FavorabilityLevel;
    private int _HeroInteractionId;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int FavorabilityLevel
    {
      get
      {
        return this._FavorabilityLevel;
      }
      set
      {
        this._FavorabilityLevel = value;
      }
    }

    [ProtoMember(2)]
    public int HeroInteractionId
    {
      get
      {
        return this._HeroInteractionId;
      }
      set
      {
        this._HeroInteractionId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
