﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.WayPointState
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "WayPointState")]
  [Serializable]
  public class WayPointState : IExtensible
  {
    private int _ID;
    private CollectionActivityWaypointStateType _State;
    private IExtension extensionObject;

    [ProtoMember(1)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(2)]
    public CollectionActivityWaypointStateType State
    {
      get
      {
        return this._State;
      }
      set
      {
        this._State = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
