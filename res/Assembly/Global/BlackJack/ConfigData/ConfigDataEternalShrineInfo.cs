﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataEternalShrineInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataEternalShrineInfo")]
  [Serializable]
  public class ConfigDataEternalShrineInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private string _TeamName;
    private List<int> _LevelList;
    private List<int> _StartTimeList;
    private string _Model;
    private int _UI_ModelScale;
    private int _UI_ModelOffsetX;
    private int _UI_ModelOffsetY;
    private List<int> _UpHeroTag_IDS;
    private int _UpSkill_ID;
    private IExtension extensionObject;
    public List<ConfigDataEternalShrineLevelInfo> m_levelInfos;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEternalShrineInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(5)]
    public string TeamName
    {
      get
      {
        return this._TeamName;
      }
      set
      {
        this._TeamName = value;
      }
    }

    [ProtoMember(6)]
    public List<int> LevelList
    {
      get
      {
        return this._LevelList;
      }
      set
      {
        this._LevelList = value;
      }
    }

    [ProtoMember(7)]
    public List<int> StartTimeList
    {
      get
      {
        return this._StartTimeList;
      }
      set
      {
        this._StartTimeList = value;
      }
    }

    [ProtoMember(8)]
    public string Model
    {
      get
      {
        return this._Model;
      }
      set
      {
        this._Model = value;
      }
    }

    [ProtoMember(9)]
    public int UI_ModelScale
    {
      get
      {
        return this._UI_ModelScale;
      }
      set
      {
        this._UI_ModelScale = value;
      }
    }

    [ProtoMember(10)]
    public int UI_ModelOffsetX
    {
      get
      {
        return this._UI_ModelOffsetX;
      }
      set
      {
        this._UI_ModelOffsetX = value;
      }
    }

    [ProtoMember(11)]
    public int UI_ModelOffsetY
    {
      get
      {
        return this._UI_ModelOffsetY;
      }
      set
      {
        this._UI_ModelOffsetY = value;
      }
    }

    [ProtoMember(12)]
    public List<int> UpHeroTag_IDS
    {
      get
      {
        return this._UpHeroTag_IDS;
      }
      set
      {
        this._UpHeroTag_IDS = value;
      }
    }

    [ProtoMember(13)]
    public int UpSkill_ID
    {
      get
      {
        return this._UpSkill_ID;
      }
      set
      {
        this._UpSkill_ID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
