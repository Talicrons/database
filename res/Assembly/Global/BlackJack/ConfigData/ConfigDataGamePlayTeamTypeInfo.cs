﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataGamePlayTeamTypeInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataGamePlayTeamTypeInfo")]
  [Serializable]
  public class ConfigDataGamePlayTeamTypeInfo : IExtensible
  {
    private int _ID;
    private string _GamePlayName;
    private int _TeamType;
    private int _BattleType;
    private int _SubGameType_ID;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string GamePlayName
    {
      get
      {
        return this._GamePlayName;
      }
      set
      {
        this._GamePlayName = value;
      }
    }

    [ProtoMember(4)]
    public int TeamType
    {
      get
      {
        return this._TeamType;
      }
      set
      {
        this._TeamType = value;
      }
    }

    [ProtoMember(5)]
    public int BattleType
    {
      get
      {
        return this._BattleType;
      }
      set
      {
        this._BattleType = value;
      }
    }

    [ProtoMember(6)]
    public int SubGameType_ID
    {
      get
      {
        return this._SubGameType_ID;
      }
      set
      {
        this._SubGameType_ID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
