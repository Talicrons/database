﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataBanditInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataBanditInfo")]
  [Serializable]
  public class ConfigDataBanditInfo : IExtensible
  {
    private int _ID;
    private int _HeroID;
    private int _Gold;
    private int _Rarity;
    private string _IconResource;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public int HeroID
    {
      get
      {
        return this._HeroID;
      }
      set
      {
        this._HeroID = value;
      }
    }

    [ProtoMember(4)]
    public int Gold
    {
      get
      {
        return this._Gold;
      }
      set
      {
        this._Gold = value;
      }
    }

    [ProtoMember(5)]
    public int Rarity
    {
      get
      {
        return this._Rarity;
      }
      set
      {
        this._Rarity = value;
      }
    }

    [ProtoMember(6)]
    public string IconResource
    {
      get
      {
        return this._IconResource;
      }
      set
      {
        this._IconResource = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
