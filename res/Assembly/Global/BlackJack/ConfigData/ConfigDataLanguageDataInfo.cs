﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataLanguageDataInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataLanguageDataInfo")]
  [Serializable]
  public class ConfigDataLanguageDataInfo : IExtensible
  {
    private int _ID;
    private string _SystemLanguage;
    private string _LanguageAbbr;
    private string _LanguageName;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string SystemLanguage
    {
      get
      {
        return this._SystemLanguage;
      }
      set
      {
        this._SystemLanguage = value;
      }
    }

    [ProtoMember(4)]
    public string LanguageAbbr
    {
      get
      {
        return this._LanguageAbbr;
      }
      set
      {
        this._LanguageAbbr = value;
      }
    }

    [ProtoMember(5)]
    public string LanguageName
    {
      get
      {
        return this._LanguageName;
      }
      set
      {
        this._LanguageName = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
