﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataGroupBehavior
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataGroupBehavior")]
  [Serializable]
  public class ConfigDataGroupBehavior : IExtensible
  {
    private int _ID;
    private BehaviorCondition _SelectLeaderCondition;
    private string _SLCParam;
    private int _LeaderBehavior;
    private int _MemberBehavior;
    private IExtension extensionObject;
    public ConfigDataBehavior.ParamData SLCParamData;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGroupBehavior()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(4)]
    public BehaviorCondition SelectLeaderCondition
    {
      get
      {
        return this._SelectLeaderCondition;
      }
      set
      {
        this._SelectLeaderCondition = value;
      }
    }

    [ProtoMember(5)]
    public string SLCParam
    {
      get
      {
        return this._SLCParam;
      }
      set
      {
        this._SLCParam = value;
      }
    }

    [ProtoMember(6)]
    public int LeaderBehavior
    {
      get
      {
        return this._LeaderBehavior;
      }
      set
      {
        this._LeaderBehavior = value;
      }
    }

    [ProtoMember(7)]
    public int MemberBehavior
    {
      get
      {
        return this._MemberBehavior;
      }
      set
      {
        this._MemberBehavior = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Initialize()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
