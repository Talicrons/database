﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataDeviceSetting
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataDeviceSetting")]
  [Serializable]
  public class ConfigDataDeviceSetting : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _DeviceModel;
    private bool _MarginFixHorizontal;
    private bool _MarginFixVertical;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4)]
    public string DeviceModel
    {
      get
      {
        return this._DeviceModel;
      }
      set
      {
        this._DeviceModel = value;
      }
    }

    [ProtoMember(5)]
    public bool MarginFixHorizontal
    {
      get
      {
        return this._MarginFixHorizontal;
      }
      set
      {
        this._MarginFixHorizontal = value;
      }
    }

    [ProtoMember(6)]
    public bool MarginFixVertical
    {
      get
      {
        return this._MarginFixVertical;
      }
      set
      {
        this._MarginFixVertical = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
