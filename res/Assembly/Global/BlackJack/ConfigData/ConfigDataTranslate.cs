﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataTranslate
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataTranslate")]
  [Serializable]
  public class ConfigDataTranslate : IExtensible
  {
    private int _ID;
    private string _Chinese;
    private string _TargetLanguage;
    private IExtension extensionObject;

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Chinese
    {
      get
      {
        return this._Chinese;
      }
      set
      {
        this._Chinese = value;
      }
    }

    [ProtoMember(4)]
    public string TargetLanguage
    {
      get
      {
        return this._TargetLanguage;
      }
      set
      {
        this._TargetLanguage = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
