﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataWaypointInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 142BFF12-D252-4EE9-9FD6-F278387333E3
// Assembly location: C:\Users\box\Downloads\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataWaypointInfo")]
  [Serializable]
  public class ConfigDataWaypointInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private WaypointFuncType _FuncType;
    private int _FuncTypeParam1;
    private List<int> _Waypoints_ID;
    private WaypointStyleType _StyleType;
    private string _Model;
    private string _Background;
    private List<WaypointInfoStateList> _StateList;
    private IExtension extensionObject;
    public ConfigDataWaypointInfo[] m_waypointInfos;
    public ConfigDataRegionInfo m_regionInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataWaypointInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2)]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3)]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(7)]
    public WaypointFuncType FuncType
    {
      get
      {
        return this._FuncType;
      }
      set
      {
        this._FuncType = value;
      }
    }

    [ProtoMember(8)]
    public int FuncTypeParam1
    {
      get
      {
        return this._FuncTypeParam1;
      }
      set
      {
        this._FuncTypeParam1 = value;
      }
    }

    [ProtoMember(9)]
    public List<int> Waypoints_ID
    {
      get
      {
        return this._Waypoints_ID;
      }
      set
      {
        this._Waypoints_ID = value;
      }
    }

    [ProtoMember(10)]
    public WaypointStyleType StyleType
    {
      get
      {
        return this._StyleType;
      }
      set
      {
        this._StyleType = value;
      }
    }

    [ProtoMember(11)]
    public string Model
    {
      get
      {
        return this._Model;
      }
      set
      {
        this._Model = value;
      }
    }

    [ProtoMember(12)]
    public string Background
    {
      get
      {
        return this._Background;
      }
      set
      {
        this._Background = value;
      }
    }

    [ProtoMember(13)]
    public List<WaypointInfoStateList> StateList
    {
      get
      {
        return this._StateList;
      }
      set
      {
        this._StateList = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
