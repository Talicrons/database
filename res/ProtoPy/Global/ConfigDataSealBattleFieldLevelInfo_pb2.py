# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataSealBattleFieldLevelInfo.proto

from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataSealBattleFieldLevelInfo.proto',
  package='ConfigDataSealBattleFieldLevelInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=b'\n(ConfigDataSealBattleFieldLevelInfo.proto\x12\"ConfigDataSealBattleFieldLevelInfo\"k\n\x08SUBGoods\x12\x44\n\tGoodsType\x18\x01 \x02(\x0e\x32\x31.ConfigDataSealBattleFieldLevelInfo.ENUMGoodsType\x12\n\n\x02Id\x18\x02 \x02(\x05\x12\r\n\x05\x43ount\x18\x03 \x02(\x05\"\xbd\x02\n\"ConfigDataSealBattleFieldLevelInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\x12\x11\n\tLevelName\x18\x03 \x02(\t\x12\x14\n\x0cLevelAheadID\x18\x04 \x02(\x05\x12\x14\n\x0cMonsterLevel\x18\x05 \x02(\x05\x12\x10\n\x08\x42\x61ttleID\x18\x06 \x02(\x05\x12\x45\n\x0f\x46irstPassReward\x18\x07 \x03(\x0b\x32,.ConfigDataSealBattleFieldLevelInfo.SUBGoods\x12\x1a\n\x12\x44isplayRewardCount\x18\x08 \x02(\x05\x12\x13\n\x0b\x41\x63hievement\x18\t \x03(\x05\x12\x1f\n\x17OurPartAddPassiveSkills\x18\n \x03(\x05\x12!\n\x19\x45nemyPatrAddPassiveSkills\x18\x0b \x03(\x05\"^\n\x05Items\x12U\n\x05items\x18\x01 \x03(\x0b\x32\x46.ConfigDataSealBattleFieldLevelInfo.ConfigDataSealBattleFieldLevelInfo*\xde\x06\n\rENUMGoodsType\x12\x12\n\x0eGoodsType_None\x10\x00\x12\x12\n\x0eGoodsType_Gold\x10\x01\x12\x15\n\x11GoodsType_Crystal\x10\x02\x12\x14\n\x10GoodsType_Energy\x10\x03\x12\x12\n\x0eGoodsType_Hero\x10\x04\x12\x19\n\x15GoodsType_JobMaterial\x10\x05\x12\x12\n\x0eGoodsType_Item\x10\x06\x12\x17\n\x13GoodsType_Equipment\x10\x07\x12\x19\n\x15GoodsType_ArenaTicket\x10\x08\x12\x19\n\x15GoodsType_ArenaHonour\x10\t\x12\x17\n\x13GoodsType_PlayerExp\x10\n\x12(\n$GoodsType_TrainingGroundTechMaterial\x10\x0b\x12\x1e\n\x1aGoodsType_FriendshipPoints\x10\x0c\x12\x1a\n\x16GoodsType_EnchantStone\x10\r\x12\x17\n\x13GoodsType_MonthCard\x10\x0e\x12\x17\n\x13GoodsType_HeadFrame\x10\x0f\x12\x16\n\x12GoodsType_HeroSkin\x10\x10\x12\x19\n\x15GoodsType_SoldierSkin\x10\x11\x12\x18\n\x14GoodsType_SkinTicket\x10\x12\x12\x1e\n\x1aGoodsType_RealTimePVPHonor\x10\x13\x12\x1b\n\x17GoodsType_MemoryEssence\x10\x14\x12\x1a\n\x16GoodsType_MithralStone\x10\x15\x12$\n GoodsType_BrillianceMithralStone\x10\x16\x12\x18\n\x14GoodsType_GuildMedal\x10\x17\x12\x1c\n\x18GoodsType_ChallengePoint\x10\x18\x12\x1a\n\x16GoodsType_FashionPoint\x10\x19\x12\x13\n\x0fGoodsType_Title\x10\x1a\x12\x1b\n\x17GoodsType_RefineryStone\x10\x1b\x12\"\n\x1eGoodsType_TimeSlotClockPendant\x10\x1c\x12$\n GoodsType_VisitingCardBackGround\x10\x1d\x12\x19\n\x15GoodsType_AncientGold\x10\x1e'
)

_ENUMGOODSTYPE = _descriptor.EnumDescriptor(
  name='ENUMGoodsType',
  full_name='ConfigDataSealBattleFieldLevelInfo.ENUMGoodsType',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='GoodsType_None', index=0, number=0,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_Gold', index=1, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_Crystal', index=2, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_Energy', index=3, number=3,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_Hero', index=4, number=4,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_JobMaterial', index=5, number=5,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_Item', index=6, number=6,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_Equipment', index=7, number=7,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_ArenaTicket', index=8, number=8,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_ArenaHonour', index=9, number=9,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_PlayerExp', index=10, number=10,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_TrainingGroundTechMaterial', index=11, number=11,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_FriendshipPoints', index=12, number=12,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_EnchantStone', index=13, number=13,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_MonthCard', index=14, number=14,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_HeadFrame', index=15, number=15,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_HeroSkin', index=16, number=16,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_SoldierSkin', index=17, number=17,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_SkinTicket', index=18, number=18,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_RealTimePVPHonor', index=19, number=19,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_MemoryEssence', index=20, number=20,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_MithralStone', index=21, number=21,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_BrillianceMithralStone', index=22, number=22,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_GuildMedal', index=23, number=23,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_ChallengePoint', index=24, number=24,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_FashionPoint', index=25, number=25,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_Title', index=26, number=26,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_RefineryStone', index=27, number=27,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_TimeSlotClockPendant', index=28, number=28,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_VisitingCardBackGround', index=29, number=29,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_AncientGold', index=30, number=30,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=606,
  serialized_end=1468,
)
_sym_db.RegisterEnumDescriptor(_ENUMGOODSTYPE)

ENUMGoodsType = enum_type_wrapper.EnumTypeWrapper(_ENUMGOODSTYPE)
GoodsType_None = 0
GoodsType_Gold = 1
GoodsType_Crystal = 2
GoodsType_Energy = 3
GoodsType_Hero = 4
GoodsType_JobMaterial = 5
GoodsType_Item = 6
GoodsType_Equipment = 7
GoodsType_ArenaTicket = 8
GoodsType_ArenaHonour = 9
GoodsType_PlayerExp = 10
GoodsType_TrainingGroundTechMaterial = 11
GoodsType_FriendshipPoints = 12
GoodsType_EnchantStone = 13
GoodsType_MonthCard = 14
GoodsType_HeadFrame = 15
GoodsType_HeroSkin = 16
GoodsType_SoldierSkin = 17
GoodsType_SkinTicket = 18
GoodsType_RealTimePVPHonor = 19
GoodsType_MemoryEssence = 20
GoodsType_MithralStone = 21
GoodsType_BrillianceMithralStone = 22
GoodsType_GuildMedal = 23
GoodsType_ChallengePoint = 24
GoodsType_FashionPoint = 25
GoodsType_Title = 26
GoodsType_RefineryStone = 27
GoodsType_TimeSlotClockPendant = 28
GoodsType_VisitingCardBackGround = 29
GoodsType_AncientGold = 30



_SUBGOODS = _descriptor.Descriptor(
  name='SUBGoods',
  full_name='ConfigDataSealBattleFieldLevelInfo.SUBGoods',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='GoodsType', full_name='ConfigDataSealBattleFieldLevelInfo.SUBGoods.GoodsType', index=0,
      number=1, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Id', full_name='ConfigDataSealBattleFieldLevelInfo.SUBGoods.Id', index=1,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Count', full_name='ConfigDataSealBattleFieldLevelInfo.SUBGoods.Count', index=2,
      number=3, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=80,
  serialized_end=187,
)


_CONFIGDATASEALBATTLEFIELDLEVELINFO = _descriptor.Descriptor(
  name='ConfigDataSealBattleFieldLevelInfo',
  full_name='ConfigDataSealBattleFieldLevelInfo.ConfigDataSealBattleFieldLevelInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataSealBattleFieldLevelInfo.ConfigDataSealBattleFieldLevelInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='LevelName', full_name='ConfigDataSealBattleFieldLevelInfo.ConfigDataSealBattleFieldLevelInfo.LevelName', index=1,
      number=3, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='LevelAheadID', full_name='ConfigDataSealBattleFieldLevelInfo.ConfigDataSealBattleFieldLevelInfo.LevelAheadID', index=2,
      number=4, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='MonsterLevel', full_name='ConfigDataSealBattleFieldLevelInfo.ConfigDataSealBattleFieldLevelInfo.MonsterLevel', index=3,
      number=5, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='BattleID', full_name='ConfigDataSealBattleFieldLevelInfo.ConfigDataSealBattleFieldLevelInfo.BattleID', index=4,
      number=6, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='FirstPassReward', full_name='ConfigDataSealBattleFieldLevelInfo.ConfigDataSealBattleFieldLevelInfo.FirstPassReward', index=5,
      number=7, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='DisplayRewardCount', full_name='ConfigDataSealBattleFieldLevelInfo.ConfigDataSealBattleFieldLevelInfo.DisplayRewardCount', index=6,
      number=8, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Achievement', full_name='ConfigDataSealBattleFieldLevelInfo.ConfigDataSealBattleFieldLevelInfo.Achievement', index=7,
      number=9, type=5, cpp_type=1, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='OurPartAddPassiveSkills', full_name='ConfigDataSealBattleFieldLevelInfo.ConfigDataSealBattleFieldLevelInfo.OurPartAddPassiveSkills', index=8,
      number=10, type=5, cpp_type=1, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='EnemyPatrAddPassiveSkills', full_name='ConfigDataSealBattleFieldLevelInfo.ConfigDataSealBattleFieldLevelInfo.EnemyPatrAddPassiveSkills', index=9,
      number=11, type=5, cpp_type=1, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=190,
  serialized_end=507,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataSealBattleFieldLevelInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataSealBattleFieldLevelInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=509,
  serialized_end=603,
)

_SUBGOODS.fields_by_name['GoodsType'].enum_type = _ENUMGOODSTYPE
_CONFIGDATASEALBATTLEFIELDLEVELINFO.fields_by_name['FirstPassReward'].message_type = _SUBGOODS
_ITEMS.fields_by_name['items'].message_type = _CONFIGDATASEALBATTLEFIELDLEVELINFO
DESCRIPTOR.message_types_by_name['SUBGoods'] = _SUBGOODS
DESCRIPTOR.message_types_by_name['ConfigDataSealBattleFieldLevelInfo'] = _CONFIGDATASEALBATTLEFIELDLEVELINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
DESCRIPTOR.enum_types_by_name['ENUMGoodsType'] = _ENUMGOODSTYPE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

SUBGoods = _reflection.GeneratedProtocolMessageType('SUBGoods', (_message.Message,), {
  'DESCRIPTOR' : _SUBGOODS,
  '__module__' : 'ConfigDataSealBattleFieldLevelInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataSealBattleFieldLevelInfo.SUBGoods)
  })
_sym_db.RegisterMessage(SUBGoods)

ConfigDataSealBattleFieldLevelInfo = _reflection.GeneratedProtocolMessageType('ConfigDataSealBattleFieldLevelInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATASEALBATTLEFIELDLEVELINFO,
  '__module__' : 'ConfigDataSealBattleFieldLevelInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataSealBattleFieldLevelInfo.ConfigDataSealBattleFieldLevelInfo)
  })
_sym_db.RegisterMessage(ConfigDataSealBattleFieldLevelInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataSealBattleFieldLevelInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataSealBattleFieldLevelInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
