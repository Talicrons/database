import json
import re
import os
from .ProtoGenerator import generateProtoPy,generateProtos
from .ConfigSearcherCN import CollectConfigs, CollectEnums
#from lib.ProtoGenerator2 import generateProtos

PATH=os.path.dirname(os.path.realpath(__file__))


def ExtractProtos_CN(
		proto_path='',
		protopy_path='',
		config_path=''
		):
	#collect data
	configs=CollectConfigs(config_path)
	enums = CollectEnums(config_path)

	types={
		key:{
			int(item['num']):{
				'index':	int(item['num']),
				'name':		item['name'],
				'required':	item['required'],
				'typ':		item['type'],
				'format': 	item['format']
			}
			for name,item in var.items()
		}
		for key,var in configs.items()
	}

	#removed ConfigData

	#create .proto files
	os.makedirs(proto_path,exist_ok=True)
	generateProtos(types,enums,proto_path)

	#generate proto py files
	os.makedirs(protopy_path,exist_ok=True)
	generateProtoPy(proto_path,protopy_path)