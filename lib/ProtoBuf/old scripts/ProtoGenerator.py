import os
import subprocess
import copy
# from .EnumSearcher import CollectEnums
# from .ConfigSearcher import CollectConfigs

# def PG_Init(config_path):
# 	global enums
# 	global configs
# 	enums = CollectEnums(config_path)
# 	configs = CollectConfigs(config_path)

# 	configs={
# 		key:{
# 			var[1][1:]:var[0]
# 			for var in items
# 			if var[1][0]=='_'
# 		}
# 		for key,items in configs.items()
# 	}

# 	return configs

def generateProtos(ctypes,cenums,fpath):
	global enums
	global configs
	enums=cenums
	configs=ctypes
	for key,items in ctypes.items():
		if 'ConfigData' == key[:10]:
			generateProto(key,items,fpath)

def generateProto(name,items,fpath,syntax=2):
	open(
		file=os.path.join(fpath,'%s.proto'%name),
		mode='wt',
		encoding='utf8'
	).write(
		'\n'.join([
			'syntax = "proto%s";'%syntax,
			'package %s;'%name,
			generateProtoMessage(name,items,syntax),
			''
		])
	)

def generateProtoVarType(var):
	required='required' if var['required'] else 'optional'
	typ='variant'
	if 'typ' in var:
		typ = var['typ']
		#list?
		if typ[:5]=='List<':
			required='repeated'
			typ=typ[5:-1]
		#typ fix
		if typ in ['string','bool','float','double']:
			pass
		elif typ=='int':
			typ='int32'
		elif typ=='uint':
			typ='uint32'
		elif typ in enums: #~enum ~ atm as int
			typ='ENUM%s'%typ
		elif typ in configs:
			typ='SUB%s'%typ
		else:
			print('Unknown Var Type,',var['typ'])
	else:
		typ='string'# if var['format']==0 else 'variant'


	return '{required} {type}'.format(
		required= required,
		type=typ,
	)

def usedSpecialTypes(items):
	#print(items)
	ltypes=set([
		item['typ'] if item['typ'][-1]!='>' else item['typ'][5:-1] 
		for index,item in items.items()
		if 'typ' in item
	])	#remove list and remove copies
	return [typ for typ in ltypes if typ not in ['string','int','bool']]

def generateProtoEnum(ename):
	return '\n'.join([
		'enum ENUM%s {'%ename,
		*[
			'	{name} = {index};'.format(
				name=sename,#generateEName(sename[len(ename):]),#'ArmyTag_None' -> None
				index=str(index)
			)
			for index,sename in enums[ename].items()
		],
		'}',
		''
		])

def generateProtoMessage(key,items,syntax=2,_main=True,used_subs=[]):
	if _main:
		used_subs=[]
	names=[]
	def checkName(name):
		if name not in names:
			names.append(name)
			return name
		else:
			for i in range(1,99):
				tname='%s%s'%(name,i)
				if tname not in names:
					names.append(tname)
					return tname

	ret=[]
	#enums
	print(key)
	print(items)
	for typ in usedSpecialTypes(items):
		if typ in used_subs:
			#print(key,'SUB',typ)
			continue
		if typ in enums:
			#print(key,'ENUM',typ)
			ret.append(generateProtoEnum(typ))
			used_subs.append(typ)
		elif typ in configs:
			#print(key,'SUB',typ)
			ret.append(generateProtoMessage('SUB%s'%typ,
				{
				num+1:{
					'index':num+1,
					'name':vals[0],
					'required':True,
					'typ':vals[1]
				}
				for num,vals in enumerate(list(configs[typ].items()))
				}
			,_main=False, used_subs=used_subs))
			used_subs.append(typ)

	ret.extend([
		'message %s {'%key,
		*[
			'	{typ} {name} = {index};'.format(
				typ= generateProtoVarType(item),
				name=checkName(item['name']),
				index=item['index']
			)
			for index,item in sorted(list(items.items()), key=lambda item:int(item[0]))
		],
		'}'
	])
	if _main:
		ret.extend([
		'',
		'message Items {',
		'	repeated %s items = 1;'%key,
		'}'
		])
	return '\n'.join(ret)

def generateProtoPy(src,dest):
	names=[]
	for fp in os.listdir(src):
		if '.proto' == fp[-6:] and fp[0]!='_':
			result= subprocess.run([
				"protoc",
				"--proto_path=%s"%src,
				"--python_out=%s"%dest,
				os.path.join(src,fp)
				], stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True
			)
			if 'already defined' in str(result):
				print(str(result)[str(result).find('stderr'):])
			else:
				names.append(fp[:-6])
	
	#init
	open(os.path.join(dest,'__init__.py'),'wt',encoding='utf8').write(
		'\n'.join([
			'from .%s_pb2 import Items as %s'%(name,name)
			for name in [fp[:-7] for fp in os.listdir(dest) if fp[0]!='_']
			])
	)