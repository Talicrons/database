import json
import re
import os
from .ProtoGenerator import generateProtoPy,generateProtos,# PG_Init
from .libil2cppScrapper import ProtoScrapper

def ExtractProtos(
		proto_path='',
		protopy_path='',
		libil2cpp_c_path='',
		config_path=''
		):
	#collect data
	types=ProtoScrapper(libil2cpp_c_path)
	configs=PG_Init(config_path)
	#connect
	#data types from configs to types
	for key,items in types.items():
		if key in configs:
			for num, item in items.items():
				if item['name'] in configs[key]:
					item['typ']=configs[key][item['name']]

	types.update({
		key:{
			num+2:{
				'index':num+2,
				'name':vals[0],
				'required':True,
				'typ':vals[1]
			}
			for num,vals in enumerate(list(var.items()))
		}
		for key,var in configs.items()
		if var and key not in types
	})

	#removed ConfigData
	#types={key[10:]:var for key,var in types.items()}

	#open(os.path.join(PATH,'FoundTypes.json'),'wb').write(json.dumps(types, indent='\t').encode('utf8'))

	#create .proto files
	os.makedirs(proto_path,exist_ok=True)
	generateProtos(types,proto_path)

	#hotfix
	FixProto(proto_path,'ConfigDataItemInfo.proto',[
		('required string GetPathDesc = 15;','required int32 GetPathDesc = 15;')
		])
	FixProto(proto_path,'ConfigDataTreasureLevelInfo.proto',[
		('required string GetPathDesc = 15;','required int32 GetPathDesc = 15;')
		])
	FixProto(proto_path,'ConfigDataTreasureLevelInfo.proto',[
		('required string UI_ModelScale = 14;','required int32 UI_ModelScale = 14;'),
		('required string UI_ModelOffsetX = 15;','required int32 UI_ModelOffsetX = 15;'),
		('required string UI_ModelOffsetY = 16;','required int32 UI_ModelOffsetY = 16;')
		])

	#generate proto py files
	os.makedirs(protopy_path,exist_ok=True)
	generateProtoPy(proto_path,protopy_path)

def FixProto(fpath,name,replacements):
	fpath=os.path.join(fpath,name)
	data=open(fpath,'rt',encoding='utf8').read()
	for re in replacements:
		data = data.replace(re[0],re[1])
	open(fpath,'wt',encoding='utf8').write(data)