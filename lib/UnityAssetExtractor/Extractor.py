import io
import os
import unitypack
from unitypack.asset import Asset
from unitypack.export import listFiles, BundleExporter, AssetExporter
from unitypack.export.audio import StreamingAssetsConvertion
from unitypack.unityfolder import UnityFolder

def processUnityFile(f,assetPath, destFolder):
	if assetPath and assetPath[0]=='\\':
		assetPath=assetPath[1:]
	cleanFilename=os.path.basename(assetPath)
	dest=os.path.dirname(assetPath)
	dest = dest[1:]
	destFolder=os.path.join(destFolder,dest)
	os.makedirs(destFolder,exist_ok=True)

	b=False
	if type(f)==bytes:
		b=True
		Bytes=f
		f = io.BytesIO(f)
		f.name = cleanFilename

	firstChars = bytearray(f.read(12))
	f.seek(0)

	if firstChars[:len("UnityFS")] == bytearray("UnityFS".encode()):
		try:
			bundle = unitypack.load(f)
			processBundle(bundle, destFolder, cleanFilename)
		except Exception as e:
			msg="Error extracting bundle:\t%s,\nError:\t%s"%(assetPath,e)
			print(msg)
			#errorLog.write(msg)

	elif firstChars[:len("UnityWeb")] == bytearray("UnityWeb".encode()):
		# unitypack can't process these files
		print("\n\tIgnoring file",assetPath)

	elif firstChars[:len("UnityRaw")] == bytearray("UnityRaw".encode()) or firstChars[:len("UnityArchive")] == bytearray("UnityArchive".encode()):
		print("\n\tFile with unprocessed type found", assetPath)

	else:
		try:  # might be .assets, lets try to open them like this
			asset = Asset.from_file(f)
			processAsset(asset, destFolder)
		except:  # just copy the file then, might be json or some other format
			print("\tUnable to process file, saving\t", assetPath)
			if b:
				with open (os.path.join(destFolder,cleanFilename), 'wb') as fh:
					fh.write(Bytes)
			else:
				f.seek(0)
				with open (os.path.join(destFolder,cleanFilename), 'wb') as fh:
					fh.write(f.read())



def processBundle(bundle, destFolder, cleanFilename):
	BundleExporter(bundle,os.path.join(destFolder,cleanFilename))


def processAsset(asset, destFolder):
	AssetExporter(asset,destFolder)
