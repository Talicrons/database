I stopped working on this project.

# Langrisser Database
Code used to extract data from the assets of the game [Langrisser Mobile](https://play.google.com/store/apps/details?id=com.zlongame.un.mhmnz).

The latest assets can be downloaded via DownloadAssets.py.

The extracted assets will be stored in [\assets] (https://gitlab.com/langrisser/assets).

## Requirements
Python 3.7
* [protobuf](https://github.com/protocolbuffers/protobuf/tree/master/python) 
* [unitypack (modified version)](https://github.com/K0lb3/UnityPack)
* [pillow](https://pillow.readthedocs.io/en/stable)

To update the proto structures you also require
[protoc - protobuf compiler](https://github.com/protocolbuffers/protobuf/blob/master/src/README.md)

## Usage

### normal usage
1. DownloadAssets.py
    Downloads all missing assets and update existing ones.
    Downloaded assets will be extracted during runtime.

2. ConvertConfigs.py
    Converts the proto configdata into json.
    If the configdata looks wrong, you might have to update the proto message structures.
    See CreateProtos.py

3. FixImages.py
    Adds the alpha channel to images which have alpha files, which contain that channel

### other scripts
* CreateProtos.py
    Creates the .proto message structures to convert the configdata proto messages
    based on the assemblies in res\Disassembly.
    For more details and how to update those files look at res\Disassembly.

* AIOImages.py
    Puts all images from global and china into one folder, global version is preferred


# Structure

## lib\ProtoBuf
Scripts used for the generation of the .proto files.
A complete res\Disassembly is required for them.

## lib\UnityAssetExtractor
Scripts used for the extraction of the game data from the unity assets.
At the moment they are just a modified copy of the The-Alchemist-Code version.
Credits:
* [Rangedz](https://bitbucket.org/Rangedz/alchemycodeextractor)
* [sanktanglia](https://langrisser.gg) (sprites)
* [moesoha](https://github.com/moesoha/unity-game-resource-unpacker) (fonts, audioclip and movieclip)

## res\ProtoPy
The proto structures for python usage.

## res\ProtoBuffer
All .proto files used to decode the configdata are stored here.

## res\FoundSettings
All found proto structures and enums of the assemblies are saved here.

## res\Assembly
Contains the extracted source code of Langrisser.
The code of the current global and china version are partially encrypted,
so the assembly of global and china is only used to create the proto structures for the configdata.
The folder old contains the original source code of an old Langrisser version.

To update the global and china assemblies you require Windows and have to have the PC-Version of them installed.
Besides that you also need [Jetbrains dotpeek](https://www.jetbrains.com/decompiler/), otherwise the CreateProtos.py script will probably fail.

1. Open the assembly-csharp.dll in `Langrisser\client\Langrisser_Data\Managed` with dotpeek.
2. Export the .dll as project. (right-click on .dll, export to project).
3. Copy the exported files (content of the Assembly folder and not the folder) to`res\Assembly\{game version}` and replace all existing files.
