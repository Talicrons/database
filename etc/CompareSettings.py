import os, json

def Test():
	PATH = os.path.dirname(os.path.realpath(__file__))
	set_path = os.path.join(PATH,'FoundSettings')

	data={}
	for fp in os.listdir(set_path):
		typ, ver = fp.rsplit('.',1)[0].rsplit('_',1)
		if typ not in data:
			data[typ]={}
		data[typ][ver]=json.loads(open(os.path.join(set_path,fp),'rb').read())

	#	find stuff which is in both and different
	for typ, vers in data.items():
		print(typ)
		#different config/enums
		versions = list(vers.keys())
		keys = [key for key in vers[versions[0]].keys() if all([key in vers[v] for v in versions])]
		#vers[key]
		for key in keys:
			item=vers[versions[0]][key]
			for v in versions[1:]:
				if vers[v][key] != item:
					if 'changed' in Difference(item,vers[v][key]):
						print(key)
					break



def Transformed_Difference(new,old):
	new,old=(Transform(new),Transform(old))
	dif={
		key:Difference(new[key],old[key]) if key in old else {'added':new[key]}
		for key in list(new.keys())
		if key not in old or new[key]!=old[key]
	}
	return dif

def Transform(master):
    for main, tree in master.items():
        if type(tree)==list and type(tree[0])==dict and 'iname' in tree[0]:
            master[main]={item['ID']:item for item in tree}
    return master

def Difference(new,old):
    dif={}
    if new==old:
        return dif
        
    def cleanUp(dif):
        if not dif['added']:
            del dif['added']
        if not dif['removed']:
            del dif['removed']
        if not dif['changed']:
            del dif['changed']
        return dif

    if type(new)==dict:
        dif['added']={}
        dif['removed']={}
        dif['changed']={}
        oldkeys=list(old.keys())
        for key in list(new.keys()):
            if key in old:
                tdif=Difference(new[key],old[key])
                if tdif:
                    dif['changed'][key]=tdif
                oldkeys.remove(key)
            else:
                #print('added',key)
                dif['added'][key]=new[key]
        for key in oldkeys:
            #print('removed',key)
            dif['removed']={
                key:old[key]
                for key in oldkeys
            }
        return cleanUp(dif)

    elif type(new)==list:
        if type(new[0])==dict:
            for i in range(len(new)):
                return ([
                    Difference(new[i],old[i])
                ])
        dif['added']=[]
        dif['removed']=[]
        dif['changed']=[]
        for item in new:
            if item not in old:
                dif['added'].append(item)
        for item in old:
            if item not in new:
                dif['removed'].append(item)
        return cleanUp(dif)

    else:
        if new!=old:
            return {'new':new,'old':old}
        else:
            return {}

if __name__ == '__main__':
	Test()