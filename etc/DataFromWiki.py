import os, sys, json, urllib, re
import html as HTML

reMain = re.compile(r'<li data-id=".+?"><a href="(.+?)" title="(.+?)">(<img src="(.+?)")?')
reStats = re.compile(r'\t+<td style="width.+\n[\t<a-z>]+([A-Za-z]+)[\xa0 ]+([A-Z][a-z]+|\d+)([\xa0 ]+)?<\/[tddiv]+>',re.MULTILINE)#(r'\t+<td style="width.+\n[\t<a-z>]+([A-Z].+?)[\xa0 ]+(\d+)([\xa0 ]+)?<\/[tddiv]+>',re.MULTILINE)
reEquipTypeRank = re.compile(r'arms([A-Z][a-z]+)([A-Za-z]+)')
reHeroDetails= re.compile(r'<p>(.+?)<.+\n.+?>(.+?)[<\t\n]',re.MULTILINE)
reHeroStats= re.compile(r'<td class.+?>(.+?)<.+\n.+?>(.+?)<',re.MULTILINE)
reHeroClasses = re.compile(r'(.+src="(.+?)")?.+\d+"> ?<span>((.+?)?)<\/span><\/div>', re.MULTILINE)
reSkills= re.compile(r'(.+src="(.+?)")?.+\d+"> ?<\/div>\n.+\n.+?<p>(.+?)<.+\n.+?>(.+?)[<\t\n]', re.MULTILINE)

api='http://us-news.zlongame.com:80'
sites= ['hero','soldier','arms']
url = 'https://us-news.zlongame.com/%s/index.jhtml'

CACHING=False
cache=os.path.join('cache')
Classes={}

def main():
	data={
		s:{
			match[2].title():{
				'url': api+match[1] if match[1][0]=='/' else match[1],
				'img': (api+match[4] if match[4][0]=='/' else match[4]) if len(match.regs)==5 and type(match[4])==str else '',
				**FetchDetails(match[1],s)
			}
			for match in reMain.finditer(Between(Download(url%s).decode('utf8'),'<!-- 全部_begin -->','<!-- 全部_end -->'))
		}
		for s in sites
	}
	#data['class']=Classes
	open('WikiDatabase.json','wb').write(json.dumps(data, indent='\t', ensure_ascii=False).encode('utf8'))

def FetchDetails(url,typ):
	ret={}
	if typ=='arms':
		match = reEquipTypeRank.findall(url)[0]
		html = HTML.unescape(Between(Download(url).decode('utf8'),'<div class="wrap-con">','<p>&nbsp;</p>'))
		ret={
			'name':Between(html,'<h2>','</h2>').title(),
			'type': EquipmentTypes[match[0]],
			'rarity': match[1],
			'date':Between(html,'<p>','</p>'),
			'img': api+Between(html,'<img alt="" src="','"'),
			'expr':Between(html,'<td colspan="3" style=" text-align:left; padding:5px 5px;">\r\n\t\t\t\t','</td').lstrip('\t'),
			'stats': {match[0]:match[1] for match in reStats.findall(html)}
		}
		

	elif typ=='soldier':
		html = HTML.unescape(Between(Download(url).decode('utf8'),'<div class="wrap-con">','<p>&nbsp;</p>'))
		ret={
			'name':Between(html,'<h2>','</h2>').title(),
			'date':Between(html,'<p>','</p>'),
			'img': api+Between(html,'<img alt="" src="','"'),
			'expr':Between(html,'<td colspan="2" style="text-align:left; padding:5px 5px 0 5px;">\r\n\t\t\t\t','</td').lstrip('\t'),
			'stats': {match[0]:match[1] for match in reStats.findall(html)}
		}
		try:
			ret['type']=SoldierTypes[url.split('/')[3][7:]]
		except KeyError:
			ret['type']=SoldierTypes[url.split('/')[2][7:]]
		#S - Strong Against
		#W - Weak Against

	elif typ=='hero':
		#hero part
		site=Download(url).decode('utf8')
		html = HTML.unescape(Between(site,'<div class="wrap-con">','<div class="hero_list">'))
		match = reHeroDetails.findall(html)
		ret={
			'name':Between(html,'<p class="hero-name">','</p>').title(),
			'rarity': url.split('/')[3][4:],
			'CV': Between(html,'<p class="hero-remark"><span>CV</span>','</p>'),
			'img_full': Between(html,'src="','"'),
			'details': {key:val for (key,val) in match[:-1]},
			'story': Between(html, '<div class="index_list_content">','</div>')[1:].lstrip(' ').replace('<div>','').replace('</div>',''),
			'talent': match[-1],
			'stats': {key:val for (key,val) in reHeroStats.findall(html)},
		}
		if ret['details']['Measurements'] == '</p>':
			ret['details']['Measurements']='/'
		#classes main
		html=HTML.unescape(Between(site,'<div class="hero_list">','<div class="hero_list_footer"></div>'))#)<div class="dialog"></div>'))
		classes= [{'name':match[2],'img':match[1]} for match in reHeroClasses.findall(html)]

		#class details
		html=HTML.unescape(Between(site,'<div class="hero_list_footer"></div>','<div class="dialog"></div>'))
		#soldier class="hero-dapei wrap_index_list"
		i=0
		for part in html.split('<ul>'):
			if '<table>' in part: #unit
				classes[i]['soldiers']=[
					{
					'name':match[3],
					'img': match[2],
					'stats': {key:val for (key,val) in reHeroStats.findall(spart)},
					'expr': Between(spart,'SKILL：','<') if 'SKILL：' in spart and not 'L: <' in spart else ''
					}
					for spart in part.split('<li>')
					for match in reHeroClasses.finditer(spart)
					if match[3]
				]
			elif 'hero-tianfu-pi' in part: #skill
				classes[i]['skills']= [
					{
						'name':match[2],
						'expr':match[3],
						'img': match[1]
					}
					for match in reSkills.findall(part)
				]
				i+=1

		for i,lclass in enumerate(classes):
			name=lclass['name']
			if name not in Classes:
				Classes[name]=lclass
			elif Classes[name] != lclass:
				print(name)
		ret['classes']=classes
	return ret

def Download(url):
	if url[0]=='/':
		url=api+url

	if CACHING:
		fp=os.path.join(cache,url.replace('/','').replace(':',''))
		if os.path.isfile(fp):
			return open(fp,'rb').read()
		else:
			print(url)
			res = urllib.request.urlopen(url,timeout=300).read()
			open(fp,'wb').write(res)
			return res
	else:
		return urllib.request.urlopen(url,timeout=300).read()

def Between(string,pre,post):
	prep=string.find(pre)+len(pre)
	postp=string[prep:].find(post)+prep
	return string[prep:postp]

SoldierTypes={
	"Jianbing" : "Infantry",
	"Qiangbing" : "Lancer",
	"Qibing" : "Cavalary",
	"Feibing" : "Flier",
	"Cike" : "Assassin",
	"Gongb" : "Archer",
	"Mowu" : "Demon",
	"Sengb" : "Holy",
	"Shuib" : "Aquatic",
}

EquipmentTypes={
	"Wuqi" : "Weapon",
	"Kaijia" : "Armor",
	"Toukui" : "Helmet",
	"Shipin" : "Accessories",
}

if __name__=='__main__':
	main()

