import os
import time
from lib import processUnityFile, MultiThreadHandler, listFiles

PATH=os.path.dirname(os.path.realpath(__file__))
PATH_ASSETS=os.path.join(PATH,'assets')

debug = True

def main():
	v=input('Version:\t').lower()
	if v in ['g','gl','global']:
		Convert('global')
	elif v in ['c','cn','china']:
		Convert('china')
	else:
		Convert('extra')


def Convert(name):
	raw_path=os.path.join(PATH_ASSETS,'raw_%s'%name)
	dest_path=os.path.join(PATH_ASSETS,name)
	proto_path=os.path.join(PATH,*['res','ProtoBuffer'])
	####	Images	###########################################
	originFolder=os.path.join(raw_path,'ExportAssetBundle')
	destFolder = os.path.join(dest_path,'ExportAssetBundle')

	os.makedirs(destFolder, exist_ok = True)

	if debug:
		for asset in listFiles(originFolder, False):
			fp = open(os.path.join(originFolder,asset),'rb')
			dest=modifiedPath(destFolder,asset)
			processUnityFile(fp,modifiedPath(destFolder,asset),destFolder)
	else:
		ProcessHandler = MultiThreadHandler()
		for asset in listFiles(originFolder, False):
			fp = open(os.path.join(originFolder,asset),'rb')
			dest=modifiedPath(destFolder,asset)
			ProcessHandler.queue.put((processUnityFile,{'f':fp,'assetPath':modifiedPath(destFolder,asset),'destFolder':destFolder}))
		ProcessHandler.RunThreads()
	#Music - CriRes
	#StreamingAssetsConvertion(join(dest_path,'CriRes'))

def modifiedPath(destfolder,asset, fullPath=False):
	#abs.b/en/fr/de fix ~ b is normal, en/de/fr language specific
	asset=asset.rsplit('.',1)
	if len(asset)==2:
		if asset[0][-4:]=='_abs':
			asset[0]=asset[0][:-4]
		if asset[1] != 'b':
			asset[0]+='_%s'%asset[1]
	asset=asset[0]

	#remove useless clutter and parse the rest into a path
	asset=asset.replace('assets_gameproject_runtimeassets_','',1).split('_')
	asset=[val for val in asset if val]	#remove possible empty parts
	lpath=destfolder
	os.makedirs(lpath,exist_ok=True)
	#generate sub folders
	if len(asset)>1:
		for folder in asset[:-1]:
			lpath=os.path.join(lpath,folder)
			os.makedirs(lpath, exist_ok=True)

		lpath=os.path.join(lpath,asset[-1])
	else:
		lpath=os.path.join(lpath,asset[0])
	#make relativ path ~ stuff before was to ensure that the path exists
	if not fullPath:
		lpath=lpath.replace(str(destfolder),'')
	#print(asset,'\n',lpath)
	return lpath

if __name__ == '__main__':
	main()
