import os, json

path = os.path.dirname(os.path.realpath(__file__))
configs_path = os.path.join(path,*[os.pardir,'assets','global','ExportAssetBundle','configdata'])
out_path = os.path.join(path,'output')

def IfAddText(obj,key,string):
	if key in obj and obj[key]:
		if type(obj[key]) == bool:
			return string
		else:
			return string%obj[key]
	else:
		return ''

def LoadConfig(name):
	return json.loads(open(os.path.join(configs_path,name),'rb').read())

def SaveOut(name,dir):
	open(os.path.join(out_path,'%s.json'%name),'wb').write(json.dumps(dir,ensure_ascii=False,indent='\t').encode('utf8'))